/* 
 
  Created by Ferenc Kiss on 2013.06.23..

*/

/*!
 *  \details		This library help to use new features of GS, while the old versions remain supported.
 *  \author		Kiss Ferenc
 *  \date		2013
 *	\version		2
 *  \copyright	GNU Public License V2.
 *	\note		Special thanks for krinn from tt-forums.net
 *	\section Usage
	You must create an instance of this class (with minimum supported system): \code this.versionController = FGVersionController(1, 3, 1); \endcode
	And usage is simple: \code if (this.versionController.IsCurrentVersionLessThan(1, 3, 0) {...} \endcode
 */


class FGVersionController {
	majorVersion = 0;
	minorVersion = 0;
	subVersion = 0;
	
	openttd_version = 0;
	supported = true;
	
	/*!
	 *	\param majVer		minimum supported major version  parameters
	 *	\param minVer		minimum supported minor version parameters
	 *	\param subVer		minimum supported subversion version parameters
	 *
	 *	You must set the minimum supported version of your script in constructor.
	 */
	constructor (majVer, minVer, subVer) {
		local v = GSController.GetVersion();
		this.majorVersion =   (v & 0xF0000000) >> 28;
		this.minorVersion =   (v & 0x0F000000) >> 24;
		this.subVersion =   (v & 0x00F00000) >> 20;
		
		this.supported = true;
		this.openttd_version = GSController.GetVersion() >> 0x14;
		
		if (this.IsCurrentVersionLessThan(majVer, minVer, subVer)) {
			this.supported = false;
			GSLog.Error("Not supported Openttd version: " + this.VersionString() + ", minimum OpenTTD requirement version to use this script: " + majVer + "." + minVer + "." + subVer);
		}
	}
	
	/*!
	 *	This function call in constructor. Not necessary to use this function.
	 *	\deprecated
	 */
	function UpdateVersion () {
		GSLog.Warning("FGVersionController::UpdateVersion() function is deprecated in version 2");
		return;
		/* deprecated code
		local openTTDVersion = GSController.GetVersion();
		// alapbol az 1.2-es elotti verziokat nem engedelyezzuk (meg a beta verziokat sem)
		if (openTTDVersion < 302538294) {
			GSLog.Warning("Invalid OpenTTDVersion: " + openTTDVersion);
			return;
		}
		
		else if (openTTDVersion == 302538294) {
			this.majorVersion = 1;
			this.minorVersion = 2;
			this.subVersion = 0;
		}
		
		else if (openTTDVersion <= 303587058) {
			this.majorVersion = 1;
			this.minorVersion = 2;
			this.subVersion = 1;
		}
		
		else if (openTTDVersion <= 304635804) {
			this.majorVersion = 1;
			this.minorVersion = 2;
			this.subVersion = 2;
		}
		
		else if (openTTDVersion <= 305684561) {
			this.majorVersion = 1;
			this.minorVersion = 2;
			this.subVersion = 3;
		}
		
		else if (openTTDVersion <= 319316527) {
			this.majorVersion = 1;
			this.minorVersion = 3;
			this.subVersion = 0;
		}
		
		else if (openTTDVersion <= 320365278) {
			this.majorVersion = 1;
			this.minorVersion = 3;
			this.subVersion = 1;
		}
		
		else if (openTTDVersion <= 321414176) {
			this.majorVersion = 1;
			this.minorVersion = 3;
			this.subVersion = 2;
		}
		
		else { // az osszes nightly verzio mindig a kovetkezo nagy kiadast jeloli, hiszen az uj funkciokat is ugy jelenitik meg, hogy ott lesznek elerhetoek
			GSLog.Warning("OpenTTDVersion: " + openTTDVersion);
			this.majorVersion = 1;
			this.minorVersion = 4;
			this.subVersion = 0;
		}
		 */
	}
	
	/*!
	 *	\return boolean	Return true if int parameters of constructor lower than current OpenTTD version
	 */
	function IsSupportedVersion ();
	
	/*!
	 *	\return string	Return the actual OpenTTD version, example: "1.3.2"
	 */
	function VersionString ();
	
	/*!
	 *	\return boolean	Return true if the current OpenTTD version less than that given parameters
	 *
	 *	\param majVer		major version to be tested
	 *	\param minVer		minor version to be tested
	 *	\param subVer		subversion version to be tested
	 */
	function IsCurrentVersionLessThan (majVer, minVer, subVer);
	
	/*!
	 *	\return boolean	Return true if the current OpenTTD version less than or equal to given parameters
	 *
	 *	\param majVer		major version to be tested
	 *	\param minVer		minor version to be tested
	 *	\param subVer		subversion version to be tested
	 */
	function IsCurrentVersionLessThanOrEqualTo (majVer, minVer, subVer);
	
	/*!
	 *	\return boolean	Return true if the current OpenTTD version equal to given parameters
	 *
	 *	\param majVer		major version to be tested
	 *	\param minVer		minor version to be tested
	 *	\param subVer		subversion version to be tested
	 */
	function IsCurrentVersionEqualTo (majVer, minVer, subVer);
	
	/*!
	 *	\return boolean	Return true if the current OpenTTD version greather than or equal to given parameters
	 *
	 *	\param majVer		major version to be tested
	 *	\param minVer		minor version to be tested
	 *	\param subVer		subversion version to be tested
	 */
	function IsCurrentVersionGreatherThanOrEqualTo (majVer, minVer, subVer);
	
	/*!
	 *	\return boolean	Return true if the current OpenTTD version greather than to given parameters
	 *
	 *	\param majVer		major version to be tested
	 *	\param minVer		minor version to be tested
	 *	\param subVer		subversion version to be tested
	 */
	function IsCurrentVersionGreatherThan (majVer, minVer, subVer);
}

function FGVersionController::IsSupportedVersion () {
	return this.supported; // mivel az UpdateVersion() lefuttatasaval minimum 1-nek kellene lennie a major verzionak, ahhoz, hogy tamogatott legyen
}

function FGVersionController::VersionString () {
	return this.majorVersion + "." + this.minorVersion + "." + this.subVersion;
}

// verzio osszehasonlitasok
function FGVersionController::IsCurrentVersionLessThan (majVer, minVer, subVer) {
	local user_maj = majVer << 8;
	local user_min = minVer << 4;
	local user_version = user_maj + user_min + subVer;
	
	return user_version > this.openttd_version;
}

function FGVersionController::IsCurrentVersionLessThanOrEqualTo (majVer, minVer, subVer) {
	local user_maj = majVer << 8;
	local user_min = minVer << 4;
	local user_version = user_maj + user_min + subVer;
	
	return user_version >= this.openttd_version;
}

function FGVersionController::IsCurrentVersionEqualTo (majVer, minVer, subVer) {
	local user_maj = majVer << 8;
	local user_min = minVer << 4;
	local user_version = user_maj + user_min + subVer;
	
	return user_version == this.openttd_version;
}

function FGVersionController::IsCurrentVersionGreatherThanOrEqualTo (majVer, minVer, subVer) {
	local user_maj = majVer << 8;
	local user_min = minVer << 4;
	local user_version = user_maj + user_min + subVer;
	
	return user_version <= this.openttd_version;
}

function FGVersionController::IsCurrentVersionGreatherThan (majVer, minVer, subVer) {
	local user_maj = majVer << 8;
	local user_min = minVer << 4;
	local user_version = user_maj + user_min + subVer;
	
	return user_version < this.openttd_version;
}
