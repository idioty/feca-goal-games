class FGIndustryCargos {
	company_id = null;
	industry_id = null;
	cargos_delivery = null;
	cargos_pickup = null;
	logLevel = 0;
	
	constructor (a_company_id, a_industry_id, a_logLevel) {
		this.company_id = a_company_id;
		this.industry_id = a_industry_id;
		this.logLevel = a_logLevel;
		this.cargos_delivery = [];
		this.cargos_pickup = [];
	}
}

function FGIndustryCargos::SaveData() {
	return {
		industry_id = this.industry_id,
		cargos_delivery = this.cargos_delivery,
		cargos_pickup = this.cargos_pickup,
	}
}

function FGIndustryCargos::LoadFromData(data) {
	this.industry_id = data.industry_id;
	this.cargos_delivery = data.cargos_delivery;
	this.cargos_pickup = data.cargos_pickup;
}

function FGIndustryCargos::UpdateAllMonths(elteltMonths) {
	foreach (cargo_object in this.cargos_delivery) {
		for (local i = cargo_object.months.len(); i <= elteltMonths; i++)
			cargo_object.months.append(0);
	}
	foreach (cargo_object in this.cargos_pickup) {
		for (local i = cargo_object.months.len(); i <= elteltMonths; i++)
			cargo_object.months.append(0);
	}
}

function FGIndustryCargos::CargoObject(cargo_id, cargos) {
	foreach (cargo_object in cargos) {
		if (cargo_object.cargo_id == cargo_id)
			return cargo_object;
	}
	
	return null;
}

function FGIndustryCargos::RemoveCargo(cargo_id, cargos) {
	for (local i = 0; i < cargos.len(); i++) {
		if (cargos[i].cargo_id == cargo_id) {
			cargos.remove(i);
			if (this.logLevel >= 3)
				GSLog.Info("[FGIndustryCargos::RemoveCargo] " + GSCompany.GetName(this.company_id) + " succesfull unsubscrubed from monitoring cargo: " + GSCargo.GetCargoLabel(cargo_id) + " from industry: " + GSIndustry.GetName(this.industry_id));
			break;
		}
	}
}
