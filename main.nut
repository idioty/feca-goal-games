/*

local list = [];
list.append();
list.remove();
list.pop();
list.sort();
list.len();	// ez a count

*/

/*
	// igy tobb lakosa lehet a varosnak proba kedveert
	local townList = GSTownList();
	local firstTown = GSTownList()[0];
	GSTown.ExpandTown(firstTown, 1000);
	GSTown.SetGrowthRate(firstTown, 1);
	GSTown.SetCargoGoal(firstTown, GSCargo.TE_MAIL, 100);
	GSTown.SetCargoGoal(firstTown, GSCargo.TE_PASSENGERS, 100);
 
 
 
 
 
 // assert
 assert(typeof(list) == "instance");
 assert(typeof(valuator) == "function");
 
 // typeof
 if (typeof(value) == "bool") ...
 if (typeof(value) != "integer") ...
 if (typeof(value) == "instance") ...
 if (typeof(value) == "function") ...
 if (typeof(value) == "string") ...
 
 // many data
 function valami(...) {
	local args = [null];
	 for(local c = 0; c < vargc; c++)
		 args.append(vargv[c]);
 }
 
 */

require("FGObjects/main.nut");
require("FGCargoMonitor/FGCargoMonitor.nut");
require("FGVersionController/FGVersionController.nut");

class FecaGame extends GSController {
	// feladat tipusok elraktarozva ertelmes nevvel
	static GF_MAIN = 0;
	static GF_AWARD = 1;
	static GF_WEAK = 2;
	
	/*
	// show mode, azaz, hogyan jelenjen meg a feladat
	static SM_NONE = 0;
	static SM_GLOBAL = 1;
	static SM_COMPANY = 2;
	 */
	
	testenabled = false;
	testoszto = 150;
	logmode = true;
	

	on_loading = false;
	loaded_game = false;
	starttimebeallitva = false;
	
	systemYear = 0;
	systemMonth = 0;
	systemDay = 0;
	systemHour = 0;
	systemMinute = 0;
	systemSecond = 0;
	
	// az uj startot ezzel csinalom meg
	game_start_hour = 0;
	game_start_minute = 0;
	start_Minute = 0;
	last_hour = -1;
	sentStartMessages = null;
	
	// uzenetek azonositoja
	message_counter = 10; // fenntartjuk statikus uzeneteknek az elso par helyet....
	message_goal_information = 0;
	message_award_information = 1;
	
	companies = null; // ebben tarolom az osszes vallalatot
	
	cargo = null; // cargo objekt, hogy lekerdezzuk a cargo dolgokat
	cargoMonitor = null; // FGCargoMonitor object
	versionController = null; // ezzel kerdezzuk le az aktualis openttd verziot
	
	startyear = 0; // indulasi ev
	
	game_started = false; // elindult-e mar a jatek
	
	// játék befelyezve
	game_ended = false; // fo jatek befejezve, ha ez false, nincs cel ellenorzes
	
	// játék befejezése után 60 másdoperccel folytatódik a játék
	game_started_after_ended = false;
	game_started_after_ended_counter = 0;
	
	// ezek a beallitasokbol szamitodnak
	main_goals = 0;
	award_goals = 0;
	weak_award_goals = 0;
	
	// Ebben elmentem az osszes main goal id-t, mert ha valaki bankruptol, akkor futtatok egy ellenorzest, es eltavolitom, ami senkie sem...
	allMainGoals = null;
	awards = null;
	weak_awards = null;
	
	weak_award_goals_counter = 0;		// weak award goal azonosito letrehozasahoz kell ez
	award_enabled_by_possible = false;
	
	main_goal_level = 0;				// ez kell ahhoz, hogy tudjuk teljesitve van-e mar valamelyik szint
	award_goal_level = 0;				// ugyanugy a szinteket ellenorizzuk ezzel, hogy hanyadik szintu feladatot kell megoldani
	weak_award_goal_level = 0;			// ugyanugy a szinteket ellenorizzuk ezzel, hogy hanyadik szintu feladatot kell megoldani
	
	gameStateCompanies = null;			// ebben az array-ban fogom tarolni az utolso kiiratott jatek allast, azert, hogy le tudjam ellenorizni, hogy tenyleg kell-e frissiteni a story oldalt
	
	companyNames = null;				// ebben fogom tarolni, hogy mik a vallalatok nevei
	companyEventInfoInDiary = null;		// ebben fogom tarolni a company informaciokat, alapitas, csod, felvasarlas... 1.4.0 utan kell csak. Az elozo azert nem jo, mert itt lehet -1 erteke is a company_id -nek csod utan
	
	// kezdeti lehetosegek valtozoi
	initialPossibilities = {};
	
	// ebben fogom tarolni az altalanos feladat beallitasokat
	generalGoalSettings = {};
	
	// Transport Goal varibles
	baseTransportGoalSettings = null;
	transportGoalSettings = {};

	// Town Growth variables
	baseTownGrowthGoalSettings = null;
	townGrowthGoalSettings = {};
	
	// lefoglalt varosokat tarolom ebben a towngrowthchallenge-hez
	engagedTownGrowth = [];
	
	
	// story page azonositok
	detailedStoryBookPageID = null; // a jatek indulasakor jelen levo vallalatokat iratom ki ide


	// Feca Goal Games Controller active
	fggc_active = true;
	
	
	constructor() {
		this.companies = [];
		this.cargo = FGCargo(null, this.logmode);
		this.cargoMonitor = FGCargoMonitor(this, this.logmode ? FGCargoMonitor.LOGLEVEL_ALL : FGCargoMonitor.LOGLEVEL_ERRORS_AND_WARNINGS); // ha logmodeban vagyunk, akkor mindent megjelenitunk, egyebkent csak error es warningot
		this.cargoMonitor.SetEnabledCompanyMonitor(true);
		this.versionController = FGVersionController(1, 3, 0);
		
		// Version Controller teszter
		/*
		local majVer = 1;
		local minVer = 4;
		local subVer = 1;
		
		GSLog.Warning("--------");
		GSLog.Info("");
		GSLog.Info((this.versionController.IsSupportedVersion() ? "is supported version: " : "not supported version: ") + this.versionController.VersionString());
		GSLog.Info((this.versionController.IsCurrentVersionLessThan (majVer, minVer, subVer) ? "yes, current version LESS than: " : "no, current version is not LESS than: ") + majVer + "." + minVer + "." + subVer);
		GSLog.Info((this.versionController.IsCurrentVersionLessThanOrEqualTo (majVer, minVer, subVer) ? "yes, current version LESS than or EQUAL to: " : "no, current version is not LESS than or EQUAL to: ") + majVer + "." + minVer + "." + subVer);
		GSLog.Info((this.versionController.IsCurrentVersionEqualTo (majVer, minVer, subVer) ? "yes, current version is EQUAL to: " : "no, current version is not EQUAL to: ") + majVer + "." + minVer + "." + subVer);
		GSLog.Info((this.versionController.IsCurrentVersionGreatherThanOrEqualTo (majVer, minVer, subVer) ? "yes, current version GREATHER than or EQUAL to: " : "no, current version is not GREATHER than or EQUAL to: ") + majVer + "." + minVer + "." + subVer);
		GSLog.Info((this.versionController.IsCurrentVersionGreatherThan (majVer, minVer, subVer) ? "yes, current version GREATHER than: " : "no, current version is not GREATHER than: ") + majVer + "." + minVer + "." + subVer);
		GSLog.Info("");
		GSLog.Warning("--------");
		*/
		
		this.awards = [];
		this.weak_awards = [];
		this.allMainGoals = [];
		this.gameStateCompanies = [];
		this.companyNames = [];
		this.companyEventInfoInDiary = [];
		this.sentStartMessages = [];
		
		this.generalGoalSettings = {
			enableChanceToAnyNumberOfAward = true, // veletlenszeruen (persze kis esellyel) lehet, hogy a jutalmat orokre megkapja, pl akarmennyi allomast vehet
			enableDoubleAmountOfAwardWhenWeak = false,
			
			neverRunOutMain = false, // ha ez true, akkor sosem fogy el a fo goal, es mindig a this.main_goals mennyisegu goal van
			neverRunOutAwards = false, // ha ez true, akkor sosem fogy el az award goal, es mindig a this.award_goals mennyisegu goal van
			neverRunOutWeakAwards = false, // ha ez true, akkor sosem fogy el az award goal, es mindig a this.weak_award_goals mennyisegu goal van
			
			allAtOnceTimeShowedMainGoals = 0, // ha 0, akkor egyszerre latszik az osszes feladat, ha meg nagyobb, mint 0, akkor annyi feladat latszik egyszerre
			allAtOnceTimeShowedAwardGoals = 0, // ha 0, akkor egyszerre latszik az osszes feladat, ha meg nagyobb, mint 0, akkor annyi feladat latszik egyszerre
			
			newsForAll = true,
			cupUpdates = 6,
			enableCupUpdatesPoints = true,
			enableStoryCupUpdates = 2,
			comapnyAwardGoalsUpdateViaQuestion = 6,
			detailedStoryBook = true,
			
			// pontozas is idekerul
			
			// feladatok utan kapott pontok
			gameTime = 0,
			completion_main_scores = 100,
			completion_award_scores = 10,
			completion_weak_scores = 5,
			completion_first_main_level = 30,
			completion_goal_months = 60, // tehat ennyi honap alatt ha teljesiti, akkor kap plusz jutalmat
		}
		
		this.initialPossibilities = {
			shouldRailDepos = -1,
			shouldRoadDepos = -1,
			shouldWaterDepos = -1,
			shouldRailStations = -1,
			shouldTruckStops = -1,
			shouldBusStops = -1,
			shouldWaterDocks = -1,
			shouldAirPorts = -1,
			shouldRailVehicles = -1,
			shouldTruckVehicles = -1,
			shouldBusVehicles = -1,
			shouldWaterVehicles = -1,
			shouldAirVehicles = -1,
			
			awardRailDepos = true,
			awardRoadDepos = true,
			awardWaterDepos = true,
			awardRailStations = true,
			awardTruckStops = true,
			awardBusStops = true,
			awardWaterDocks = true,
			awardAirPorts = true,
			awardRailVehicles = true,
			awardTruckVehicles = true,
			awardBusVehicles = true,
			awardWaterVehicles = true,
			awardAirVehicles = true,
		}
		
		this.baseTransportGoalSettings = FGBaseGoalSettings(null, null); // ebben vanank a kotelezo, minten feladat tipus szamara kotelezo elemek
		
		this.transportGoalSettings = {			
			// possibilities lehet-e award
			
			// cargo types
			enableAllCargoTypes = true,
			onlyMainGoalLevels = true,
			transportGoalSettingsUpdateAll = false,
			
			cargotype_valuables_min = 0,				// VALU
			cargotype_valuables_max = 0,				// VALU
			cargotype_steel_min = 0,					// STEL
			cargotype_steel_max = 0,					// STEL
			cargotype_ironore_min = 0,					// IORE
			cargotype_ironore_max = 0,					// IORE
			cargotype_wood_min = 0,						// WOOD
			cargotype_wood_max = 0,						// WOOD
			cargotype_grain_min = 0,					// GRAI
			cargotype_grain_max = 0,					// GRAI
			cargotype_goods_min = 0,					// GOOD
			cargotype_goods_max = 0,					// GOOD
			cargotype_livestock_min = 0,				// LVST
			cargotype_livestock_max = 0,				// LVST
			cargotype_oil_min = 0,						// OIL_
			cargotype_oil_max = 0,						// OIL_
			cargotype_mail_min = 0,						// MAIL
			cargotype_mail_max = 0,						// MAIL
			cargotype_coal_min = 0,						// COAL
			cargotype_coal_max = 0,						// COAL
			cargotype_passengers_min = 0,				// PASS
			cargotype_passengers_max = 0,				// PASS
			
			cargotype_paper_min = 0,					// PAPR	Paper	 0020 Piece goods	ECS
			cargotype_paper_max = 0,					// PAPR	Paper	 0020 Piece goods	ECS
			cargotype_wheat_min = 0,					// WHEA	Wheat	 0010 Bulk			 Arctic; see also GRAI, MAIZ, CERE
			cargotype_wheat_max = 0,					// WHEA	Wheat	 0010 Bulk			 Arctic; see also GRAI, MAIZ, CERE
			cargotype_food_min = 0,						// FOOD	Food	 0084 Express, refrigerated	ECS	FIRS
			cargotype_food_max = 0,						// FOOD	Food	 0084 Express, refrigerated	ECS	FIRS
			cargotype_gold_min = 0,						// GOLD	Gold	 0008 Armoured	ECS		 Arctic; see also VALU, DIAM
			cargotype_gold_max = 0,						// GOLD	Gold	 0008 Armoured	ECS		 Arctic; see also VALU, DIAM
			cargotype_rubber_min = 0,					// RUBR	Rubber	0040 Liquid	ECS
			cargotype_rubber_max = 0,					// RUBR	Rubber	0040 Liquid	ECS
			cargotype_fruit_min = 0,					// FRUT  Fruit	 0090 Bulk, refrigerated	ECS
			cargotype_fruit_max = 0,					// FRUT  Fruit	 0090 Bulk, refrigerated	ECS
			cargotype_maize_min = 0,					// MAIZ	Maize	 0010 Bulk			 Tropic; see also GRAI, WHEA, CERE
			cargotype_maize_max = 0,					// MAIZ	Maize	 0010 Bulk			 Tropic; see also GRAI, WHEA, CERE
			cargotype_copperore_min = 0,				// CORE	Copper Ore	 0010 Bulk	ECS
			cargotype_copperore_max = 0,				// CORE	Copper Ore	 0010 Bulk	ECS
			cargotype_water_min = 0,					// WATR	Water	 0040 Liquid	ECS
			cargotype_water_max = 0,					// WATR	Water	 0040 Liquid	ECS
			cargotype_diamonds_min = 0,					// DIAM	Diamonds	 0008 Armoured	ECS		 Tropic; see also VALU, GOLD
			cargotype_diamonds_max = 0,					// DIAM	Diamonds	 0008 Armoured	ECS		 Tropic; see also VALU, GOLD
			cargotype_sugar_min = 0,					// SUGR	Sugar	 0010 Bulk			 Toyland
			cargotype_sugar_max = 0,					// SUGR	Sugar	 0010 Bulk			 Toyland
			cargotype_toys_min = 0,						// TOYS	Toys	 0020 Piece goods			 Toyland
			cargotype_toys_max = 0,						// TOYS	Toys	 0020 Piece goods			 Toyland
			cargotype_batteries_min = 0,				// BATT	Batteries	 0020 Piece goods			 Toyland
			cargotype_batteries_max = 0,				// BATT	Batteries	 0020 Piece goods			 Toyland
			cargotype_sweets_min = 0,					// SWET	Sweets (Candy)	0004 Express			 Toyland
			cargotype_sweets_max = 0,					// SWET	Sweets (Candy)	0004 Express			 Toyland
			cargotype_toffee_min = 0,					// TOFF	Toffee	0010 Bulk			 Toyland
			cargotype_toffee_max = 0,					// TOFF	Toffee	0010 Bulk			 Toyland
			cargotype_cola_min = 0,						// COLA	Cola	0040 Liquid			 Toyland
			cargotype_cola_max = 0,						// COLA	Cola	0040 Liquid			 Toyland
			cargotype_cottoncandy_min = 0,				// CTCD	Cotton Candy (Candyfloss)	0010 Bulk			 Toyland
			cargotype_cottoncandy_max = 0,				// CTCD	Cotton Candy (Candyfloss)	0010 Bulk			 Toyland
			cargotype_bubbles_min = 0,					// BUBL	Bubbles	0020 Piece goods			 Toyland
			cargotype_bubbles_max = 0,					// BUBL	Bubbles	0020 Piece goods			 Toyland
			cargotype_plastic_min = 0,					// PLST	Plastic	0040 Liquid			 Toyland; see also PLAS
			cargotype_plastic_max = 0,					// PLST	Plastic	0040 Liquid			 Toyland; see also PLAS
			cargotype_fizzy_min = 0,					// FZDR	Fizzy Drinks	0020 Piece goods			 Toyland
			cargotype_fizzy_max = 0,					// FZDR	Fizzy Drinks	0020 Piece goods			 Toyland
			
			//	New Cargos	 these cargos are only present when NewGRF industry sets are used
			cargotype_bauxite_min = 0,					// AORE	Bauxite (Aluminium ore)	0010 Bulk	ECS	FIRS
			cargotype_bauxite_max = 0,					// AORE	Bauxite (Aluminium ore)	0010 Bulk	ECS	FIRS
			cargotype_alcohol_min = 0,					// BEER	Alcohol	0064 Express, piece goods, liquids		FIRS
			cargotype_alcohol_max = 0,					// BEER	Alcohol	0064 Express, piece goods, liquids		FIRS
			cargotype_buildingmaterials_min = 0,		// BDMT	Building Materials	0220 Piece goods, covered/sheltered	ECS	FIRS	 In FIRS: 0030 Bulk, piece goods
			cargotype_buildingmaterials_max = 0,		// BDMT	Building Materials	0220 Piece goods, covered/sheltered	ECS	FIRS	 In FIRS: 0030 Bulk, piece goods
			cargotype_bricks_min = 0,					// BRCK	Bricks	 0020 Piece goods	ECS
			cargotype_bricks_max = 0,					// BRCK	Bricks	 0020 Piece goods	ECS
			cargotype_ceramics_min = 0,					// CERA	Ceramics	 0020 Piece goods	ECS
			cargotype_ceramics_max = 0,					// CERA	Ceramics	 0020 Piece goods	ECS
			cargotype_cereals_min = 0,					// CERE	Cereals	 0210 Bulk, covered/sheltered	ECS		[1]
			cargotype_cereals_max = 0,					// CERE	Cereals	 0210 Bulk, covered/sheltered	ECS		[1]
			cargotype_clay_min = 0,						// CLAY	Clay	0210 Bulk covered/sheltered		FIRS	 In FIRS: 0010 Bulk
			cargotype_clay_max = 0,						// CLAY	Clay	0210 Bulk covered/sheltered		FIRS	 In FIRS: 0010 Bulk
			cargotype_cement_min = 0,					// CMNT	Cement	 0210 Bulk covered/sheltered	ECS
			cargotype_cement_max = 0,					// CMNT	Cement	 0210 Bulk covered/sheltered	ECS
			cargotype_copper_min = 0,					// COPR	Copper	0020 Piece goods
			cargotype_copper_max = 0,					// COPR	Copper	0020 Piece goods
			cargotype_dyes_min = 0,						// DYES	Dyes	 0060 Piece goods, liquids	ECS
			cargotype_dyes_max = 0,						// DYES	Dyes	 0060 Piece goods, liquids	ECS
			cargotype_engineeringsupplies_min = 0,		// ENSP	Engineering Supplies	0024 Express, piece goods		FIRS
			cargotype_engineeringsupplies_max = 0,		// ENSP	Engineering Supplies	0024 Express, piece goods		FIRS
			cargotype_fertiliser_min = 0,				// FERT	Fertiliser	 0030 Bulk, piece goods	ECS		[1]
			cargotype_fertiliser_max = 0,				// FERT	Fertiliser	 0030 Bulk, piece goods	ECS		[1]
			cargotype_fibrecrops_min = 0,				// FICR	Fibre crops	 0030 Bulk, piece goods	ECS	FIRS
			cargotype_fibrecrops_max = 0,				// FICR	Fibre crops	 0030 Bulk, piece goods	ECS	FIRS
			cargotype_fish_min = 0,						// FISH	Fish	 0084 Express, refrigerated	ECS	FIRS
			cargotype_fish_max = 0,						// FISH	Fish	 0084 Express, refrigerated	ECS	FIRS
			cargotype_farmsupplies_min = 0,				// FMSP	Farm Supplies	0024 Express, piece goods		FIRS
			cargotype_farmsupplies_max = 0,				// FMSP	Farm Supplies	0024 Express, piece goods		FIRS
			cargotype_frvgfruit_min = 0,				// FRVG	Fruit (and optionally Vegetables)	00A4 Express, piece goods, refrigerated		FIRS
			cargotype_frvgfruit_max = 0,				// FRVG	Fruit (and optionally Vegetables)	00A4 Express, piece goods, refrigerated		FIRS
			cargotype_glass_min = 0,					// GLAS	Glass	 0420 Piece goods, oversized	ECS
			cargotype_glass_max = 0,					// GLAS	Glass	 0420 Piece goods, oversized	ECS
			cargotype_gravelballast_min = 0,			// GRVL	Gravel / Ballast	0010 Bulk		FIRS
			cargotype_gravelballast_max = 0,			// GRVL	Gravel / Ballast	0010 Bulk		FIRS
			cargotype_limestone_min = 0,				// LIME	Lime stone	 0010 Bulk	ECS
			cargotype_limestone_max = 0,				// LIME	Lime stone	 0010 Bulk	ECS
			cargotype_milk_min = 0,						// MILK	Milk	00C4 Express, liquid, refrigerated		FIRS
			cargotype_milk_max = 0,						// MILK	Milk	00C4 Express, liquid, refrigerated		FIRS
			cargotype_manufacturingsupplies_min = 0,	// MNSP	Manufacturing Supplies	0024 Piece Goods, express		FIRS
			cargotype_manufacturingsupplies_max = 0,	// MNSP	Manufacturing Supplies	0024 Piece Goods, express		FIRS
			cargotype_metal_min = 0,					// STEL ez az acelt irja felul!!!
			cargotype_metal_max = 0,					// STEL ez az acelt irja felul!!!
			cargotype_oilseed_min = 0,					// OLSD	Oil seed	 0210 Bulk, covered/sheltered	ECS		[1]
			cargotype_oilseed_max = 0,					// OLSD	Oil seed	 0210 Bulk, covered/sheltered	ECS		[1]
			cargotype_petrolfueloil_min = 0,			// PETR	Petrol / Fuel Oil	 0040 Liquid	ECS	FIRS
			cargotype_petrolfueloil_max = 0,			// PETR	Petrol / Fuel Oil	 0040 Liquid	ECS	FIRS
			cargotype_plasplastic_min = 0,				// PLAS	Plastic	 0060 Piece goods, liquid	ECS
			cargotype_plasplastic_max = 0,				// PLAS	Plastic	 0060 Piece goods, liquid	ECS
			cargotype_potash_min = 0,					// POTA	Potash	 0210 Bulk, covered/sheltered	ECS		[1]
			cargotype_potash_max = 0,					// POTA	Potash	 0210 Bulk, covered/sheltered	ECS		[1]
			cargotype_recyclables_min = 0,				// RCYC	Recyclables	0220 Piece Goods, covered		FIRS
			cargotype_recyclables_max = 0,				// RCYC	Recyclables	0220 Piece Goods, covered		FIRS
			cargotype_refinedproducts_min = 0,			// RFPR	Refined products	 0040 Liquid	ECS	FIRS
			cargotype_refinedproducts_max = 0,			// RFPR	Refined products	 0040 Liquid	ECS	FIRS
			cargotype_sand_min = 0,						// SAND	Sand	 0010 Bulk	ECS	FIRS
			cargotype_sand_max = 0,						// SAND	Sand	 0010 Bulk	ECS	FIRS
			cargotype_scraptmetal_min = 0,				// SCMT	Scrap Metal	1010 Bulk, non-pourable		FIRS
			cargotype_scraptmetal_max = 0,				// SCMT	Scrap Metal	1010 Bulk, non-pourable		FIRS
			cargotype_sugarbeet_min = 0,				// SGBT	Sugar beet	0010 Bulk		FIRS	 not in tropical
			cargotype_sugarbeet_max = 0,				// SGBT	Sugar beet	0010 Bulk		FIRS	 not in tropical
			cargotype_sugarcane_min = 0,				// SGCN	Sugarcane	1010 Bulk, non-pourable		FIRS	 only tropical
			cargotype_sugarcane_max = 0,				// SGCN	Sugarcane	1010 Bulk, non-pourable		FIRS	 only tropical
			cargotype_sulphur_min = 0,					// SULP	Sulphur	 0210 Bulk, covered/sheltered	ECS		[1]
			cargotype_sulphur_max = 0,					// SULP	Sulphur	 0210 Bulk, covered/sheltered	ECS		[1]
			cargotype_tourists_min = 0,					// TOUR	Tourists	 0005 Passengers, express	ECS
			cargotype_tourists_max = 0,					// TOUR	Tourists	 0005 Passengers, express	ECS
			cargotype_vehicles_min = 0,					// VEHI	Vehicles	 0420 Piece goods, oversized	ECS
			cargotype_vehicles_max = 0,					// VEHI	Vehicles	 0420 Piece goods, oversized	ECS
			cargotype_wdprwood_min = 0,					// WDPR	Wood Products	 0030 Bulk, piece goods	ECS	FIRS
			cargotype_wdprwood_max = 0,					// WDPR	Wood Products	 0030 Bulk, piece goods	ECS	FIRS
			cargotype_wool_min = 0,						// WOOL	Wool	 0220 Piece goods, covered/sheltered	ECS	FIRS	[1]
			cargotype_wool_max = 0,						// WOOL	Wool	 0220 Piece goods, covered/sheltered	ECS	FIRS	[1]
			
			
			//	Special Cargos	 these cargos are for use outside industry sets and do not represent transporting anything
			cargotype_default_min = 0,					// DFLT	'Default'	 ---- None			 Used by andythenorth to identify default cargo graphics in newgrfs that use code/graphics generation.
			cargotype_default_max = 0,					// DFLT	'Default'	 ---- None			 Used by andythenorth to identify default cargo graphics in newgrfs that use code/graphics generation.
			
			cargotype_locomotiveregearing_min = 0,		// GEAR	Locomotive regearing	8000 Special			 Using this cargo may make your vehicle set incompatible with some industry sets
			cargotype_locomotiveregearing_max = 0,		// GEAR	Locomotive regearing	8000 Special			 Using this cargo may make your vehicle set incompatible with some industry sets
			
			//	Deprecated Cargos	 these cargos are not used by any current industry set, and are listed here for backwards compatibility
			cargotype_fuel_min = 0,						// FUEL	Fuel	0040 Liquid			Use PETR for refined-oil fuel
			cargotype_fuel_max = 0,						// FUEL	Fuel	0040 Liquid			Use PETR for refined-oil fuel
			cargotype_rawsugar_min = 0,					// RSGR	Raw Sugar	0010 Bulk			 Deprecated in FIRS. See SGBT and SGCN
			cargotype_rawsugar_max = 0,					// RSGR	Raw Sugar	0010 Bulk			 Deprecated in FIRS. See SGBT and SGCN
			cargotype_scrpscrapmetal_min = 0,			// SCRP	Scrap Metal	0010 Piece Goods			 Deprecated in FIRS, use SCMT instead
			cargotype_scrpscrapmetal_max = 0,			// SCRP	Scrap Metal	0010 Piece Goods			 Deprecated in FIRS, use SCMT instead
			cargotype_tropicwood_min = 0,				// TWOD	Tropic Wood	0020 Piece goods			 formerly intended as a default cargo
			cargotype_tropicwood_max = 0,				// TWOD	Tropic Wood	0020 Piece goods			 formerly intended as a default cargo
			cargotype_waste_min = 0,					// WSTE	Waste	0010 Bulk			 Deprecated in FIRS.						// PAPR	Paper	 0020 Piece goods	ECS
			cargotype_waste_max = 0,					// WSTE	Waste	0010 Bulk			 Deprecated in FIRS.						// PAPR	Paper	 0020 Piece goods	ECS
		}
		
		
		this.baseTownGrowthGoalSettings = FGBaseGoalSettings(null, null); // ebben vanank a kotelezo, minten feladat tipus szamara kotelezo elemek;
		this.townGrowthGoalSettings = {
			level_min = 0,
			level_max = 0,
			population = 0,			// ez vonatkozik arra, hogy mennyi legyen a lakossag kezdetekben
			customPopulationMax = 0, // ez a feladat lakossag minimum
			customPopulationMin = 0, // ez a feladat lakossag maximum
			custom_goal_population = 0, // ez pedig a feladat lakossag egyeni szam
			//population_max = 300,
			difficultyGoal = false,
		};
	}
}

function FecaGame::Start() {
	/* Wait for the game to start */
	this.Sleep(1);
	
	if (this.logmode)
		GSLog.Info("[FecaGame::Start] GameScript started");

	if (this.on_loading) {
		this.on_loading = false;

		if (this.game_ended && GSGame.IsPaused()) {
			// ha netan mar vegigjatszottuk a jatekot, es utana levo reszt toltottunk be, akkor azonnal elinditjuk a jatekot.
			this.game_started_after_ended = true;
			GSGame.Unpause();
		}
	}
	
	
	local runAllAtHour = true; // ezt kell falsera allitani a csak multiplayerben jatszashoz
	if (runAllAtHour || GSGame.IsMultiplayer()) {
		if (this.loaded_game == false) {
			this.LoadPreferences();
			
			
			// EZ FONTOS: tehat ha 0 az ido, akkor nem lehet vegtelen a fo feladatok...
			if (this.generalGoalSettings.gameTime == 0)
				this.generalGoalSettings.neverRunOutMain = false;

			
			this.startyear = GSDate.GetYear(GSDate.GetCurrentDate());
				
			// ellenorizzuk a possible dolgokat, es ha minden engedelyezve van, akkor letiltjuk az awardokat
			if ((this.initialPossibilities.shouldRailDepos >= 0 && this.initialPossibilities.awardRailDepos) ||
				(this.initialPossibilities.shouldRoadDepos >= 0 && this.initialPossibilities.awardRoadDepos) ||
				(this.initialPossibilities.shouldWaterDepos >= 0 && this.initialPossibilities.awardWaterDepos) ||
				(this.initialPossibilities.shouldRailStations >= 0 && this.initialPossibilities.awardRailStations) ||
				(this.initialPossibilities.shouldTruckStops >= 0 && this.initialPossibilities.awardTruckStops) ||
				(this.initialPossibilities.shouldBusStops >= 0 && this.initialPossibilities.awardBusStops) ||
				(this.initialPossibilities.shouldWaterDocks >= 0 && this.initialPossibilities.awardWaterDocks) ||
				(this.initialPossibilities.shouldAirPorts >= 0 && this.initialPossibilities.awardAirPorts) ||
				(this.initialPossibilities.shouldRailVehicles >= 0 && this.initialPossibilities.awardRailVehicles) ||
				(this.initialPossibilities.shouldTruckVehicles >= 0 && this.initialPossibilities.awardTruckVehicles) ||
				(this.initialPossibilities.shouldBusVehicles >= 0 && this.initialPossibilities.awardBusVehicles) ||
				(this.initialPossibilities.shouldWaterVehicles >= 0 && this.initialPossibilities.awardWaterVehicles) ||
				(this.initialPossibilities.shouldAirVehicles >= 0 && this.initialPossibilities.awardAirVehicles))
				this.award_enabled_by_possible = true;
		
			if (!this.award_enabled_by_possible)
				GSLog.Info("[FecaGame::Start] Jutalom ki van kapcsolva, mert nincs jutalom.");
			
			// megprobalom azt, hogy a
			// main es award goalok kiszamitasa, hogy mindenkinek mindig ugyanannyi legyen
			// transport goalok
			
			// main goals
			if (this.generalGoalSettings.neverRunOutMain) {
				if (this.generalGoalSettings.allAtOnceTimeShowedMainGoals > 0)
					this.main_goals = this.generalGoalSettings.allAtOnceTimeShowedMainGoals;
				else
					this.main_goals = 1; // ha nagyobb nullanal, akkor annyit jelenitunk meg, ha viszont annyi, vagy kisebb, akkor szigoruan egyet!
			}
			else {
				for (local i = FGBaseGoal.GOAL_TYPES_MIN; i <= FGBaseGoal.GOAL_TYPES_MAX; i++) {
					local baseSettingsTable = FGBaseGoal.BaseSettinsgFromGoalType(i, this);
					if (baseSettingsTable != null) {
						if (baseSettingsTable.minMainGoal >= baseSettingsTable.maxMainGoal)
							this.main_goals += baseSettingsTable.minMainGoal;
						else
							this.main_goals += (GSBase.RandRange(baseSettingsTable.maxMainGoal - baseSettingsTable.minMainGoal + 1) + baseSettingsTable.minMainGoal);
					}
				}
			}
			
			// award goals
			if (this.generalGoalSettings.neverRunOutAwards) {
				if (this.generalGoalSettings.allAtOnceTimeShowedAwardGoals > 0)
					this.award_goals = this.generalGoalSettings.allAtOnceTimeShowedAwardGoals;
				else
					this.award_goals = 1; // ha nagyobb nullanal, akkor annyit jelenitunk meg, ha viszont annyi, vagy kisebb, akkor szigoruan egyet!
			}
			else {
				for (local i = FGBaseGoal.GOAL_TYPES_MIN; i <= FGBaseGoal.GOAL_TYPES_MAX; i++) {
					local baseSettingsTable = FGBaseGoal.BaseSettinsgFromGoalType(i, this);
					if (baseSettingsTable != null) {
						if (baseSettingsTable.minAwardGoal >= baseSettingsTable.maxAwardGoal)
							this.award_goals += baseSettingsTable.minAwardGoal;
						else
							this.award_goals += (GSBase.RandRange(baseSettingsTable.maxAwardGoal - baseSettingsTable.minAwardGoal + 1) + baseSettingsTable.minAwardGoal);
					}
				}
			}
			
			// weak award goals
			for (local i = FGBaseGoal.GOAL_TYPES_MIN; i <= FGBaseGoal.GOAL_TYPES_MAX; i++) {
				local baseSettingsTable = FGBaseGoal.BaseSettinsgFromGoalType(i, this);
				if (baseSettingsTable != null) {
					if (baseSettingsTable.minWeakAwardGoal >= baseSettingsTable.maxWeakAwardGoal)
						this.weak_award_goals += baseSettingsTable.minWeakAwardGoal;
					else
						this.weak_award_goals += (GSBase.RandRange(baseSettingsTable.maxWeakAwardGoal - baseSettingsTable.minWeakAwardGoal + 1) + baseSettingsTable.minWeakAwardGoal);
				}
			}
		
			// letrehozunk annyi award goal-t, amennyi most van :)
			for (local i = 0; i < this.award_goals; i++) {
				this.award_goal_level++;
				local goal = this.NewRandomGoal(null, false, null, this.award_goal_level, GF_AWARD);
				
				if (goal != null) {
					goal.goal_main = false;
					
					local baseSettingsTable = FGBaseGoal.BaseSettinsgFromGoalType(goal.goal_type, this);
					if (baseSettingsTable != null) {
						if (baseSettingsTable.awardsForSuccesGoalAwardMin > 0 && baseSettingsTable.awardsForSuccesGoalAwardMax > 0 && this.award_enabled_by_possible)
							goal.goal_award = true;
					}
					
					local vangoalid = false;
					if (this.generalGoalSettings.allAtOnceTimeShowedAwardGoals == 0 || (i < this.generalGoalSettings.allAtOnceTimeShowedAwardGoals)) {
						this.ShowGoal(null, goal);
						if (goal.goal_id == null)
							GSLog.Error("[FecaGame::Start] Can not create award goal, invalid goal_id: " + goal.goal_id);
						else
							vangoalid = true;
					}
					
					if (vangoalid)
						this.awards.append(goal);
				}
			}
		
			// letrehozzuk a weak_award goalokat
			for (local i = 0; i < this.weak_award_goals; i++) {
				this.weak_award_goal_level++;
				local goal = this.NewRandomGoal(null, false, null, this.weak_award_goal_level, GF_WEAK);
				
				if (goal != null) {
					goal.goal_main = false;
					
					local baseSettingsTable = FGBaseGoal.BaseSettinsgFromGoalType(goal.goal_type, this);
					if (baseSettingsTable != null) {
						if (baseSettingsTable.awardsForSuccesGoalAwardMin > 0 && baseSettingsTable.awardsForSuccesGoalAwardMax > 0 && this.award_enabled_by_possible)
							goal.goal_award = true;
					}
					
					goal.goal_weak = true;
					
					// mivel a weakgoaloknak sosem lesz maguktol id-juk, ezert csinalok neki
					goal.goal_id = ("weak_award_goal_" + this.weak_award_goals_counter);
					this.weak_award_goals_counter++;
					
					this.weak_awards.append(goal);
				}
			}
			
			// elvileg most mar letre van hozva a jatek es beallitasi betoltve, johetnek az udvozlo uzenetek
			if (this.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0)) {
				// kikuldjuk eloszor az udvozlo uzenetet
				local welcomePageID = GSStoryPage.New(GSCompany.COMPANY_INVALID, GSText(GSText.STR_WELCOME_PAGE_TITLE));
				if (welcomePageID == GSStoryPage.STORY_PAGE_INVALID) {
					GSLog.Error("[FGCompany::Start] Nem sikerült a köszöntő oldal létrehozása!");
				}
				else {
					GSStoryPage.NewElement(welcomePageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_NEWLINE));
					GSStoryPage.NewElement(welcomePageID, GSStoryPage.SPET_TEXT, 0, this.GetWelcomeMessage());
					GSStoryPage.NewElement(welcomePageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_AUTHORS));
					GSStoryPage.NewElement(welcomePageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_READ));
					
					GSStoryPage.Show(welcomePageID);
				}
				
				// johet a jatek leiras ide, ennek az azonositojat nem kell elmenteni, mert nem akarjuk majd kesobb modositani
				local szabalyokPageID = GSStoryPage.New(GSCompany.COMPANY_INVALID, GSText(GSText.STR_SZABALYOK));
				if (szabalyokPageID == GSStoryPage.STORY_PAGE_INVALID) {
					GSLog.Error("[FGCompany::Start] Nem sikerült a szabályok oldal létrehozása!");
				}
				else {
					GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_1));
					if (this.award_goals > 0 || this.weak_award_goals > 0)
						GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_2));
					
					
					local vanKorlatozas = false;
					
					if (this.initialPossibilities.shouldRailDepos >= 0 ||
						this.initialPossibilities.shouldRoadDepos >= 0 ||
						this.initialPossibilities.shouldWaterDepos >= 0 ||
						this.initialPossibilities.shouldRailStations >= 0 ||
						this.initialPossibilities.shouldTruckStops >= 0 ||
						this.initialPossibilities.shouldBusStops >= 0 ||
						this.initialPossibilities.shouldWaterDocks >= 0 ||
						this.initialPossibilities.shouldAirPorts >= 0 ||
						this.initialPossibilities.shouldRailVehicles >= 0 ||
						this.initialPossibilities.shouldTruckVehicles >= 0 ||
						this.initialPossibilities.shouldBusVehicles >= 0 ||
						this.initialPossibilities.shouldWaterVehicles >= 0 ||
						this.initialPossibilities.shouldAirVehicles >= 0)
						vanKorlatozas = true;
					
					// korlatozasok
					if (vanKorlatozas)
						//GSStoryPage.NewElement(welcomePageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_KORLATOZASOK));
						GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_KORLATOZASOK));
					
					local et = GSText(GSText.STR_EMPTY);
					
					// feladatok
					GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_MAIN_FELADATOK));
					
					if (this.generalGoalSettings.gameTime > 0 && this.generalGoalSettings.neverRunOutMain)
						GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_GAME_TIME_WITHOUT_MAIN_WIN, this.generalGoalSettings.gameTime));
					else if (this.generalGoalSettings.gameTime > 0 && !this.generalGoalSettings.neverRunOutMain)
						GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_GAME_TIME_OR_MAIN_WIN, this.generalGoalSettings.gameTime));
					else
						GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_MAIN_WIN));
					
					
					local megjelenesiSzoveg = null;
					if (this.generalGoalSettings.neverRunOutMain && this.generalGoalSettings.allAtOnceTimeShowedMainGoals == 0)
						megjelenesiSzoveg = GSText(GSText.STR_SZABALYOK_FELADATOK_VISIBLE_COUNT, this.main_goals);
					else if (this.generalGoalSettings.allAtOnceTimeShowedMainGoals > 0)
						megjelenesiSzoveg = GSText(GSText.STR_SZABALYOK_FELADATOK_VISIBLE_COUNT, this.generalGoalSettings.allAtOnceTimeShowedMainGoals);
					else
						megjelenesiSzoveg = GSText(GSText.STR_SZABALYOK_FELADATOK_VISIBLE_ALL, et);
					
					if (this.generalGoalSettings.gameTime > 0 && this.generalGoalSettings.neverRunOutMain)
						GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_MAIN_FELADATOK_ENDLESS, megjelenesiSzoveg));
					else if (this.generalGoalSettings.gameTime > 0 && !this.generalGoalSettings.neverRunOutMain)
						GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_MAIN_FELADATOK_COUNT_OR_GAME_TIME, this.main_goals, megjelenesiSzoveg));
					else
						GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_MAIN_FELADATOK_COUNT, this.main_goals, megjelenesiSzoveg));
					
					if (this.award_goals > 0) {
						if (this.generalGoalSettings.neverRunOutAwards && this.generalGoalSettings.allAtOnceTimeShowedAwardGoals == 0)
							megjelenesiSzoveg = GSText(GSText.STR_SZABALYOK_FELADATOK_VISIBLE_COUNT, this.award_goals);
						else if (this.generalGoalSettings.allAtOnceTimeShowedAwardGoals > 0)
							megjelenesiSzoveg = GSText(GSText.STR_SZABALYOK_FELADATOK_VISIBLE_COUNT, this.generalGoalSettings.allAtOnceTimeShowedAwardGoals);
						else
							megjelenesiSzoveg = GSText(GSText.STR_SZABALYOK_FELADATOK_VISIBLE_ALL, et);
						
						if (this.generalGoalSettings.neverRunOutAwards)
							GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_AWARD_FELADATOK_ENDLESS, megjelenesiSzoveg));
						else
							GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_WEAK_AWARD_FELADATOK_COUNT, this.award_goals, megjelenesiSzoveg));
					}
					
					if (this.weak_award_goals > 0) {
//						if (this.generalGoalSettings.neverRunOutWeakAwards)
							megjelenesiSzoveg = GSText(GSText.STR_SZABALYOK_FELADATOK_VISIBLE_COUNT, this.weak_award_goals);
//						else
//							megjelenesiSzoveg = GSText(GSText.STR_SZABALYOK_FELADATOK_VISIBLE_ALL, et);
						
						if (this.generalGoalSettings.neverRunOutWeakAwards)
							GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_WEAK_AWARD_FELADATOK_ENDLESS, megjelenesiSzoveg));
						else
							GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_WEAK_AWARD_FELADATOK_COUNT, this.weak_award_goals, megjelenesiSzoveg));
					}
					
					// feladatok lezarasa
					GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_FELADATOK_KESZ));
					
					// jutalmaj juuratasa
					if (this.award_enabled_by_possible)
						GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_JUTALOM));
					
					// pontozas
					GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_PONTOZAS));
					GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_PONT_MAIN, this.generalGoalSettings.completion_main_scores));
					
					if (this.award_goals > 0)
						GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_PONT_AWARD, this.generalGoalSettings.completion_award_scores));
					
					if (this.weak_award_goals > 0)
						GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_PONT_WEAK_AWARD, this.generalGoalSettings.completion_weak_scores));
						
					if (this.generalGoalSettings.completion_first_main_level > 0)
						GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_PONT_SZINT, this.generalGoalSettings.completion_first_main_level));
					
					if (this.generalGoalSettings.completion_goal_months > 0)
						GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_PONT_HONAP, this.generalGoalSettings.completion_goal_months));
					
					// vegszo
					GSStoryPage.NewElement(szabalyokPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_SZABALYOK_VEGSZO));
				}

				// reszletes vallalati informaciok
				if (this.generalGoalSettings.detailedStoryBook) {
					// johet a jatek leiras ide, ennek az azonositojat nem kell elmenteni, mert nem akarjuk majd kesobb modositani
					this.detailedStoryBookPageID = GSStoryPage.New(GSCompany.COMPANY_INVALID, GSText(GSText.STR_COMPANY_EVENTS));
					if (this.detailedStoryBookPageID == GSStoryPage.STORY_PAGE_INVALID) {
						GSLog.Error("[FGCompany::Start] Nem sikerült az induló vállalatok oldal létrehozása!");
					}
					else {
						// most nem adunk hozza uj sort, felesleges, ott a cim
//						GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_START_COMPANIES));
					}
				}
			}
		}
		else {
			if (this.logmode)
				GSLog.Warning("[FGCompany::Start] Loaded saved game...");
		}
		
		while (true) {
			if (!this.game_ended) {
				local ticks = GSController.GetTick();
				
				this.HandleEvents();
				
				if (!this.testenabled) {
					// tehat gombra inditast csinalom meg
					if (!this.game_started && (GSController.GetSetting("startGameImmediately") != -1) && GSController.GetSetting("startGameImmediately")) {
						if (this.logmode)
							GSLog.Info("[FGCompany::Start] Jatek inditasa gombbal");
						
						this.starttimebeallitva = true;
						this.game_started = true;
						
						GSGame.Unpause();
						
						this.UpdateSystemDate();
						this.SendStartMessages();
					}
					
					if (!this.game_started && this.start_Minute != GSController.GetSetting("startAfterMinute")) {
						this.starttimebeallitva = false;
						this.sentStartMessages.clear();
					}
					
					// amig nincs beallitva a kezdo idopont, addig csak ez a resz fut le
					if (!this.starttimebeallitva) {
						if (!GSGame.IsPaused()) {
							GSGame.Pause();
						}
						
						// uj modszer
						this.start_Minute = GSController.GetSetting("startAfterMinute");
						
						if (this.start_Minute == 0) { // ha nincs ilyen beallitas, akkor -1 az eredmeny, ha meg nagyobb, akkor
							local ticks_used = GSController.GetTick() - ticks;
							this.Sleep(33 - ticks_used); // kb a 33 az egy masodpercnyi ido
							continue;
						}
						
						// frissitjuk a rendszer idot
						this.UpdateSystemDate();
						
						
						this.game_start_minute = this.systemMinute + this.start_Minute;
						this.game_start_hour = this.systemHour;
						
						while (this.game_start_minute >= 60) {
							this.game_start_hour++;
							this.game_start_minute -= 60;
						}
						
						if (this.game_start_hour > 23) // mivel max 1440 lesz az erteke az indulasi idonek, igy nem kell tobb napot figyelnunk
							this.game_start_hour -= 24;
						
						this.starttimebeallitva = true;
						
						
						local sth = this.game_start_hour;
						
						if (GSController.GetSetting("timeZone") != -1)
							sth += (GSController.GetSetting("timeZone") - 12);
						
						if (sth >= 24)
							sth -= 24;
						else if (sth < 0)
							sth += 24;
						
						if (sth < 10)
							sth = "0" + sth;
						
						local stm = this.game_start_minute;
						if (stm < 10)
							stm = "0" + stm;
						/*
						local sh = this.systemHour;
						if (sh < 10)
							sh = "0" + sh;
						
						local sm = this.systemMinute;
						if (sm < 10)
							sm = "0" + sm;
						 */
						
						GSLog.Info("");
//						GSLog.Info("Start Ido OK: " + sth + ":" + stm + " (rendszer ido: " + sh + ":" + sm + ")   (időzóna beállítással)");
						GSLog.Info("[FGCompany::Start] Start Ido OK: " + sth + ":" + stm + ".");
						GSLog.Info("");
						
						/* regi modszer
						this.UpdateSystemDate();
						if (!GSGame.IsPaused()) {
							GSGame.Pause();
						}
						
						if ((GSController.GetSetting("startHour") > this.systemHour) || (GSController.GetSetting("startHour") == this.systemHour && GSController.GetSetting("startMinute") > this.systemMinute))	{
							this.starttimebeallitva = true;
							
							if (this.logmode) {
								GSLog.Info("");
								GSLog.Info("Start time OK (" + GSController.GetSetting("startHour") + ":" + GSController.GetSetting("startMinute") + "), (rendszerido: " + this.systemHour + ":" + this.systemMinute + ")!");
								GSLog.Info("");
							}
						}
						else {
							local ticks_used = GSController.GetTick() - ticks;
							this.Sleep(33 - ticks_used);
							continue;
						}
						 */
					}
						
					// ha mar be van allitva a kezdo ido, akkor ellenorizzuk, hogy elerkezett-e az ido a jatekra.
					// ez a resz fut addig, amig nem indult el a jatek
					if (!this.game_started) {
						if (!GSGame.IsPaused()) {
							GSGame.Pause();
						}
						
						
						// uj modszer
						this.UpdateSystemDate();
						
						
						this.SendBeginWarning(30);
						this.SendBeginWarning(20);
						this.SendBeginWarning(15);
						this.SendBeginWarning(10);
						this.SendBeginWarning(5);
						this.SendBeginWarning(1);
						
						
						if (this.last_hour == -1)
							this.last_hour = this.systemHour;
						
//						GSLog.Info("lasth: " + this.last_hour + ", sysh: " + this.systemHour);
						
						// amig nagyobb, mint 60, addig biztosan nem ebben az oraban kell elinditani a jatekot
						// ez azert csinaltam igy: pl mostani ido: 11:50 ha valaki megadja, hogy 11:40-kor induljon a jatek, akkor ha ez nincs
						// akkor mivel az ora megegyezik, de az indulasi ido kisebb, mint a jelenlegi ido, ezert elindul a jatek, pedig nem kellene
						if (this.start_Minute >= 60) {
							if (this.last_hour != this.systemHour) {
								this.start_Minute -= 60;
								this.last_hour = this.systemHour;
							}
							
							if (this.start_Minute >= 60) { // ha meg mindig nagyobb, mint 60, akkor kell csak ez
								local ticks_used = GSController.GetTick() - ticks;
								this.Sleep(33 - ticks_used);
								continue;
							}
						}
						
						// tehat most elerkeztunk ahhoz az orahoz, amiben kezdodik a jatek
						// persz lehet, hogy a kovetkezo oraban indul, pl: mostani ido: 11:50, indulasi perc: 30, ekkor kisebb, mint 60, de az ora nem fog egyezni...
						// tehat, ha nem egyezik az ora, vagy ha egyezik, de az indulasi ido kisebb, mint a rendszer ido, akkor meg nem kezdunk
						if (this.game_start_hour != this.systemHour || this.game_start_minute > this.systemMinute) {
							local ticks_used = GSController.GetTick() - ticks;
							this.Sleep(33 - ticks_used);
							continue;
						}
						
						// tehat most ott tartunk, hogy kezdodhet a jatek
						this.game_started = true;
						GSGame.Unpause();
						
						this.SendStartMessages();
					}
				}
				else {
					if (!this.game_started) {
						this.UpdateSystemDate();
						this.SendStartMessages();
						this.game_started = true;
					}
				}




/******************************************************************************************************************************************************************************************************************************************************************************************************************
 
				Game loop begin
 
******************************************************************************************************************************************************************************************************************************************************************************************************************/

				
				local days = this.GetCurrentDaysCount();
				local actday = this.GetCurrentDay();
				
				// ellenorizzuk a jatekra szant ido letelt-e. Mindezt meg azelott, hogy ellenoriznenk az uj lerakott dolgokat.
				if (this.generalGoalSettings.gameTime > 0 && actday == 1) {
					if (this.GetMonthsCountFromStarted() >= this.generalGoalSettings.gameTime) {
						this.EndGame(null);
					}
				}
				
				if (!this.game_ended) {
					// frissitjuk mindig a vallalatok nevet:
					this.UpdateCompanyNames();
					
					// mindennap frissitjuk a cargo dolgokat, nehogy lemaradjunk valamirol :D
					this.cargoMonitor.Update();
					
					// ellenorizzuk, hogy mit szabad es mit nem vasarolni vagy epiteni
					foreach (company in this.companies) {
						if (company.company_id == GSCompany.COMPANY_INVALID) continue;
						
						company.CheckAllShould();
						
						// 1.4.0-tol updatelni kell allandoan, hogy ki mit vett. Sajnos masodpercenkent kell ezt megtenni...
						if (company.possibleWindowUpdateEnabled && this.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0))
							company.SendPossibleState(false);
					}
					
					
					// Megiscsak ide kellene az eredmenyek ellenorzese, mert most mar a nap elejen fut le ez a script es nem a nap vegen
					// ezert ha a nap elejen ellenorizzuk a dolgokat, akkor vonatkozik az meg csak az elozo honapra, tehat az informaciokat utana kell kuldeni
					// eredmenyek ellenorzese
					this.CheckGoalsCompleted();
					if (!this.game_ended)
						this.UpdateAllGoalText();

					// Vallatok eredmenyeit es jatek allasat kijelzo uzenetek frissitese tallhato itt
					// Ez a regi 1.3.0 eseteben fontos, mert ott meg nincs naplo uzenet
					// ezt meg a honap elso napjan osszeszamolt eredmenyek utan ellenorizzuk, mert a nap elejen van a script futtatas, igy elsejen meg az elozo honap is szamit igy
					local elteltMonths = this.GetMonthsCountFromStarted();
					if (actday == 1 && elteltMonths > 0) {
						local maradek = 0;
						
						
						// ezt kell eloszor megcsinalni, hogy a SendScore alatt jelenjen meg
						if (this.generalGoalSettings.comapnyAwardGoalsUpdateViaQuestion) {
							maradek = elteltMonths % this.generalGoalSettings.comapnyAwardGoalsUpdateViaQuestion;
							if (maradek == 0) // az elso nap ne jojjon mar uzenet...
								this.SendCompanyInformations();
						}
						
						if (this.generalGoalSettings.cupUpdates > 0) {
							maradek = elteltMonths % this.generalGoalSettings.cupUpdates;
							if (maradek == 0) // az elso nap ne jojjon mar uzenet...
								this.SendScores();
						}
					}
				}
				
				// Loop with a frequency of five days
				local ticks_used = GSController.GetTick() - ticks;
				local day = 1;
				local allticks = (day * 74) - ticks_used;
				if (allticks < 1)
					allticks = 1;

				this.Sleep(allticks);
			}
			else {
				if (this.game_started_after_ended)
					this.Sleep(10000);
				else {
					this.game_started_after_ended_counter ++;
					if (game_started_after_ended_counter >= 60) {
						this.game_started_after_ended = true;
						GSGame.Unpause();
					}
					this.Sleep(30);
				}
			}



/******************************************************************************************************************************************************************************************************************************************************************************************************************

					Game loop end

 ******************************************************************************************************************************************************************************************************************************************************************************************************************/

		}
	}
}

function FecaGame::SendStartMessages() {
	this.SendStartTimeToLog();
	this.AddStartInfoToCompanyStoryPageInfo();

	// hozzaadjuk a vallalatok neveit
	/* TODO: ezt most kiveszem, mert ott van a startkor jelenlevo vallalatok, szerintem felesleges megegyszer, mert ugyanaz a datum
	foreach (company in this.companies) {
		this.AddNewCompanyToCompanyStoryPageInfo(company.company_id, this.CompanyName(company.company_id));
	}
	 */

	GSNews.Create(GSNews.NT_GENERAL, GSText(GSText.STR_GAME_STARTED), GSCompany.COMPANY_INVALID);
}

// hopsz, ezt hasznalja az FGCompany is
function FecaGame::GetWelcomeMessage() {
	local rand = GSBase.RandRange(8);
	switch (rand) {
		case 0:
			return GSText(GSText.STR_WELCOME_1);
		case 1:
			return GSText(GSText.STR_WELCOME_2);
		case 2:
			return GSText(GSText.STR_WELCOME_3);
		case 3:
			return GSText(GSText.STR_WELCOME_4);
		case 4:
			return GSText(GSText.STR_WELCOME_5);
		case 5:
			return GSText(GSText.STR_WELCOME_6);
		case 6:
			return GSText(GSText.STR_WELCOME_7);
		case 7:
			return GSText(GSText.STR_WELCOME_8);
	}
	
	return GSText(GSText.STR_WELCOME_1);
}

function FecaGame::LoadPreferences() {
	if (this.logmode)
		GSLog.Info("[FecaGame::LoadPreferences] Loading preferences...");
	
	// possible betoltes
	if ((GSController.GetSetting("shouldRailDeposMore") != -1) && (GSController.GetSetting("shouldRailDeposNum") != -1))
		if (!GSController.GetSetting("shouldRailDeposMore"))
			this.initialPossibilities.shouldRailDepos = GSController.GetSetting("shouldRailDeposNum");
	if ((GSController.GetSetting("shouldRoadDeposMore") != -1) && (GSController.GetSetting("shouldRoadDeposNum") != -1))
		if (!GSController.GetSetting("shouldRoadDeposMore"))
			this.initialPossibilities.shouldRoadDepos = GSController.GetSetting("shouldRoadDeposNum");
	if ((GSController.GetSetting("shouldWaterDeposMore") != -1) && (GSController.GetSetting("shouldWaterDeposNum") != -1))
		if (!GSController.GetSetting("shouldWaterDeposMore"))
			this.initialPossibilities.shouldWaterDepos = GSController.GetSetting("shouldWaterDeposNum");
	if ((GSController.GetSetting("shouldRailStationsMore") != -1) && (GSController.GetSetting("shouldRailStationsNum") != -1))
		if (!GSController.GetSetting("shouldRailStationsMore"))
			this.initialPossibilities.shouldRailStations = GSController.GetSetting("shouldRailStationsNum");
	if ((GSController.GetSetting("shouldTruckStopsMore") != -1) && (GSController.GetSetting("shouldTruckStopsNum") != -1))
		if (!GSController.GetSetting("shouldTruckStopsMore"))
			this.initialPossibilities.shouldTruckStops = GSController.GetSetting("shouldTruckStopsNum");
	if ((GSController.GetSetting("shouldBusStopsMore") != -1) && (GSController.GetSetting("shouldBusStopsNum") != -1))
		if (!GSController.GetSetting("shouldBusStopsMore"))
			this.initialPossibilities.shouldBusStops = GSController.GetSetting("shouldBusStopsNum");
	if ((GSController.GetSetting("shouldWaterDocksMore") != -1) && (GSController.GetSetting("shouldWaterDocksNum") != -1))
		if (!GSController.GetSetting("shouldWaterDocksMore"))
			this.initialPossibilities.shouldWaterDocks = GSController.GetSetting("shouldWaterDocksNum");
	if ((GSController.GetSetting("shouldAirPortsMore") != -1) && (GSController.GetSetting("shouldAirPortsNum") != -1))
		if (!GSController.GetSetting("shouldAirPortsMore"))
			this.initialPossibilities.shouldAirPorts = GSController.GetSetting("shouldAirPortsNum");
	if ((GSController.GetSetting("shouldRailVehiclesMore") != -1) && (GSController.GetSetting("shouldRailVehiclesNum") != -1))
		if (!GSController.GetSetting("shouldRailVehiclesMore"))
			this.initialPossibilities.shouldRailVehicles = GSController.GetSetting("shouldRailVehiclesNum");
	if ((GSController.GetSetting("shouldTruckVehiclesMore") != -1) && (GSController.GetSetting("shouldTruckVehiclesNum") != -1))
		if (!GSController.GetSetting("shouldTruckVehiclesMore"))
			this.initialPossibilities.shouldTruckVehicles = GSController.GetSetting("shouldTruckVehiclesNum");
	if ((GSController.GetSetting("shouldBusVehiclesMore") != -1) && (GSController.GetSetting("shouldBusVehiclesNum") != -1))
		if (!GSController.GetSetting("shouldBusVehiclesMore"))
			this.initialPossibilities.shouldBusVehicles = GSController.GetSetting("shouldBusVehiclesNum");
	if ((GSController.GetSetting("shouldWaterVehiclesMore") != -1) && (GSController.GetSetting("shouldWaterVehiclesNum") != -1))
		if (!GSController.GetSetting("shouldWaterVehiclesMore"))
			this.initialPossibilities.shouldWaterVehicles = GSController.GetSetting("shouldWaterVehiclesNum");
	if ((GSController.GetSetting("shouldAirVehiclesMore") != -1) && (GSController.GetSetting("shouldAirVehiclesNum") != -1))
		if (!GSController.GetSetting("shouldAirVehiclesMore"))
			this.initialPossibilities.shouldAirVehicles = GSController.GetSetting("shouldAirVehiclesNum");

	// possibilities lehet-e award
	if (GSController.GetSetting("awardRailDepos") != -1)
		this.initialPossibilities.awardRailDepos = GSController.GetSetting("awardRailDepos");
	if (GSController.GetSetting("awardRoadDepos") != -1)
		this.initialPossibilities.awardRoadDepos = GSController.GetSetting("awardRoadDepos");
	if (GSController.GetSetting("awardWaterDepos") != -1)
		this.initialPossibilities.awardWaterDepos = GSController.GetSetting("awardWaterDepos");
	if (GSController.GetSetting("awardRailStations") != -1)
		this.initialPossibilities.awardRailStations = GSController.GetSetting("awardRailStations");
	if (GSController.GetSetting("awardTruckStops") != -1)
		this.initialPossibilities.awardTruckStops = GSController.GetSetting("awardTruckStops");
	if (GSController.GetSetting("awardBusStops") != -1)
		this.initialPossibilities.awardBusStops = GSController.GetSetting("awardBusStops");
	if (GSController.GetSetting("awardWaterDocks") != -1)
		this.initialPossibilities.awardWaterDocks = GSController.GetSetting("awardWaterDocks");
	if (GSController.GetSetting("awardAirPorts") != -1)
		this.initialPossibilities.awardAirPorts = GSController.GetSetting("awardAirPorts");
	if (GSController.GetSetting("awardRailVehicles") != -1)
		this.initialPossibilities.awardRailVehicles = GSController.GetSetting("awardRailVehicles");
	if (GSController.GetSetting("awardTruckVehicles") != -1)
		this.initialPossibilities.awardTruckVehicles = GSController.GetSetting("awardTruckVehicles");
	if (GSController.GetSetting("awardBusVehicles") != -1)
		this.initialPossibilities.awardBusVehicles = GSController.GetSetting("awardBusVehicles");
	if (GSController.GetSetting("awardWaterVehicles") != -1)
		this.initialPossibilities.awardWaterVehicles = GSController.GetSetting("awardWaterVehicles");
	if (GSController.GetSetting("awardAirVehicles") != -1)
		this.initialPossibilities.awardAirVehicles = GSController.GetSetting("awardAirVehicles");
	
	// alap jatekbeallitasok
	if (GSController.GetSetting("newsForAll") != -1)
		this.generalGoalSettings.newsForAll = GSController.GetSetting("newsForAll");
	if (GSController.GetSetting("cupUpdates") != -1)
		this.generalGoalSettings.cupUpdates = GSController.GetSetting("cupUpdates");
	if (GSController.GetSetting("enableCupUpdatesPoints") != -1)
		this.generalGoalSettings.enableCupUpdatesPoints = GSController.GetSetting("enableCupUpdatesPoints");
	if (GSController.GetSetting("enableStoryCupUpdates") != -1)
		this.generalGoalSettings.enableStoryCupUpdates = GSController.GetSetting("enableStoryCupUpdates");
	if (GSController.GetSetting("comapnyAwardGoalsUpdateViaQuestion") != -1)
		this.generalGoalSettings.comapnyAwardGoalsUpdateViaQuestion = GSController.GetSetting("comapnyAwardGoalsUpdateViaQuestion");
	if (GSController.GetSetting("allAtOnceTimeShowedMainGoals") != -1)
		this.generalGoalSettings.allAtOnceTimeShowedMainGoals = GSController.GetSetting("allAtOnceTimeShowedMainGoals");
	if (GSController.GetSetting("allAtOnceTimeShowedAwardGoals") != -1)
		this.generalGoalSettings.allAtOnceTimeShowedAwardGoals = GSController.GetSetting("allAtOnceTimeShowedAwardGoals");
	if (GSController.GetSetting("neverRunOutMain") != -1)
		this.generalGoalSettings.neverRunOutMain = GSController.GetSetting("neverRunOutMain");
	if (GSController.GetSetting("neverRunOutAwards") != -1)
		this.generalGoalSettings.neverRunOutAwards = GSController.GetSetting("neverRunOutAwards");
	if (GSController.GetSetting("neverRunOutWeakAwards") != -1)
		this.generalGoalSettings.neverRunOutWeakAwards = GSController.GetSetting("neverRunOutWeakAwards");
	if (GSController.GetSetting("enableChanceToAnyNumberOfAward") != -1)
		this.generalGoalSettings.enableChanceToAnyNumberOfAward = GSController.GetSetting("enableChanceToAnyNumberOfAward");
	if (GSController.GetSetting("enableDoubleAmountOfAwardWhenWeak") != -1)
		this.generalGoalSettings.enableDoubleAmountOfAwardWhenWeak = GSController.GetSetting("enableDoubleAmountOfAwardWhenWeak");
	if (GSController.GetSetting("detailedStoryBook") != -1)
		this.generalGoalSettings.detailedStoryBook = GSController.GetSetting("detailedStoryBook");
	
	
	// pontbeallitasok
	if (GSController.GetSetting("mainGoalScores") != -1)
		this.generalGoalSettings.completion_main_scores = GSController.GetSetting("mainGoalScores");
	if (GSController.GetSetting("awardGoalScores") != -1)
		this.generalGoalSettings.completion_award_scores = GSController.GetSetting("awardGoalScores");
	if (GSController.GetSetting("weakAwardGoalScores") != -1)
		this.generalGoalSettings.completion_weak_scores = GSController.GetSetting("weakAwardGoalScores");
	if (GSController.GetSetting("monthsScores") != -1)
		this.generalGoalSettings.completion_goal_months = GSController.GetSetting("monthsScores");
	if (GSController.GetSetting("levelsScores") != -1)
		this.generalGoalSettings.completion_first_main_level = GSController.GetSetting("levelsScores");
	if (GSController.GetSetting("gameTime") != -1)
		this.generalGoalSettings.gameTime = GSController.GetSetting("gameTime");
	
	
	// Town Growth beallitasok
	if (GSController.GetSetting("townGrowthGoalSettingsMinMainGoal") != -1)
		this.baseTownGrowthGoalSettings.minMainGoal = GSController.GetSetting("townGrowthGoalSettingsMinMainGoal");
	if (GSController.GetSetting("townGrowthGoalSettingsMaxMainGoal") != -1)
		this.baseTownGrowthGoalSettings.maxMainGoal = GSController.GetSetting("townGrowthGoalSettingsMaxMainGoal");
	if (GSController.GetSetting("townGrowthGoalSettingsMinDifficulty") != -1)
		this.baseTownGrowthGoalSettings.minDifficulty = GSController.GetSetting("townGrowthGoalSettingsMinDifficulty");
	if (GSController.GetSetting("townGrowthGoalSettingsMaxDifficulty") != -1)
		this.baseTownGrowthGoalSettings.maxDifficulty = GSController.GetSetting("townGrowthGoalSettingsMaxDifficulty");
	if (GSController.GetSetting("townGrowthGoalSettingsAwardsForSuccesGoalAwardMin") != -1)
		this.baseTownGrowthGoalSettings.awardsForSuccesGoalMainMin = GSController.GetSetting("townGrowthGoalSettingsAwardsForSuccesGoalMainMax");
	if (GSController.GetSetting("townGrowthGoalSettingsAwardsForSuccesGoalAwardMax") != -1)
		this.baseTownGrowthGoalSettings.awardsForSuccesGoalMainMax = GSController.GetSetting("townGrowthGoalSettingsAwardsForSuccesGoalMainMax");
	
	if (GSController.GetSetting("townGrowthGoalSettingsMinLevel") != -1)
		this.townGrowthGoalSettings.level_min = GSController.GetSetting("townGrowthGoalSettingsMinLevel");
	if (GSController.GetSetting("townGrowthGoalSettingsMaxLevel") != -1)
		this.townGrowthGoalSettings.level_max = GSController.GetSetting("townGrowthGoalSettingsMaxLevel");
	if (GSController.GetSetting("townGrowthGoalSettingsCustomLevel") != -1)
		this.townGrowthGoalSettings.custom_goal_population = GSController.GetSetting("townGrowthGoalSettingsCustomLevel");
	if (GSController.GetSetting("townGrowthGoalSettingsDifficultyGoal") != -1)
		this.townGrowthGoalSettings.difficultyGoal = GSController.GetSetting("townGrowthGoalSettingsDifficultyGoal");
	if (GSController.GetSetting("townGrowthGoalSettingsPopulation") != -1)
		this.townGrowthGoalSettings.population = GSController.GetSetting("townGrowthGoalSettingsPopulation");
	if (GSController.GetSetting("townGrowthGoalSettingsCustomPopulationMin") != -1)
		this.townGrowthGoalSettings.customPopulationMin = GSController.GetSetting("townGrowthGoalSettingsCustomPopulationMin");
	if (GSController.GetSetting("townGrowthGoalSettingsCustomPopulationMax") != -1)
		this.townGrowthGoalSettings.customPopulationMax = GSController.GetSetting("townGrowthGoalSettingsCustomPopulationMax");
//	if (GSController.GetSetting("townGrowthGoalSettingsPopulation_max") != -1)
//		this.townGrowthGoalSettings.population_max = GSController.GetSetting("townGrowthGoalSettingsPopulation_max");
	
	
	
	
	// transport goal beallitasok betoltese
	if (GSController.GetSetting("transportGoalSettingsEnableMultiplierForAfterMainGoals") != -1)
		this.baseTransportGoalSettings.enableMultiplierForAfterMainGoals = GSController.GetSetting("transportGoalSettingsEnableMultiplierForAfterMainGoals");
	if (GSController.GetSetting("transportGoalSettingsAfterMainGoalsMultiplier") != -1)
		this.baseTransportGoalSettings.afterMainGoalsMultiplier = GSController.GetSetting("transportGoalSettingsAfterMainGoalsMultiplier");
	
	if (GSController.GetSetting("transportGoalSettingsMinMainGoal") != -1)
		this.baseTransportGoalSettings.minMainGoal = GSController.GetSetting("transportGoalSettingsMinMainGoal");
	if (GSController.GetSetting("transportGoalSettingsMaxMainGoal") != -1)
		this.baseTransportGoalSettings.maxMainGoal = GSController.GetSetting("transportGoalSettingsMaxMainGoal");
	if (GSController.GetSetting("transportGoalSettingsMinAwardGoal") != -1)
		this.baseTransportGoalSettings.minAwardGoal = GSController.GetSetting("transportGoalSettingsMinAwardGoal");
	if (GSController.GetSetting("transportGoalSettingsMaxAwardGoal") != -1)
		this.baseTransportGoalSettings.maxAwardGoal = GSController.GetSetting("transportGoalSettingsMaxAwardGoal");
	if (GSController.GetSetting("transportGoalSettingsMinWeakAwardGoal") != -1)
		this.baseTransportGoalSettings.minWeakAwardGoal = GSController.GetSetting("transportGoalSettingsMinWeakAwardGoal");
	if (GSController.GetSetting("transportGoalSettingsMaxWeakAwardGoal") != -1)
		this.baseTransportGoalSettings.maxWeakAwardGoal = GSController.GetSetting("transportGoalSettingsMaxWeakAwardGoal");
	
	if (GSController.GetSetting("transportGoalSettingsMinDifficulty") != -1)
		this.baseTransportGoalSettings.minDifficulty = GSController.GetSetting("transportGoalSettingsMinDifficulty");
	if (GSController.GetSetting("transportGoalSettingsMaxDifficulty") != -1)
		this.baseTransportGoalSettings.maxDifficulty = GSController.GetSetting("transportGoalSettingsMaxDifficulty");
	if (GSController.GetSetting("transportGoalSettingsMonthWithDifficulty") != -1)
		this.baseTransportGoalSettings.monthWithDifficulty = GSController.GetSetting("transportGoalSettingsMonthWithDifficulty");
	if (GSController.GetSetting("transportGoalSettingsMinMonthDifficulty") != -1)
		this.baseTransportGoalSettings.minMonthDifficulty = GSController.GetSetting("transportGoalSettingsMinMonthDifficulty");
	if (GSController.GetSetting("transportGoalSettingsMaxMonthDifficulty") != -1)
		this.baseTransportGoalSettings.maxMonthDifficulty = GSController.GetSetting("transportGoalSettingsMaxMonthDifficulty");
	if (GSController.GetSetting("transportGoalSettingsMinMonthWithNumber") != -1)
		this.baseTransportGoalSettings.minMonthNumber = GSController.GetSetting("transportGoalSettingsMinMonthWithNumber");
	if (GSController.GetSetting("transportGoalSettingsMaxMonthWithNumber") != -1)
		this.baseTransportGoalSettings.maxMonthNumber = GSController.GetSetting("transportGoalSettingsMaxMonthWithNumber");
	
	if (GSController.GetSetting("transportGoalSettingsAwardsForSuccesGoalMainMin") != -1)
		this.baseTransportGoalSettings.awardsForSuccesGoalMainMin = GSController.GetSetting("transportGoalSettingsAwardsForSuccesGoalMainMin");
	if (GSController.GetSetting("transportGoalSettingsAwardsForSuccesGoalMainMax") != -1)
		this.baseTransportGoalSettings.awardsForSuccesGoalMainMax = GSController.GetSetting("transportGoalSettingsAwardsForSuccesGoalMainMax");
	if (GSController.GetSetting("transportGoalSettingsAwardsForSuccesGoalAwardMin") != -1)
		this.baseTransportGoalSettings.awardsForSuccesGoalAwardMin = GSController.GetSetting("transportGoalSettingsAwardsForSuccesGoalAwardMin");
	if (GSController.GetSetting("transportGoalSettingsAwardsForSuccesGoalAwardMax") != -1)
		this.baseTransportGoalSettings.awardsForSuccesGoalAwardMax = GSController.GetSetting("transportGoalSettingsAwardsForSuccesGoalAwardMax");
	if (GSController.GetSetting("transportGoalSettingsAwardsForSuccesGoalWeakAwardMin") != -1)
		this.baseTransportGoalSettings.awardsForSuccesGoalWeakAwardMin = GSController.GetSetting("transportGoalSettingsAwardsForSuccesGoalWeakAwardMin");
	if (GSController.GetSetting("transportGoalSettingsAwardsForSuccesGoalWeakAwardMax") != -1)
		this.baseTransportGoalSettings.awardsForSuccesGoalWeakAwardMax = GSController.GetSetting("transportGoalSettingsAwardsForSuccesGoalWeakAwardMax");
	
	
	
	
	// cargotype betoltesek
	if (GSController.GetSetting("transportGoalSettingsUpdateAll") != -1)
		this.transportGoalSettings.transportGoalSettingsUpdateAll = GSController.GetSetting("transportGoalSettingsUpdateAll");
	if (GSController.GetSetting("enableAllCargoTypes") != -1)
		this.transportGoalSettings.enableAllCargoTypes = GSController.GetSetting("enableAllCargoTypes");
	if (GSController.GetSetting("onlyMainGoalLevels") != -1)
		this.transportGoalSettings.onlyMainGoalLevels = GSController.GetSetting("onlyMainGoalLevels");
	
	
	if (GSController.GetSetting("cargotype_valuables_min") != -1)
		this.transportGoalSettings.cargotype_valuables_min = GSController.GetSetting("cargotype_valuables_min");				// VALU
	if (GSController.GetSetting("cargotype_valuables_max") != -1)
		this.transportGoalSettings.cargotype_valuables_max = GSController.GetSetting("cargotype_valuables_max");				// VALU
	if (GSController.GetSetting("cargotype_steel_min") != -1)
		this.transportGoalSettings.cargotype_steel_min = GSController.GetSetting("cargotype_steel_min");					// STEL
	if (GSController.GetSetting("cargotype_steel_max") != -1)
		this.transportGoalSettings.cargotype_steel_max = GSController.GetSetting("cargotype_steel_max");					// STEL
	if (GSController.GetSetting("cargotype_ironore_min") != -1)
		this.transportGoalSettings.cargotype_ironore_min = GSController.GetSetting("cargotype_ironore_min");					// IORE
	if (GSController.GetSetting("cargotype_ironore_max") != -1)
		this.transportGoalSettings.cargotype_ironore_max = GSController.GetSetting("cargotype_ironore_max");					// IORE
	if (GSController.GetSetting("cargotype_wood_min") != -1)
		this.transportGoalSettings.cargotype_wood_min = GSController.GetSetting("cargotype_wood_min");						// WOOD
	if (GSController.GetSetting("cargotype_wood_max") != -1)
		this.transportGoalSettings.cargotype_wood_max = GSController.GetSetting("cargotype_wood_max");						// WOOD
	if (GSController.GetSetting("cargotype_grain_min") != -1)
		this.transportGoalSettings.cargotype_grain_min = GSController.GetSetting("cargotype_grain_min");					// GRAI
	if (GSController.GetSetting("cargotype_grain_max") != -1)
		this.transportGoalSettings.cargotype_grain_max = GSController.GetSetting("cargotype_grain_max");					// GRAI
	if (GSController.GetSetting("cargotype_goods_min") != -1)
		this.transportGoalSettings.cargotype_goods_min = GSController.GetSetting("cargotype_goods_min");					// GOOD
	if (GSController.GetSetting("cargotype_goods_max") != -1)
		this.transportGoalSettings.cargotype_goods_max = GSController.GetSetting("cargotype_goods_max");					// GOOD
	if (GSController.GetSetting("cargotype_livestock_min") != -1)
		this.transportGoalSettings.cargotype_livestock_min = GSController.GetSetting("cargotype_livestock_min");				// LVST
	if (GSController.GetSetting("cargotype_livestock_max") != -1)
		this.transportGoalSettings.cargotype_livestock_max = GSController.GetSetting("cargotype_livestock_max");				// LVST
	if (GSController.GetSetting("cargotype_oil_min") != -1)
		this.transportGoalSettings.cargotype_oil_min = GSController.GetSetting("cargotype_oil_min");						// OIL_
	if (GSController.GetSetting("cargotype_oil_max") != -1)
		this.transportGoalSettings.cargotype_oil_max = GSController.GetSetting("cargotype_oil_max");						// OIL_
	if (GSController.GetSetting("cargotype_mail_min") != -1)
		this.transportGoalSettings.cargotype_mail_min = GSController.GetSetting("cargotype_mail_min");						// MAIL
	if (GSController.GetSetting("cargotype_mail_max") != -1)
		this.transportGoalSettings.cargotype_mail_max = GSController.GetSetting("cargotype_mail_max");						// MAIL
	if (GSController.GetSetting("cargotype_coal_min") != -1)
		this.transportGoalSettings.cargotype_coal_min = GSController.GetSetting("cargotype_coal_min");						// COAL
	if (GSController.GetSetting("cargotype_coal_max") != -1)
		this.transportGoalSettings.cargotype_coal_max = GSController.GetSetting("cargotype_coal_max");						// COAL
	if (GSController.GetSetting("cargotype_passengers_min") != -1)
		this.transportGoalSettings.cargotype_passengers_min = GSController.GetSetting("cargotype_passengers_min");				// PASS
	if (GSController.GetSetting("cargotype_passengers_max") != -1)
		this.transportGoalSettings.cargotype_passengers_max = GSController.GetSetting("cargotype_passengers_max");				// PASS
	
	if (GSController.GetSetting("cargotype_paper_min") != -1)						// PAPR	Paper	 0020 Piece goods	ECS
		this.transportGoalSettings.cargotype_paper_min = GSController.GetSetting("cargotype_paper_min");
	if (GSController.GetSetting("cargotype_paper_max") != -1)						// PAPR	Paper	 0020 Piece goods	ECS
		this.transportGoalSettings.cargotype_paper_max = GSController.GetSetting("cargotype_paper_max");
	if (GSController.GetSetting("cargotype_wheat_min") != -1)						// WHEA	Wheat	 0010 Bulk			 Arctic; see also GRAI, MAIZ, CERE
		this.transportGoalSettings.cargotype_wheat_min = GSController.GetSetting("cargotype_wheat_min");
	if (GSController.GetSetting("cargotype_wheat_max") != -1)						// WHEA	Wheat	 0010 Bulk			 Arctic; see also GRAI, MAIZ, CERE
		this.transportGoalSettings.cargotype_wheat_max = GSController.GetSetting("cargotype_wheat_max");
	if (GSController.GetSetting("cargotype_food_min") != -1)						// FOOD	Food	 0084 Express, refrigerated	ECS	FIRS
		this.transportGoalSettings.cargotype_food_min = GSController.GetSetting("cargotype_food_min");
	if (GSController.GetSetting("cargotype_food_max") != -1)						// FOOD	Food	 0084 Express, refrigerated	ECS	FIRS
		this.transportGoalSettings.cargotype_food_max = GSController.GetSetting("cargotype_food_max");
	if (GSController.GetSetting("cargotype_gold_min") != -1)						// GOLD	Gold	 0008 Armoured	ECS		 Arctic; see also VALU, DIAM
		this.transportGoalSettings.cargotype_gold_min = GSController.GetSetting("cargotype_gold_min");
	if (GSController.GetSetting("cargotype_gold_max") != -1)						// GOLD	Gold	 0008 Armoured	ECS		 Arctic; see also VALU, DIAM
		this.transportGoalSettings.cargotype_gold_max = GSController.GetSetting("cargotype_gold_max");
	if (GSController.GetSetting("cargotype_rubber_min") != -1)					// RUBR	Rubber	0040 Liquid	ECS
		this.transportGoalSettings.cargotype_rubber_min = GSController.GetSetting("cargotype_rubber_min");
	if (GSController.GetSetting("cargotype_rubber_max") != -1)					// RUBR	Rubber	0040 Liquid	ECS
		this.transportGoalSettings.cargotype_rubber_max = GSController.GetSetting("cargotype_rubber_max");
	if (GSController.GetSetting("cargotype_fruit_min") != -1)						// FRUT  Fruit	 0090 Bulk, refrigerated	ECS
		this.transportGoalSettings.cargotype_fruit_min = GSController.GetSetting("cargotype_fruit_min");
	if (GSController.GetSetting("cargotype_fruit_max") != -1)						// FRUT  Fruit	 0090 Bulk, refrigerated	ECS
		this.transportGoalSettings.cargotype_fruit_max = GSController.GetSetting("cargotype_fruit_max");
	if (GSController.GetSetting("cargotype_maize_min") != -1)						// MAIZ	Maize	 0010 Bulk			 Tropic; see also GRAI, WHEA, CERE
		this.transportGoalSettings.cargotype_maize_min = GSController.GetSetting("cargotype_maize_min");
	if (GSController.GetSetting("cargotype_maize_max") != -1)						// MAIZ	Maize	 0010 Bulk			 Tropic; see also GRAI, WHEA, CERE
		this.transportGoalSettings.cargotype_maize_max = GSController.GetSetting("cargotype_maize_max");
	if (GSController.GetSetting("cargotype_copperore_min") != -1)					// CORE	Copper Ore	 0010 Bulk	ECS
		this.transportGoalSettings.cargotype_copperore_min = GSController.GetSetting("cargotype_copperore_min");
	if (GSController.GetSetting("cargotype_copperore_max") != -1)					// CORE	Copper Ore	 0010 Bulk	ECS
		this.transportGoalSettings.cargotype_copperore_max = GSController.GetSetting("cargotype_copperore_max");
	if (GSController.GetSetting("cargotype_water_min") != -1)						// WATR	Water	 0040 Liquid	ECS
		this.transportGoalSettings.cargotype_water_min = GSController.GetSetting("cargotype_water_min");
	if (GSController.GetSetting("cargotype_water_max") != -1)						// WATR	Water	 0040 Liquid	ECS
		this.transportGoalSettings.cargotype_water_max = GSController.GetSetting("cargotype_water_max");
	if (GSController.GetSetting("cargotype_diamonds_min") != -1)					// DIAM	Diamonds	 0008 Armoured	ECS		 Tropic; see also VALU, GOLD
		this.transportGoalSettings.cargotype_diamonds_min = GSController.GetSetting("cargotype_diamonds_min");
	if (GSController.GetSetting("cargotype_diamonds_max") != -1)					// DIAM	Diamonds	 0008 Armoured	ECS		 Tropic; see also VALU, GOLD
		this.transportGoalSettings.cargotype_diamonds_max = GSController.GetSetting("cargotype_diamonds_max");
	if (GSController.GetSetting("cargotype_sugar_min") != -1)						// SUGR	Sugar	 0010 Bulk			 Toyland
		this.transportGoalSettings.cargotype_sugar_min = GSController.GetSetting("cargotype_sugar_min");
	if (GSController.GetSetting("cargotype_sugar_max") != -1)						// SUGR	Sugar	 0010 Bulk			 Toyland
		this.transportGoalSettings.cargotype_sugar_max = GSController.GetSetting("cargotype_sugar_max");
	if (GSController.GetSetting("cargotype_toys_min") != -1)						// TOYS	Toys	 0020 Piece goods			 Toyland
		this.transportGoalSettings.cargotype_toys_min = GSController.GetSetting("cargotype_toys_min");
	if (GSController.GetSetting("cargotype_toys_max") != -1)						// TOYS	Toys	 0020 Piece goods			 Toyland
		this.transportGoalSettings.cargotype_toys_max = GSController.GetSetting("cargotype_toys_max");
	if (GSController.GetSetting("cargotype_batteries_min") != -1)					// BATT	Batteries	 0020 Piece goods			 Toyland
		this.transportGoalSettings.cargotype_batteries_min = GSController.GetSetting("cargotype_batteries_min");
	if (GSController.GetSetting("cargotype_batteries_max") != -1)					// BATT	Batteries	 0020 Piece goods			 Toyland
		this.transportGoalSettings.cargotype_batteries_max = GSController.GetSetting("cargotype_batteries_max");
	if (GSController.GetSetting("cargotype_sweets_min") != -1)					// SWET	Sweets (Candy)	0004 Express			 Toyland
		this.transportGoalSettings.cargotype_sweets_min = GSController.GetSetting("cargotype_sweets_min");
	if (GSController.GetSetting("cargotype_sweets_max") != -1)					// SWET	Sweets (Candy)	0004 Express			 Toyland
		this.transportGoalSettings.cargotype_sweets_max = GSController.GetSetting("cargotype_sweets_max");
	if (GSController.GetSetting("cargotype_toffee_min") != -1)					// TOFF	Toffee	0010 Bulk			 Toyland
		this.transportGoalSettings.cargotype_toffee_min = GSController.GetSetting("cargotype_toffee_min");
	if (GSController.GetSetting("cargotype_toffee_max") != -1)					// TOFF	Toffee	0010 Bulk			 Toyland
		this.transportGoalSettings.cargotype_toffee_max = GSController.GetSetting("cargotype_toffee_max");
	if (GSController.GetSetting("cargotype_cola_min") != -1)						// COLA	Cola	0040 Liquid			 Toyland
		this.transportGoalSettings.cargotype_cola_min = GSController.GetSetting("cargotype_cola_min");
	if (GSController.GetSetting("cargotype_cola_max") != -1)						// COLA	Cola	0040 Liquid			 Toyland
		this.transportGoalSettings.cargotype_cola_max = GSController.GetSetting("cargotype_cola_max");
	if (GSController.GetSetting("cargotype_cottoncandy_min") != -1)				// CTCD	Cotton Candy (Candyfloss)	0010 Bulk			 Toyland
		this.transportGoalSettings.cargotype_cottoncandy_min = GSController.GetSetting("cargotype_cottoncandy_min");
	if (GSController.GetSetting("cargotype_cottoncandy_max") != -1)				// CTCD	Cotton Candy (Candyfloss)	0010 Bulk			 Toyland
		this.transportGoalSettings.cargotype_cottoncandy_max = GSController.GetSetting("cargotype_cottoncandy_max");
	if (GSController.GetSetting("cargotype_bubbles_min") != -1)					// BUBL	Bubbles	0020 Piece goods			 Toyland
		this.transportGoalSettings.cargotype_bubbles_min = GSController.GetSetting("cargotype_bubbles_min");
	if (GSController.GetSetting("cargotype_bubbles_max") != -1)					// BUBL	Bubbles	0020 Piece goods			 Toyland
		this.transportGoalSettings.cargotype_bubbles_max = GSController.GetSetting("cargotype_bubbles_max");
	if (GSController.GetSetting("cargotype_plastic_min") != -1)					// PLST	Plastic	0040 Liquid			 Toyland; see also PLAS
		this.transportGoalSettings.cargotype_plastic_min = GSController.GetSetting("cargotype_plastic_min");
	if (GSController.GetSetting("cargotype_plastic_max") != -1)					// PLST	Plastic	0040 Liquid			 Toyland; see also PLAS
		this.transportGoalSettings.cargotype_plastic_max = GSController.GetSetting("cargotype_plastic_max");
	if (GSController.GetSetting("cargotype_fizzy_min") != -1)						// FZDR	Fizzy Drinks	0020 Piece goods			 Toyland
		this.transportGoalSettings.cargotype_fizzy_min = GSController.GetSetting("cargotype_fizzy_min");
	if (GSController.GetSetting("cargotype_fizzy_max") != -1)						// FZDR	Fizzy Drinks	0020 Piece goods			 Toyland
		this.transportGoalSettings.cargotype_fizzy_max = GSController.GetSetting("cargotype_fizzy_max");
	
	//	New Cargos	 these cargos are only present when NewGRF industry sets are used
	if (GSController.GetSetting("cargotype_bauxite_min") != -1)					// AORE	Bauxite (Aluminium ore)	0010 Bulk	ECS	FIRS
		this.transportGoalSettings.cargotype_bauxite_min = GSController.GetSetting("cargotype_bauxite_min");
	if (GSController.GetSetting("cargotype_bauxite_max") != -1)					// AORE	Bauxite (Aluminium ore)	0010 Bulk	ECS	FIRS
		this.transportGoalSettings.cargotype_bauxite_max = GSController.GetSetting("cargotype_bauxite_max");
	if (GSController.GetSetting("cargotype_alcohol_min") != -1)					// BEER	Alcohol	0064 Express, piece goods, liquids		FIRS
		this.transportGoalSettings.cargotype_alcohol_min = GSController.GetSetting("cargotype_alcohol_min");
	if (GSController.GetSetting("cargotype_alcohol_max") != -1)					// BEER	Alcohol	0064 Express, piece goods, liquids		FIRS
		this.transportGoalSettings.cargotype_alcohol_max = GSController.GetSetting("cargotype_alcohol_max");
	if (GSController.GetSetting("cargotype_buildingmaterials_min") != -1)			// BDMT	Building Materials	0220 Piece goods, covered/sheltered	ECS	FIRS	 In FIRS: 0030 Bulk, piece goods
		this.transportGoalSettings.cargotype_buildingmaterials_min = GSController.GetSetting("cargotype_buildingmaterials_min");
	if (GSController.GetSetting("cargotype_buildingmaterials_max") != -1)			// BDMT	Building Materials	0220 Piece goods, covered/sheltered	ECS	FIRS	 In FIRS: 0030 Bulk, piece goods
		this.transportGoalSettings.cargotype_buildingmaterials_max = GSController.GetSetting("cargotype_buildingmaterials_max");
	if (GSController.GetSetting("cargotype_bricks_min") != -1)					// BRCK	Bricks	 0020 Piece goods	ECS
		this.transportGoalSettings.cargotype_bricks_min = GSController.GetSetting("cargotype_bricks_min");
	if (GSController.GetSetting("cargotype_bricks_max") != -1)					// BRCK	Bricks	 0020 Piece goods	ECS
		this.transportGoalSettings.cargotype_bricks_max = GSController.GetSetting("cargotype_bricks_max");
	if (GSController.GetSetting("cargotype_ceramics_min") != -1)					// CERA	Ceramics	 0020 Piece goods	ECS
		this.transportGoalSettings.cargotype_ceramics_min = GSController.GetSetting("cargotype_ceramics_min");
	if (GSController.GetSetting("cargotype_ceramics_max") != -1)					// CERA	Ceramics	 0020 Piece goods	ECS
		this.transportGoalSettings.cargotype_ceramics_max = GSController.GetSetting("cargotype_ceramics_max");
	if (GSController.GetSetting("cargotype_cereals_min") != -1)					// CERE	Cereals	 0210 Bulk, covered/sheltered	ECS		[1]
		this.transportGoalSettings.cargotype_cereals_min = GSController.GetSetting("cargotype_cereals_min");
	if (GSController.GetSetting("cargotype_cereals_max") != -1)					// CERE	Cereals	 0210 Bulk, covered/sheltered	ECS		[1]
		this.transportGoalSettings.cargotype_cereals_max = GSController.GetSetting("cargotype_cereals_max");
	if (GSController.GetSetting("cargotype_clay_min") != -1)						// CLAY	Clay	0210 Bulk covered/sheltered		FIRS	 In FIRS: 0010 Bulk
		this.transportGoalSettings.cargotype_clay_min = GSController.GetSetting("cargotype_clay_min");
	if (GSController.GetSetting("cargotype_clay_max") != -1)						// CLAY	Clay	0210 Bulk covered/sheltered		FIRS	 In FIRS: 0010 Bulk
		this.transportGoalSettings.cargotype_clay_max = GSController.GetSetting("cargotype_clay_max");
	if (GSController.GetSetting("cargotype_cement_min") != -1)					// CMNT	Cement	 0210 Bulk covered/sheltered	ECS
		this.transportGoalSettings.cargotype_cement_min = GSController.GetSetting("cargotype_cement_min");
	if (GSController.GetSetting("cargotype_cement_max") != -1)					// CMNT	Cement	 0210 Bulk covered/sheltered	ECS
		this.transportGoalSettings.cargotype_cement_max = GSController.GetSetting("cargotype_cement_max");
	if (GSController.GetSetting("cargotype_copper_min") != -1)					// COPR	Copper	0020 Piece goods
		this.transportGoalSettings.cargotype_copper_min = GSController.GetSetting("cargotype_copper_min");
	if (GSController.GetSetting("cargotype_copper_max") != -1)					// COPR	Copper	0020 Piece goods
		this.transportGoalSettings.cargotype_copper_max = GSController.GetSetting("cargotype_copper_max");
	if (GSController.GetSetting("cargotype_dyes_min") != -1)						// DYES	Dyes	 0060 Piece goods, liquids	ECS
		this.transportGoalSettings.cargotype_dyes_min = GSController.GetSetting("cargotype_dyes_min");
	if (GSController.GetSetting("cargotype_dyes_max") != -1)						// DYES	Dyes	 0060 Piece goods, liquids	ECS
		this.transportGoalSettings.cargotype_dyes_max = GSController.GetSetting("cargotype_dyes_max");
	if (GSController.GetSetting("cargotype_engineeringsupplies_min") != -1)		// ENSP	Engineering Supplies	0024 Express, piece goods		FIRS
		this.transportGoalSettings.cargotype_engineeringsupplies_min = GSController.GetSetting("cargotype_engineeringsupplies_min");
	if (GSController.GetSetting("cargotype_engineeringsupplies_max") != -1)		// ENSP	Engineering Supplies	0024 Express, piece goods		FIRS
		this.transportGoalSettings.cargotype_engineeringsupplies_max = GSController.GetSetting("cargotype_engineeringsupplies_max");
	if (GSController.GetSetting("cargotype_fertiliser_min") != -1)				// FERT	Fertiliser	 0030 Bulk, piece goods	ECS		[1]
		this.transportGoalSettings.cargotype_fertiliser_min = GSController.GetSetting("cargotype_fertiliser_min");
	if (GSController.GetSetting("cargotype_fertiliser_max") != -1)				// FERT	Fertiliser	 0030 Bulk, piece goods	ECS		[1]
		this.transportGoalSettings.cargotype_fertiliser_max = GSController.GetSetting("cargotype_fertiliser_max");
	if (GSController.GetSetting("cargotype_fibrecrops_min") != -1)				// FICR	Fibre crops	 0030 Bulk, piece goods	ECS	FIRS
		this.transportGoalSettings.cargotype_fibrecrops_min = GSController.GetSetting("cargotype_fibrecrops_min");
	if (GSController.GetSetting("cargotype_fibrecrops_max") != -1)				// FICR	Fibre crops	 0030 Bulk, piece goods	ECS	FIRS
		this.transportGoalSettings.cargotype_fibrecrops_max = GSController.GetSetting("cargotype_fibrecrops_max");
	if (GSController.GetSetting("cargotype_fish_min") != -1)						// FISH	Fish	 0084 Express, refrigerated	ECS	FIRS
		this.transportGoalSettings.cargotype_fish_min = GSController.GetSetting("cargotype_fish_min");
	if (GSController.GetSetting("cargotype_fish_max") != -1)						// FISH	Fish	 0084 Express, refrigerated	ECS	FIRS
		this.transportGoalSettings.cargotype_fish_max = GSController.GetSetting("cargotype_fish_max");
	if (GSController.GetSetting("cargotype_farmsupplies_min") != -1)				// FMSP	Farm Supplies	0024 Express, piece goods		FIRS
		this.transportGoalSettings.cargotype_farmsupplies_min = GSController.GetSetting("cargotype_farmsupplies_min");
	if (GSController.GetSetting("cargotype_farmsupplies_max") != -1)				// FMSP	Farm Supplies	0024 Express, piece goods		FIRS
		this.transportGoalSettings.cargotype_farmsupplies_max = GSController.GetSetting("cargotype_farmsupplies_max");
	if (GSController.GetSetting("cargotype_frvgfruit_min") != -1)					// FRVG	Fruit (and optionally Vegetables)	00A4 Express, piece goods, refrigerated		FIRS
		this.transportGoalSettings.cargotype_frvgfruit_min = GSController.GetSetting("cargotype_frvgfruit_min");
	if (GSController.GetSetting("cargotype_frvgfruit_max") != -1)					// FRVG	Fruit (and optionally Vegetables)	00A4 Express, piece goods, refrigerated		FIRS
		this.transportGoalSettings.cargotype_frvgfruit_max = GSController.GetSetting("cargotype_frvgfruit_max");
	if (GSController.GetSetting("cargotype_glass_min") != -1)						// GLAS	Glass	 0420 Piece goods, oversized	ECS
		this.transportGoalSettings.cargotype_glass_min = GSController.GetSetting("cargotype_glass_min");
	if (GSController.GetSetting("cargotype_glass_max") != -1)						// GLAS	Glass	 0420 Piece goods, oversized	ECS
		this.transportGoalSettings.cargotype_glass_max = GSController.GetSetting("cargotype_glass_max");
	if (GSController.GetSetting("cargotype_gravelballast_min") != -1)				// GRVL	Gravel / Ballast	0010 Bulk		FIRS
		this.transportGoalSettings.cargotype_gravelballast_min = GSController.GetSetting("cargotype_gravelballast_min");
	if (GSController.GetSetting("cargotype_gravelballast_max") != -1)				// GRVL	Gravel / Ballast	0010 Bulk		FIRS
		this.transportGoalSettings.cargotype_gravelballast_max = GSController.GetSetting("cargotype_gravelballast_max");
	if (GSController.GetSetting("cargotype_limestone_min") != -1)					// LIME	Lime stone	 0010 Bulk	ECS
		this.transportGoalSettings.cargotype_limestone_min = GSController.GetSetting("cargotype_limestone_min");
	if (GSController.GetSetting("cargotype_limestone_max") != -1)					// LIME	Lime stone	 0010 Bulk	ECS
		this.transportGoalSettings.cargotype_limestone_max = GSController.GetSetting("cargotype_limestone_max");
	if (GSController.GetSetting("cargotype_milk_min") != -1)						// MILK	Milk	00C4 Express, liquid, refrigerated		FIRS
		this.transportGoalSettings.cargotype_milk_min = GSController.GetSetting("cargotype_milk_min");
	if (GSController.GetSetting("cargotype_milk_max") != -1)						// MILK	Milk	00C4 Express, liquid, refrigerated		FIRS
		this.transportGoalSettings.cargotype_milk_max = GSController.GetSetting("cargotype_milk_max");
	if (GSController.GetSetting("cargotype_manufacturingsupplies_min") != -1)		// MNSP	Manufacturing Supplies	0024 Piece Goods, express		FIRS
		this.transportGoalSettings.cargotype_manufacturingsupplies_min = GSController.GetSetting("cargotype_manufacturingsupplies_min");
	if (GSController.GetSetting("cargotype_manufacturingsupplies_max") != -1)		// MNSP	Manufacturing Supplies	0024 Piece Goods, express		FIRS
		this.transportGoalSettings.cargotype_manufacturingsupplies_max = GSController.GetSetting("cargotype_manufacturingsupplies_max");
	if (GSController.GetSetting("cargotype_metal_min") != -1)						// STEL ez az acelt irja felul!!!
		this.transportGoalSettings.cargotype_metal_min = GSController.GetSetting("cargotype_metal_min");
	if (GSController.GetSetting("cargotype_metal_max") != -1)						// STEL ez az acelt irja felul!!!
		this.transportGoalSettings.cargotype_metal_max = GSController.GetSetting("cargotype_metal_max");
	if (GSController.GetSetting("cargotype_oilseed_min") != -1)					// OLSD	Oil seed	 0210 Bulk, covered/sheltered	ECS		[1]
		this.transportGoalSettings.cargotype_oilseed_min = GSController.GetSetting("cargotype_oilseed_min");
	if (GSController.GetSetting("cargotype_oilseed_max") != -1)					// OLSD	Oil seed	 0210 Bulk, covered/sheltered	ECS		[1]
		this.transportGoalSettings.cargotype_oilseed_max = GSController.GetSetting("cargotype_oilseed_max");
	if (GSController.GetSetting("cargotype_petrolfueloil_min") != -1)				// PETR	Petrol / Fuel Oil	 0040 Liquid	ECS	FIRS
		this.transportGoalSettings.cargotype_petrolfueloil_min = GSController.GetSetting("cargotype_petrolfueloil_min");
	if (GSController.GetSetting("cargotype_petrolfueloil_max") != -1)				// PETR	Petrol / Fuel Oil	 0040 Liquid	ECS	FIRS
		this.transportGoalSettings.cargotype_petrolfueloil_max = GSController.GetSetting("cargotype_petrolfueloil_max");
	if (GSController.GetSetting("cargotype_plasplastic_min") != -1)				// PLAS	Plastic	 0060 Piece goods, liquid	ECS
		this.transportGoalSettings.cargotype_plasplastic_min = GSController.GetSetting("cargotype_plasplastic_min");
	if (GSController.GetSetting("cargotype_plasplastic_max") != -1)				// PLAS	Plastic	 0060 Piece goods, liquid	ECS
		this.transportGoalSettings.cargotype_plasplastic_max = GSController.GetSetting("cargotype_plasplastic_max");
	if (GSController.GetSetting("cargotype_potash_min") != -1)					// POTA	Potash	 0210 Bulk, covered/sheltered	ECS		[1]
		this.transportGoalSettings.cargotype_potash_min = GSController.GetSetting("cargotype_potash_min");
	if (GSController.GetSetting("cargotype_potash_max") != -1)					// POTA	Potash	 0210 Bulk, covered/sheltered	ECS		[1]
		this.transportGoalSettings.cargotype_potash_max = GSController.GetSetting("cargotype_potash_max");
	if (GSController.GetSetting("cargotype_recyclables_min") != -1)				// RCYC	Recyclables	0220 Piece Goods, covered		FIRS
		this.transportGoalSettings.cargotype_recyclables_min = GSController.GetSetting("cargotype_recyclables_min");
	if (GSController.GetSetting("cargotype_recyclables_max") != -1)				// RCYC	Recyclables	0220 Piece Goods, covered		FIRS
		this.transportGoalSettings.cargotype_recyclables_max = GSController.GetSetting("cargotype_recyclables_max");
	if (GSController.GetSetting("cargotype_refinedproducts_min") != -1)			// RFPR	Refined products	 0040 Liquid	ECS	FIRS
		this.transportGoalSettings.cargotype_refinedproducts_min = GSController.GetSetting("cargotype_refinedproducts_min");
	if (GSController.GetSetting("cargotype_refinedproducts_max") != -1)			// RFPR	Refined products	 0040 Liquid	ECS	FIRS
		this.transportGoalSettings.cargotype_refinedproducts_max = GSController.GetSetting("cargotype_refinedproducts_max");
	if (GSController.GetSetting("cargotype_sand_min") != -1)						// SAND	Sand	 0010 Bulk	ECS	FIRS
		this.transportGoalSettings.cargotype_sand_min = GSController.GetSetting("cargotype_sand_min");
	if (GSController.GetSetting("cargotype_sand_max") != -1)						// SAND	Sand	 0010 Bulk	ECS	FIRS
		this.transportGoalSettings.cargotype_sand_max = GSController.GetSetting("cargotype_sand_max");
	if (GSController.GetSetting("cargotype_scraptmetal_min") != -1)				// SCMT	Scrap Metal	1010 Bulk, non-pourable		FIRS
		this.transportGoalSettings.cargotype_scraptmetal_min = GSController.GetSetting("cargotype_scraptmetal_min");
	if (GSController.GetSetting("cargotype_scraptmetal_max") != -1)				// SCMT	Scrap Metal	1010 Bulk, non-pourable		FIRS
		this.transportGoalSettings.cargotype_scraptmetal_max = GSController.GetSetting("cargotype_scraptmetal_max");
	if (GSController.GetSetting("cargotype_sugarbeet_min") != -1)					// SGBT	Sugar beet	0010 Bulk		FIRS	 not in tropical
		this.transportGoalSettings.cargotype_sugarbeet_min = GSController.GetSetting("cargotype_sugarbeet_min");
	if (GSController.GetSetting("cargotype_sugarbeet_max") != -1)					// SGBT	Sugar beet	0010 Bulk		FIRS	 not in tropical
		this.transportGoalSettings.cargotype_sugarbeet_max = GSController.GetSetting("cargotype_sugarbeet_max");
	if (GSController.GetSetting("cargotype_sugarcane_min") != -1)					// SGCN	Sugarcane	1010 Bulk, non-pourable		FIRS	 only tropical
		this.transportGoalSettings.cargotype_sugarcane_min = GSController.GetSetting("cargotype_sugarcane_min");
	if (GSController.GetSetting("cargotype_sugarcane_max") != -1)					// SGCN	Sugarcane	1010 Bulk, non-pourable		FIRS	 only tropical
		this.transportGoalSettings.cargotype_sugarcane_max = GSController.GetSetting("cargotype_sugarcane_max");
	if (GSController.GetSetting("cargotype_sulphur_min") != -1)					// SULP	Sulphur	 0210 Bulk, covered/sheltered	ECS		[1]
		this.transportGoalSettings.cargotype_sulphur_min = GSController.GetSetting("cargotype_sulphur_min");
	if (GSController.GetSetting("cargotype_sulphur_max") != -1)					// SULP	Sulphur	 0210 Bulk, covered/sheltered	ECS		[1]
		this.transportGoalSettings.cargotype_sulphur_max = GSController.GetSetting("cargotype_sulphur_max");
	if (GSController.GetSetting("cargotype_tourists_min") != -1)					// TOUR	Tourists	 0005 Passengers, express	ECS
		this.transportGoalSettings.cargotype_tourists_min = GSController.GetSetting("cargotype_tourists_min");
	if (GSController.GetSetting("cargotype_tourists_max") != -1)					// TOUR	Tourists	 0005 Passengers, express	ECS
		this.transportGoalSettings.cargotype_tourists_max = GSController.GetSetting("cargotype_tourists_max");
	if (GSController.GetSetting("cargotype_vehicles_min") != -1)					// VEHI	Vehicles	 0420 Piece goods, oversized	ECS
		this.transportGoalSettings.cargotype_vehicles_min = GSController.GetSetting("cargotype_vehicles_min");
	if (GSController.GetSetting("cargotype_vehicles_max") != -1)					// VEHI	Vehicles	 0420 Piece goods, oversized	ECS
		this.transportGoalSettings.cargotype_vehicles_max = GSController.GetSetting("cargotype_vehicles_max");
	if (GSController.GetSetting("cargotype_wdprwood_min") != -1)					// WDPR	Wood Products	 0030 Bulk, piece goods	ECS	FIRS
		this.transportGoalSettings.cargotype_wdprwood_min = GSController.GetSetting("cargotype_wdprwood_min");
	if (GSController.GetSetting("cargotype_wdprwood_max") != -1)					// WDPR	Wood Products	 0030 Bulk, piece goods	ECS	FIRS
		this.transportGoalSettings.cargotype_wdprwood_max = GSController.GetSetting("cargotype_wdprwood_max");
	if (GSController.GetSetting("cargotype_wool_min") != -1)						// WOOL	Wool	 0220 Piece goods, covered/sheltered	ECS	FIRS	[1]
		this.transportGoalSettings.cargotype_wool_min = GSController.GetSetting("cargotype_wool_min");
	if (GSController.GetSetting("cargotype_wool_max") != -1)						// WOOL	Wool	 0220 Piece goods, covered/sheltered	ECS	FIRS	[1]
		this.transportGoalSettings.cargotype_wool_max = GSController.GetSetting("cargotype_wool_max");
	
	
	//	Special Cargos	 these cargos are for use outside industry sets and do not represent transporting anything
	if (GSController.GetSetting("cargotype_default_min") != -1)					// DFLT	'Default'	 ---- None			 Used by andythenorth to identify default cargo graphics in newgrfs that use code/graphics generation.
		this.transportGoalSettings.cargotype_default_min = GSController.GetSetting("cargotype_default_min");
	if (GSController.GetSetting("cargotype_default_max") != -1)					// DFLT	'Default'	 ---- None			 Used by andythenorth to identify default cargo graphics in newgrfs that use code/graphics generation.
		this.transportGoalSettings.cargotype_default_max = GSController.GetSetting("cargotype_default_max");
	
	if (GSController.GetSetting("cargotype_locomotiveregearing_min") != -1)		// GEAR	Locomotive regearing	8000 Special			 Using this cargo may make your vehicle set incompatible with some industry sets
		this.transportGoalSettings.cargotype_locomotiveregearing_min = GSController.GetSetting("cargotype_locomotiveregearing_min");
	if (GSController.GetSetting("cargotype_locomotiveregearing_max") != -1)		// GEAR	Locomotive regearing	8000 Special			 Using this cargo may make your vehicle set incompatible with some industry sets
		this.transportGoalSettings.cargotype_locomotiveregearing_max = GSController.GetSetting("cargotype_locomotiveregearing_max");
	
	//	Deprecated Cargos	 these cargos are not used by any current industry set, and are listed here for backwards compatibility
	if (GSController.GetSetting("cargotype_fuel_min") != -1)						// FUEL	Fuel	0040 Liquid			Use PETR for refined-oil fuel
		this.transportGoalSettings.cargotype_fuel_min = GSController.GetSetting("cargotype_fuel_min");
	if (GSController.GetSetting("cargotype_fuel_max") != -1)						// FUEL	Fuel	0040 Liquid			Use PETR for refined-oil fuel
		this.transportGoalSettings.cargotype_fuel_max = GSController.GetSetting("cargotype_fuel_max");
	if (GSController.GetSetting("cargotype_rawsugar_min") != -1)					// RSGR	Raw Sugar	0010 Bulk			 Deprecated in FIRS. See SGBT and SGCN
		this.transportGoalSettings.cargotype_rawsugar_min = GSController.GetSetting("cargotype_rawsugar_min");
	if (GSController.GetSetting("cargotype_rawsugar_max") != -1)					// RSGR	Raw Sugar	0010 Bulk			 Deprecated in FIRS. See SGBT and SGCN
		this.transportGoalSettings.cargotype_rawsugar_max = GSController.GetSetting("cargotype_rawsugar_max");
	if (GSController.GetSetting("cargotype_scrpscrapmetal_min") != -1)			// SCRP	Scrap Metal	0010 Piece Goods			 Deprecated in FIRS, use SCMT instead
		this.transportGoalSettings.cargotype_scrpscrapmetal_min = GSController.GetSetting("cargotype_scrpscrapmetal_min");
	if (GSController.GetSetting("cargotype_scrpscrapmetal_max") != -1)			// SCRP	Scrap Metal	0010 Piece Goods			 Deprecated in FIRS, use SCMT instead
		this.transportGoalSettings.cargotype_scrpscrapmetal_max = GSController.GetSetting("cargotype_scrpscrapmetal_max");
	if (GSController.GetSetting("cargotype_tropicwood_min") != -1)				// TWOD	Tropic Wood	0020 Piece goods			 formerly intended as a default cargo
		this.transportGoalSettings.cargotype_tropicwood_min = GSController.GetSetting("cargotype_tropicwood_min");
	if (GSController.GetSetting("cargotype_tropicwood_max") != -1)				// TWOD	Tropic Wood	0020 Piece goods			 formerly intended as a default cargo
		this.transportGoalSettings.cargotype_tropicwood_max = GSController.GetSetting("cargotype_tropicwood_max");
	if (GSController.GetSetting("cargotype_waste_min") != -1)						// WSTE	Waste	0010 Bulk			 Deprecated in FIRS.
		this.transportGoalSettings.cargotype_waste_min = GSController.GetSetting("cargotype_waste_min");
	if (GSController.GetSetting("cargotype_waste_max") != -1)						// WSTE	Waste	0010 Bulk			 Deprecated in FIRS.
		this.transportGoalSettings.cargotype_waste_max = GSController.GetSetting("cargotype_waste_max");
	
	if (this.testenabled) {
		GSLog.Warning("[FecaGame::LoadPreferences] Proba beallitasokat ki kell kapcsolni.");
//		population_min = 700;
//		population_max = 800;
		
//		this.generalGoalSettings.enableChanceToAnyNumberOfAward = true;
		
		
		 this.initialPossibilities.shouldRailDepos = -1;
		 this.initialPossibilities.shouldRoadDepos = -1;
		 this.initialPossibilities.shouldWaterDepos = -1;
		 this.initialPossibilities.shouldRailStations = -1;
		 this.initialPossibilities.shouldTruckStops = -1;
		 this.initialPossibilities.shouldBusStops = -1;
		 this.initialPossibilities.shouldWaterDocks = -1;
		 this.initialPossibilities.shouldAirPorts = -1;
		 this.initialPossibilities.shouldRailVehicles = -1;
		 this.initialPossibilities.shouldTruckVehicles = -1;
		 this.initialPossibilities.shouldBusVehicles = -1;
		 this.initialPossibilities.shouldWaterVehicles = -1;
		 this.initialPossibilities.shouldAirVehicles = -1;
		 
		 
		 this.initialPossibilities.awardRailDepos = true;
		 this.initialPossibilities.awardRoadDepos = true;
		 this.initialPossibilities.awardWaterDepos = true;
		 this.initialPossibilities.awardRailStations = true;
		 this.initialPossibilities.awardTruckStops = true;
		 this.initialPossibilities.awardBusStops = true;
		 this.initialPossibilities.awardWaterDocks = true;
		 this.initialPossibilities.awardAirPorts = true;
		 this.initialPossibilities.awardRailVehicles = true;
		 this.initialPossibilities.awardTruckVehicles = true;
		 this.initialPossibilities.awardBusVehicles = true;
		 this.initialPossibilities.awardWaterVehicles = true;
		 this.initialPossibilities.awardAirVehicles = true;
		 
		 
		
		
		
		
		// general goal settings
		if (this.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0)) {
			this.generalGoalSettings.cupUpdates = 0;
			this.generalGoalSettings.comapnyAwardGoalsUpdateViaQuestion = 0;
		}
		else {
			this.generalGoalSettings.cupUpdates = 1;
			this.generalGoalSettings.comapnyAwardGoalsUpdateViaQuestion = 1;
		}
		
		this.generalGoalSettings.gameTime = 0;
		this.generalGoalSettings.neverRunOutMain = false;
		this.generalGoalSettings.neverRunOutAwards = false;
		this.generalGoalSettings.neverRunOutWeakAwards = false;
		this.generalGoalSettings.allAtOnceTimeShowedMainGoals = 0;
		this.generalGoalSettings.allAtOnceTimeShowedAwardGoals = 0;
		
		
		
		// towngrowth settings
		this.baseTownGrowthGoalSettings.minMainGoal = 0;
		this.baseTownGrowthGoalSettings.maxMainGoal = 0;
		this.baseTownGrowthGoalSettings.minDifficulty = 5;
		this.baseTownGrowthGoalSettings.maxDifficulty = 5;
		
		this.townGrowthGoalSettings.level_min = 1;
		this.townGrowthGoalSettings.level_max = 0;

		// ha population == 5, akkor eletbe lep a custom population
		this.townGrowthGoalSettings.population = 5;
		this.townGrowthGoalSettings.customPopulationMin = 1900;
		this.townGrowthGoalSettings.customPopulationMax = 2000;


		this.townGrowthGoalSettings.custom_goal_population = 2120; // difficulty
		
		
		
		// transport goal settings
		this.baseTransportGoalSettings.minMainGoal = 1;
		this.baseTransportGoalSettings.maxMainGoal = 1;
		this.baseTransportGoalSettings.minAwardGoal = 0;
		this.baseTransportGoalSettings.maxAwardGoal = 0;
		this.baseTransportGoalSettings.minWeakAwardGoal = 0;
		this.baseTransportGoalSettings.maxWeakAwardGoal = 0;
		
		this.baseTransportGoalSettings.awardsForSuccesGoalMainMin = 0; // mennyi jutalmat kaphat egy feladatert minimum
		this.baseTransportGoalSettings.awardsForSuccesGoalMainMax = 0; // mennyi jutalmat kaphat egy feladatert maximum
		this.baseTransportGoalSettings.awardsForSuccesGoalAwardMin = 0; // mennyi jutalmat kaphat egy feladatert minimum
		this.baseTransportGoalSettings.awardsForSuccesGoalAwardMax = 0; // mennyi jutalmat kaphat egy feladatert maximum
		this.baseTransportGoalSettings.awardsForSuccesGoalWeakAwardMin = 0; // mennyi jutalmat kaphat egy feladatert minimum
		this.baseTransportGoalSettings.awardsForSuccesGoalWeakAwardMax = 0; // mennyi jutalmat kaphat egy feladatert maximum
		
		this.baseTransportGoalSettings.minMonthNumber = 0;
		this.baseTransportGoalSettings.maxMonthNumber = 0;
		
		this.transportGoalSettings.enableAllCargoTypes = false;
		this.transportGoalSettings.onlyMainGoalLevels = false;
		this.transportGoalSettings.transportGoalSettingsUpdateAll = false;
		
		this.transportGoalSettings.cargotype_valuables_min = 0;
		this.transportGoalSettings.cargotype_valuables_max = 0;
		this.transportGoalSettings.cargotype_steel_min = 0;
		this.transportGoalSettings.cargotype_steel_max = 0;
		this.transportGoalSettings.cargotype_ironore_min = 0;
		this.transportGoalSettings.cargotype_ironore_max = 0;
		this.transportGoalSettings.cargotype_wood_min = 0;
		this.transportGoalSettings.cargotype_wood_max = 0;
		this.transportGoalSettings.cargotype_grain_min = 0;
		this.transportGoalSettings.cargotype_grain_max = 0;
		this.transportGoalSettings.cargotype_goods_min = 0;
		this.transportGoalSettings.cargotype_goods_max = 0;
		this.transportGoalSettings.cargotype_livestock_min = 0;
		this.transportGoalSettings.cargotype_livestock_max = 0;
		this.transportGoalSettings.cargotype_oil_min = 0;
		this.transportGoalSettings.cargotype_oil_max = 0;
		this.transportGoalSettings.cargotype_mail_min = 0;
		this.transportGoalSettings.cargotype_mail_max = 0;
		this.transportGoalSettings.cargotype_coal_min = 0;
		this.transportGoalSettings.cargotype_coal_max = 0;
		this.transportGoalSettings.cargotype_passengers_min = 1;
		this.transportGoalSettings.cargotype_passengers_max = 1;


		
	}
}

function FecaGame::SendStartTimeToLog() {
	if (!this.logmode)
		return;
	
	// eloszor az orat szamoljuk ki az idoeltolodas miatt
	local sd = this.systemDay;
	local sh = this.systemHour;
	local sm = this.systemMonth;
	local sy = this.systemYear;
	
	local months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	
	if (GSController.GetSetting("timeZone") != -1)
		sh += (GSController.GetSetting("timeZone") - 12);
	
	if (sh >= 24) {
		sh -= 24;
		sd++;
	}
	else if (sh < 0) {
		sh += 24;
		sd--;
	}
	
	if (sd > months[sm - 1]) {
		sm++;
		sd = 1;
		if (sm > 12) {
			sy++;
			sm = 1;
		}
	}
	else if (sd < 1) {
		sm--;
		if (sm < 1) {
			sy--;
			sm = 12;
		}
		sd = months[sm - 1];
	}
	
	if (sd < 10)
		sd = "0" + sd;
	
	if (sh < 10)
		sh = "0" + sh;
	
	if (sm < 10)
		sm = "0" + sm;
	
	local smi = this.systemMinute;
	if (smi < 10)
		smi = "0" + smi;
	
	GSLog.Info("");
	GSLog.Info("Start Game at " + sy + ". " + sm + ". " + sd + ". " + sh + ":" + smi + "!");
	GSLog.Info("");
}

function FecaGame::SendBeginWarning(beforeminute) {
	// uj modszer
	local starthour = this.game_start_hour;
	local startminute = this.game_start_minute;
	
	
	local et = GSText(GSText.STR_EMPTY);
	local textNull = GSText(GSText.STR_NULL);
	
	local textHour = starthour;
	local textMinute = startminute - beforeminute;
	if (textMinute < 0) {
		textMinute += 60;
		textHour--;
	}
	
	local hourNull;
	local minuteNull;
	
	if (starthour > 9)
		hourNull = et;
	else
		hourNull = textNull;
	
	if (startminute > 9)
		minuteNull = et;
	else
		minuteNull = textNull;
	
	if (this.systemHour == textHour && this.systemMinute == textMinute) {
		// megnezzuk, hogy kuldtunk-e mar uzenetet. Ha nem, akkor kikuldjuk...
		local van = false;
		foreach (bmin in this.sentStartMessages) {
			if (bmin == beforeminute) {
				van = true;
				break;
			}
		}
		
		if (!van) {
			if (GSController.GetSetting("timeZone") != -1)
				starthour += (GSController.GetSetting("timeZone") - 12);
			
			if (starthour >= 24)
				starthour -= 24;
			else if (starthour < 0)
				starthour += 24;
			
			this.SendMesesageWithQuestion(GSCompany.COMPANY_INVALID, GSText(GSText.STR_GAME_BEGIN, beforeminute, hourNull, starthour, minuteNull, startminute));
			this.sentStartMessages.append(beforeminute);
		}
	}
}

function FecaGame::HandleEvents() {
	if (GSEventController.IsEventWaiting()) {
		local ev = GSEventController.GetNextEvent();
		
		switch (ev.GetEventType()) {
				
		
/*
		if (ev == null) {
			return;
		}

		local ev_type = ev.GetEventType();
*/

			case GSEvent.ET_ADMIN_PORT:
				if (!this.fggc_active)
					break;

				local ec = GSEventAdminPort.Convert(ev);
				local data = ec.GetObject();
				if (data) {
					if (data.rawin("newspaper")) {
						GSNews.Create(GSNews.NT_GENERAL, data.newspaper, GSCompany.COMPANY_INVALID);
					}
					else if (data.rawin("newgoal")) {
						GSGoal.New(GSCompany.COMPANY_INVALID, data.newgoal, GSGoal.GT_NONE, 0);
					}
					else if (data.rawin("auth")) {
						local value = data.auth.tointeger();
						local van = false;
						foreach (company in this.companies) {
							if (company.fggc_id == value) {
								van = true;

								local compName = this.ReplaceStringWithString("\"", "%22", this.CompanyName(company.company_id));
								compName = this.ReplaceStringWithString("'", "%27", compName);

								if (!GSAdmin.Send({auth = "success", companyID = company.company_id, companyName = compName, scores = company.GetScores(), csatlakozas = this.GetCurrentDateTimeString()}))
									GSLog.Error("Nem sikerult elkuldeni a success authot francba! cID: " + company.company_id + ", fgg: " + value);
							}
						}
						if (!van) {
							if (!GSAdmin.Send({auth = "error"}))
								GSLog.Error("Nem sikerult elkuldeni az errort a francba, fgg: " + value);
							else
								GSLog.Info("elkuldve");
						}
					}
					else if (data.rawin("join")) {
						local value = data.join.tointeger();
						local van = false;
						foreach (company in this.companies) {
							if (company.company_id != value)
								continue;

							van = true;
							company.fggc_joined = true;
							if (data.rawin("versenyID"))
								company.fggc_versenyID = data.versenyID;
							GSGoal.CloseQuestion(company.fggc_question);
							GSGoal.Question(company.fggc_question, company.company_id, GSText(GSText.STR_ADMINPORT_SUCCESS_JOIN), GSGoal.QT_INFORMATION, GSGoal.BUTTON_CLOSE);
							if (!GSAdmin.Send({join = "success", companyID = company.company_id, scores = company.GetScores()}))
								GSLog.Error("Nem sikerult elkuldeni a csatlakozazast a francba! cID: " + company.company_id + "!");
						}

						if (!van) {
							if (!GSAdmin.Send({join = "error", message = "Nem található a megadott vállalat azonosító!"}))
								GSLog.Error("Nem sikerult elkuldeni az error csatlakozazast a francba! cID: " + company.company_id + "!");
						}

					}
					else if (data.rawin("resendcode")) {
						local company = this.GetCompany(data.resendcode.tointeger() - 1); // a megjeleniteskor egyel nagyobbat mutat az OpenTTD, ezert kell csokkenteni egyel
						if (company) {
							company.SendCode();
						}
						else if (this.logmode)
							GSLog.Warning("Nem található ez a company ID: " + data.resendcode);
					}
					else if (data.rawin("sendscoresupdate")) {
						foreach (company in this.companies) {
							if (company.fggc_joined) { // elkuldjuk a frissitest, ha be van jelentkezve
								local ascores = company.GetScores();
								if (!GSAdmin.Send({updatescore = ascores, companyID = company.company_id, versenyID = company.fggc_versenyID}) && this.logmode)
									GSLog.Warning("[sendscoresupdate] Can not send company (" + company.company_id + ") scores to AdminPort!");
							}
						}
					}
					else {
						GSLog.Warning("Unknown ADMIN PORT command: " + data);
						if (!GSAdmin.Send({error = key}))
							GSLog.Error("Nem sikerult elkuldeni a francba! cID: " + data);
					}
				}
				break;


//		if (ev_type == GSEvent.ET_TOWN_FOUNDED) {

//		}
		/*
		if (ev_type == GSEvent.ET_STATION_FIRST_VEHICLE) {
			local fv = GSEventStationFirstVehicle.Convert(ev);
			local station_id = fv.GetStationID();
			local company_id = GSStation.GetOwner(station_id);
			local company = this.GetCompany(company_id);

			if (company == null)
				return;
			
			// az osszes allomast hozza kell adni a megfigyeleshez, mert elofordulhat, hogy kesobb mast is elfogad, vagy pont azt nem, mikor leraktuk az allomast
			company.AddStation(station_id);
		}
		*/
			case GSEvent.ET_COMPANY_NEW:
				local instance = GSEventCompanyNew.Convert(ev);
				local company_id = instance.GetCompanyID();
				if (company_id == GSCompany.COMPANY_INVALID) break;
				
				if (!this.versionController.IsSupportedVersion()) {
					// kuldunk uzenetet a vallalatoknak
					GSGoal.Question(this.message_counter++, company_id, GSText(GSText.STR_NOT_COMPATIBLE), GSGoal.QT_INFORMATION, GSGoal.BUTTON_CLOSE);
					break;
				}
				
				local compName = GSCompany.GetName(company_id);
				if (this.logmode) {
					GSLog.Info("");
					GSLog.Info("--------------------------------------------------------------------------------");
					GSLog.Info("[FecaGame::HandleEvents] New company: " + compName + ", id: " + company_id);
					GSLog.Info("--------------------------------------------------------------------------------");
					GSLog.Info("");
				}
				
				this.AddToCompanyNames(company_id, compName);

				if (this.game_started) // cask akkor adjuk hozza a vallalat nevet egybol, ha mar elindult a jatek
					this.AddNewCompanyToCompanyStoryPageInfo(company_id, compName);
				
				local company = FGCompany(company_id, this.cargo);
				company.setdelegate(this);

				if (this.fggc_active) {
					company.SendCode()
				}





				/*

				// --------------------------------------- probalgatasok...

				// MONITOR_ALL	MONITOR_ALL_DELIVERY		MONITOR_ALL_PICKUP	MONITOR_TOWN_DELIVERY		MONITOR_TOWN_PICKUP		MONITOR_INDUSTRY_DELIVERY		MONITOR_INDUSTRY_PICKUP

				// REFRESH_IMMEDIATELY	REFRESH_EVERY_MONTHS		REFRESH_EVERY_HALF_YEARS		REFRESH_EVERY_YEARS

				// LAST_MONTH_CURRENT		LAST_MONTH_ALL

				local townlist = GSTownList();
				local indelist = GSIndustryList_CargoAccepting(this.cargo.cargotype_coal);
				local inpilist = GSIndustryList_CargoProducing(this.cargo.cargotype_coal);

				local ind_de_ids = [];
				foreach (id, _ in indelist) {
					ind_de_ids.append(id);
				}

				local ind_pi_ids = [];
				foreach (id, _ in inpilist) {
					ind_pi_ids.append(id);
				}

				local tile = GSIndustry.GetLocation(ind_de_ids[0]);
				GSLog.Info("indulist x: " + GSMap.GetTileX(tile) + ", y: " + GSMap.GetTileY(tile));

				local monitor_type = FGCargoMonitor.REFRESH_EVERY_MONTHS;
				local month_type = 1;

				// probagoal 1
				local asgoal1 = FGBaseGoal();
				asgoal1.goal_type = FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE;
				asgoal1.goal_id = "proba1";
				local cargo_id1 = this.cargo.cargotype_coal;
				this.cargoMonitor.SubscribeToMonitor(company_id, cargo_id1, asgoal1.goal_id, monitor_type, FGCargoMonitor.MONITOR_ALL, null, month_type); // townlist[0]


				// probagoal 2
				local asgoal2 = FGBaseGoal();
				asgoal2.goal_type = FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE;
				asgoal2.goal_id = "proba2";
				local cargo_id2 = this.cargo.cargotype_coal;
				this.cargoMonitor.SubscribeToMonitor(company_id, cargo_id2, asgoal2.goal_id, monitor_type, FGCargoMonitor.MONITOR_ALL_DELIVERY, null, month_type);


				// probagoal 3
				local asgoal3 = FGBaseGoal();
				asgoal3.goal_type = FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE;
				asgoal3.goal_id = "proba3";
				local cargo_id3 = this.cargo.cargotype_coal;
				this.cargoMonitor.SubscribeToMonitor(company_id, cargo_id3, asgoal3.goal_id, monitor_type, FGCargoMonitor.MONITOR_ALL_PICKUP, null, month_type);


				// probagoal 4
				local asgoal4 = FGBaseGoal();
				asgoal4.goal_type = FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE;
				asgoal4.goal_id = "proba4";
				local cargo_id4 = this.cargo.cargotype_coal;
				this.cargoMonitor.SubscribeToMonitor(company_id, cargo_id4, asgoal4.goal_id, monitor_type, FGCargoMonitor.MONITOR_TOWN_DELIVERY, townlist[0], month_type);


				// probagoal 5
				local asgoal5 = FGBaseGoal();
				asgoal5.goal_type = FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE;
				asgoal5.goal_id = "proba5";
				local cargo_id5 = this.cargo.cargotype_coal;
				this.cargoMonitor.SubscribeToMonitor(company_id, cargo_id5, asgoal5.goal_id, monitor_type, FGCargoMonitor.MONITOR_TOWN_PICKUP, townlist[0], month_type);


				// probagoal 6
				local asgoal6 = FGBaseGoal();
				asgoal6.goal_type = FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE;
				asgoal6.goal_id = "proba6";
				local cargo_id6 = this.cargo.cargotype_coal;
				this.cargoMonitor.SubscribeToMonitor(company_id, cargo_id6, asgoal6.goal_id, monitor_type, FGCargoMonitor.MONITOR_INDUSTRY_DELIVERY, ind_de_ids[0], month_type);


				// probagoal 7
				local asgoal7 = FGBaseGoal();
				asgoal7.goal_type = FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE;
				asgoal7.goal_id = "proba7";
				local cargo_id7 = this.cargo.cargotype_coal;
				this.cargoMonitor.SubscribeToMonitor(company_id, cargo_id7, asgoal7.goal_id, monitor_type, FGCargoMonitor.MONITOR_INDUSTRY_PICKUP, ind_pi_ids[2], month_type);



				// -------------------------------------- vege a probalgatasnak

				 */
				
				
				// kiiratjuk, az udvozlo uzenetet es a korlatozasok listajat
				company.SetPossible(this.initialPossibilities);
				company.SendPossibleState(true);
				
				// itt most hozzaadom, mert a kovetkezo funkcioban szukseg van a listara
				this.companies.append(company);
				
				// kiiratjuk a jatek allasat a storybook-kal
				this.UpdateGameStateStoryBoards();
				
				if (this.testenabled)
					GSGoal.Question(0, company_id, GSText(GSText.STR_TEST), GSGoal.QT_INFORMATION, GSGoal.BUTTON_CLOSE);
				
				
				for (local i = 0; i < this.main_goals; i++) {
					local goal = this.NewRandomGoal(company_id, company.main_goals.len() == 0, null, i + 1, GF_MAIN);
					
					if (goal == null)
						continue;
					
					goal.goal_main = true;
					
					local baseSettingsTable = null;
					switch (goal.goal_type) {
						case FGBaseGoal.GoalTypes.GT_TRANSPORT:
							baseSettingsTable = this.baseTransportGoalSettings;
							break;
						case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
							break;
						case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
							baseSettingsTable = this.baseTownGrowthGoalSettings;
							break;
						case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
							break;
						case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
							break;
							// TODO [feladat tipus] tobbi goal-t is hozza kell adni
					}
					
					if (baseSettingsTable != null) {
						local afterMainGoalsMultiplier = 1.0;
						local afterSzozo = 0.0;
						
						switch (baseSettingsTable.afterMainGoalsMultiplier) {
							case 0:
								afterSzozo = 0.1;
								break;
							case 1:
								afterSzozo = 0.2;
								break;
							case 2:
								afterSzozo = 0.5;
								break;
							case 3:
								afterSzozo = 0.8;
								break;
							case 4:
								afterSzozo = 1.0;
								break;
						}
						
						if (baseSettingsTable.enableMultiplierForAfterMainGoals) {
							afterMainGoalsMultiplier += afterSzozo;
							
							switch (goal.goal_type) {
								case FGBaseGoal.GoalTypes.GT_TRANSPORT:
									local ammu = goal.amount * afterMainGoalsMultiplier;
									goal.amount = ammu.tointeger();
									break;
								case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
									break;
								case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
									break;
								case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
									break;
								case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
									break;
									// TODO [feladat tipus] tobbi goal-t is hozza kell adni
							}
						}
						
						if (baseSettingsTable.awardsForSuccesGoalMainMin > 0 && baseSettingsTable.awardsForSuccesGoalMainMax > 0 && this.award_enabled_by_possible)
							goal.goal_award = true; // kapunk vele jutalmat vagy nem
					}
					
					if (this.generalGoalSettings.allAtOnceTimeShowedMainGoals == 0 || (i < this.generalGoalSettings.allAtOnceTimeShowedMainGoals)) {
						this.ShowGoal(company, goal);
						if (goal.goal_id == null || !GSGoal.IsValidGoal(goal.goal_id)) {
							GSLog.Error("[FecaGame::HandleEvents] Can not create main goal, invalid goal_id: " + goal.goal_id);
						}
					}
					
					// es hozzaadjuk a feladatot
					company.AddGoalToMain(goal);
				}
				
				
				// hozzaadjuk az award es weak golokat, hogy ellenorizzek azokat is!
				for (local i = 0; i < this.awards.len(); i++) {
					local goal = this.awards[i];
					// letre kell hozni sajat goal instance-ot, mert ha nem, akkor ugyanugy megkapja mindenki a nyeremenyt
					switch (goal.goal_type) {
						case FGBaseGoal.GoalTypes.GT_TRANSPORT:
							company.AddGoalToAward(FGTransportGoal.Copy(goal));
							// bekapcsoljuk a monitorozast is
							if (this.generalGoalSettings.allAtOnceTimeShowedAwardGoals == 0 || (i < this.generalGoalSettings.allAtOnceTimeShowedAwardGoals)) {
								if (goal.goal_id == null || !GSGoal.IsValidGoal(goal.goal_id)) {
									GSLog.Error("[FecaGame::HandleEvents] Can not create award goal, invalid goal_id: " + goal.goal_id);
								}
							}
							break;
						case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
							break;
						case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
							company.AddGoalToAward(FGTownGrowthGoal.Copy(goal));
							break;
						case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
							break;
						case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
							break;
						// TODO [feladat tipus] tobbi goal-t is hozza kell adni
					}
					
				}
								
				for (local i = 0; i < this.weak_awards.len(); i++) {
					local goal = this.weak_awards[i];
					// letre kell hozni sajat goal instance-ot, mert ha nem, akkor ugyanugy megkapja mindenki a nyeremenyt
					switch (goal.goal_type) {
						case FGBaseGoal.GoalTypes.GT_TRANSPORT:
							company.AddGoalToWeakAward(FGTransportGoal.Copy(goal));
							// bekapcsoljuk a monitorozast is
							if (goal.goal_id == null) {
								GSLog.Error("[FecaGame::HandleEvents] Can not create weak_award goal, invalid goal_id: " + goal.goal_id);
							}
							break;
						case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
							break;
						case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
							company.AddGoalToWeakAward(FGTownGrowthGoal.Copy(goal));
							break;
						case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
							break;
						case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
							break;
						// TODO [feladat tipus] tobbi goal-t is hozza kell adni
					}
				}
				break;
				
			case GSEvent.ET_COMPANY_BANKRUPT:
				local instance = GSEventCompanyBankrupt.Convert(ev);
				local company_id = instance.GetCompanyID();
				if (company_id == GSCompany.COMPANY_INVALID) break;
				local company = this.GetCompany(company_id);
				if (company == null) break;
				
				local companyScores = company.GetScores();
				local compName = this.CompanyName(company_id);

				GSLog.Warning("bankrupt company 1");
				if (this.fggc_active && company.fggc_joined) {
					GSLog.Warning("bankrupt company 2");
					local datestring = this.GetCurrentDateTimeString();
					local currentyear = GSDate.GetYear(GSDate.GetCurrentDate());
					if (!GSAdmin.Send({removedcompany = company_id, versenyID = company.fggc_versenyID, year = currentyear, datetime = datestring, scores = companyScores}))
						GSLog.Error("BANKRUPT Nem sikerult elkuldeni a francba! cID: " + data);
				}

				this.RemoveCompany(company);
				this.AddBankruptedCompanyToCompanyStoryPageInfo(company_id, compName, companyScores);

				break;
				
				
			case GSEvent.ET_COMPANY_MERGER:
				local instance = GSEventCompanyMerger.Convert(ev);
				local company_id = instance.GetOldCompanyID();
				if (company_id == GSCompany.COMPANY_INVALID) break;
				local company = this.GetCompany(company_id);
				if (company == null) break;
				
				
				local new_company_id = instance.GetNewCompanyID();
				local newCompName = this.CompanyName(new_company_id);
				
				local companyScores = company.GetScores();
				local oldCompName = this.CompanyName(company_id);

				if (this.fggc_active && company.fggc_joined) {
					local datestring = this.GetCurrentDateTimeString();
					local currentyear = GSDate.GetYear(GSDate.GetCurrentDate());
					if (!GSAdmin.Send({removedcompany = company_id, versenyID = company.fggc_versenyID, year = currentyear, datetime = datestring, scores = companyScores}))
						GSLog.Error("MERGER Nem sikerult elkuldeni a francba! cID: " + data);
				}
				
				this.RemoveCompany(company);
				this.AddMergedCompanyToCompanyStoryPageInfo(company_id, oldCompName, new_company_id, newCompName, companyScores);

				break;
		}
		
	}
}

function FecaGame::SendCargoUpdate(company_id, goal_id, transported) {
	/*
	if (goal_id == "proba1") {
		GSLog.Warning("proba1: " + GSCompany.GetName(company_id) + " elszallitott " + transported + " valamit valahova!");
		return;
	}

	if (goal_id == "proba2") {
		GSLog.Warning("proba2: " + GSCompany.GetName(company_id) + " elszallitott " + transported + " valamit valahova!");
		return;
	}

	if (goal_id == "proba3") {
		GSLog.Warning("proba3: " + GSCompany.GetName(company_id) + " elszallitott " + transported + " valamit valahova!");
		return;
	}

	if (goal_id == "proba4") {
		GSLog.Warning("proba4: " + GSCompany.GetName(company_id) + " elszallitott " + transported + " valamit valahova!");
		return;
	}

	if (goal_id == "proba5") {
		GSLog.Warning("proba5: " + GSCompany.GetName(company_id) + " elszallitott " + transported + " valamit valahova!");
		return;
	}

	if (goal_id == "proba6") {
		GSLog.Warning("proba6: " + GSCompany.GetName(company_id) + " elszallitott " + transported + " valamit valahova!");
		return;
	}

	if (goal_id == "proba7") {
		GSLog.Warning("proba7: " + GSCompany.GetName(company_id) + " elszallitott " + transported + " valamit valahova!");
		return;
	}
	 */
	
	local company = this.GetCompany(company_id);
	if (company == null) {
		GSLog.Error("[FecaGame::SendCargoUpdate] Can not update transported cargo to company... COMPANY bankrupted???");
		return;
	}

	/*
	if (this.logmode) {
		GSLog.Info("    ****    ****    [FecaGame::SendCargoUpdate] " + GSCompany.GetName(company_id) + " update goal id (" + goal_id + "), transported: " + transported);
	}
	 */
	
	company.TransportedCargo(goal_id, transported);

	/*
	if (this.logmode) {
		GSLog.Info("    ****    ****    [FecaGame::SendCargoUpdate] end");
	}
	 */
}

function FecaGame::CheckMainGoalsExists() {
	if (this.allMainGoals.len() > 0) {
		for (local i = this.allMainGoals.len() - 1; i >= 0; i--) {
			local goal = this.allMainGoals[i];
			if (goal.goal_id != null) {
				local van = false;
				
				foreach (company in this.companies) {
					foreach (agoal in company.main_goals) {
						if (agoal.goal_id == goal.goal_id)
							van = true;
					}
				}
				
				if (!van) {
//					GSLog.Error("[FecaGame::CheckMainGoalsxists] Talaltam goalt, ami nem el!");
					if (GSGoal.IsValidGoal(goal.goal_id))
						GSGoal.Remove(goal.goal_id);
				}
			}
		}
	}
}

/*
 * @param company_id:		csak a main goal letrehozasa eseten van jelentosege, ha nem main goal, akkor lehet null az erteke
 * @param firstgoal:		bool, elso feladatkent hozzuk-e letre
 * @param prev_goal_type:	elozo feladat tipusa, ami az FGBaseGoal.GoalTypes
 * @param level:			hanyadik feladat jon
 * @param goal_fajta:		main vagy award vagy weak_award goalrol van szo
 *
 * if prev_goal_type == null, akkor foleg barmilyen goal johet
 */
function FecaGame::NewRandomGoal(company_id, firstgoal, prev_goal_type, level, goal_fajta) {
	local selectedGoal = null;
	
	local availableGoalsCount = 0;
	local availableGoals = [];

	local test_goal_fajta = null; // ertekek: null, GF_MAIN, GF_AWARD, GF_WEAK
	local company = this.GetCompany(company_id); // lehet ez null is!!!

	switch (goal_fajta) {
		case GF_MAIN:
			if (company != null)
				availableGoalsCount = this.main_goals - company.main_goals.len();

			availableGoals.append(FGBaseGoal.GoalTypes.GT_TRANSPORT);
			availableGoals.append(FGBaseGoal.GoalTypes.GT_TOWNGROWTH);
			break;
		case GF_AWARD:
			availableGoalsCount = this.award_goals - this.awards.len();

			availableGoals.append(FGBaseGoal.GoalTypes.GT_TRANSPORT);
			break;
		case GF_WEAK:
			availableGoalsCount = this.weak_award_goals - this.weak_awards.len();

			availableGoals.append(FGBaseGoal.GoalTypes.GT_TRANSPORT);
			break;
	}
	
	if (this.logmode && goal_fajta == test_goal_fajta) {
		GSLog.Info("...............................");
		GSLog.Info("");
	}
	// ekkor vagy az elozo feladat tipus ervenytelen (ami elvileg hulyeseg :D), vagy null, tehat most barmi lehet, ami elerheto
	
	if (availableGoals.len() == 1 && FGBaseGoal.IsValidGoalType(availableGoals[0])) {
		// ha csak egy goal tipus lehet, es ervenyes, akkor mehet az
		selectedGoal = availableGoals[0];
		if (this.logmode && goal_fajta == test_goal_fajta)
			GSLog.Info("1: csak ez az egyfajta feladat tipus letezik");
	}
	else {
		if (this.logmode && goal_fajta == test_goal_fajta)
			GSLog.Info("2: tobb feladat tipus letezik");
		
		local extendedAvailableGoals = [];
		extendedAvailableGoals.extend(availableGoals);
		
		// eltavolitjuk azokat a feladatokat, amik ki vannak kapcsolva
		for (local i = 0; i < extendedAvailableGoals.len(); i++) {
			local baseSettingsTable = FGBaseGoal.BaseSettinsgFromGoalType(extendedAvailableGoals[i], this);
			if (baseSettingsTable == null) {
				if (this.logmode && goal_fajta == test_goal_fajta)
					GSLog.Info("2.11: valamiert nincs ilyen baseSettingsFile: " + extendedAvailableGoals[i]);
				extendedAvailableGoals.remove(i);
				i--;
				continue;
			}
			
			local minGoal = 0;
			
			switch (goal_fajta) {
				case GF_MAIN:
					minGoal = baseSettingsTable.minMainGoal;
					break;
				case GF_AWARD:
					minGoal = baseSettingsTable.minAwardGoal;
					break;
				case GF_WEAK:
					minGoal = baseSettingsTable.minWeakAwardGoal;
					break;
			}
			
			if (minGoal == 0) {
				if (this.logmode && goal_fajta == test_goal_fajta)
					GSLog.Info("2.12: elvileg ez a fajta goal nem is lehet, ezert kivesszuk: " + extendedAvailableGoals[i]);
				extendedAvailableGoals.remove(i);
				i--;
				continue;
			}
		}
		
		if (extendedAvailableGoals.len() == 0) {
			// ide nem kell semmi, mert ugyis adunk akkor veletlen goal-t
		}
		if (extendedAvailableGoals.len() == 1 && FGBaseGoal.IsValidGoalType(extendedAvailableGoals[0])) {
			selectedGoal = extendedAvailableGoals[0];
			if (this.logmode && goal_fajta == test_goal_fajta)
				GSLog.Info("2.13: csak ez az egyfajta feladat tipus letezik: " + selectedGoal);
		}
		else {
			
			for (local i = 0; i < extendedAvailableGoals.len(); i++) {
				if (!FGBaseGoal.IsAvailableGoalOnLevel(this, level, extendedAvailableGoals[i])) {
					if (this.logmode && goal_fajta == test_goal_fajta)
						GSLog.Info("2.2: no available type: " + extendedAvailableGoals[i]);
					extendedAvailableGoals.remove(i);
					i--;
				}
			}
			
			if (this.logmode && goal_fajta == test_goal_fajta) {
				for (local i = 0; i < extendedAvailableGoals.len(); i++)
					GSLog.Info("2.3: maradt type: " + extendedAvailableGoals[i]);
			}
			
			// ha meg mindig nagyobb a lista, akkor most ellenorizzuk le
			// hogy van-e ervenyes feladat a legutoljara teljesitett feladatbol, es akkor azt adjuk
			if (extendedAvailableGoals.len() > 1 && prev_goal_type != null && FGBaseGoal.IsValidGoalType(prev_goal_type)) {
				if (FGBaseGoal.IsAvailableGoalOnLevel(this, level, prev_goal_type)) {
					selectedGoal = prev_goal_type;
					if (this.logmode && goal_fajta == test_goal_fajta)
						GSLog.Info("2.4: meg mindig tobb volt, viszont elerheto az elozo feladat tipus is: " + prev_goal_type);
				}
			}
			
			// ha meg mindig nagyobb, akkor megnezzuk, hogy melyik goal melyik szinten elerheto,
			// es amelyik csak ezen a szinten erheto el, akkor azt adjuk be
			if (selectedGoal == null && extendedAvailableGoals.len() > 1) {
				// azert csinaltam igy, hogy ne 'seruljon' az eredeti eztended goals
				// tehat leellenorizzuk, hogy van-e tobb olyan feladat, ami csak ezen a szinten erheto el
				// ha csak egy marad, akkor biztosan azt a feladatot adjuk
				local masikExtendedGoals = [];
				masikExtendedGoals.extend(extendedAvailableGoals);
				for (local i = 0; i < masikExtendedGoals.len(); i++) {
					if (FGBaseGoal.IsAvailableGoalOnLevel(this, level + 1, masikExtendedGoals[i])) {
						if (this.logmode && goal_fajta == test_goal_fajta)
							GSLog.Info("2.51: elerheto a kovetkezo szinten is a type: " + masikExtendedGoals[i]);
						masikExtendedGoals.remove(i);
						i--;
					}
				}
				
				if (masikExtendedGoals.len() == 1 && FGBaseGoal.IsValidGoalType(masikExtendedGoals[0])) {
					selectedGoal = masikExtendedGoals[0];
					if (this.logmode && goal_fajta == test_goal_fajta)
						GSLog.Info("2.52: csak egy elerheto feladat maradt ezen a szinten, ezert ot valasztjuk: " + selectedGoal);
				}
				else if (masikExtendedGoals.len() > 1) {
					// ebben az esetben pedig fogjuk, es kitoroljuk azokat, amelyik masik szinten is megtalalhatoak voltak
					// igy egyszerubb, mint ujraellenorizni az egeszet
					extendedAvailableGoals.clear();
					extendedAvailableGoals.extend(masikExtendedGoals);
					if (this.logmode && goal_fajta == test_goal_fajta)
						GSLog.Info("2.53: tobb elerheto feladat maradt ezen a szinten, ezert csak ezeket hagyjuk meg");
				}
			}
			
			if (selectedGoal == null) {
				if (this.logmode && goal_fajta == test_goal_fajta)
					GSLog.Info("3: meg mindig nincs feladatunk");
				if (extendedAvailableGoals.len() == 1 && FGBaseGoal.IsValidGoalType(extendedAvailableGoals[0])) {
					selectedGoal = extendedAvailableGoals[0];
					if (this.logmode && goal_fajta == test_goal_fajta)
						GSLog.Info("3.1: csak egy feladatunk maradt elerheto: " + selectedGoal);
				}
//				// csak akkor keresunk goal tipust, ha a listaban meg nincs elegendo. ha mar van, akkor veletlenul valasztunk
				else if (availableGoalsCount > 0 && extendedAvailableGoals.len() > 1) {
					if (this.logmode && goal_fajta == test_goal_fajta) {
						GSLog.Info("3.2: meg mindig tobb feladatunk maradt elerheto");
						GSLog.Info("---");
					}

					// beteszem egy while loopba, hogy probalja meg kiszedni sorban a goalokat.
					// az utolso szinten levo goalokat mindig beteszem egy tombbe, hogy azok maradtak
					local megnincsilyenfeladat = [];
					local korokszama = 0;
					while (megnincsilyenfeladat.len() == 0) {
					// --- for begin
						for (local i = 0; i < extendedAvailableGoals.len(); i++) {
							local baseSettingsTable = FGBaseGoal.BaseSettinsgFromGoalType(extendedAvailableGoals[i], this);
							if (baseSettingsTable == null) {
								if (this.logmode && goal_fajta == test_goal_fajta)
									GSLog.Info("3.21: valamiert nincs ilyen baseSettingsFile: " + extendedAvailableGoals[i]);
								extendedAvailableGoals.remove(i);
								i--;
								continue;
							}

							local minGoal = 0;
							local maxGoal = 0;
							local table = null;
							
							switch (goal_fajta) {
								case GF_MAIN:
									minGoal = baseSettingsTable.minMainGoal;
									maxGoal = baseSettingsTable.maxMainGoal;
									if (company != null)
									table = company.main_goals;
									break;
								case GF_AWARD:
									minGoal = baseSettingsTable.minAwardGoal;
									maxGoal = baseSettingsTable.maxAwardGoal;
									table = this.awards;
									break;
								case GF_WEAK:
									minGoal = baseSettingsTable.minWeakAwardGoal;
									maxGoal = baseSettingsTable.maxWeakAwardGoal;
									table = this.weak_awards;
									break;
							}
							
							local createdGoalCount = 0;
							if (table != null) {
								foreach (goal in table) {
									if (goal.goal_type == extendedAvailableGoals[i])
										createdGoalCount++;
								}
							}

							if (createdGoalCount == korokszama)
								megnincsilyenfeladat.append(extendedAvailableGoals[i]);
							
							if (this.logmode && goal_fajta == test_goal_fajta) {
								GSLog.Info("minGoal: " + minGoal + ", forType: " + extendedAvailableGoals[i]);
								GSLog.Info("maxGoal: " + maxGoal + ", forType: " + extendedAvailableGoals[i]);
								GSLog.Info("available goals count: " + availableGoalsCount);
								GSLog.Info("created goals count: " + createdGoalCount);
							}
							// azert csinalom igy, hogy tudjam logolni, hogy mi tortenik
							local kellEltavolitani = false;
							if (minGoal == 0) {
								if (this.logmode && goal_fajta == test_goal_fajta)
									GSLog.Info("3.22: nem is lehet ilyen feladat tipus, min goal == 0: " + extendedAvailableGoals[i]);
								kellEltavolitani = true;
							}
							else if (maxGoal < minGoal) {
								if (this.logmode && goal_fajta == test_goal_fajta)
									GSLog.Info("3.23: kisebb a max, mint a min, tehat barmennyi lehet: " + extendedAvailableGoals[i]);
								kellEltavolitani = true;
							}
							else if ((maxGoal - createdGoalCount) <= korokszama) { // igy, hogy a korok szamat nezem, eleinte nem fog olyanokat eltavolitani, amikbol meg nincs letrehozva feladat. majd olyat tavolit el, amibol mar van egy, stb...
								if (this.logmode && goal_fajta == test_goal_fajta)
									GSLog.Info("3.24: csak egy vagy kevesebb goal elerheto, ezert kivesszuk, hogy a tobb goalos feladat teljesuljon: " + extendedAvailableGoals[i]);
								kellEltavolitani = true;
							}
							
							if (kellEltavolitani) {
								if (extendedAvailableGoals.len() == 1 && megnincsilyenfeladat.len() == 0) // a biztonsag kedveert, hogy valamikor biztosan kilepjen a loopbol
									megnincsilyenfeladat.append(extendedAvailableGoals[i]);
								extendedAvailableGoals.remove(i);
								i--;
								continue;
							}
						}

						korokszama++;
					// --- for end
					}

					// ha netan kiszedtuk volna az osszes feladatot, akkor olyan feladatot adunk, ami meg nem volt.
					if (extendedAvailableGoals.len() == 0) {
						if (this.logmode && goal_fajta == test_goal_fajta)
							GSLog.Info("3.3: betoltunk olyan feladatokat, amik meg nem voltak, mennyiseg: " + megnincsilyenfeladat.len());
						extendedAvailableGoals.extend(megnincsilyenfeladat);
					}

					if (this.logmode && goal_fajta == test_goal_fajta) {
						GSLog.Info("---");
					}
					if (extendedAvailableGoals.len() == 1 && FGBaseGoal.IsValidGoalType(extendedAvailableGoals[0])) {
						selectedGoal = extendedAvailableGoals[0];
						if (this.logmode && goal_fajta == test_goal_fajta)
							GSLog.Info("3.4: megint csak egy feladatunk maradt: " + extendedAvailableGoals[0]);
					}
					else if (extendedAvailableGoals.len() > 1) {
						if (selectedGoal == null && extendedAvailableGoals.len() > 0) {
							// ok, akkor most random csinalunk feladatot az elerheto feladatokbol
							selectedGoal = extendedAvailableGoals[GSBase.RandRange(extendedAvailableGoals.len())];
							if (this.logmode && goal_fajta == test_goal_fajta)
								GSLog.Info("3.5: meg mindig tobb feladatunk maradt, ezert ezt valasztottuk: " + selectedGoal);
						}
					}
				}
			}
		}
	}
	
	if (selectedGoal == null || !FGBaseGoal.IsValidGoalType(selectedGoal)) {
		if (availableGoals.len() == 1)
			selectedGoal = availableGoals[0];
		else
			selectedGoal = availableGoals[GSBase.RandRange(availableGoals.len())];
		
		if (this.logmode && goal_fajta == test_goal_fajta)
			GSLog.Info("4: nem volt ertelmes feladat letrehozhato, igy random letrehoztunk egyet: " + selectedGoal);
	}
	
	if (!FGBaseGoal.IsValidGoalType(selectedGoal)) {
		GSLog.Error("[FecaGame::NewRandomGoal] Can not create new goal with settings, invalid selectedGoal");
		return null;
	}
	
	if (this.logmode && goal_fajta == test_goal_fajta) {
		GSLog.Info("");
		GSLog.Info("...............................");
	}
	
	switch (selectedGoal) {
		case FGBaseGoal.GoalTypes.GT_TRANSPORT:
			if (this.transportGoalSettings.onlyMainGoalLevels || this.transportGoalSettings.enableAllCargoTypes)
				level = 0;
			return this.NewRandomTransportGoalWithSettings(firstgoal, level);
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
			return this.NewRandomTownGrowthGoalWithSettings(level);
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
			break;
		// TODO [feladat tipus] tobbi goal-t is hozza kell adni
	}
	
	GSLog.Error("[FecaGame::NewRandomGoal] Can not create new goal with settings");
	
	return null;
}

function FecaGame::NewRandomTransportGoalWithSettings(firstgoal, level) {
	local goal = FGTransportGoal(null, this);
	goal.SetCargo(this.cargo);
	goal.goal_award = true;
	goal.goal_type = FGBaseGoal.GoalTypes.GT_TRANSPORT;
	goal.SetGoalSettings(this.transportGoalSettings);
	
	local month_difficulty_enabled = false;
	local randomMonth = 0;
	local month_difficulty = 0;
	
	if (this.baseTransportGoalSettings.monthWithDifficulty) {
		if (this.baseTransportGoalSettings.minMonthDifficulty > 0 && this.baseTransportGoalSettings.maxMonthDifficulty > 0)
			month_difficulty_enabled = true;
		if (month_difficulty_enabled)
			month_difficulty = GSBase.RandRange(this.baseTransportGoalSettings.maxMonthDifficulty - this.baseTransportGoalSettings.minMonthDifficulty + 1) + this.baseTransportGoalSettings.minMonthDifficulty;
	}
	else {
		if (this.baseTransportGoalSettings.minMonthNumber > this.baseTransportGoalSettings.maxMonthNumber)
			this.baseTransportGoalSettings.maxMonthNumber = this.baseTransportGoalSettings.minMonthNumber;
		
		if (this.baseTransportGoalSettings.minMonthNumber == this.baseTransportGoalSettings.maxMonthNumber)
			randomMonth = this.baseTransportGoalSettings.minMonthNumber;
		else
			randomMonth = GSBase.RandRange(this.baseTransportGoalSettings.maxMonthNumber - this.baseTransportGoalSettings.minMonthNumber + 1) + this.baseTransportGoalSettings.minMonthNumber;
	}
		
	local cargo_difficulty = GSBase.RandRange(this.baseTransportGoalSettings.maxDifficulty - this.baseTransportGoalSettings.minDifficulty + 1) + this.baseTransportGoalSettings.minDifficulty;
	
	local cargo_id = goal.GetRandomTransportGoalType(firstgoal, level);
	
	local month = 0;
	if (month_difficulty_enabled)
		month = goal.GetRandomMonthsWithDifficulty(month_difficulty);
	else
		month = randomMonth;

	local amount = goal.GetTransportGoalValueWithDifficultyAndCargoID(cargo_difficulty, cargo_id);
	
	if (testenabled) {
		local sa = amount / testoszto;
		amount = sa.tointeger();
	}
	
	goal.cargo_id = cargo_id;
	goal.amount = amount;
	goal.month = month;
	
	// az award szamitasahoz szukseges difficulty 0 es 9 kozti szam, a cargo_diff es a month diff is 0 es 4 kozotti szam
	local monthdiff = 0;
	
	if (this.baseTransportGoalSettings.monthWithDifficulty) {
		if (randomMonth < 4)
			monthdiff = 5;
		else if (randomMonth < 6)
			monthdiff = 4;
		else if (randomMonth < 8)
			monthdiff = 3;
		else if (randomMonth < 10)
			monthdiff = 2;
		else if (randomMonth <= 12)
			monthdiff = 1;
//		else
//			monthdiff = 0; // ez nem kell, mivel ez az alap
	}
	else {
		if (month_difficulty >= 0)
			monthdiff = month_difficulty; // nehogy -1-et adjunk hozza...
	}
	
	goal.goal_difficulty = (cargo_difficulty + monthdiff);
	
	return goal;
}

// TODO: a levelt nem veszi a script figyelembe jelenleg...
// pedig jo lenne, hogy ha figyelembe venne, ha korabban erre a szintre letre lett hozva egy jatek
function FecaGame::NewRandomTownGrowthGoalWithSettings(level) {
	local townList = GSTownList();
	local small_towns = [];
	
	local minTownPop;
	local maxTownPop;
	
	switch (this.townGrowthGoalSettings.population) {
		case 0:
			minTownPop = 0;
			maxTownPop = 200;
			break;
		case 1:
			minTownPop = 201;
			maxTownPop = 800;
			break;
		case 2:
			minTownPop = 801;
			maxTownPop = 1500;
			break;
		case 3:
			minTownPop = 1501;
			maxTownPop = 2000;
			break;
		case 4:
			minTownPop = 2001;
			maxTownPop = 2500;
			break;
		case 5:
			minTownPop = this.townGrowthGoalSettings.customPopulationMin;
			maxTownPop = this.townGrowthGoalSettings.customPopulationMax;
			break;
		default:
			minTownPop = 0;
			maxTownPop = 200;
			break;
	}
	
	if (maxTownPop - minTownPop < 100) {
		if (minTownPop < 100)
			maxTownPop = minTownPop + 200;
		else
			maxTownPop = minTownPop + 100;
	}
	
	local lepeskoz = 100;
	
	while (small_towns.len() == 0) {
		foreach (town, _ in townList) {
			// varost nem lehet kiosztani
			if (!GSTown.IsCity(town)) {
				local cont = false;
				// leellenorizzuk, hogy nem osztottuk-e mar ki ezt a feladatot.
				foreach (atown in this.engagedTownGrowth) {
					if (atown == town)
						cont = true;
					
				}
				
				if (!cont) {
					local population = GSTown.GetPopulation(town);
					if (population >= minTownPop && population <= maxTownPop)
						small_towns.append(town);
				}
			}
		}
		
		minTownPop -= lepeskoz;
		maxTownPop -= lepeskoz;
		
		if (minTownPop < 0) {
			lepeskoz = 0 - lepeskoz; // azert van ez, hogy megforduljon a szamlalo, mert nem talalt ilyen kis varost
			minTownPop -= (lepeskoz * 2);
			maxTownPop -= (lepeskoz * 2);
		}
		else if (minTownPop > 2500) {
			// ha meg igy sem lenne megfelelo varos, akkor az van, hogy tul sokan jatszanak tul kicsi terkepen... kapjak be... majd rajonnek...
			GSGoal.Question(0, GSCompany.COMPANY_INVALID, GSText(GSText.STR_ERROR_TOWN_REQUIRED_MINIMUM), GSGoal.QT_ERROR, GSGoal.BUTTON_CLOSE);
		}
	}
	
	if (small_towns.len() == 0) {
		GSLog.Warning("[FecaGame::NewRandomTownGrowthGoalWithSettings] Can not create Town Growth Goal");
		return null;
	}
	
	local town_id = small_towns[GSBase.RandRange(small_towns.len())];
	this.engagedTownGrowth.append(town_id);
	
	local tPop = GSTown.GetPopulation(town_id);
	
	local townDifficulty;
	local customDifficulty = false;
	local goal_population = 0;

	if (this.baseTownGrowthGoalSettings.minDifficulty == FGTownGrowthGoal.TPS_CUSTOM_VALUE || this.baseTownGrowthGoalSettings.maxDifficulty == FGTownGrowthGoal.TPS_CUSTOM_VALUE) {
		customDifficulty = true;
		townDifficulty = FGTownGrowthGoal.TS_CUSTOM;
		goal_population = this.townGrowthGoalSettings.custom_goal_population;
	}
	else {
		if (this.baseTownGrowthGoalSettings.minDifficulty >= this.baseTownGrowthGoalSettings.maxDifficulty)
			townDifficulty = this.baseTownGrowthGoalSettings.minDifficulty;
		else
			townDifficulty = GSBase.RandRange(this.baseTownGrowthGoalSettings.maxDifficulty - this.baseTownGrowthGoalSettings.minDifficulty + 1) + this.baseTownGrowthGoalSettings.minDifficulty;
		
		// azert +1, mert 0-tol kezdodik es a 0 az invalid
		townDifficulty++;
		local kesz = false;
		
		while (!kesz) {
			if ((FGTownGrowthGoal.GetPopulationFromType(townDifficulty) - tPop) >= 500) {
				kesz = true;
			}
			else {
				townDifficulty++;
			
				if (townDifficulty > 4)
					break;
			}
		}

		goal_population = FGTownGrowthGoal.GetPopulationFromType(townDifficulty);
	}
	
	local goal = FGTownGrowthGoal(null, this, townDifficulty, town_id);

	if (customDifficulty) {
		// beallitjuk az elerni kivant erteket
		goal.custom_goal_population = goal_population;
	}

	// 0 - 4 koze kell esnie...
	// azert 5-tel szorzom, mert a szazalekhoz 100-al kellene 1-100-ig kapmamk eredmenyt
	// aztan el kellene oszztani 20-al, hogy 1-5 koze essen
	// azert 4 - ..., mert 5 - ... kellene, de a vegen le kellene vonni 1-et, hogy a 0 es 4 koze essen, igy megsporoljuk...
	local goal_difficulty = 4 - ((tPop * 5 / goal_population).tointeger()); // igy meg tudjuk alapbol hany szazalekrol indul, es szazalekosan adjuk a nehezseget

	goal.goal_award = false; // alapbol nem kapunk erte dijat
//	goal.goal_type = FGBaseGoal.GoalTypes.GT_TOWNGROWTH; // felesleges, mert a constructorban benne van
	goal.SetGoalSettings(this.townGrowthGoalSettings);

	// difficultyGoal azt jelenti, hogy a varos novekedesehez szallitani is kell.
	// azert + 5, mert a difficulty 10 (0-9) lehet, es eleg nehez egy ilyen feladat, igy minimum 5-os szintu lesz igy egy ilyen feladat
	if (this.townGrowthGoalSettings.difficultyGoal)
		goal_difficulty += 5;

	goal.goal_difficulty = goal_difficulty;
	
	return goal;
}
// TODO [feladat tipus] tobbi goal-t is hozza kell adni

// - # Goal functions
function FecaGame::CheckGoalsCompleted () {
	foreach (company in this.companies) {
		local company_id = company.company_id;
		if (company_id == GSCompany.COMPANY_INVALID) continue;
		
		// itt is frissitjuk a goalokat, mielott lekerdezzuk, hogy nyert-e valaki
//		company.UpdateTowns();
		
		// ez megiscsak kell, mert nem csak transport feladatunk van, hanem mas is, es azoknak az ellenorzese ott folyik
		company.UpdateGoals();
		
		/*
		 * tehat a feladatok ellenorzesenel ha vegzett az osszes feladattal valaki es nem nincs idore jatszas,
		 * vagy ha ez utobbi megis, akkor ha sosem fogy ek a fo goal, akkor nincs meg vege
		 */

		/* TODO: innen athelyeztem mashova a befejezes ellenorzeset, meg a CheckPlusTransportedCargos() funkciot is
		 sokkal ertelmesebb helyen van a CompletedGoal-nal...
		local endGame = false;
		if (company.CountOfMainGoals() <= 0 && (this.generalGoalSettings.gameTime == 0 || !this.generalGoalSettings.neverRunOutMain))
			endGame = true;
		
		if (endGame) {
			this.EndGame(company);
			break;
		}


		// itt ellenorzom le, hogy maradt-e bent a vallalatnak cargo-ja, ami mar nem kellett az elozo feladathoz,
		// mert mar tul sokat szallitott be, igy kellhet masik feladathoz
		company.CheckPlusTransportedCargos();
		*/


		/* a CheckPlusTransportedCargos() funkcioban levo while gondoskodik arrol, hogy minden feladatot lekerdezzen,
		 ott van a funkcio elott a pontosabb leiras errol

		// addig ellenorizzuk, amig van a listan cucc
		while (company.IsAvailablePlusCargos()) {
			company.CheckPlusTransportedCargos();
		}
		company.RemoveAllPlusCargos();
		 */
	}
}

function FecaGame::EndGame(company) {
	if (this.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0)) {
		local allcompanies = [];
		foreach (acompany in this.companies)
			allcompanies.append(acompany);
		
		local sortedcompanies = [];
		
		while (allcompanies.len() > 0) {
			local acomp = null;
			local bestpont = -1;
			local index = -1;
			
			for (local i = 0; i < allcompanies.len(); i++) {
				local acompany = allcompanies[i];
				local comppoint = acompany.GetScores();
				if (comppoint > bestpont) {
					bestpont = comppoint;
					acomp = acompany;
					index = i;
				}
			}
			
			if (acomp != null) {
				sortedcompanies.append(acomp);
				allcompanies.remove(index);
			}
		}
		
		
		foreach (acompany in this.companies) {
			local scoresPageID = GSStoryPage.New(acompany.company_id, GSText(GSText.STR_GAME_OVER_TITLE));
			if (scoresPageID == GSStoryPage.STORY_PAGE_INVALID || scoresPageID = null) {
				GSLog.Error("[FGCompany::EndGame] Nem sikerült a játék vége oldal létrehozása!");
			}
			else {
				GSStoryPage.NewElement(scoresPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_NEWLINE));
				if (company != null)
					GSStoryPage.NewElement(scoresPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_GOAL_WIN_COMPANY_COMPLETED_GAME, company.company_id));
				else
					GSStoryPage.NewElement(scoresPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_GOAL_WIN_WITHOUT_COMPANY_COMPLATED));
				
				
				if (sortedcompanies.len() > 0)
					GSStoryPage.NewElement(scoresPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_GOAL_WIN_FIRST_COMPANY, sortedcompanies[0].company_id, sortedcompanies[0].GetScores()));
				
				if (sortedcompanies.len() > 1)
					GSStoryPage.NewElement(scoresPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_GOAL_WIN_SECOND_COMPANY, sortedcompanies[1].company_id, sortedcompanies[1].GetScores()));
				
				if (sortedcompanies.len() > 2)
					GSStoryPage.NewElement(scoresPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_GOAL_WIN_THIRD_COMPANY, sortedcompanies[2].company_id, sortedcompanies[2].GetScores()));
				
				if (sortedcompanies.len() > 3)
					for (local i = 3; i < sortedcompanies.len(); i++)
						GSStoryPage.NewElement(scoresPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_GOAL_WIN_OTHER_COMPANY, i + 1, sortedcompanies[i].company_id, sortedcompanies[i].GetScores()));
				
				
				GSStoryPage.NewElement(scoresPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_GOAL_WIN_END));
				
				// eloszor eltavolitjuk a regi nem kello oldalakat
				if (acompany.companysGoalStatesStoryPageID != null && acompany.companysGoalStatesStoryPageID != GSStoryPage.STORY_PAGE_INVALID)
					GSStoryPage.Remove(acompany.companysGoalStatesStoryPageID);
				
				if (acompany.gameStateSoryPageID != null && acompany.gameStateSoryPageID != GSStoryPage.STORY_PAGE_INVALID)
					GSStoryPage.Remove(acompany.gameStateSoryPageID);
				
				if (acompany.possibleStoryPageID != null && acompany.possibleStoryPageID != GSStoryPage.STORY_PAGE_INVALID)
					GSStoryPage.Remove(acompany.possibleStoryPageID);
				
				// ezutan jelenitjuk meg a vege oldalt, mert ha ey hamarabb van, mint az elozo, akkor az elso oldal jelenik meg.
				GSStoryPage.Show(scoresPageID);
			}
		}

		if (company != null)
			this.AddEndInfoToCompanyStoryPageInfo(company.company_id);
	}
	else {
		local firstComp = null;
		local secondComp = null;
		local thirdComp = null;
		
		foreach (acompany in this.companies) {
			if (firstComp == null || firstComp.GetScores() < acompany.GetScores())
				firstComp = acompany;
		}
		
		foreach (acompany in this.companies) {
			if (acompany == firstComp)
				continue;
			
			if (secondComp == null || secondComp.GetScores() < acompany.GetScores())
				secondComp = acompany;
		}
		
		foreach (acompany in this.companies) {
			if (acompany == firstComp || acompany == secondComp)
				continue;
			
			if (thirdComp == null || thirdComp.GetScores() < acompany.GetScores())
				thirdComp = acompany;
		}
		
		if (firstComp == GSCompany.COMPANY_INVALID)
			firstComp = null;
		
		if (secondComp == GSCompany.COMPANY_INVALID)
			secondComp = null;
		
		if (thirdComp == GSCompany.COMPANY_INVALID)
			thirdComp = null;
		
		// most osszerakjuk a szoveget
		if (firstComp == null)
			return; // nincs gyoztes???
		
		GSGoal.CloseQuestion(this.message_goal_information);
		
		this.message_goal_information = this.message_counter++;
		
		foreach (acompany in this.companies) {
			local text = null;
			local companyText = null;
			local et = GSText(GSText.STR_EMPTY);
			
			if (firstComp == acompany || secondComp == acompany || thirdComp == acompany)
				companyText = et;
			else
				companyText = GSText(GSText.STR_GOAL_SCORES_COMPANY, acompany.GetScores());
			
			local secondText = null;
			if (secondComp == null)
				secondText = GSText(GSText.STR_GOAL_SCORES_COMPANY_EMPTY, et, et);
			else
				secondText = GSText(GSText.STR_GOAL_SCORES_TWO, secondComp.company_id, secondComp.GetScores());
			
			local thirdText = null;
			if (thirdComp == null)
				thirdText = GSText(GSText.STR_GOAL_SCORES_COMPANY_EMPTY, et, et);
			else
				thirdText = GSText(GSText.STR_GOAL_SCORES_THREE, thirdComp.company_id, thirdComp.GetScores());
			
			if (company != null)
				text = GSText(GSText.STR_GOAL_WIN_COMPANY, company.company_id, firstComp.company_id, firstComp.GetScores(), secondText, thirdText, companyText)
			else
				text = GSText(GSText.STR_GOAL_WIN_WITHOUT_COMPANY, firstComp.company_id, firstComp.GetScores(), secondText, thirdText, companyText)
			
			GSGoal.Question(this.message_goal_information, acompany.company_id, text, GSGoal.QT_INFORMATION, GSGoal.BUTTON_CLOSE);
		}
	}


	GSGame.Pause();
	
	if (this.logmode && company != null)
		GSLog.Warning("[FecaGame::EndGame] Vege: " + GSCompany.GetName(company.company_id));
	
	this.game_ended = true;
	
	// eltavolitjuk az osszes vallalat osszes maradek goaljat is, hogy ne latszodjon
	foreach (goal in this.awards) {
		if (GSGoal.IsValidGoal(goal.goal_id))
			GSGoal.Remove(goal.goal_id);
	}
	
	// eltavolitjuk az osszes feladatot, valamint a mellek feladatok allasa es a jatek allasa oldalakat bezarjuk
	foreach (acompany in this.companies) {
		// score update
		if (this.fggc_active && acompany.fggc_joined) {
			if (!GSAdmin.Send({companyID = acompany.company_id, updatescore = acompany.GetScores(), versenyID = acompany.fggc_versenyID}) && this.logmode)
				GSLog.Error("Nem sikerult elkuldeni a scoreupdate-et! cID: " + acompany.company_id);
		}

		acompany.RemoveAllGoals();
	}
	
	// az osszes ki nem kapcsolt monitorozast leallitjuk
	this.cargoMonitor.UnSubscribeAllFromMonitor();
	GSCargoMonitor.StopAllMonitoring();

	if (this.fggc_active) {
		local currentyear = GSDate.GetYear(GSDate.GetCurrentDate());
		if (!GSAdmin.Send({endgame = "endgame", year = currentyear}) && this.logmode)
			GSLog.Error("Nem sikerult elkuldeni az endgame-et!");
	}

	this.fggc_active = false;
}

function FecaGame::CompanyCompletedGoal(company, goal) {
	if (goal == null || company = null)
		return;
	
	if (this.logmode) {
		local text = "";
		switch (goal.goal_type) {
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				text = goal.amount + " " + GSCargo.GetCargoLabel(goal.cargo_id);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				text = "population of town: " + GSTown.GetName(goal.town_id) + " is growth to " + FGTownGrowthGoal.GetPopulation(goal);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}
		local gtype = "";
		
		if (goal.goal_main)
			gtype = "MAIN";
		else if (goal.goal_weak)
			gtype = "WEAK";
		else
			gtype = "AWARD";
		
		GSLog.Warning("[FecaGame::CompanyCompletedGoal] " + GSCompany.GetName(company.company_id) + " completed " + gtype + " goal: " + text);
	}
	
	if (goal == null) {
		GSLog.Error("[FecaGame::CompanyCompletedGoal] goal is null");
		return;
	}
	
	local baseSettingsTable = FGBaseGoal.BaseSettinsgFromGoalType(goal.goal_type, this);
	
	if (baseSettingsTable == null) {
		GSLog.Warning("[FecaGame::CompanyCompletedGoal] " + GSCompany.GetName(company.company_id) + ", invalid baseSettingsTable.");
		return;
	}
	
	if (goal.goal_main) {
		// eltvaolitom a monitorozast, meg torlom a goal-t.
		company.RemoveGoal(goal);
		this.cargoMonitor.UnSubscribeFromMonitor(company.company_id, goal.goal_id);
		
		// ellenorizzuk, hogy elsokent vegezte-e el a main goal szintet
		company.main_goals_completed++;
		if ((this.generalGoalSettings.completion_first_main_level > 0) && (company.main_goals_completed > this.main_goal_level)) {
			this.main_goal_level++;
			company.main_goals_completed_first_plusz_scores += this.generalGoalSettings.completion_first_main_level;
			if (this.logmode)
				GSLog.Info("[FecaGame::CompanyCompletedGoal] " + GSCompany.GetName(company.company_id) + " completed first main level: " + this.main_goal_level);
		}
		
		if (this.generalGoalSettings.completion_goal_months > 0) {
			local pluspont = this.generalGoalSettings.completion_goal_months - (this.GetMonthsCountFromStarted() - goal.goal_begin_month);
			
			if (pluspont > 0) { // eloszor rosszul csinaltam, mert ha ez nincs, es tulmegy a beallitott honapon, akkor negativ lesz, es levonas fog erte jarni ahelyett, hogy jo lenne.
				company.main_goals_completed_months += pluspont;
				if (this.logmode)
					GSLog.Info("[FecaGame::CompanyCompletedGoal] " + GSCompany.GetName(company.company_id) + " honap utan is kapott jutalmat: " + pluspont + ", igy " + company.main_goals_completed_months + " honapos pluszpontja van");
			}
		}
		
		if (this.generalGoalSettings.neverRunOutMain || company.CountOfMainGoals() > 0) {
			local goal_amount = 0;
			
			if (baseSettingsTable != null) {
				if (baseSettingsTable.awardsForSuccesGoalMainMin == baseSettingsTable.awardsForSuccesGoalMainMax)
					goal_amount = baseSettingsTable.awardsForSuccesGoalMainMin;
				else
					goal_amount = (GSBase.RandRange(baseSettingsTable.awardsForSuccesGoalMainMax - baseSettingsTable.awardsForSuccesGoalAwardMin + 1) + baseSettingsTable.awardsForSuccesGoalAwardMin);
			}
			
			// ha sosem fogy el a main goal, akkor letrehozunk annyit, hogy megint jo legyen
			if (this.generalGoalSettings.neverRunOutMain) {
				local mennyit = this.main_goals - company.main_goals.len();
				for (local i = 0; i < mennyit; i++) {
					local level = this.main_goals + company.main_goals_completed;
					
					local newgoal = this.NewRandomGoal(company.company_id, false, goal.goal_type, level, GF_MAIN);
					company.AddGoalToMain(newgoal);
				}
			}
			
			this.HandOutAwards(company, goal, goal_amount);
			
			local mennyit = this.generalGoalSettings.allAtOnceTimeShowedMainGoals;
			if (mennyit == 0)
				mennyit = this.main_goals;
			
			// eredeti:
//			company.ShowFirstItemsMainGoal(mennyit);
			// uj:
			for (local i = 0; i < mennyit; i++) {
				if (company.main_goals.len() > i) {
					local agoal = company.main_goals[i];
					this.ShowGoal(company, agoal);
					
					switch (agoal.goal_type) {
						case FGBaseGoal.GoalTypes.GT_TRANSPORT:
							// azonnal feliratkozunk erre a feladatra
							// agoal.month - 1 azert kell, mert ha month == 0, akkor nem erdekel minket a dolog, igy az egeszet lehet monitorozni,
							// de ha month == 1, akkor a 0 kell nekunk, mert az az aktualis honap
							this.cargoMonitor.SubscribeToMonitor(company.company_id, agoal.cargo_id, agoal.goal_id, FGCargoMonitor.REFRESH_IMMEDIATELY, FGCargoMonitor.MONITOR_ALL_DELIVERY, 0, agoal.month - 1);
							break;
						case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
							break;
						case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
							break;
						case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
							break;
						case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
							break;
							// TODO [feladat tipus] tobbi goal-t is hozza kell adni
					}
				}
			}
			
		}
		else if (this.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0)) {
			// ez csak azert kell, hogy bekeruljon a vallalat konyvebe is, hogy gyozott
			local award = FGAward(company, this, 1, goal.goal_difficulty);
			award.AddAwardListToCompany(company, null, goal);
		}
	}
	else {
		local goal_amount = 0;
		local awardMin = 0;
		local awardMax = 0;
		
		local localAwardsList = null; // pointer
		local awardGoals = 0;
		
		local neverRunOut = false;
		local level = 0;
		
		local gf;
		
		if (goal.goal_weak) {
			awardMin = baseSettingsTable.awardsForSuccesGoalWeakAwardMin;
			awardMax = baseSettingsTable.awardsForSuccesGoalWeakAwardMax;
			localAwardsList = this.weak_awards;
			awardGoals = this.weak_award_goals;
			neverRunOut = this.generalGoalSettings.neverRunOutWeakAwards;
			gf = GF_WEAK;
			
			this.weak_award_goal_level++;
			level = this.weak_award_goal_level;
		}
		else {
			awardMin = this.baseTransportGoalSettings.awardsForSuccesGoalAwardMin;
			awardMax = this.baseTransportGoalSettings.awardsForSuccesGoalAwardMax;
			localAwardsList = this.awards;
			awardGoals = this.award_goals;
			neverRunOut = this.generalGoalSettings.neverRunOutAwards;
			gf = GF_AWARD;
			
			this.award_goal_level++;
			level = this.award_goal_level;
		}
		
		// ha feladat tipusonkent tobb fajta beallitas letezik,
		// tehat GT_TRANSPORT eseteben be lehet allitani, hogy csak a main goal legyen szintenkent letrehozva, a tobbi total random
		switch (goal.goal_type) {
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				if (this.transportGoalSettings.onlyMainGoalLevels || this.transportGoalSettings.enableAllCargoTypes)
					level = 0;
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}
		
		if (awardMin == awardMax)
			goal_amount = awardMin;
		else
			goal_amount = (GSBase.RandRange(awardMax - awardMin + 1) + awardMin);
		
		this.HandOutAwards(company, goal, goal_amount);
		
		if (this.logmode)
			GSLog.Info("[FecaGame::CompanyCompletedGoal] 1 local list award length: " + localAwardsList.len());
		
		for (local i = localAwardsList.len() - 1; i >= 0; i--) {
			if (goal.goal_id == localAwardsList[i].goal_id) {
				localAwardsList.remove(i);
				break;
			}
		}
		
		if (this.logmode)
			GSLog.Info("[FecaGame::CompanyCompletedGoal] 2 local list award length: " + localAwardsList.len());
		
		local subscribeList = []; // ebbe mentem el az ujonnan letrehozott goalokat, hogy kesobb feliratkozhassunk rajuk, hamarabb nem lehet feliratkozni
		local newgoalList = []; // ebbe is hozzaadom
		// uj award goal letrehozasa, ha az van beallitva, hogy mindig frissuljon
		if (neverRunOut) {
			if (this.logmode)
				GSLog.Info("[FecaGame::CompanyCompletedGoal] Never run out");
			for (local i = localAwardsList.len(); i < awardGoals; i++) {
				local awardgoal = this.NewRandomGoal(null, false, goal.goal_type, level, gf); // azert null a company (elso parameter), mert nincs jelentosege, mivel nem main goal
				if (awardgoal == null) {
					GSLog.Error("[FecaGame::CompanyCompletedGoal] Can not create goal, invalid awardgoal");
					continue;
				}
				
				awardgoal.goal_award = true;
				
				local kell_monitorozni = false;
				local succesfulSetup = false;
				
				if (goal.goal_weak) {
					awardgoal.goal_weak = true;
					kell_monitorozni = true;
					succesfulSetup = true;
					awardgoal.goal_id = ("weak_award_goal_" + this.weak_award_goals_counter);
					this.weak_award_goals_counter++;
				}
				else {
					if (this.generalGoalSettings.allAtOnceTimeShowedAwardGoals == 0 || (i < this.generalGoalSettings.allAtOnceTimeShowedAwardGoals)) {
						this.ShowGoal(null, awardgoal);
						if (awardgoal.goal_id == null || !GSGoal.IsValidGoal(awardgoal.goal_id)) {
							GSLog.Error("[FecaGame::CompanyCompletedGoal] Can not create goal, invalid goal_id: " + awardgoal.goal_id);
						}
						else {
							if (this.logmode)
								GSLog.Info("[FecaGame::CompanyCompletedGoal] New award goal_id: " + awardgoal.goal_id);
							
							kell_monitorozni = true;
							succesfulSetup = true;
						}
					}
					else {
						succesfulSetup = true;
					}
				}
				
				switch (goal.goal_type) {
					case FGBaseGoal.GoalTypes.GT_TRANSPORT:
						// azonnal feliratkozunk erre a feladatra, azaz az osszes company feliratkozik erre
						if (kell_monitorozni && succesfulSetup)
							subscribeList.append(awardgoal);
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
						break;
						// TODO [feladat tipus] tobbi goal-t is hozza kell adni
				}
				
				if (succesfulSetup) {
					localAwardsList.append(awardgoal);
					newgoalList.append(awardgoal);
				}
			}
		}
		
		if (!goal.goal_weak) {
			if (this.logmode)
				GSLog.Info("[FecaGame::CompanyCompletedGoal] Remove award goal_id: " + goal.goal_id);
			// eltavolitjuk a kijelzorol a regi goal-t
			if (GSGoal.IsValidGoal(goal.goal_id))
				GSGoal.Remove(goal.goal_id);
		}
		
		// az osszes letezo vallalattol elvesszuk az award goal-t
		foreach (acompany in this.companies) {
			// eltavolitjuk a goal-t
			this.cargoMonitor.UnSubscribeFromMonitor(acompany.company_id, goal.goal_id);
			acompany.RemoveGoal(goal);
			
			// hozzaadjuk az uj feladatot
			if (newgoalList.len() > 0) {
				for (local i = 0; i < newgoalList.len(); i++) {
					local awardgoal = newgoalList[i]
					if (goal.goal_weak || (this.generalGoalSettings.allAtOnceTimeShowedAwardGoals == 0 || (i < this.generalGoalSettings.allAtOnceTimeShowedAwardGoals)))
						acompany.AddGoal(FGTransportGoal.Copy(awardgoal));
				}
			}
			
			// es hozzaadjuk a monitorozashoz is
			if (subscribeList.len() > 0) {
				foreach (awardgoal in subscribeList) {
					// azonnal feliratkozunk erre a feladatra
					// agoal.month - 1 azert kell, mert ha month == 0, akkor nem erdekel minket a dolog, igy az egeszet lehet monitorozni,
					// de ha month == 1, akkor a 0 kell nekunk, mert az az aktualis honap
					this.cargoMonitor.SubscribeToMonitor(acompany.company_id, awardgoal.cargo_id, awardgoal.goal_id, FGCargoMonitor.REFRESH_IMMEDIATELY, FGCargoMonitor.MONITOR_ALL_DELIVERY, 0, awardgoal.month - 1);
				}
			}
		}
	}

	// kulon naplo oldal a feladat elvegzeserol
	if (this.generalGoalSettings.newsForAll) {
		local text = null;
		switch (goal.goal_type) {
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				text = goal.StrGetTransportCargoGoalCompleted(company);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				text = goal.StrGoalCompletedTextCompany(company);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}
		
		// mindenki masnak elkuldjuk, mert magunknak mashonnan megy az uzenet
		if (text != null && text != "") {
			foreach (acompany in this.companies) {
				if (acompany == company)
					continue;
				
				// storybook-ba be kell kerulnie a szovegnek
				if (this.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0)) {
					local title = null;
					if (goal.goal_main)
						title = GSText(GSText.STR_COMPLETED_MAIN_GOAL_PAGE_TITLE, company.company_id);
					else if (goal.goal_weak)
						title = GSText(GSText.STR_COMPLETED_WEAK_GOAL_PAGE_TITLE, company.company_id);
					else
						title = GSText(GSText.STR_COMPLETED_AWARD_GOAL_PAGE_TITLE, company.company_id);
					
					acompany.AddNewStoryPage(title, [{type = GSStoryPage.SPET_TEXT, reference = 0, text = text}], true);
				}
				else
					// regebbi verzioknal uzenetet kuldunk
					GSGoal.Question(this.message_counter++, acompany.company_id, text, GSGoal.QT_INFORMATION, GSGoal.BUTTON_CLOSE);
			}
		}
	}

	// esemeny naploba valo beregisztralas
	if (this.generalGoalSettings.detailedStoryBook && this.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0)) {
		local text = null;
		switch (goal.goal_type) {
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				text = goal.StrGetTransportCargoGoalCompletedEventStoryPage();
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				text = goal.StrGoalCompletedTextCompanyEventPage();
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}

		if (text == null) {
			GSLog.Warning("[FecaGame::CompanyCompletedGoal] Can not find goal text for goal_id: " + awardgoal.goal_id);
		}
		else {
			this.AddCompanyGoalCompletedToCompanyStoryPageInfo(company.company_id, GSCompany.GetName(company.company_id), text);
		}
	}

	if (goal.goal_main && company.CountOfMainGoals() <= 0) {
		if (this.generalGoalSettings.gameTime == 0 || !this.generalGoalSettings.neverRunOutMain) {
			this.EndGame(company);
		}
	}

	switch (goal.goal_type) {
		case FGBaseGoal.GoalTypes.GT_TRANSPORT:
		case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
		case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
			// itt ellenorzom le, hogy maradt-e bent a vallalatnak cargo-ja, ami mar nem kellett az elozo feladathoz,
			// mert mar tul sokat szallitott be, igy kellhet masik feladathoz
			company.CheckPlusTransportedCargos();
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
			break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
	}
}

function FecaGame::HandOutAwards(company, goal, goal_amount) {
	local awardlist = [];

	if (goal.goal_award && company.award_enabled && goal_amount > 0 && this.award_enabled_by_possible) {
		for (local i = 0; i < goal_amount; i++) { // annyi jutalmat osztunk ki, amennyit kell...
			local multiplier = 1;

			// cargo alapjan szamolt kedvezmeny
			local transportedmultiplier = 1; // a lenyeg, hogy cargo_id alapjan probaljunk egy szorzot adni a vegeredmenynek, igy lehet, hogy egy nehezebb feladatnak tobb lesz a jutalma
			if (this.cargo != null)
				transportedmultiplier /= this.cargo.CargoMultiplier(goal.cargo_id); // azert kell osztani, mert ott pont forditva van pontozva a dolog

			multiplier *= transportedmultiplier;

			// veletlen factor
			local vf = GSBase.RandRange(10);
			if (vf == 0)
				multiplier *= 0.8;
			else if (vf == 1)
				multiplier *= 0.9;
			else if (vf == 8)
				multiplier *= 1.1;
			else if (vf == 9)
				multiplier *= 1.2;

			// weak factor
			if (this.generalGoalSettings.enableDoubleAmountOfAwardWhenWeak && goal.goal_weak)
				multiplier *= 2;

			local award = FGAward(company, this, multiplier, goal.goal_difficulty);

			local amountOfAward = award.Award();
			local typeofaward = award.award;

			if (typeofaward == null) {
				if (this.logmode)
					GSLog.Info("[FecaGame::HandOutAwards] " + GSCompany.GetName(company.company_id) + " nem kapott jutalmat, mert mindent tud venni");
			}
			else {
				local awardtable = {
					award_type = typeofaward,
					award_amount = amountOfAward,
				}
				awardlist.append(awardtable);

				if (this.logmode)
					GSLog.Warning("[FecaGame::HandOutAwards] award type: " + typeofaward + ", amount: " + amountOfAward + ", multiplier: " + multiplier);
			}
		}
		if (this.logmode)
			GSLog.Info("");
	}

	// mindenkeppen kell uzenetet kuldeni a sajat vallalatnak, hogy elkeszult a feladattal
	if (this.logmode && this.award_enabled_by_possible)
		GSLog.Info("[FecaGame::HandOutAwards] " + GSCompany.GetName(company.company_id) + "-nak award kuldese");

	// viszomt ez mindenkeppen kell, mert igy kerul be a story bookba es a sima uzenetbe
	local award = FGAward(company, this, 1, goal.goal_difficulty);
	if (award.AddAwardListToCompany(company, awardlist, goal)) {
		// ha kapott jutalmat, akkor updateljuk a korlatozasok listajat
		company.possibleListNeedUpdate = true;
	}
	else {
		if (this.logmode && this.award_enabled_by_possible) // minek irjuk ki, ha eleve ugysem kaphatott...
			GSLog.Info("[FecaGame::HandOutAwards] " + GSCompany.GetName(company.company_id) + " mégsem kapott jutalmat!");

	}
	// megvizsgaljuk megy egyszer, hogy egyaltalan kaphat-e jutalmat meg a ceg, ha nem, akkor eltavolitjuk a StoryPage-t
	if (this.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0) && !company.award_enabled && company.possibleStoryPageID && GSStoryPage.IsValidStoryPage(company.possibleStoryPageID)) {
		GSStoryPage.Remove(company.possibleStoryPageID);
		company.possibleWindowUpdateEnabled = false; // kikapcsoljuk, hogy ne kelljen feleslegesen frissitest keresni
	}
}

function FecaGame::UpdateAllGoalText() {
	foreach (company in this.companies) {
		company.UpdateAllScoresAndTexts();
	}
}

function FecaGame::ShowGoal(company, goal) {
	local company_id = GSCompany.COMPANY_INVALID;
	if (company != null)
		company_id = company.company_id;
	
	if (goal == null || goal.goal_weak || (goal.goal_id != null && GSGoal.IsValidGoal(goal.goal_id)))
		return;
	
	local szoveg = "";
	local gt = goal.goal_gt_type;
	local dest = goal.goal_gt_destintation;
	
	/* TODO:
	 at kell gondolni a gt es a dest valtozot:
	 azt hiszem a goal-ban kellene menteni a gt es a dest erteket,
	 es akkor az alabbi switch kitorolheto, a gt es dest igy alakulhatna:
	 
	 local gt = goal.GSGoalType;
	 local dest = goal.GSGoalDestination;
	 vagy valami ilyesmi...
	 
	 es akkor meg lehetne oldani, hogy ha egyeni varos fejlesztesrol van szo, akkor
	 a dest az egyeni varosrol szolo storypage azonositojat es GT_StoryPage, vagy GT_Town es a town_id lesz a dest.
	 valamint ha global, akkor ugyanigy a globalis cel globalis story oldalat mutathatna meg, vagy a globalis varost...
	 
	*/
	
	/*
	 if (!goal.goal_main && !goal.goal_weak) {
	 // ha mellek kuldetesrol van szo
	 gt = GSGoal.GT_STORY_PAGE;
	 dest = company.companysGoalStatesStoryPageID;
	 }
	 */
	
	
	switch (goal.goal_type) {
		case FGBaseGoal.GoalTypes.GT_TRANSPORT:
			szoveg = goal.StrTransportCargoGoal(false);
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
			szoveg = goal.StrGoalText();
//			gt = GSGoal.GT_TOWN;
//			dest = goal.town_id;
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
			break;
			// TODO [feladat tipus] tobbi goal-t is hozza kell adni
	}
	
	if (szoveg == null || szoveg == "") {
		local errormessage = "szoveg == \"\"";
		if (szoveg == null)
			errormessage = "szoveg == null";
		
		GSLog.Error("[FGCompany::ShowGoal] Can not show goal, text not found! (" + errormessage + ")");
		return;
	}
	
	// weak ugysem lehet
	local fulltext = null;
	if (goal.goal_main)
		fulltext = GSText(GSText.STR_GOAL_MAIN_TEXT, szoveg);
	else
		fulltext = GSText(GSText.STR_GOAL_AWARD_TEXT, szoveg);
	
	goal.goal_id = GSGoal.New(company_id, fulltext, gt, dest);
	
	if (goal.goal_id == null || !GSGoal.IsValidGoal(goal.goal_id)) {
		GSLog.Error("[FGCompany::ShowGoal] Can not create goal, invalid goal_id: " + goal.goal_id);
	}
	
	if (goal.goal_main)
		goal.goal_begin_month = this.GetMonthsCountFromStarted();
}

function FecaGame::SendScores() {
	// eloszor megkeressuk az elso, masodik es harmadik helyezett vallalatot
	local firstComp = null;
	local secondComp = null;
	local thirdComp = null;
	
	foreach (company in this.companies) {
		if (firstComp == null || firstComp.GetScores() < company.GetScores())
			firstComp = company;
	}
	
	foreach (company in this.companies) {
		if (company == firstComp)
			continue;
		
		if (secondComp == null || secondComp.GetScores() < company.GetScores())
			secondComp = company;
	}
	
	foreach (company in this.companies) {
		if (company == firstComp || company == secondComp)
			continue;
		
		if (thirdComp == null || thirdComp.GetScores() < company.GetScores())
			thirdComp = company;
	}
	
	if (firstComp == GSCompany.COMPANY_INVALID)
		firstComp = null;
	
	if (secondComp == GSCompany.COMPANY_INVALID)
		secondComp = null;
	
	if (thirdComp == GSCompany.COMPANY_INVALID)
		thirdComp = null;
	
	// most osszerakjuk a szoveget
	if (firstComp == null)
		return; // nincs gyoztes???
	
	GSGoal.CloseQuestion(this.message_goal_information);
	foreach (company in this.companies) {
		local text = null;
		local companyText = null;
		local et = GSText(GSText.STR_EMPTY);
		
		if (firstComp == company || secondComp == company || thirdComp == company || !this.generalGoalSettings.enableCupUpdatesPoints) {
			companyText = et;
		}
		else {
			companyText = GSText(GSText.STR_GOAL_SCORES_COMPANY, company.GetScores());
		}
		
		local secondText = null;
		if (secondComp == null) {
			secondText = GSText(GSText.STR_GOAL_SCORES_COMPANY_EMPTY, et, et);
		}
		else {
			if (this.generalGoalSettings.enableCupUpdatesPoints)
				secondText = GSText(GSText.STR_GOAL_SCORES_TWO, secondComp.company_id, secondComp.GetScores());
			else
				secondText = GSText(GSText.STR_GOAL_SCORES_TWO_WITHOUT_POINTS, secondComp.company_id);
		}
		
		local thirdText = null;
		if (thirdComp == null) {
			thirdText = GSText(GSText.STR_GOAL_SCORES_COMPANY_EMPTY, et, et);
		}
		else {
			if (this.generalGoalSettings.enableCupUpdatesPoints)
				thirdText = GSText(GSText.STR_GOAL_SCORES_THREE, thirdComp.company_id, thirdComp.GetScores());
			else
				thirdText = GSText(GSText.STR_GOAL_SCORES_THREE_WITHOUT_POINTS, thirdComp.company_id);
		}
		
		if (this.generalGoalSettings.enableCupUpdatesPoints)
			text = GSText(GSText.STR_GOAL_SCORES, firstComp.company_id, firstComp.GetScores(), secondText, thirdText, companyText);
		else
			text = GSText(GSText.STR_GOAL_SCORES_WITHOUT_POINTS, firstComp.company_id, secondText, thirdText, companyText);
		
		GSGoal.Question(this.message_goal_information, company.company_id, text, GSGoal.QT_INFORMATION, GSGoal.BUTTON_CLOSE);
	}
}

function FecaGame::SendCompanyInformations() {
	// 1.4.0 felett storybook oldalt mutatunk meg a mellek feladatok allasarol
	if (this.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0)) {
		foreach (company in this.companies) {
			if (company.companysGoalStatesStoryPageID != null && company.companysGoalStatesStoryPageID != GSStoryPage.STORY_PAGE_ELEMENT_INVALID)
				GSStoryPage.Show(company.companysGoalStatesStoryPageID);
		}
		return;
	}
	
	GSGoal.CloseQuestion(this.message_award_information);
	local et = GSText(GSText.STR_EMPTY);
	
//	GSNews.Create(GSNews.NT_GENERAL, "company updates", GSCompany.COMPANY_INVALID);
	// meg kell keresni a harom legjobb award feladatot a valalalatnak
	foreach (company in this.companies) {
		if (company.award_goals.len() == 0)
			continue;
		
		local firstGoal = null;
		local secondGoal = null;
		local thirdGoal = null;
		
		local firstGoalText = "";
		local secondGoalText = "";
		local thirdGoalText = "";
		
		foreach (goal in company.award_goals) {
			if (firstGoal == null || firstGoal.GetMaxGoalPercent() < goal.GetMaxGoalPercent())
				firstGoal = goal;
		}
		
		foreach (goal in company.award_goals) {
			if (firstGoal.cargo_id != goal.cargo_id && (secondGoal == null || secondGoal.GetMaxGoalPercent() < goal.GetMaxGoalPercent()))
				secondGoal = goal;
		}
		
		foreach (goal in company.award_goals) {
			if (firstGoal.cargo_id != goal.cargo_id && secondGoal.cargo_id != goal.cargo_id && (thirdGoal == null || thirdGoal.GetMaxGoalPercent() < goal.GetMaxGoalPercent()))
				thirdGoal = goal;
		}
		
		if (firstGoal != null) {
			local txt = GSText(GSText.STR_GOAL_AWARD_TRANSPORT, firstGoal.cargo_id, firstGoal.amount);
			firstGoalText = GSText(GSText.STR_GOAL_AWARD_PERCENT, txt, firstGoal.GetMaxGoalPercent());
			
			if (this.logmode)
				GSLog.Info("[FecaGame::SendCompanyInformations] " + GSCompany.GetName(company.company_id) + " firstGoal: " + GSCargo.GetCargoLabel(firstGoal.cargo_id) + ": " + firstGoal.GetMaxGoalPercent() + "%");
		}
		else {
			firstGoalText = GSText(GSText.STR_GOAL_AWARD_EMPTY, et, et, et, et);
		}
		
		if (secondGoal != null) {
			local txt = GSText(GSText.STR_GOAL_AWARD_TRANSPORT, secondGoal.cargo_id, secondGoal.amount);
			secondGoalText = GSText(GSText.STR_GOAL_AWARD_PERCENT, txt, secondGoal.GetMaxGoalPercent());
			
			if (this.logmode)
				GSLog.Info("[FecaGame::SendCompanyInformations] " + GSCompany.GetName(company.company_id) + " secondGoal: " + GSCargo.GetCargoLabel(secondGoal.cargo_id) + ": " + secondGoal.GetMaxGoalPercent() + "%");
		}
		else {
			secondGoalText = GSText(GSText.STR_GOAL_AWARD_EMPTY, et, et, et, et);
		}
		
		if (thirdGoal != null) {
			local txt = GSText(GSText.STR_GOAL_AWARD_TRANSPORT, thirdGoal.cargo_id, thirdGoal.amount);
			thirdGoalText = GSText(GSText.STR_GOAL_AWARD_PERCENT, txt, thirdGoal.GetMaxGoalPercent());
			
			if (this.logmode)
				GSLog.Info("[FecaGame::SendCompanyInformations] " + GSCompany.GetName(company.company_id) + " thirdGoal: " + GSCargo.GetCargoLabel(thirdGoal.cargo_id) + ": " + thirdGoal.GetMaxGoalPercent() + "%");
		}
		else {
			thirdGoalText = GSText(GSText.STR_GOAL_AWARD_EMPTY, et, et, et, et);
		}
		
		if (firstGoal != null || secondGoal != null || thirdGoal != null) {
			local goaltext = GSText(GSText.STR_GOAL_AWARD, firstGoalText, secondGoalText, thirdGoalText);
			GSGoal.Question(this.message_award_information, company.company_id, goaltext, GSGoal.QT_INFORMATION, GSGoal.BUTTON_CLOSE);
		}
		else {
			GSLog.Error("No available award goal for company: " + GSCompany.GetName(company.company_id));
		}
	}
	if (this.logmode)
		GSLog.Info("");
}

function FecaGame::UpdateGameStateStoryBoards() {
	if (this.versionController.IsCurrentVersionLessThan(1, 4, 0) || this.generalGoalSettings.enableStoryCupUpdates <= 0) // ha nincs beallitas, akkor -1 is lehet elvileg...
		return; // hat ha ki kan kapcsolva, akkor nem kell frissiteni...
	
	local acompanies = [];
	acompanies.extend(this.companies);
	
	local sortedCompanies = [];
	
	while (acompanies.len() > 0) {
		local best = -1;
		local id = -1;
		local company = null;
		
		for (local i = 0; i < acompanies.len(); i++) {
			local acomp = acompanies[i];
			local cbest = acomp.GetScores(); // ezert csinaltam kulon neki valtozot, hogy ne kelljen megegyszer lekerdezni
			if (cbest > best) {
				company = acomp;
				best = cbest;
				id = i;
			}
		}
		
		// ha megtalaltuk a legjobbat, akkor egyszeruen hozzaadjuk a listahoz
		if (id > -1) {
			sortedCompanies.append({company_id = company.company_id, company_scores = company.GetScores()});
			acompanies.remove(id);
		}
	}
	
	// ha nem egyezik a szamuk, akkor biztosan frissiteni kell
	if (sortedCompanies.len() == this.gameStateCompanies.len()) {
		local vanValtozas = false;
		
		for (local i = 0; i < sortedCompanies.len(); i++) {
			// ha nem egyezik a vallalat azonosito, akkor van valtozas, vagy
			// ha pontokat is megjelenitunk es valtozik a pont, akkor van valtozas
			if (sortedCompanies[i].company_id != this.gameStateCompanies[i].company_id || ((this.generalGoalSettings.enableStoryCupUpdates == 2) && (sortedCompanies[i].company_scores != this.gameStateCompanies[i].company_scores))) {
				vanValtozas = true;
				break;
			}
		}
		
		if (!vanValtozas) // tehat ha nem kell valtoztatni az allast, akkor szepen kilepunk
			return;
	}
	
	// eltavolitjuk az elozoleg eltarolt sorrendet
	this.gameStateCompanies.clear();
	
	local elements = [];
	
	for (local i = 0; i < sortedCompanies.len(); i++) {
		local company_id = sortedCompanies[i].company_id;
		local scores = sortedCompanies[i].company_scores;
		
		// eltaroljuk a kesobbi osszehasonlitas miatt
		this.gameStateCompanies.append(sortedCompanies[i]);
		
		if (elements.len() == 0) {
			elements.append(GSText(GSText.STR_NEWLINE)); // hozzaadunk egy ures sort
			elements.append(GSText(GSText.STR_GOAL_SCORES_STORY_FIRST, company_id, scores));
		}
		else if (elements.len() == 2) {
			elements.append(GSText(GSText.STR_GOAL_SCORES_STORY_SECOND, company_id, scores));
		}
		else if (elements.len() == 3) {
			elements.append(GSText(GSText.STR_GOAL_SCORES_STORY_THIRD, company_id, scores));
		}
		else {
			if (elements.len() == 4) {
				elements.append(GSText(GSText.STR_NEWLINE)); // hozzaadunk egy ures sort
			}
			
			elements.append(GSText(GSText.STR_GOAL_SCORES_STORY_NUM, elements.len() - 1, company_id, scores));
		}
	}
	
	if (elements.len() > 0) {
		foreach (company in this.companies) {
			company.UpdateGameStateStoryPage(elements);
		}
	}
}

// - # messages functions
function FecaGame::SendMesesageWithQuestion(company_id, text) {
	// ha invalid company, akkor elkuldjuk az osszes felhasznalonak
	if (company_id == GSCompany.COMPANY_INVALID) {
		foreach (company in this.companies)
			GSGoal.Question(this.message_counter++, company.company_id, text, GSGoal.QT_INFORMATION, GSGoal.BUTTON_CLOSE);
	}
	else
		GSGoal.Question(this.message_counter++, company_id, text, GSGoal.QT_INFORMATION, GSGoal.BUTTON_CLOSE);
}

function FecaGame::GetCompany(company_id) {
	if (company_id == null) // gyorsitas...
		return null;

	foreach (company in this.companies) {
		if (company.company_id == company_id)
			return company;
	}
	
	return null;
}

function FecaGame::RemoveCompany(company) {
	local compname = this.CompanyName(company.company_id);
	this.cargoMonitor.UnSubscribeCompanyFromMonitor(company.company_id);
	this.RemoveFromCompanyNames(company.company_id);
	company.ResetCompany();

	for (local i = 0; i < this.companies.len(); i++) {
		if (this.companies[i].company_id == company.company_id) {
			if (this.logmode)
				GSLog.Info("[FecaGame::RemoveCompany] Remove company: " + compname);
			this.companies.remove(i);
			this.UpdateGameStateStoryBoards();
			break;
		}
	}
}

function FecaGame::AddToCompanyNames(company_id, companyName) {
	this.companyNames.append({company_id = company_id, company_name = companyName});
}

function FecaGame::CompanyName(company_id) {
	for (local i = 0; i < this.companyNames.len(); i++) {
		if (this.companyNames[i].company_id == company_id)
			return this.companyNames[i].company_name;
	}
	
	return null;
}

function FecaGame::RemoveFromCompanyNames(company_id) {
	for (local i = 0; i < this.companyNames.len(); i++) {
		if (this.companyNames[i].company_id == company_id) {
			this.companyNames.remove(i);
			return;
		}
	}
}

function FecaGame::UpdateCompanyNames() {
	foreach (compinfo in this.companyNames) {
		if (GSCompany.ResolveCompanyID(compinfo.company_id) == GSCompany.COMPANY_INVALID) continue;
		local newName = GSCompany.GetName(compinfo.company_id);
		if (newName != null && newName != compinfo.company_name) {
			if (this.logmode)
				GSLog.Info("[FGCompany::UpdateCompanyNames] " + compinfo.company_name + " company renamed to: " + newName);

			compinfo.company_name = newName;
			this.UpdateCompanyNameInCompanyStoryPageInfo(compinfo.company_id, newName);

			if (this.fggc_active) {
				// kicserljuk a newname-bol az idezojeleket %22-re
				newName = this.ReplaceStringWithString("\"", "%22", newName);
				newName = this.ReplaceStringWithString("'", "%27", newName);

				local company = this.GetCompany(compinfo.company_id);
				if (company && company.fggc_joined && !GSAdmin.Send({namechange = newName, companyID = company.company_id, versenyID = company.fggc_versenyID}) && this.logmode)
					GSLog.Warning("Can not send company (" + company.company_id + ") name (" + newName + ") to AdminPort!");
			}
		}
	}
}


// - # Company StoryPage Info
function FecaGame::AddStartInfoToCompanyStoryPageInfo() {
	if (this.detailedStoryBookPageID == null ||
		this.versionController.IsCurrentVersionLessThan(1, 4, 0) ||
		!this.generalGoalSettings.detailedStoryBook ||
		this.detailedStoryBookPageID == GSStoryPage.STORY_PAGE_INVALID)
		return;

	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_NEWLINE));
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_COMPANY_EVENTS_DATE, GSDate.GetCurrentDate()));
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_COMPANY_EVENTS_START_COMPANIES));

	foreach (company in this.companies) {
		local compName = this.CompanyName(company.company_id);
		if (compName != null && compName != "") {
			local spid = GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, compName);
			this.companyEventInfoInDiary.append({company_id = company.company_id, spid = spid});
		}
	}
}

function FecaGame::AddEndInfoToCompanyStoryPageInfo(company_id) {
	if (this.detailedStoryBookPageID == null ||
		this.versionController.IsCurrentVersionLessThan(1, 4, 0) ||
		!this.generalGoalSettings.detailedStoryBook ||
		this.detailedStoryBookPageID == GSStoryPage.STORY_PAGE_INVALID)
		return;

	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_NEWLINE));
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_COMPANY_EVENTS_DATE, GSDate.GetCurrentDate()));

	if (company_id != null)
		GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_GOAL_WIN_COMPANY_COMPLETED_GAME, company_id));
	else
		GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_GOAL_WIN_WITHOUT_COMPANY_COMPLATED));
}

function FecaGame::AddNewCompanyToCompanyStoryPageInfo(company_id, compName) {
	assert(company_id != null);
	assert(company_id != -1);
	assert(compName != null);
	assert(compName != "");

	if (this.detailedStoryBookPageID == null ||
		this.versionController.IsCurrentVersionLessThan(1, 4, 0) ||
		!this.generalGoalSettings.detailedStoryBook ||
		this.detailedStoryBookPageID == GSStoryPage.STORY_PAGE_INVALID)
		return;

	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_NEWLINE));
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_COMPANY_EVENTS_DATE, GSDate.GetCurrentDate()));
	local spid = GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, compName);
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_COMPANY_EVENTS_NEW_COMPANY));

	this.companyEventInfoInDiary.append({company_id = company_id, spid = spid});
}

function FecaGame::AddBankruptedCompanyToCompanyStoryPageInfo(company_id, compName, companyScores) {
	assert(company_id != null);
	assert(company_id != -1);
	assert(compName != null);
	assert(compName != "");

	assert(typeof(companyScores) == "integer");

	if (this.detailedStoryBookPageID == null ||
		this.versionController.IsCurrentVersionLessThan(1, 4, 0) ||
		!this.generalGoalSettings.detailedStoryBook ||
		this.detailedStoryBookPageID == GSStoryPage.STORY_PAGE_INVALID)
		return;

	// eloszor a regieket atnezzuk es atallitjuk, aztan adjuk hozza az ujat

	// ebben az esetben csak az adott company id-t -1 re kell allitani, mert az infora meg szukseg van,
	// de mar kesobbiekben nem kell, ill. nem fogjuk tudni frissiteni,
	// mivel a company_id-t megkapja a kovetkezo vallalat...
	for (local i = 0; i < this.companyEventInfoInDiary.len(); i++) {
		if (this.companyEventInfoInDiary[i].company_id == company_id) {
			this.companyEventInfoInDiary[i].company_id = -1;
			// azert nincs break, mert az osszes company_id-t at kell allitani
		}
	}

	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_NEWLINE));
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_COMPANY_EVENTS_DATE, GSDate.GetCurrentDate()));
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, compName);
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_COMPANY_EVENTS_BANKRUP_COMPANY, companyScores));

	// itt most nem kell elmenteni a vallalatot, hiszen kesobb nem fogjuk mar frissiteni
	this.companyEventInfoInDiary.append({company_id = -1, spid = null});
}

function FecaGame::AddMergedCompanyToCompanyStoryPageInfo(old_company_id, oldCompName, new_company_id, newCompName, companyScores) {
	assert(old_company_id != null);
	assert(old_company_id != -1);
	assert(oldCompName != null);
	assert(oldCompName != "");

	assert(new_company_id != null);
	assert(new_company_id != -1);
	assert(newCompName != null);
	assert(newCompName != "");

	assert(typeof(companyScores) == "integer");

	if (this.detailedStoryBookPageID == null ||
		this.versionController.IsCurrentVersionLessThan(1, 4, 0) ||
		!this.generalGoalSettings.detailedStoryBook ||
		this.detailedStoryBookPageID == GSStoryPage.STORY_PAGE_INVALID)
		return;

	// eloszor a regieket atnezzuk es atallitjuk, aztan adjuk hozza az ujat

	// ebben az esetben csak az adott company id-t -1 re kell allitani, mert az infora meg szukseg van,
	// de mar kesobbiekben nem kell, ill. nem fogjuk tudni frissiteni,
	// mivel a company_id-t megkapja a kovetkezo vallalat...
	for (local i = 0; i < this.companyEventInfoInDiary.len(); i++) {
		if (this.companyEventInfoInDiary[i].company_id == old_company_id) {
			this.companyEventInfoInDiary[i].company_id = -1;
			// azert nincs break, mert az osszes company_id-t at kell allitani
		}
	}

	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_NEWLINE));
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_COMPANY_EVENTS_DATE, GSDate.GetCurrentDate()));
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_COMPANY_EVENTS_MERGE_COMPANIES, companyScores));
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_COMPANY_EVENTS_MERGED_COMPANY));
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, oldCompName);
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_COMPANY_EVENTS_MERGE_NEW_COMPANY));
	local spid = GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, newCompName);

	this.companyEventInfoInDiary.append({company_id = new_company_id, spid = spid});
}

function FecaGame::AddCompanyGoalCompletedToCompanyStoryPageInfo(company_id, compName, goaltext) {
	assert(company_id != null);
	assert(company_id != -1);
	assert(compName != null);
	assert(compName != "");
	assert(goaltext != null);
	assert(goaltext != "");

	if (this.detailedStoryBookPageID == null ||
		this.versionController.IsCurrentVersionLessThan(1, 4, 0) ||
		!this.generalGoalSettings.detailedStoryBook ||
		this.detailedStoryBookPageID == GSStoryPage.STORY_PAGE_INVALID)
		return;

	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_NEWLINE));
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_COMPANY_EVENTS_DATE, GSDate.GetCurrentDate()));
	local spid = GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, compName);
	GSStoryPage.NewElement(this.detailedStoryBookPageID, GSStoryPage.SPET_TEXT, 0, goaltext);

	this.companyEventInfoInDiary.append({company_id = company_id, spid = spid});
}

function FecaGame::UpdateCompanyNameInCompanyStoryPageInfo(company_id, newCompName) {
	assert(company_id != null);
	assert(company_id > -1);
	assert(newCompName != null);
	assert(newCompName != "");

	if (this.detailedStoryBookPageID == null ||
		this.versionController.IsCurrentVersionLessThan(1, 4, 0) ||
		!this.generalGoalSettings.detailedStoryBook ||
		this.detailedStoryBookPageID == GSStoryPage.STORY_PAGE_INVALID)
		return;

	for (local i = 0; i < this.companyEventInfoInDiary.len(); i++) {
		local info = this.companyEventInfoInDiary[i];
		if (info.company_id == company_id) {
			if (info.spid != null && GSStoryPage.IsValidStoryPageElement(info.spid)) {
				GSStoryPage.UpdateElement(info.spid, 0, newCompName);
			}
		}
	}
}

// TODO: talan nem is lehet hozzaadni ezzel a modszerrel a feladatok elvegzeset.
// ha azt is akarom, akkor kulon sorba kell tenni a vallalat nevet, hogy frissitheto legyen

// - # Date functions
function FecaGame::GetCurrentDateTimeString() {
	local date = GSDate.GetCurrentDate();
	local month = GSDate.GetMonth(date);
	local year = GSDate.GetYear(date);
	local day = GSDate.GetDayOfMonth(month)

	return year + "-" + (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day) + " 00:00";
}

function FecaGame::GetMonthsCountFromStarted () {
	local year = GSDate.GetYear(GSDate.GetCurrentDate());
	return ((year - this.startyear) * 12) + GSDate.GetMonth(GSDate.GetCurrentDate()) - 1; // azert a -1, hogy 0 legyen a kezdohonap a lista miatt
}

function FecaGame::GetCurrentMonth () {
	return GSDate.GetMonth(GSDate.GetCurrentDate());
}

function FecaGame::GetPreviousMonthDayCount () {
	local date = GSDate.GetCurrentDate();
	local month = GSDate.GetMonth(date);
	local year = GSDate.GetYear(date);
	
	if (month == 1) {
		month = 12;
		year--;
	}
	else
		month--;
	
	return this.GetDayCountOfMonth(month, year);
}

function FecaGame::GetCurrentDay () {
	return GSDate.GetDayOfMonth(GSDate.GetCurrentDate());
}

function FecaGame::GetCurrentDaysCount () {
	local date = GSDate.GetCurrentDate();
	local month = GSDate.GetMonth(date);
	local year = GSDate.GetYear(date);
	
	return this.GetDayCountOfMonth(month, year);
}

function FecaGame::GetDayCountOfMonth (month, year) {
	local szokoev = false;
	if ((year % 4) == 0) {
		if ((year % 100) > 0) {
			szokoev = true;
		}
		else {
			// szazadfordulok csak akkor szokoevek, ha 400-al oszthatoak
			if ((year % 400) == 0)
				szokoev = true;
		}
	}	
	
	switch (month) {
		case 1: // januar
			return 31;
			break;
		case 2: // februar
			if (szokoev)
				return 29;
			else
				return 28;
			break;
		case 3: // marcius
			return 31;
			break;
		case 4: // aprilis
			return 30;
			break;
		case 5: // majus
			return 31;
			break;
		case 6: // junius
			return 30;
			break;
		case 7: // julius
			return 31;
			break;
		case 8: // augusztus
			return 31;
			break;
		case 9: // szeptember
			return 30;
			break;
		case 10: // oktober
			return 31;
			break;
		case 11: // november
			return 30;
			break;
		case 12: // december
			return 31;
			break;
		default:
			return 30;
			break;
	}
	
	return 30;
}

function FecaGame::UpdateSystemDate() {
	local systemsec = GSDate.GetSystemTime();
	
	/*
	
	1492 sima, 478 szoko
	1492 * 365 = 544580 nap
	478 * 366 = 174948 nap
	osszesen = 719528 nap telt el 0-ttol 1970-ig
	 
	*/
	
//	systemsec += 3600; // timezone miatt kell ez....
	
	local elteltNapokSzamaEpochtol = systemsec / 86400; // 86400 = (60 * 60 * 24)
	local elteltnapokszama0tol = elteltNapokSzamaEpochtol + 719528; // 719528 ez az eltelt napok szama 0-tol 1970-ig, fentebb kijott
	
	this.systemYear = GSDate.GetYear(elteltnapokszama0tol);
	this.systemMonth = GSDate.GetMonth(elteltnapokszama0tol);
	this.systemDay = GSDate.GetDayOfMonth(elteltnapokszama0tol);

	// azert mukodik ez, mert ugyebar div van az elteltNapokSzamaEpochtol, es a maradek a mai ido masodpercben...
	// tehat ez is jo lenne:
//	local maradekMasodpercekEpochtol = systemsec % 86400;
	local maradekMasodpercekEpochtol = systemsec - (elteltNapokSzamaEpochtol * 86400);
	
	this.systemHour = (maradekMasodpercekEpochtol / 3600).tointeger();
	maradekMasodpercekEpochtol -= (this.systemHour * 3600).tointeger();
	this.systemMinute = maradekMasodpercekEpochtol / 60;
	maradekMasodpercekEpochtol -= (this.systemMinute * 60);
	this.systemSecond = maradekMasodpercekEpochtol;
}
/*
function FecaGame::UpdateSystemDate() {
	if (!this.testenabled)
		return;
	local seconds = GSDate.GetSystemTime();
	// Reference: Fliegel, H. F. and van Flandern, T. C. (1968).
	// Communications of the ACM, Vol. 11, No. 10 (October, 1968).
	// Original code in Fortran
	
	local I, J, K, L, N;
	
	L = seconds / 86400 + 2509157;
	N = 4 * L / 146097;
	L = L - (146097 * N + 3) / 4;
	I = 4000 * (L + 1) / 1461001;
	L = L - 1461 * I / 4 + 31;
	J = 80 * L / 2447;
	K = L - 2447 * J / 80;
	L = J / 11;
	J = J + 2 - 12 * L;
	I = 100 * (N - 49) + I + L;
	
	systemYear = I.tointeger();
	systemMonth = J.tointeger();
	systemDay = K.tointeger();
}
//*/

function FecaGame::ReplaceStringWithString(mit, mivel, string) {
	local result = string;
	while (true) {
		local index = result.find(mit);

		if (index == null) {
			break;
		}

		if (index == 0) {
			result = mivel + result.slice(1);
		}
		else {
			local eleje = result.slice(0, index);
			local vege = result.slice(index + 1);
			result = eleje + mivel + vege;
		}
	}

	return result;
}

function FecaGame::Save() {
	if (this.logmode)
		GSLog.Info("Saving game...");
	
	local company_save_list = [];
	foreach(company in this.companies)
		company_save_list.append(company.SaveToTable());
	
	local maingoals = [];
	foreach (goal in this.allMainGoals)
		maingoals.append(goal.SaveToTable());
	
	local awardgoals = [];
	foreach (goal in this.awards)
		awardgoals.append(goal.SaveToTable());
	
	local weakawardgoals = [];
	foreach (goal in this.weak_awards)
		weakawardgoals.append(goal.SaveToTable());
	
	
	return {
		
		game_started = this.game_started,
		game_ended = this.game_ended,
		testenabled = this.testenabled,
		testoszto = this.testoszto,
//		logmode = this.logmode,
		starttimebeallitva = this.starttimebeallitva,

		fggc_active = this.fggc_active,
		
		// uzenetek azonositoja
		message_counter = this.message_counter,
		message_goal_information = this.message_goal_information,
		message_award_information = this.message_award_information,
		
		engagedTownGrowth = this.engagedTownGrowth,
		
		startyear = this.startyear,

		cargoMonitor = this.cargoMonitor.SaveData(),

		main_goals = this.main_goals,
		award_goals = this.award_goals,
		weak_award_goals = this.weak_award_goals,
		weak_award_goals_counter = this.weak_award_goals_counter,
		award_enabled_by_possible = this.award_enabled_by_possible,
		
		
		// el kell menteni a companykat is
		company_list = company_save_list,
		
		
		allMainGoals = maingoals,
		awards = awardgoals,
		weak_awards = weakawardgoals,
		
		main_goal_level = this.main_goal_level,
		award_goal_level = this.award_goal_level,
		weak_award_goal_level = this.weak_award_goal_level,
		
		gameStateCompanies = this.gameStateCompanies,
		companyNames = this.companyNames,
		companyEventInfoInDiary = this.companyEventInfoInDiary,

		generalGoalSettings = this.generalGoalSettings,
		initialPossibilities = this.initialPossibilities,
		baseTransportGoalSettings = this.baseTransportGoalSettings.SaveToTable(),
		transportGoalSettings = this.transportGoalSettings,
		baseTownGrowthGoalSettings = this.baseTownGrowthGoalSettings.SaveToTable(),
		townGrowthGoalSettings = this.townGrowthGoalSettings,

		detailedStoryBookPageID = this.detailedStoryBookPageID,
	};
}

function FecaGame::Load(version, table) {
	if (this.logmode)
		GSLog.Info("Loading game...");
	
	this.loaded_game = true;
	this.on_loading = true; // sok helyen le kell kerdezni, hogy betoltes van-e, mert addig nem szabad frissiteni

	// Store a copy of the table from the save game
	// but do not process the loaded data yet. Wait with that to Init
	// so that OpenTTD doesn't kick us for taking too long to load.	
	local loaded_data = {}
   	foreach(key, value in table)
		loaded_data.rawset(key, value);
	
	
	this.game_started = loaded_data.game_started;
	this.game_ended = loaded_data.game_ended;
	this.testenabled = loaded_data.testenabled;
	this.testoszto = loaded_data.testoszto;
//	this.logmode = loaded_data.logmode;
	this.starttimebeallitva = loaded_data.starttimebeallitva;

	this.fggc_active = loaded_data.fggc_active
	
	// uzenetek azonositoja
	this.message_counter = loaded_data.message_counter;
	this.message_goal_information = loaded_data.message_goal_information;
	this.message_award_information = loaded_data.message_award_information;
	
	this.engagedTownGrowth = loaded_data.engagedTownGrowth;
	
	this.startyear = loaded_data.startyear;

	this.cargoMonitor.LoadFromData(loaded_data.cargoMonitor);

	this.main_goals = loaded_data.main_goals;
	this.award_goals = loaded_data.award_goals;
	this.weak_award_goals = loaded_data.weak_award_goals;
	this.weak_award_goals_counter = loaded_data.weak_award_goals_counter;
	this.award_enabled_by_possible = loaded_data.award_enabled_by_possible;
	
	
	this.main_goal_level = loaded_data.main_goal_level;
	this.award_goal_level = loaded_data.award_goal_level;
	this.weak_award_goal_level = loaded_data.weak_award_goal_level;
	
	this.gameStateCompanies = loaded_data.gameStateCompanies;
	this.companyNames = loaded_data.companyNames;
	this.companyEventInfoInDiary = loaded_data.companyEventInfoInDiary;

	this.generalGoalSettings = loaded_data.generalGoalSettings;
	this.initialPossibilities = loaded_data.initialPossibilities;
	this.baseTransportGoalSettings = FGBaseGoalSettings(loaded_data.baseTransportGoalSettings, version); // a constructor egybol a LoadFromTable-t hivja meg
	this.transportGoalSettings = loaded_data.transportGoalSettings;
	this.baseTownGrowthGoalSettings = FGBaseGoalSettings(loaded_data.baseTownGrowthGoalSettings, version);
	this.townGrowthGoalSettings = loaded_data.townGrowthGoalSettings;

	this.detailedStoryBookPageID = loaded_data.detailedStoryBookPageID;

	if (this.logmode)
		GSLog.Info("Loading data from savegame made with version " + version + " of the game script");
	
	foreach (goalTable in loaded_data.allMainGoals) {
		local goal = null;
		switch (goalTable.goal_type) {
			case -1:
			case FGBaseGoal.GoalTypes.GT_INVALID: // ez a 0 erteku
				GSLog.Error("[FecaGame::Load] Invalid goal type!");
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				goal = FGTransportGoal(null, this);
				goal.LoadFromTable(goalTable, version, this.cargo, this.transportGoalSettings);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				goal = FGTownGrowthGoal(null, this, goalTable.goal_town_type, goalTable.town_id);
				goal.LoadFromTable(goalTable, version, this.townGrowthGoalSettings);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
			default:
				GSLog.Error("[FecaGame::Load] Invalid goal type!");
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}
		
		if (goal != null)
			this.allMainGoals.append(goal);
	}
	
	foreach (goalTable in loaded_data.awards) {
		local goal = null;
		switch (goalTable.goal_type) {
			case -1:
			case FGBaseGoal.GoalTypes.GT_INVALID: // ez a 0 erteku
				GSLog.Error("[FecaGame::Load] Invalid goal type!");
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				goal = FGTransportGoal(null, this);
				goal.LoadFromTable(goalTable, version, this.cargo, this.transportGoalSettings);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				goal = FGTownGrowthGoal(null, this, goalTable.goal_town_type, goalTable.town_id);
				goal.LoadFromTable(goalTable, version, this.townGrowthGoalSettings);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
			default:
				GSLog.Error("[FecaGame::Load] Invalid goal type!");
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}
		
		if (goal != null)
			this.awards.append(goal);
	}
	
	foreach (goalTable in loaded_data.weak_awards) {
		local goal = null;
		switch (goalTable.goal_type) {
			case -1:
			case FGBaseGoal.GoalTypes.GT_INVALID: // ez a 0 erteku
				GSLog.Error("[FecaGame::Load] Invalid goal type!");
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				goal = FGTransportGoal(null, this);
				goal.LoadFromTable(goalTable, version, this.cargo, this.transportGoalSettings);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				goal = FGTownGrowthGoal(null, this, goalTable.goal_town_type, goalTable.town_id);
				goal.LoadFromTable(goalTable, version, this.townGrowthGoalSettings);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
			default:
				GSLog.Error("[FecaGame::Load] Invalid goal type!");
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}
		
		if (goal != null)
			this.weak_awards.append(goal);
	}
	
	local acompanies = loaded_data.company_list;
	foreach (companyTable in acompanies) {
		local company = FGCompany(null, this.cargo);
		company.setdelegate(this);
		company.LoadFromTable(companyTable, version, this.transportGoalSettings);
		company.SetPossible(this.initialPossibilities);
		
		this.companies.append(company);
			 /*
		// betoltjuk a cargo monitorozast es egyebeket
		foreach (agoal in company.main_goals) {
			if (agoal.goal_id != null) {
				switch (agoal.goal_type) {
			  case FGBaseGoal.GoalTypes.GT_TRANSPORT:
					  // azonnal feliratkozunk erre a feladatra
					  // agoal.month - 1 azert kell, mert ha month == 0, akkor nem erdekel minket a dolog, igy az egeszet lehet monitorozni,
					  // de ha month == 1, akkor a 0 kell nekunk, mert az az aktualis honap
						this.cargoMonitor.SubscribeToMonitor(company.company_id, agoal.cargo_id, agoal.goal_id, FGCargoMonitor.REFRESH_IMMEDIATELY, FGCargoMonitor.MONITOR_ALL_DELIVERY, 0, agoal.month - 1);
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
						break;
						// TODO [feladat tipus] tobbi goal-t is hozza kell adni
				}
			}
		}
		
		foreach (agoal in company.award_goals) {
			if (agoal.goal_id != null) {
				switch (agoal.goal_type) {
			  case FGBaseGoal.GoalTypes.GT_TRANSPORT:
					  // azonnal feliratkozunk erre a feladatra
					  // agoal.month - 1 azert kell, mert ha month == 0, akkor nem erdekel minket a dolog, igy az egeszet lehet monitorozni,
					  // de ha month == 1, akkor a 0 kell nekunk, mert az az aktualis honap
						this.cargoMonitor.SubscribeToMonitor(company.company_id, agoal.cargo_id, agoal.goal_id, FGCargoMonitor.REFRESH_IMMEDIATELY, FGCargoMonitor.MONITOR_ALL_DELIVERY, 0, agoal.month - 1);
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
						break;
						// TODO [feladat tipus] tobbi goal-t is hozza kell adni
				}
			}
		}
		
		
		foreach (agoal in company.weak_award_goals) {
			switch (agoal.goal_type) {
			  case FGBaseGoal.GoalTypes.GT_TRANSPORT:
					  // azonnal feliratkozunk erre a feladatra
					  // agoal.month - 1 azert kell, mert ha month == 0, akkor nem erdekel minket a dolog, igy az egeszet lehet monitorozni,
					  // de ha month == 1, akkor a 0 kell nekunk, mert az az aktualis honap
					this.cargoMonitor.SubscribeToMonitor(company.company_id, agoal.cargo_id, agoal.goal_id, FGCargoMonitor.REFRESH_IMMEDIATELY, FGCargoMonitor.MONITOR_ALL_DELIVERY, 0, agoal.month - 1);
					break;
				case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
					break;
				case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
					break;
				case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
					break;
				case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
					break;
					// TODO [feladat tipus] tobbi goal-t is hozza kell adni
			}
		}
			  
			  */
	}
}
