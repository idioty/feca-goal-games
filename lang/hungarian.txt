# Progress news:
# STR_GOAL_PROGRESS_NEWS								:Goal progress{}{}{CARGO_LIST}: {CARGO_SHORT} transported ({NUM} %){}{CARGO_LIST}: {CARGO_SHORT} transported ({NUM} %){}{CARGO_LIST}: {CARGO_SHORT} transported ({NUM} %)
#STR_GOAL_PROGRESS									:Nahát{}{}12 {CARGO_LIST}:{CARGO_SHORT} transported{}:{CARGO_LONG} transported
#STR_COAL											:szállíts el {{CARGO_LONG}.t}!

# CARGO_TINY		elso param: cargo_id, masodik, hogy mennyit, azaz: 24 elszallitasa, kimarad a szen es a tonna is, de hogy mi ertelme...
# CARGO_SHORT		elso param: cargo_id, masodik, hogy mennyit, azaz: 24 tonna elszallitasa, kimarad a szen
# CARGO_LONG		elso param: cargo_id, masodik, hogy mennyit, azaz: 24 tonna szen elszallitasa
# CARGO_LIST		ezt nem probaltam, valoszinuleg egy tomb kell neki, amiben benne vannak a cargok

# proba
#STR_FONT_PROBA										:{STRING}{STRING}
#STR_BIGFONT_PROBA									:{BIG_FONT}Üdvözöllek vaze!
#STR_TINYFONT_PROBA									:{TINY_FONT}Ennek már jó kicsinek kellene lennie.



# altalanos szovegek
# Ezek általános véletlenszerű szövegek, ebből elég egy is, csak a poén kedvéért csináltam többet.
STR_WELCOME_1										:{WHITE}Üdvözöllek a Feca Goal Games univerzumában! Nyerj ha mersz :D!{}{}{}
STR_WELCOME_2										:{WHITE}Légy üdvözölve a Feca Goal Games univerzumban! Remélem élvezni fogod!{}{}{}
STR_WELCOME_3										:{WHITE}Ez a Feca Goal Games szerver! A legkirályabb játék ami valaha létezett!!!{}(bár igaz ez csak egy kiegészítő :D){}{}{}
STR_WELCOME_4										:{WHITE}Üdvözöllek dicső lovag, szép a ruhád, de hol a vonat???{}Jó játékot!{}{}{}
STR_WELCOME_5										:{WHITE}Üdvözöllek a Feca Goal Games univerzumában! Kívánok ezúttal is sikerekben gazdag jó játékot!{}{}{}
STR_WELCOME_6										:{WHITE}Üdvözöllek a Feca Goal Games univerzumában! Kívánok ezúttal is sikerekben gazdag jó játékot!{}{}{}
STR_WELCOME_7										:{WHITE}Üdvözöllek a Feca Goal Games univerzumában! Kívánok ezúttal is sikerekben gazdag jó játékot!{}{}{}
STR_WELCOME_8										:{WHITE}Üdvözöllek a Feca Goal Games univerzumában! Kívánok ezúttal is sikerekben gazdag jó játékot!{}{}{}
STR_AUTHORS											:{BLACK}Játék ötlet:{}{NBSP}{NBSP}{NBSP}{NBSP}{GOLD}Kiss Ferenc {BLACK}({GREEN}idioty{BLACK}){}{NBSP}{NBSP}{NBSP}{NBSP}{GOLD}Bacskai Zsolt{NBSP}{BLACK}({GREEN}kertyü{BLACK}){}{}A játékot programozta:{}{NBSP}{NBSP}{NBSP}{NBSP}{GOLD}Kiss Ferenc{BLACK}{NBSP}({GREEN}idioty{BLACK}).{}{}{}Weboldal: (egyelőre) nincs :D.
STR_SZABALYOK_READ									:{}{}{RED}Kérlek olvasd el figyelmesen a szabályokat a következő oldalon!
STR_GAME_STARTED									:{BLACK}Hurrá!!!{}{}{RED}A játék elindult!!!{}{}{BLACK}Sok sikert és kellemes időtöltést!
# példa: Figyelem!!! A játék '1' percen belül kezdődik! Start: '15':'00'.
STR_GAME_BEGIN										:{BIG_FONT}{RED}Figyelem!!!{}{}{BLACK}A játék {GREEN}{NUM}{NBSP}percen {BLACK}belül kezdődik!{}{GOLD}Start:{NBSP}{STRING}{NUM}:{STRING}{NUM}.
STR_TEST											:{RED}{BIG_FONT}Figyelem!!!{}{}{BLACK}Jelenleg teszt verzió fut!
STR_EMPTY											:
STR_NEWLINE											:{}
STR_NULL											:0
STR_ONE												:1
STR_NOT_COMPATIBLE									:Ez a játék verzió nem támogatott.{}Minimum támogatott verzió: 1.3.0!


# hibauzenetek
STR_ERROR_TOWN_REQUIRED_MINIMUM						:Túl kevés a város található a térképen, hogy minden játékosnak tudjunk osztani külön feladatot. Javaslom a térkép növelését!


# szabalyok
STR_SZABALYOK										:Szabályok
STR_SZABALYOK_1										:{GOLD}Alapszabályok:{}{BLACK}Ez a szabályok oldal a játék beállítások alapján mindig változik. Érdemes mindig átfutni egy-egy új játék előtt!{}A legfontosabb dolog, hogy a {BLUE}fő {BLACK}feladatok a játék "{SILVER}Célok listája{BLACK}" menü "{SILVER}Vállalat céljai {BLACK}" felsorolás alatt jelennek meg.{}Amint valaki teljesítette a {BLUE}fő {BLACK} feladatait, vagy lejárt a játékidő, akkor azonnal vége a játéknak, és az a {RED}győztes, aki a legtöbb pontot gyűjtötte össze{BLACK}.
STR_SZABALYOK_2										:Vannak a játékban {BLUE}mellék {BLACK}és {BLUE}titkos {BLACK}feladatok is. Ezek után szintén jár(hat) jutalom és pont. A {BLUE}titkos {BLACK}feladatok (ha vannak) értelemszerűenę nem jelennek meg. Ezekről csak akkor kapsz információt, ha teljesítettél (vagy valaki más teljesített) egy ilyen feladatot.
STR_SZABALYOK_KORLATOZASOK							:{RED}A játékban korlátozva van az építkezés és/vagy vásárlás{BLACK}, amiről bővebb információt kaphatsz a "{SILVER}Vásárlási megkötések{BLACK}" oldalon. Ha nem találod már a "{SILVER}Vásárlási megkötések {BLACK}" oldalt, akkor bármit vásárolhatsz vagy építhetsz. Ami nincs benne a felsorolásban, azokból szintén bármit vásárolhatsz vagy építhetsz.{}{RED}Fontos{BLACK}, hogy ha egy korlátozott számú eszközből vásárolsz, és később eladod, akkor nem veheted meg újra az eszközt. Tehát ha 5 buszt vehetsz, majd veszel kettőt, akkor marad 3 busz, amit még megvásárolhatsz. Viszont ha eladsz egyet, akkor ugyanúgy csak 3 buszt vásárolhatsz a későbbiekben. Ugyanez a helyzet az épületekkel is, például: korlátozva van a fűtőház, csak egyet lehet építeni. Ezek után építesz egyet. Majd ha lerombolod, és megpróbálsz építeni máshová, akkor automatikusan lerombolódik az új helyen lévő épület. Viszont ebben az esetben lehetséges még egyszer ugyanarra a területre építeni az épületet, akár úgy is, hogy például a fűtőház egy másik irányba néz! Egyéként a vasútállomások esetében egy-egy épületet lehet későbbiekben is nyugodtan bővíteni, az újabb épületrész hozzáadása nem minősül új épület építésnek! Tehát a kapott jutalmat jól be kell osztani!{}
STR_SZABALYOK_MAIN_FELADATOK						:{GOLD}Feladatok:{BLACK}
# példa: Ez a játék véget ér, ha valaki teljesíti a fő feladatait!
STR_SZABALYOK_MAIN_WIN								:Ez a játék véget ér, ha valaki teljesíti a {BLUE}fő {BLACK}feladatait!
# példa: Ez a játék véget ér, ha valaki teljesíti a fő feladatait, vagy letelik a játék teljesítésére szánt idő: '12' hónap!
STR_SZABALYOK_GAME_TIME_OR_MAIN_WIN					:Ez a játék véget ér, ha valaki teljesíti a {BLUE}fő {BLACK}feladatait, vagy letelik a játék teljesítésére szánt idő: {GREEN}{NUM} {BLACK}hónap!
# példa: Ez a játék véget ér, ha letelik a játék teljesítésére szánt idő: '12' hónap!
STR_SZABALYOK_GAME_TIME_WITHOUT_MAIN_WIN			:Ez a játék véget ér, ha letelik a játék teljesítésére szánt idő: {GREEN}{NUM} {BLACK}hónap!
# példa: A mostani játékban '1' fő feladatot kell megoldani a győzelemhez (vagy letelik az idő)!
STR_SZABALYOK_MAIN_FELADATOK_COUNT_OR_GAME_TIME		:A mostani játékban {GREEN}{NUM} {BLUE}fő {BLACK}feladatot lehet megoldani a győzelemhez (vagy letelik az idő)! {STRING}
# példa: A mostani játékban '1' fő feladatot kell megoldani a győzelemhez!
STR_SZABALYOK_MAIN_FELADATOK_COUNT					:A mostani játékban {GREEN}{NUM} {BLUE}fő {BLACK}feladatot kell megoldani a győzelemhez! {STRING}
STR_SZABALYOK_MAIN_FELADATOK_ENDLESS				:A mostani játékban {GREEN}végtelen számú {BLUE}fő {BLACK}feladatot lehet megoldani! {STRING}
# példa: A mostani játékban '1' mellék feladatot lehet megoldani!
STR_SZABALYOK_AWARD_FELADATOK_COUNT					:A mostani játékban {GREEN}{NUM} {BLUE}mellék {BLACK}feladatot lehet megoldani! {STRING}
STR_SZABALYOK_AWARD_FELADATOK_ENDLESS				:A mostani játékban {GREEN}végtelen számú {BLUE}mellék {BLACK}feladatot lehet megoldani! {STRING}
# példa: A mostani játékban '1' titkos feladatot lehet megoldani!
STR_SZABALYOK_WEAK_AWARD_FELADATOK_COUNT			:A mostani játékban {GREEN}{NUM} {BLUE}titkos {BLACK}feladatot lehet megoldani! {STRING}
STR_SZABALYOK_WEAK_AWARD_FELADATOK_ENDLESS			:A mostani játékban {GREEN}végtelen számú {BLUE}titkos {BLACK}feladatot lehet megoldani! {STRING}
STR_SZABALYOK_FELADATOK_VISIBLE_ALL					:Most az összes feladat látható.{STRING}
# példa: Ebben a játékban ezekből a célokból egyszerre '1' feladat jelenik meg.
STR_SZABALYOK_FELADATOK_VISIBLE_COUNT				:Ebben a játékban ezekből a célokból egyszerre {GREEN}{NUM} {BLACK}feladat jelenik meg.
STR_SZABALYOK_FELADATOK_KESZ						:Egy feladat sikeres teljesítéséről a naplóban részletes bejegyzés olvasható, valamint a feladat eltűnik a célok listájáról. A naplóba kerül a sikeres teljesítés dátuma, a megoldandó feladat és a kapott jutalmak leírása.{}
STR_SZABALYOK_JUTALOM								:{GOLD}Jutalmakról:{}{BLACK}Minden feladat elvégzése után jár(hat) jutalom. A jutalmat a lekorlátozott eszközökből kaphatsz. Például, ha a játék kezdetekor nem vehetsz csak 5 buszt, akkor egy-egy feladat teljesítése után további buszokat vásárolhatsz. A kapott jutalmakról az elvégzett feladat teljesítésekor kapsz részletes információt.{}
STR_SZABALYOK_PONTOZAS								:{GOLD}Potozásról:{}{BLACK}Minden feladat elvégzése után jár(hat) pont, ami beleszámít a győzelemhez. A pontot mindig az adott feladat legjobb eredménye után kapjuk százalékosan. Tehát ha 100 pont jár egy feladatért, és valaki még csak a 10%-át teljesítette, akkor 10 pontot kap érte a játékos. Ez a 10 pont megmarad még akkor is, ha a jelenlegi teljesítménye alulmarad. Tehát mindig a legjobb elért eremény után jár a pont. Ha egy {BLUE}mellék {BLACK}küldetést egyszerre több ember is teljesíteni szeretné, és egyiküknek sikerül, akkor ő megkapja a maximális pontot, de a többieknek a feladatból addig összegyüjtött pontjai is megmaradnak.
# példa: Egy fő feladat teljesítéséért '100' pont jár.
STR_SZABALYOK_PONT_MAIN								:Egy {BLUE} fő {BLACK}feladat teljesítéséért {GREEN}{NUM} {BLACK}pont jár.
STR_SZABALYOK_PONT_AWARD							:Egy {BLUE} mellék {BLACK}feladat teljesítéséért {GREEN}{NUM} {BLACK}pont jár.
STR_SZABALYOK_PONT_WEAK_AWARD						:Egy {BLUE} titkos {BLACK}feladat teljesítéséért {GREEN}{NUM} {BLACK}pont jár.
# példa: Szintenként is lehet pontot szerezni, szintenként '30' pont jár...
STR_SZABALYOK_PONT_SZINT							:Szintenként is lehet pontot szerezni, szintenként {GREEN}{NUM} {BLACK}pont jár. Ez azt jelenti: például ha valaki elsőként teljesíti az első feladatot, megkapja érte ezeket a pontokat. Ugyanígy, ha valaki a második feladatot teljesíti előként, ő szintén mekapja ezt a pontot, függetlenül attól, hogy az első feladatot más teljesítette először.
# példa: Ha valaki '60' hónapon belül teljesíti a feladatot, akkor szintén kap plusz pontot...
STR_SZABALYOK_PONT_HONAP							:Ha valaki {GREEN}{NUM} {BLACK} hónapon belül teljesíti a feladatot, akkor szintén kap plusz pontot, méghozzá a következő képpen: például 60 hónap alatt kell teljesíteni a feladatot. Ha sikerül neki a második hónapban teljesítenie a feladatot, akkor (60 - 2) + 1, azaz 59 plusz pontot fog kapni.{}
STR_SZABALYOK_VEGSZO								:{GOLD}Végszó:{}{BLACK}Egymás zavarása, direkt módon történő korlátozása, tereprendezés miatt vállalat létrehozása {RED}azonnali kitiltással jár{BLACK}!!!{}Bár ez a játék verseny, mégis a könnyed szórakozásra jött létre. Remélem elnyeri tetszésed ez a játékmód, és legközelebb is találkozunk!{}Kellemes és sikerekben gazdag játékot!


# feladatok
STR_GOAL_MAIN_TEXT									:{BLACK}(Fő feladat) {STRING}
STR_GOAL_AWARD_TEXT									:{BLACK}(Mellék feladat) {STRING}

# példa: '2800 tonna szén' elszállítása '6' hónap alatt!
STR_GOAL_TRANSPORT_MONTHS							:{GOLD}{NBSP}{CARGO_LONG} elszállítása {GREEN}{NUM}{BLACK}{NBSP}hónap{NBSP}alatt!
# példa: '2800 tonna szén' elszállítása!
STR_GOAL_TRANSPORT									:{GOLD}{NBSP}{CARGO_LONG}{BLACK} elszállítása!
# példa: '2800 tonna szén' elszállítása '6' hónap alatt: aktuális: '25'%, max: '30'%
STR_GOAL_TRANSPORT_MONTHS_MAIN						:{GOLD}{NBSP}{CARGO_LONG} {BLACK}elszállítása {GREEN}{NUM}{BLACK}{NBSP}hónap{NBSP}alatt: aktuális:{NBSP}{GREEN}{NUM}{NBSP}%{BLACK}, max:{NBSP}{GREEN}{NUM}{NBSP}%
# példa: '2800 tonna szén' elszállítása: aktuális: '251%, max: '30'%
STR_GOAL_TRANSPORT_MAIN								:{GOLD}{NBSP}{CARGO_LONG} {BLACK}elszállítása: aktuális:{NBSP}{GREEN}{NUM}{NBSP}%{BLACK}, max:{NBSP}{GREEN}{NUM}{NBSP}%

# példa: '2800 tonna szén' elszállíva '6' hónap alatt!
STR_GOAL_TRANSPORTED_MONTHS							:{GOLD}{CARGO_LONG}{NBSP}{BLACK}elszállítva {NUM}{NBSP}hónap{NBSP}alatt!
# példa: '2800 tonna szén' elszállítva!
STR_GOAL_TRANSPORTED								:{GOLD}{CARGO_LONG}{NBSP}{BLACK}elszállítva!

# példa: '2800 tonna szén' sikeresen elszállítva '6' hónap alatt!
STR_GOAL_TRANSPORTED_MONTHS_ME						:{GOLD}{CARGO_LONG}{NBSP}{BLACK}sikeresen elszállítva {GREEN}{NUM}{NBSP}{BLACK}hónap{NBSP}alatt!
# példa: '2800 tonna szén' sikeresen elszállítva!
STR_GOAL_TRANSPORTED_ME								:{GOLD}{CARGO_LONG}{NBSP}{BLACK}sikeresen elszállítva!


# 1.4.0 verziotol mas lesz a feladat kiirasa
# azaz aktuális és maximum rövidítve
# példa: akt: '25'%, max: '30'%
STR_GOAL_TRANSPORTED_PERCENT						:{BLACK}akt: {GREEN}{NUM}%{BLACK}, max: {GREEN}{NUM}%


# ezek itt napló bejegyzések címei
STR_AWARD_GOALS_PAGE_TITLE							:Mellék feladatok teljesítménye
STR_GAME_STATE_TITLE								:Játék állása
STR_WELCOME_PAGE_TITLE								:Üdvözlő üzenet
STR_KORLATOZASOK_PAGE_TITLE							:Vásárlási megkötések
STR_GAME_OVER_TITLE									:Vége a játéknak
# példa: 'Király Rt.' teljesített egy fő feladatot
STR_COMPLETED_MAIN_GOAL_PAGE_TITLE					:{COMPANY} teljesített egy fő feladatot
STR_COMPLETED_AWARD_GOAL_PAGE_TITLE					:{COMPANY} teljesített egy mellék feladatot
STR_COMPLETED_WEAK_GOAL_PAGE_TITLE					:{COMPANY} teljesített egy titkos feladatot
STR_ME_COMPLETED_MAIN_GOAL_PAGE_TITLE				:Elvégeztem egy fő feladatot
STR_ME_COMPLETED_AWARD_GOAL_PAGE_TITLE				:Elvégeztem egy mellék feladatot
STR_ME_COMPLETED_WEAK_GOAL_PAGE_TITLE				:Elvégeztem egy titkos feladatot




# feladat teljesitve
# példa: 'Király Rt.' sikeresen teljesített egy 'fő feladatot':...
STR_GOAL_COMPANY_COMPLETED							:{GOLD}{COMPANY} {BLACK}sikeresen teljesített egy {GOLD}{STRING}{BLACK}:{}{}{STRING}
# példa: Sikeresen teljesítettél egy 'fő feladatot':...
STR_COMPLETED_GOAL_WITH_AWARD						:{GOLD}Sikeresen teljesítettél egy {GREEN}{STRING}{NBSP}{GOLD}:{}{BLACK}{STRING}{BLACK}{STRING}

# példa: 'Király Rt.' sikeresen teljesített egy fő feladatot
STR_GOAL_COMPANY_COMPLETED_MAIN						:{GOLD}{COMPANY} {BLACK}sikeresen teljesített egy {GOLD}fő{NBSP}feladatot{BLACK}:{}{}{STRING}
STR_GOAL_COMPANY_COMPLETED_AWARD					:{GOLD}{COMPANY} {BLACK}sikeresen teljesített egy {GOLD}mellék{NBSP}küldetést{BLACK}:{}{}{STRING}
STR_GOAL_COMPANY_COMPLETED_WEAK						:{GOLD}{COMPANY} {BLACK}sikeresen teljesített egy {GOLD}titkos{NBSP}feladatot{BLACK}:{}{}{STRING}

# példa: Sikeresen teljesítettél egy fő feladatot:
STR_COMPLETED_GOAL_WITH_AWARD_MAIN					:{GOLD}Sikeresen teljesítettél egy {GREEN}fő{NBSP}feladatot{NBSP}{GOLD}:{}{YELLOW}{STRING}{BLACK}{STRING}
STR_COMPLETED_GOAL_WITH_AWARD_AWARD					:{GOLD}Sikeresen teljesítettél egy {GREEN}mellék{NBSP}küldetést{NBSP}{GOLD}:{}{YELLOW}{STRING}{BLACK}{STRING}
STR_COMPLETED_GOAL_WITH_AWARD_WEAK					:{GOLD}Sikeresen teljesítettél egy {GREEN}titkos{NBSP}feladatot{NBSP}{GOLD}:{}{YELLOW}{STRING}{BLACK}{STRING}

# példa: Sikeresen teljesített egy feladatot
STR_COMPLETED_GOAL_MAIN								:{GOLD}Sikeresen teljesítettél egy {GREEN}fő{NBSP}feladatot
STR_COMPLETED_GOAL_AWARD							:{GOLD}Sikeresen teljesítettél egy {GREEN}mellék{NBSP}küldetést
STR_COMPLETED_GOAL_WEAK								:{GOLD}Sikeresen teljesítettél egy {GREEN}titkos{NBSP}feladatot


STR_COMPLETED_GOAL_AWARD_TEXT						:{}{}Jutalma(i)d:{STRING}{STRING}{STRING}
# példa: Kaptál '5' darab 'állomást' jutalmul. Még '10' darabot építhetsz, maximum '15' darabod lehet!
STR_BUILD											:{}{BLACK}Kaptál{NBSP}{GREEN}{NUM}{NBSP}{STRING}{BLACK}{NBSP}jutalmul. Még {YELLOW}{NUM}{BLACK}{NBSP}darabot építhetsz, maximum{NBSP}{YELLOW}{NUM}{BLACK}{NBSP}darabod lehet!
# példa: Most már építhetsz bármennyi...
STR_BUILD_MORE										:{}{BLACK}Most már építhetsz {GREEN}bármennyi{NBSP}{STRING}{STRING}{STRING}{STRING}{BLACK}!
# példa: Kaptál '5' darab 'buszt' jutalmul. Még '10' darabot vehetsz, maximum '15' darabod lehet!
STR_PURCHASE										:{}{BLACK}Kaptál{NBSP}{GREEN}{NUM}{NBSP}{STRING}{NBSP}{BLACK}jutalmul. Még {YELLOW}{NUM}{BLACK}{NBSP}darabot vehetsz, maximum{NBSP}{YELLOW}{NUM}{BLACK}{NBSP}darabod lehet!
# példa: Most már vehetsz bármennyi...
STR_PURCHASE_MORE									:{}{BLACK}Most már vehetsz {GREEN}bármennyi{NBSP}{STRING}{STRING}{STRING}{STRING}{BLACK}!


# Pontok kiiratasa jatek kozben
# példa: A vállalatodnak '25' pontja van!
STR_GOAL_SCORES_COMPANY								:{}{}{RED}A vállalatodnak {RED}{NUM}{NBSP}{BLACK}pontja{NBSP}van!
STR_GOAL_SCORES_COMPANY_EMPTY						:{STRING}{STRING}
# példa: 'Király Rt.' a második helyezett ('25' ponttal)!
STR_GOAL_SCORES_TWO									:{}{}{SILVER}{COMPANY} {BLACK}a második helyezett ({SILVER}{NUM}{NBSP}{BLACK}ponttal)!
# példa: 'Király Rt.' a második helyezett!
STR_GOAL_SCORES_TWO_WITHOUT_POINTS					:{}{}{SILVER}{COMPANY} {BLACK}a második helyezett!
# példa: 'Király Rt.' a harmadik helyezett ('25' ponttal)!
STR_GOAL_SCORES_THREE								:{}{}{LTBLUE}{COMPANY} {BLACK}a harmadik helyezett ({LTBLUE}{NUM}{NBSP}{BLACK}ponttal)!
# példa: 'Király Rt.' a harmadik helyezett!
STR_GOAL_SCORES_THREE_WITHOUT_POINTS				:{}{}{LTBLUE}{COMPANY} {BLACK}a harmadik helyezett!


# 'Király Rt.' vezeti a játékot! ('25' ponttal)!
STR_GOAL_SCORES										:{GOLD}{COMPANY} {BLACK}vezeti a játékot ({GOLD}{NUM}{NBSP}{BLACK}ponttal)!{STRING}{STRING}{STRING}
# 'Király Rt.' vezeti a játékot!
STR_GOAL_SCORES_WITHOUT_POINTS						:{GOLD}{COMPANY} {BLACK}vezeti a játékot!{STRING}{STRING}{STRING}
# példa: A játéknak vége! 'Király Rt.' sikeresen teljesítette az összes feladatot! 'Király Rt.' megnyerte a játékot '25' ponttal! A játék körülbelül egy perc múlva folytatódik feladatok és korlátozások nélkül!
STR_GOAL_WIN_COMPANY								:{RED}A játéknak vége!{}{}{BLACK}{COMPANY} {BLACK}sikeresen teljesítette az összes feladatot!{}{}{}{GOLD}{COMPANY} {BLACK}megnyerte a játékot {GOLD}{NUM}{NBSP}{BLACK}ponttal!{STRING}{STRING}{STRING}{}{}{}{}A játék körülbelül egy perc múlva folytatódik feladatok és korlátozások nélkül!
# példa: A játéknak vége! Lejárt a játékidő! 'Király Rt.' megnyerte a játékot '25' ponttal! A játék körülbelül egy perc múlva folytatódik feladatok és korlátozások nélkül!
STR_GOAL_WIN_WITHOUT_COMPANY						:{RED}A játéknak vége! Lejárt a játékidő!{}{}{}{GOLD}{COMPANY} {BLACK}megnyerte a játékot {GOLD}{NUM}{NBSP}{BLACK}ponttal!{STRING}{STRING}{STRING}{}{}{}{}A játék körülbelül egy perc múlva folytatódik feladatok és korlátozások nélkül!

# uj goal szovegek a naplohoz
# 'Király Rt.' sikeresen teljesítette az összes feladatot!
STR_GOAL_WIN_COMPANY_COMPLETED_GAME					:{BLACK}{COMPANY} {BLACK}sikeresen teljesítette az összes feladatot!{}
STR_GOAL_WIN_WITHOUT_COMPANY_COMPLATED				:{RED}A játéknak vége! Lejárt a játékidő!{}{}
# példa: 'Király Rt.' megnyerte a játékot ('25' ponttal)!
STR_GOAL_WIN_FIRST_COMPANY							:{GOLD}{COMPANY} {BLACK}megnyerte a játékot ({GOLD}{NUM}{NBSP}{BLACK}ponttal)!
# példa: 'Király Rt.' végzett a dobogó második fokán ('25' ponttal)!
STR_GOAL_WIN_SECOND_COMPANY							:{SILVER}{COMPANY} {BLACK}végzett a dobogó második fokán ({SILVER}{NUM}{NBSP}{BLACK}ponttal)!
# példa: 'Király Rt.' lett a harmadik, így még épphogy felfért a dobogóra ('25' ponttal)!
STR_GOAL_WIN_THIRD_COMPANY							:{SILVER}{COMPANY} {BLACK}lett a harmadik, így még épphogy felfért a dobogóra ({SILVER}{NUM}{NBSP}{BLACK}ponttal)!
# példa: '5'. helyezett: 'Király Rt.' ('25' ponttal)!
STR_GOAL_WIN_OTHER_COMPANY							:{LTBLUE}{NUM}. helyezett: {COMPANY} {BLACK}({LTBLUE}{NUM}{NBSP}{BLACK}ponttal)!
STR_GOAL_WIN_END									:{}A játék körülbelül egy perc múlva folytatódik feladatok és korlátozások nélkül!

# story bookba az allas
# példa: 'Király Rt.'  vezeti a játékot ('25' ponttal)!
STR_GOAL_SCORES_STORY_FIRST							:{GOLD}{COMPANY} {BLACK} vezeti a játékot ({GOLD}{NUM}{NBSP}{BLACK}ponttal)!
# példa: 'Király Rt.'  a második helyezett ('25' ponttal)!
STR_GOAL_SCORES_STORY_SECOND						:{SILVER}{COMPANY} {BLACK} a második helyezett ({SILVER}{NUM}{NBSP}{BLACK}ponttal)!
# példa: 'Király Rt.'  a harmadik helyezett ('25' ponttal)!
STR_GOAL_SCORES_STORY_THIRD							:{LTBLUE}{COMPANY} {BLACK} a harmadik helyezett ({LTBLUE}{NUM}{NBSP}{BLACK}ponttal)!
# példa: '5'. helyezett: 'Király Rt.' ('25' ponttal)!
STR_GOAL_SCORES_STORY_NUM							:{BLACK}{NUM}. helyezett: {COMPANY} ({NUM}{NBSP}ponttal)!

STR_GOAL_AWARD										:Mellék küldetése(i)d teljesítménye:{}{STRING}{STRING}{STRING}
# példa: '2500 tonna szén' elszállítása!
STR_GOAL_AWARD_TRANSPORT							:{GOLD}{CARGO_LONG}{NBSP}elszállítása{NBSP}{STRING}!
# példa: Max teljesítmény: '25'%
STR_GOAL_AWARD_PERCENT								:{}{}{STRING}{}{BLACK}Max teljesítmény: {BLACK}{SILVER}{NUM}%{BLACK}
STR_GOAL_AWARD_EMPTY								:{STRING}



# story book companies events
STR_COMPANY_EVENTS									:Események
STR_COMPANY_EVENTS_START_COMPANIES					:Induláskor jelen lévő vállalatok:
STR_COMPANY_EVENTS_DATE								:{GOLD}{DATE_LONG}
STR_COMPANY_EVENTS_NEW_COMPANY						:nevű vállalatot sikeresen megalapították.
# példa: nevű vállalat sajnos csődbe ment. Eddig összegyüjtött pontjai: '25'. Reméljük legközelebb sikeresebb lesz!
STR_COMPANY_EVENTS_BANKRUP_COMPANY					:nevű vállalat sajnos csődbe ment. Eddig összegyüjtött pontjai: {GREEN}{NUM}{BLACK}.{}Reméljük legközelebb sikeresebb lesz!
# példa: Két vállalat egyesült. Reméljük nem anyagi gondok miatt került sor erre, és mindkét félnek előnye származik a felvásárlásból! Csődbe ment vállalat eddigi pontjai: 75
STR_COMPANY_EVENTS_MERGE_COMPANIES					:Két vállalat egyesült.{}Reméljük nem anyagi gondok miatt került sor erre, és mindkét félnek előnye származik a felvásárlásból!{}Felvásárolt vállalat eddigi pontjai: {GREEN}{NUM}{BLACK}.
STR_COMPANY_EVENTS_MERGED_COMPANY					:{SILVER}Felvásárolt vállalat neve:
STR_COMPANY_EVENTS_MERGE_NEW_COMPANY				:{SILVER}Felvásárló vállalat neve:

STR_COMPANY_EVENTS_GOAL_COMPANY_COMPLETED_MAIN		:sikeresen teljesített egy {SILVER}fő{NBSP}feladatot{BLACK}: {STRING}
STR_COMPANY_EVENTS_GOAL_COMPANY_COMPLETED_AWARD		:sikeresen teljesített egy {SILVER}mellék{NBSP}küldetést{BLACK}: {STRING}
STR_COMPANY_EVENTS_GOAL_COMPANY_COMPLETED_WEAK		:sikeresen teljesített egy {SILVER}titkos{NBSP}feladatot{BLACK}: {STRING}
STR_COMPANY_EVENTS_TOWN_GROWTH_GOAL_COMPLETED		:sikeresen felfejlesztette a(z) {TOWN} {BLACK}nevű települést {GOLD}{STRING}{BLACK}, a lakosság elérte a(z) {GREEN}{NUM} {BLACK}főt!

# ez nem kell
#STR_COMPANY											:valtozas company neve: {RAW_STRING}


# Varos tipusok nevei
#STR_TOWN_INVALID									:érvénytelen
STR_TOWN_VILLAGE									:községgé
STR_TOWN_TOWN										:faluvá
STR_TOWN_CITY										:várossá
STR_TOWN_METROPOLIS									:metropolisszá
STR_TOWN_SUPER_METROPOLIS							:szuper metropoisszá

# példa: Záhony nevű települést fejleszd fel 'várossá' (lakosok száma legalább '1500' legyen)!
STR_TOWN_GROWTH_GOAL								:{GOLD}{TOWN} {BLACK}nevű települést fejleszd fel {GOLD}{STRING} {BLACK}(lakosok száma legalább {GREEN}{NUM} {BLACK}legyen)!
# példa: Záhony nevű települést fejleszd fel 'várossá' (lakosok száma legalább '1500' legyen)! (akt: '5'%, max: '10'%)
STR_TOWN_GROWTH_GOAL_PERCENT						:{GOLD}{TOWN} {BLACK}nevű települést fejleszd fel {GOLD}{STRING} {BLACK}(lakosok száma legalább {GREEN}{NUM} {BLACK}legyen)! (akt:{NBSP}{GREEN}{NUM}{NBSP}%{BLACK}, max:{NBSP}{GREEN}{NUM}{NBSP}%{BLACK})
# példa: Záhony nevű települést sikeresen felfejlesztetted 'várossá', a lakosság elérte a(z) '1500' főt!
STR_TOWN_GROWTH_GOAL_COMPLETED_STORY_ME				:{GOLD}{TOWN} {BLACK}nevű települést sikeresen felfejlesztetted {GOLD}{STRING}{BLACK}, a lakosság elérte a(z) {GREEN}{NUM} {BLACK}főt!
# példa: 'Király Rt.' sikeresen felfejlesztette a(z) Záhony nevű települést 'várossá', a lakosság elérte a(z) '1500' főt!
STR_TOWN_GROWTH_GOAL_COMPLETED_STORY_COMPANY		:{COMPANY} sikeresen felfejlesztette a(z) {TOWN} {BLACK}nevű települést {GOLD}{STRING}{BLACK}, a lakosság elérte a(z) {GREEN}{NUM} {BLACK}főt!


# Possible dolgok
# FIGYELEM:
# Ne torold duplikalt STRING bejegyzeseket innen a possible uzenetektol, mert a regi 1.3.x verzio uzenet megjelenitesenel 2-2 hely van lefoglalva minden uzenetnek, es rosszul jelenhet meg...
STR_POSSIBLE										:{STRING}{GOLD}A játék a következő megkötéssel/megkötésekkel indul:{BLACK}{}{STRING}{}{STRING}{}{STRING}{}{STRING}{}{STRING}{}{STRING}
# példa: Nem építhetsz 'fűtőházat' az egész játék során!
STR_POSSIBLE_ERROR_BUILD_IN_GAME					:Nem építhetsz {GREEN}{STRING}{STRING} {BLACK}az egész játék során!
# példa: Nem vehetsz 'buszt' az egész játék során!
STR_POSSIBLE_ERROR_PURCHASE_IN_GAME					:Nem vehetsz {GREEN}{STRING}{STRING} {BLACK}az egész játék során!
# példa: Még nem építhetsz 'fűtőházat'!
STR_POSSIBLE_ERROR_MORE_BUILD						:Még nem építhetsz {GREEN}{STRING}{STRING}{BLACK}!
# példa: Még nem vehetsz 'buszt'!
STR_POSSIBLE_ERROR_MORE_PURCHASE					:Még nem vehetsz {GREEN}{STRING}{STRING}{BLACK}!
# példa: Már nem építhetsz több 'fűtőházat'!
STR_POSSIBLE_ERROR_NOT_MORE_BUILD					:Már nem építhetsz több {GREEN}{STRING}{STRING}{BLACK}!
# példa: Már nem vehetsz több 'buszt'!
STR_POSSIBLE_ERROR_NOT_MORE_PURCHASE				:Már nem vehetsz több {GREEN}{STRING}{STRING}{BLACK}!
# példa: Nem építhetsz csak '1' 'fűtőházat' az egész játék során!
STR_POSSIBLE_ERROR_NUM_BUILD						:Nem építhetsz csak {GREEN}{NUM}{NBSP}{STRING} {BLACK}az egész játék során!
# példa: Nem vehetsz csak '1' 'buszt' az egész játék során!
STR_POSSIBLE_ERROR_NUM_PURCHASE						:Nem vehetsz csak {GREEN}{NUM}{NBSP}{STRING} {BLACK}az egész játék során!
# példa: Még építhetsz '5' 'fűtőházat'!
STR_POSSIBLE_NUM_BUILD								:Még építhetsz {GREEN}{NUM}{NBSP}{STRING}{BLACK}!
# példa: Még vehetsz '5' 'buszt'!
STR_POSSIBLE_NUM_PURCHASE							:Még vehetsz {GREEN}{NUM}{NBSP}{STRING}{BLACK}!
STR_POSSIBLE_EMPTY									:{STRING}
# példa: Most már építhetsz bármennyi 'fűtőházat'!
STR_POSSIBLE_MORE_BUILD								:Most már építhetsz bármennyi {STRING}{STRING}!
# példa: Most már vehetsz bármennyi 'buszt'!
STR_POSSIBLE_MORE_PURCHASE							:Most már vehetsz bármennyi {STRING}{STRING}!

# Can't build
STR_BUILD_ERROR_PLACE								:Nem építheted át máshová az épületet, csak az eredeti helyére építheted!

STR_BUILD_ERROR_MORE_RAILDEPOS						:Nem építhetsz vasúti fűtőházat!
STR_BUILD_ERROR_MORE_ROADDEPOS						:Nem építhetsz garázst!
STR_BUILD_ERROR_MORE_WATERDEPOS						:Nem építhetsz dokkot!
STR_BUILD_ERROR_MORE_RAILSTATIONS					:Nem építhetsz vasútállomást!
STR_BUILD_ERROR_MORE_TRUCKSTOPS						:Nem építhetsz teherautó-rakoróhelyet!
STR_BUILD_ERROR_MORE_BUSSTOPS						:Nem építhetsz buszmegállót!
STR_BUILD_ERROR_MORE_WATERDOCKS						:Nem építhetsz kikötőt!
STR_BUILD_ERROR_MORE_AIRPORTS						:Nem építhetsz repülőteret!

# példa: Nem építhetsz csak '5' 'fűtőházat'.
STR_BUILD_ERROR_NUM_RAILDEPOS						:Nem építhetsz csak {NUM} vasúti fűtőházat!
STR_BUILD_ERROR_NUM_ROADDEPOS						:Nem építhetsz csak {NUM} garázst!
STR_BUILD_ERROR_NUM_WATERDEPOS						:Nem építhetsz csak {NUM} dokkot!
STR_BUILD_ERROR_NUM_RAILSTATIONS					:Nem építhetsz csak {NUM} vasútállomást!
STR_BUILD_ERROR_NUM_TRUCKSTOPS						:Nem építhetsz csak {NUM} teherautó-rakoróhelyet!
STR_BUILD_ERROR_NUM_BUSSTOPS						:Nem építhetsz csak {NUM} buszmegállót!
STR_BUILD_ERROR_NUM_WATERDOCKS						:Nem építhetsz csak {NUM} kikötőt!
STR_BUILD_ERROR_NUM_AIRPORTS						:Nem építhetsz csak {NUM} repülőteret!


# Can't purchase
STR_PURCHASE_ERROR_MORE_RAILVEHICLES				:Nem vehetsz mozdonyt!
STR_PURCHASE_ERROR_MORE_TRUCKVEHICLES				:Nem vehetsz teherautót!
STR_PURCHASE_ERROR_MORE_BUSVEHICLES					:Nem vehetsz buszt!
STR_PURCHASE_ERROR_MORE_WATERVEHICLES				:Nem vehetsz hajót!
STR_PURCHASE_ERROR_MORE_AIRVEHICLES					:Nem vehetsz repülőgépet!


# példa: Nem vehetsz csak '5' 'mozdonyt'.
STR_PURCHASE_ERROR_NUM_RAILVEHICLES					:Nem vehetsz csak {NUM} mozdonyt!
STR_PURCHASE_ERROR_NUM_TRUCKVEHICLES				:Nem vehetsz csak {NUM} teherautót!
STR_PURCHASE_ERROR_NUM_BUSVEHICLES					:Nem vehetsz csak {NUM} buszt!
STR_PURCHASE_ERROR_NUM_WATERVEHICLES				:Nem vehetsz csak {NUM} hajót!
STR_PURCHASE_ERROR_NUM_AIRVEHICLES					:Nem vehetsz csak {NUM} repülőgépet!




STR_VEHICLE_BUS										:busz
STR_VEHICLE_BUS_OBJ_CASE							:buszt
STR_VEHICLE_TRUCK									:teherautó
STR_VEHICLE_TRUCK_OBJ_CASE							:teherautót
STR_VEHICLE_TRAIN									:vonat
STR_VEHICLE_TRAIN_OBJ_CASE							:vonatot
STR_VEHICLE_SHIP									:hajó
STR_VEHICLE_SHIP_OBJ_CASE							:hajót
STR_VEHICLE_AIRPLANE								:repülőgép
STR_VEHICLE_AIRPLANE_OBJ_CASE						:repülőgépet



STR_BUILDING_RAILDEPOS								:fűtőház
STR_BUILDING_RAILDEPOS_OBJ_CASE						:fűtőházat
STR_BUILDING_ROADDEPOS								:garázs
STR_BUILDING_ROADDEPOS_OBJ_CASE						:garázst
STR_BUILDING_WATERDEPOS								:dokk
STR_BUILDING_WATERDEPOS_OBJ_CASE					:dokkot
STR_BUILDING_RAILSTATIONS							:vasútállomás
STR_BUILDING_RAILSTATIONS_OBJ_CASE					:vasútállomást
STR_BUILDING_TRUCKSTOPS								:teherautó-rakodóhely
STR_BUILDING_TRUCKSTOPS_OBJ_CASE					:teherautó-rakodóhelyet
STR_BUILDING_BUSSTOPS								:buszmegálló
STR_BUILDING_BUSSTOPS_OBJ_CASE						:buszmegállót
STR_BUILDING_WATERDOCKS								:kikötő
STR_BUILDING_WATERDOCKS_OBJ_CASE					:kikötőt
STR_BUILDING_AIRPORTS								:repülőtér
STR_BUILDING_AIRPORTS_OBJ_CASE						:repülőteret


# ADMIN PORThoz tartozo uzenetek
STR_ADMINPORT_COMPANY_WEBKEY						:A játékba bejelentkezéskor a következő kulcsot kell megadnod: {NUM}
STR_ADMINPORT_SUCCESS_JOIN							:Sikeres csatlakozás a szerverhez!


# settings window
STR_FELADATOK_BEALLITASAI							:Feladatok beállítása

