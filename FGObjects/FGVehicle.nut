class FGVehicle {
	vehicle_id = null;
	cargos_list = null;
	
	constructor (a_vehicle_id) {
		this.vehicle_id = a_vehicle_id;
		this.cargos_list = [];
	}
}

function FGVehicle::CheckCargos () {
	local cargos = GSCargoList();
	foreach (cargo_id, _ in cargos) {
		local van = false;
		
		if (GSVehicle.GetCapacity(this.vehicle_id, cargo_id) < 1)
			van = true;
			
		local counter = 0;
		foreach (cargo in this.cargos_list) {
			if (cargo.cargo_id == cargo_id) {
				if (van) {
					// ez csak akkor lehet, ha meg a cargot szamolta, de mar nem kell szamolni
					// ilyenkor mar nem kell figyelni, is eltavolitjuk
					this.cargos_list.remove(counter);
				}
				
				van = true;
				break;
			}
			counter++;
		}
		
		if (!van) {
			/* maskent kellene hozzaadni a cargo-t. Egyebkent jelenleg az egesz vehicle osztaly nincs hasznalatban...
			local cargo = FGCargo(cargo_id);
			this.cargos_list.append(cargo);
			GSLog.Info("[FGVehicle::CheckCargos] cargo: " + GSCargo.GetCargoLabel(cargo_id) + " added");
			 */
		}
	}
}
