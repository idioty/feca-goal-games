class FGTownGrowthGoal extends FGBaseGoal {
	// class variables
	// Goal types
	static TS_INVALID = 0;
	static TS_VILLAGE = 1;
	static TS_TOWN = 2;
	static TS_CITY = 3;
	static TS_METROPOLIS = 4;
	static TS_SUPER_METROPOLIS = 5;
	static TS_CUSTOM = 6;

	static TPS_CUSTOM_VALUE = 5; // ebben azt tarolom, hogy a "townGrowthGoalSettingsMinDifficulty es townGrowthGoalSettingsMaxDifficulty" beallitas hanyadik erteke a customnek. Jelenleg 5. az utolso

	// TownPopulation types
	static TP_VILLAGE = 1000;
	static TP_TOWN = 2000;
	static TP_CITY = 5000;
	static TP_METROPOLIS = 8000;
	static TP_SUPER_METROPOLIS = 12000;
	
	static function GetTypeText(goal);
	static function GetPopulation(goal);
	static function GetPopulationFromType(goal_type);
	
	// instance variables
	town_id = null;
	goal_town_type = 0;
	isCompleted = false;

	custom_goal_population = 0;

	constructor (a_goal_id, a_mainnut, Goal_Town_Type, TownID) {
		if (!GSTown.IsValidTown(TownID)) {
			GSLog.Error("[FGTownGrowthGoal::constructor] Can not create FGTownGrowthGoal goal, because invalid TodnID: " + TownID + "!");
			return;
		}

		this.goal_id = a_goal_id;
		this.goal_type = FGBaseGoal.GoalTypes.GT_TOWNGROWTH;
		this.goal_weak = false;
		
		this.goal_town_type = Goal_Town_Type;
		
		this.town_id = TownID;
		this.goal_gt_type = GSGoal.GT_TOWN;
		this.goal_gt_destintation = TownID;

		if (a_mainnut != null) {
			this.mainnut = a_mainnut;
			this.goal_begin_month = this.mainnut.GetMonthsCountFromStarted();
		}
	}
	
	function Copy(goal) {
		local new_goal = FGTownGrowthGoal(goal.goal_id, goal.mainnut, goal.goal_type, goal.town_id);
		new_goal.goal_company_id = goal.goal_company_id;
		new_goal.goal_main = goal.goal_main;
		new_goal.goal_award = goal.goal_award;
		new_goal.goal_weak = goal.goal_weak;
		new_goal.goal_type = goal.goal_type;
		
		new_goal.goal_town_type = goal.goal_town_type;
		new_goal.isCompleted = goal.isCompleted;
		
		new_goal.goal_difficulty = goal.goal_difficulty;
		new_goal.goal_settings = goal.goal_settings;
		new_goal.goal_begin_month = goal.goal_begin_month;
		return new_goal;
	}
}

// - # class functions
function FGTownGrowthGoal::GetTypeText(goal) {
	switch (goal.goal_town_type) {
		case FGTownGrowthGoal.TS_VILLAGE:
			return GSText(GSText.STR_TOWN_VILLAGE);
			break;
		case FGTownGrowthGoal.TS_TOWN:
			return GSText(GSText.STR_TOWN_TOWN);
			break;
		case FGTownGrowthGoal.TS_CITY:
			return GSText(GSText.STR_TOWN_CITY);
			break;
		case FGTownGrowthGoal.TS_METROPOLIS:
			return GSText(GSText.STR_TOWN_METROPOLIS);
			break;
		case FGTownGrowthGoal.TS_SUPER_METROPOLIS:
			return GSText(GSText.STR_TOWN_SUPER_METROPOLIS);
			break;
		case FGTownGrowthGoal.TS_CUSTOM:
			if (this.custom_goal_population <= FGTownGrowthGoal.TP_VILLAGE)
				return GSText(GSText.STR_TOWN_VILLAGE);
			else if (this.custom_goal_population <= FGTownGrowthGoal.TP_TOWN)
				return GSText(GSText.STR_TOWN_TOWN);
			else if (this.custom_goal_population <= FGTownGrowthGoal.TP_CITY)
				return GSText(GSText.STR_TOWN_CITY);
			else if(this.custom_goal_population <= FGTownGrowthGoal.TP_METROPOLIS)
				return GSText(GSText.STR_TOWN_METROPOLIS);
			else
				return GSText(GSText.STR_TOWN_SUPER_METROPOLIS);
			break;
	}
	
	return null;
}

function FGTownGrowthGoal::GetPopulation(goal) {
	if (goal.goal_town_type == FGTownGrowthGoal.TS_CUSTOM)
		return goal.custom_goal_population;

	return FGTownGrowthGoal.GetPopulationFromType(goal.goal_town_type);
}

function FGTownGrowthGoal::GetPopulationFromType(goal_type) {
	switch (goal_type) {
		case FGTownGrowthGoal.TS_VILLAGE:
			return FGTownGrowthGoal.TP_VILLAGE;
			break;
		case FGTownGrowthGoal.TS_TOWN:
			return FGTownGrowthGoal.TP_TOWN;
			break;
		case FGTownGrowthGoal.TS_CITY:
			return FGTownGrowthGoal.TP_CITY;
			break;
		case FGTownGrowthGoal.TS_METROPOLIS:
			return FGTownGrowthGoal.TP_METROPOLIS;
			break;
		case FGTownGrowthGoal.TS_SUPER_METROPOLIS:
			return FGTownGrowthGoal.TP_SUPER_METROPOLIS;
			break;
		case FGTownGrowthGoal.TS_CUSTOM: // ez itt elvileg nem is lehetseges
			return FGTownGrowthGoal.TP_CITY;
			break;
	}
	
	return null;
}

// - # instance functions
// - # save, load
function FGTownGrowthGoal::SaveToTable() {
	return {
		// Base class objects
		goal_id = this.goal_id,
		
		goal_main = this.goal_main,
		goal_award = this.goal_award,
		goal_weak = this.goal_weak,
		goal_type = this.goal_type,
		goal_difficulty = this.goal_difficulty,

		custom_goal_population = this.custom_goal_population,

		goal_company_id = this.goal_company_id,
		
		goal_best = this.goal_best,
		goal_last_best = this.goal_last_best,
		goal_last_actual = this.goal_last_actual,
		goal_last_best_percent = this.goal_last_best_percent,
		
		goal_begin_month = this.goal_begin_month,
		
		// Current class objects
		goal_town_type = this.goal_town_type,
		town_id = this.town_id,
		isCompleted = this.isCompleted,
	}
}

function FGTownGrowthGoal::LoadFromTable(table, version, goalsettings) {
	this.goal_settings = goal_settings;
	
	// Base class objects
	this.goal_id = table.goal_id;
	
	this.goal_main = table.goal_main;
	this.goal_award = table.goal_award;
	this.goal_weak = table.goal_weak;
	this.goal_type = table.goal_type;
	this.goal_difficulty = table.goal_difficulty;

	this.custom_goal_population = table.custom_goal_population;

	this.goal_company_id = table.goal_company_id;
	
	this.goal_best = table.goal_best;
	this.goal_last_best = table.goal_last_best;
	this.goal_last_actual = table.goal_last_actual;
	this.goal_last_best_percent = table.goal_last_best_percent;
	
	this.goal_begin_month = table.goal_begin_month;
	
	// Current class objects
	this.goal_town_type = table.goal_town_type;
	this.town_id = table.town_id;
	this.isCompleted = table.isCompleted;
}

// - # goal basic
function FGTownGrowthGoal::Description() {
	return "goal_id: " + goal_id + ", company: " + GSCompany.GetName(this.goal_company_id) + ", goal_type: FGTownGrowthGoal, town: " + GSTown.GetName(this.town_id) + ", goal population: " + this.GetPopulation(this);
}

function FGTownGrowthGoal::SetGoalSettings(settings) {
	this.goal_settings = settings;
}

function FGTownGrowthGoal::UpdateGoal() {
	if (this.town_id == null || !GSTown.IsValidTown(this.town_id)) {
		GSLog.Warning("[FGTownGrowthGoal::UpdateGoal] invalid town.");
	}
	
	local goal_pop = FGTownGrowthGoal.GetPopulation(this);
	if (goal_pop == null) {
		GSLog.Warning("[FGTownGrowthGoal::UpdateGoal] can not load population.");
	}
	
	local town_pop = GSTown.GetPopulation(this.town_id);
	
	if (town_pop > this.goal_best) {
		this.goal_best = town_pop;
		
		if (town_pop >= goal_pop)
			this.isCompleted = true;
	}
}

// - # goal strings
function FGTownGrowthGoal::StrGoalText() {
	if (this.town_id == null || !GSTown.IsValidTown(this.town_id)) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalText] invalid town.");
		return null;
	}
	
	local townTypeText = FGTownGrowthGoal.GetTypeText(this);
	if (townTypeText == null) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalText] can not load goal text.");
		return null;
	}
	
	local townSize = FGTownGrowthGoal.GetPopulation(this);
	if (townSize == null) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalText] can not load population.");
		return null;
	}
	
	if (this.mainnut.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0))
		return GSText(GSText.STR_TOWN_GROWTH_GOAL, this.town_id, townTypeText, townSize);
	else
		return GSText(GSText.STR_TOWN_GROWTH_GOAL_PERCENT, this.town_id, townTypeText, townSize, this.GetActualGoalPercent(), this.GetMaxGoalPercent());
}

function FGTownGrowthGoal::StrGoalCompletedTextMe() {
	if (this.town_id == null || !GSTown.IsValidTown(this.town_id)) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalCompletedTextMe] invalid town.");
		return null;
	}
	
	local townTypeText = FGTownGrowthGoal.GetTypeText(this);
	if (townTypeText == null) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalCompletedTextMe] can not load goal text.");
		return null;
	}
	
	local townSize = FGTownGrowthGoal.GetPopulation(this);
	if (townSize == null) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalCompletedTextMe] can not load population.");
		return null;
	}
	
	return GSText(GSText.STR_TOWN_GROWTH_GOAL_COMPLETED_STORY_ME, this.town_id, townTypeText, townSize);
}

function FGTownGrowthGoal::StrGoalCompletedTextCompany(company) {
	if (company.company_id == null || GSCompany.ResolveCompanyID(company.company_id) == GSCompany.COMPANY_INVALID) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalCompletedTextCompany] invalid company.");
		return null;
	}
	
	if (this.town_id == null || !GSTown.IsValidTown(this.town_id)) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalCompletedTextCompany] invalid town.");
		return null;
	}
	
	local townTypeText = FGTownGrowthGoal.GetTypeText(this);
	if (townTypeText == null) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalCompletedTextCompany] can not load goal text.");
		return null;
	}
	
	local townSize = FGTownGrowthGoal.GetPopulation(this);
	if (townSize == null) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalCompletedTextCompany] can not load population.");
		return null;
	}
	
	return GSText(GSText.STR_TOWN_GROWTH_GOAL_COMPLETED_STORY_COMPANY, company.company_id, this.town_id, townTypeText, townSize);
}

function FGTownGrowthGoal::StrGoalCompletedTextCompanyEventPage() {
	if (this.town_id == null || !GSTown.IsValidTown(this.town_id)) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalCompletedTextCompany] invalid town.");
		return null;
	}

	local townTypeText = FGTownGrowthGoal.GetTypeText(this);
	if (townTypeText == null) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalCompletedTextCompany] can not load goal text.");
		return null;
	}

	local townSize = FGTownGrowthGoal.GetPopulation(this);
	if (townSize == null) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalCompletedTextCompany] can not load population.");
		return null;
	}

	return GSText(GSText.STR_COMPANY_EVENTS_TOWN_GROWTH_GOAL_COMPLETED, this.town_id, townTypeText, townSize);
}

// - # goal state
function FGTownGrowthGoal::GetActualGoalPercent() {
	if (this.isCompleted)
		return 100;
	
	if (this.town_id == null || !GSTown.IsValidTown(this.town_id)) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalText] invalid town.");
		return 0;
	}
	
	local goal_pop = FGTownGrowthGoal.GetPopulation(this);
	if (goal_pop == null) {
		GSLog.Warning("[FGTownGrowthGoal::StrGoalText] can not load population.");
		return 0;
	}
	
	local town_pop = GSTown.GetPopulation(this.town_id);
	
	local eredmeny = town_pop * 100 / goal_pop;
	eredmeny = eredmeny.tointeger();
	
	if (eredmeny > 100) // feladat eltavolitasa elott meg szamolja a hozzaadott cuccokat, ezert fentebb mehet...
		eredmeny = 100;
	
	if (eredmeny < 0)
		eredmeny = 0;
	
	return eredmeny;
}

function FGTownGrowthGoal::GetMaxGoalPercent() {
	if (this.isCompleted)
		return 100;
	
	local goal_pop = FGTownGrowthGoal.GetPopulation(this);
	if (goal_pop == null) {
		GSLog.Warning("[FGTownGrowthGoal::GetMaxGoalPercent] can not load population.");
		return 0;
	}
	
	local eredmeny = this.goal_best * 100 / goal_pop;
	eredmeny = eredmeny.tointeger();
	
	if (eredmeny > 100) // feladat eltavolitasa elott meg szamolja a hozzaadott cuccokat, ezert fentebb mehet...
		eredmeny = 100;
	
	if (eredmeny < 0)
		eredmeny = 0;
	
	return eredmeny;
}

function FGTownGrowthGoal::IsGoalCompleted() {
	return this.isCompleted;
}

