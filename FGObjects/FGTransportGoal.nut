class FGTransportGoal extends FGBaseGoal {
	static function IsAvailableGoalOnLevel(level, transportGoalSettings);
	
	
	cargo = null;
	cargo_id = null;
	months = null; // array, ebben taroljuk a honapokban elszallitott cuccokat
	amount = 0;
	month = 0;	// hany honap alatt kell kitermelni, ha 0, akkor barmennyi honap alatt
	
	
	constructor (a_goal_id, a_mainnut) {
		this.goal_id = a_goal_id;
		this.goal_type = FGBaseGoal.GoalTypes.GT_TRANSPORT;
		this.goal_weak = false;
		this.cargo_id = null;
		this.months = [];
		this.amount = 0;
		this.month = 0;
		this.goal_gt_type = GSGoal.GT_NONE;
		this.goal_gt_destintation = 0;
		if (a_mainnut != null) {
			this.mainnut = a_mainnut;
			this.goal_begin_month = this.mainnut.GetMonthsCountFromStarted();
		}
	}
	
	function Copy(goal) {
		local new_goal = FGTransportGoal(goal.goal_id, null);
		new_goal.SetCargo(goal.cargo);
		new_goal.goal_type = goal.goal_type;
		new_goal.goal_award = goal.goal_award;
		new_goal.goal_difficulty = goal.goal_difficulty;
		new_goal.goal_weak = goal.goal_weak;
		new_goal.goal_settings = goal.goal_settings;
		new_goal.cargo_id = goal.cargo_id;
		new_goal.amount = goal.amount;
		new_goal.month = goal.month;
		new_goal.mainnut = goal.mainnut;
		new_goal.goal_begin_month = goal.goal_begin_month;
		return new_goal;
	}
}

function FGTransportGoal::SaveToTable() {
	return {
		// Base class objects
		goal_id = this.goal_id,

		goal_main = this.goal_main,
		goal_weak = this.goal_weak,
		goal_type = this.goal_type,
		goal_award = this.goal_award,
		goal_difficulty = this.goal_difficulty,

		goal_company_id = this.goal_company_id,

		goal_best = this.goal_best,
		goal_last_best = this.goal_last_best,
		goal_last_actual = this.goal_last_actual,
		goal_last_best_percent = this.goal_last_best_percent,
		
		goal_begin_month = this.goal_begin_month,
		
		// Current class objects
		cargo_id = this.cargo_id,
		goal_weak = this.goal_weak,
		months = this.months,
		amount = this.amount,
		month = this.month,
	}
}

function FGTransportGoal::LoadFromTable(table, version, cargo, goalsettings) {
	this.cargo = cargo;
	this.goal_settings = goal_settings;
	
	// Base class objects
	this.goal_id = table.goal_id;
	
	this.goal_main = table.goal_main;
	this.goal_weak = table.goal_weak;
	this.goal_type = table.goal_type;
	this.goal_award = table.goal_award;
	this.goal_difficulty = table.goal_difficulty;

	this.goal_company_id = table.goal_company_id;

	this.goal_best = table.goal_best;
	this.goal_last_best = table.goal_last_best;
	this.goal_last_actual = table.goal_last_actual;
	this.goal_last_best_percent = table.goal_last_best_percent;
	
	this.goal_begin_month = table.goal_begin_month;
	
	// Current class objects
	this.cargo_id = table.cargo_id;
	this.goal_weak = table.goal_weak;
	this.months = table.months;
	this.amount = table.amount;
	this.month = table.month;
}

function FGTransportGoal::Description() {
	return "goal_id: " + goal_id + ", company: " + GSCompany.GetName(this.goal_company_id) + ", goal_type: FGTransportGoal, cargo: " + GSCargo.GetCargoLabel(this.cargo_id) + ", amount: " + this.amount + ", month: " + this.month;
}

// - # init
function FGTransportGoal::SetGoalID(a_goal_id) {
	this.goal_id = a_goal_id;
}

function FGTransportGoal::SetCargo(a_cargo) {
	this.cargo = a_cargo;
}

function FGTransportGoal::SetForAll(forall) {
	this.goal_forall = forall;
}

function FGTransportGoal::SetGoalSettings(settingsTable) {
	this.goal_settings = settingsTable;
}

// - # alap funkciok
function FGTransportGoal::CheckMonthList(current_month) {
	for (local i = this.months.len(); i <= current_month; i++) 
		this.months.append(0);
}

function FGTransportGoal::GetCurrentMonthFromStart() {
	local current_month = this.mainnut.GetMonthsCountFromStarted();
	
	// a nap elejen fut le a script, ezert ha elsejen kerdezzuk le az infot, akkor az meg az elozo honapra vonatkozik
	// (szinte lehetetlen, hogy (current_month == 0 && a day == 1), mert nem hiszem, hogy barki mar a jatek elso napjan tudna szallitani :)
	// ezert nincs benne az ellenorzesnel, hogy minel gyorsabb legyen a script
	local day = this.mainnut.GetCurrentDay();
	if (day == 1)
		current_month--;
	
	return current_month;
}

function FGTransportGoal::AddTransportedCargo(transported_amount) {
	local current_month = this.GetCurrentMonthFromStart();
	this.CheckMonthList(current_month);
	
	local current = this.months[current_month];
	current += transported_amount;
	
	this.months[current_month] = current;
	
	// frissitjuk a legjobb eredmenyt, hogy tudjuk lekerdezni az aktualis allapotot
	local goalTransported = this.GetGoalTransportedCargo();
	if (goalTransported > this.goal_best)
		this.goal_best = goalTransported;

	// leellenorizzuk, hogy nem szallitott-e tobbet, mint kellene
	// es ki van kapcsolva az egyszerre frissites
	if (goalTransported > this.amount && !this.mainnut.transportGoalSettings.transportGoalSettingsUpdateAll) {
		local company = this.mainnut.GetCompany(this.goal_company_id);
		if (company != null) {
			local amountOfPlus = goalTransported - this.amount;
			company.AddPlusTransportedCargo(this.cargo_id, amountOfPlus);
			GSLog.Warning("tobbet szallitott a kis geci :)");
			GSLog.Warning("transported: " + transported_amount + ", goalTransported: " + goalTransported + ", amount: " + this.amount + ", plus: " + amountOfPlus);
			GSLog.Warning("descrpition: " + this.Description());
			GSLog.Warning("--------------------");
		}
		else {
			GSLog.Warning("[FGTransportGoal::AddTransportedCargo] Invalid company_id(" + this.goal_company_id + ") of goal: " + this.Description());
		}
	}
}

// visszaadjuk a szazalekot, hogy hol jarunk a feladat teljesiteseben
function FGTransportGoal::GetMaxGoalPercent() {
	if (this.amount == 0)
		return 0;
	
	local eredmeny = this.goal_best * 100 / this.amount;
	eredmeny = eredmeny.tointeger();
	
	if (eredmeny > 100) // feladat eltavolitasa elott meg szamolja a hozzaadott cuccokat, ezert fentebb mehet...
		eredmeny = 100;
	
	if (eredmeny < 0)
		eredmeny = 0;
	
	return eredmeny;
}

function FGTransportGoal::GetActualGoalPercent() {
	if (this.amount == 0)
		return 0;
	
	local eredmeny = this.GetGoalTransportedCargo() * 100 / this.amount;
	eredmeny = eredmeny.tointeger();
	
	if (eredmeny > 100) // feladat eltavolitasa elott meg szamolja a hozzaadott cuccokat, ezert fentebb mehet...
		eredmeny = 100;
	
	if (eredmeny < 0)
		eredmeny = 0;
	
	return eredmeny;
}

function FGTransportGoal::IsGoalCompleted() {
	local current_month = this.GetCurrentMonthFromStart();
	this.CheckMonthList(current_month);
	
	local transported_amount = 0;
	local begin = 0;
	if (this.month > 0)
		begin = current_month - (this.month - 1);
	
	if (begin < 0) // az elejen biztos kisebb lesz, mint 0
		begin = 0;
		
	for (local i = begin; i <= current_month; i++)
		transported_amount += this.months[i];
		
	return transported_amount >= this.amount;
}

// - # get goals
/**
 goal kezdete ota osszegyujtott cuccrol van szo
 **/
function FGTransportGoal::GetAllTransportedCargo() {
	local current_month = this.GetCurrentMonthFromStart();
	this.CheckMonthList(current_month);
	
	local transported_amount = 0;
	local i = 0;
	for (i = 0; i <= current_month; i++)
		transported_amount += this.months[i];
		
	return transported_amount;
}

/**
 feladat ideje alatt osszegyujtott cuccrol van szo
 pontosabban, a tomb utolso this.month osszeadasarol van szo, ha a this month nagyobb, mint 0
 **/
function FGTransportGoal::GetGoalTransportedCargo() {
	if (this.months.len() == 0)
		return 0;
	
	local current_month = this.GetCurrentMonthFromStart();
	this.CheckMonthList(current_month);
	
	local transported_amount = 0;
	local begin = 0;
	if (this.month > 0)
		begin = current_month - (this.month - 1);
	
	if (begin < 0)
		begin = 0;
		
	for (local i = begin; i <= current_month; i++) {
		if ((this.months.len() - 1) < i)
			break;
		transported_amount += this.months[i];
	}
		
	return transported_amount;
}

/**
 utolso months honapban osszegyujtott cuccrol van szo
 current_month-ban megadhatunk egy korabbi honapot is termeszetesen, igy azelotti es akkori honapokban osszegyujtott cuccrol van szo
 **/
function FGTransportGoal::GetGoalTransportedCargoMonths(months) {
	local current_month = this.GetCurrentMonthFromStart();
	this.CheckMonthList(current_month);
	months--; // igazabol ha 1-et irunk, akkor csak az utolso kell, ami a kivonas utan lesz utolso
	if (months > current_month)
		months = current_month;
	
	local transported_amount = 0;
	local begin = current_month - months;
	
	if (begin < 0)
		begin = 0;
	
	for (local i = begin; i <= current_month; i++) {
		if ((this.months.len() - 1) < i)
			break;
		transported_amount += this.months[i];
	}
	
	return transported_amount;
}

// - # szovegek, Szallitasi feladat
function FGTransportGoal::StrGetTransportCargoGoalCompleted(company) {
	local feladat = this.StrTransportedCargo();
	
	if (this.goal_main)
		return GSText(GSText.STR_GOAL_COMPANY_COMPLETED_MAIN, company.company_id, feladat);
	else if (this.goal_weak)
		return GSText(GSText.STR_GOAL_COMPANY_COMPLETED_WEAK, company.company_id, feladat);
	else
		return GSText(GSText.STR_GOAL_COMPANY_COMPLETED_AWARD, company.company_id, feladat);
	
	
	return null;
}
function FGTransportGoal::StrGetTransportCargoGoalCompletedEventStoryPage() {
	//	local awardtypetext = null;
	local feladat = this.StrTransportedCargo();

	if (this.goal_main)
		return GSText(GSText.STR_COMPANY_EVENTS_GOAL_COMPANY_COMPLETED_MAIN, feladat);
	else if (this.goal_weak)
		return GSText(GSText.STR_COMPANY_EVENTS_GOAL_COMPANY_COMPLETED_WEAK, feladat);
	else
		return GSText(GSText.STR_COMPANY_EVENTS_GOAL_COMPANY_COMPLETED_AWARD, feladat);


	return null;
}

function FGTransportGoal::StrTransportCargoGoal(isStoryBook) {
	if (this.amount == null)
		return null;

	local aam = this.amount.tointeger();
	local text = null;
	if (isStoryBook || (this.goal_main && this.mainnut.versionController.IsCurrentVersionLessThan(1, 4, 0))) {
		if (month > 0)
			text = GSText(GSText.STR_GOAL_TRANSPORT_MONTHS_MAIN, this.cargo_id, aam, this.month, this.GetActualGoalPercent(), this.GetMaxGoalPercent());
		else
			text = GSText(GSText.STR_GOAL_TRANSPORT_MAIN, this.cargo_id, aam, this.GetActualGoalPercent(), this.GetMaxGoalPercent());
	}
	else {
		if (month > 0)
			text = GSText(GSText.STR_GOAL_TRANSPORT_MONTHS, this.cargo_id, aam, this.month);
		else
			text = GSText(GSText.STR_GOAL_TRANSPORT, this.cargo_id, aam);
	}
	
	return text;
}

/*
 Utolso nehany honap alatt kitermelt szovegrol van szo
 Ha month 0, akkor osszesen elszallitott dologrol van sz
 */
function FGTransportGoal::StrTransportedCargo() {
	if (this.amount == null)
		return null;
	
	local aam = this.amount.tointeger();

	local text = null;
	if (month > 0) {
		text = GSText(GSText.STR_GOAL_TRANSPORTED_MONTHS, this.cargo_id, aam, this.month);
	}
	else {
		text = GSText(GSText.STR_GOAL_TRANSPORTED, this.cargo_id, aam);
	}
	
	return text;
}

function FGTransportGoal::StrTransportedMeCargo(withMount) {
	if (this.amount == null)
		return null;
	
	local aam = this.amount.tointeger();
	local text = null;
	if (withMount && (month > 0)) {
		text = GSText(GSText.STR_GOAL_TRANSPORTED_MONTHS_ME, this.cargo_id, aam, this.month);
	}
	else {
		text = GSText(GSText.STR_GOAL_TRANSPORTED_ME, this.cargo_id, aam);
	}
	
	return text;
}

// random goal generators
// - # random goal generators
function FGTransportGoal::GetRandomTransportGoalType(firstgoal, level) {
	if (this.cargo == null) {
		GSLog.Error("[FGTransportGoal::GetRandomTransportGoalType] this.cargo is null");
		return null;
	}
	
	local availableList = cargo.AvailableListInGameWithSettings(this.mainnut.transportGoalSettings, level, true);
	if (availableList == null) {
		/* 
		 * ha ures lenne, akkor is null jon vissza, igy csinaltam meg...,
		 * na most mar nem jon vissza null, mert ha null vagy ures lenne,
		 akkor a palya szerint elerheto osszes cargo jon vissza, hogy legyen mindenkeppen feladat
		 */
		GSLog.Error("[FGTransportGoal::GetRandomTransportGoalType] No available list for cargos");
		return null;
	}
	
	return availableList[GSBase.RandRange(availableList.len())];
}

// difficulty = 0..4 (0 nagyon konnyu.. 4 nagyon nehez
function FGTransportGoal::GetTransportGoalValueWithDifficulty(difficulty, firstgoal, level) {
	local cargoid = this.GetRandomTransportGoalType(firstgoal, level);
	
	return this.GetTransportGoalValueWithDifficultyAndCargoID(difficulty, cargoid);
}

function FGTransportGoal::GetTransportGoalValueWithDifficultyAndCargoID(difficulty, cargoid) {
	if (this.cargo == null || cargoid == null)
		return null;
	
	if (!GSCargo.IsValidCargo(cargoid))
		return null;
	
	local multiplier = this.cargo.CargoMultiplier(cargoid);
	if (multiplier == 0)
		return null;
	
	local min = 0;
	local max = 0;
	
	// veletleneszeruen, ha ugy van, kkor nehany szazalekkal csokkenthetjuk vagy novelhetjuk a feladatot
	local extremeMultiplier = 1.0;
	local randomvalue = GSBase.RandRange(10);
	
	if (randomvalue == 0)
		extremeMultiplier = 0.8;
	else if (randomvalue == 1)
		extremeMultiplier = 0.9;
	else if (randomvalue == 8)
		extremeMultiplier = 1.1;
	else if (randomvalue == 9)
		extremeMultiplier = 1.2;
	
	switch (difficulty) {
		case 0:
			min = 8;
			max = 12;
			break;
		case 1:
			min = 15;
			max = 18;
			break;
		case 2:
			min = 23;
			max = 27;
			break;
		case 3:
			min = 35;
			max = 43;
			break;
		case 4:
			min = 55;
			max = 65;
			break;
		default:
			min = 23;
			max = 27;
			break;
	}
	
	min *= extremeMultiplier;
	max *= extremeMultiplier;
	
	min *= multiplier;
	max *= multiplier;
	
	local vmin = min.tointeger();
	local vmax = max.tointeger();
	
	local value = vmax - vmin;
	
	value = ((GSBase.RandRange(value + 1)) + vmin) * 100;
	
	return value;
}

function FGTransportGoal::GetRandomMonthsWithDifficulty(difficulty) {
	local min = 0;
	local max = 0;
	
	switch (difficulty) {
		case 0:
			min = 9;
			max = 12;
			break;
		case 1:
			min = 7;
			max = 9;
			break;
		case 2:
			min = 5;
			max = 7;
			break;
		case 3:
			min = 3;
			max = 5;
			break;
		case 4:
			min = 1;
			max = 3;
			break;
		default:
			min = 5;
			max = 7;
			break;
	}
	
	local value = max - min;
	value = (GSBase.RandRange(value + 1)) + min;
	
	return value;
}

function FGTransportGoal::GetRandomDifficulty() {
	return GSBase.RandRange(5);
}
