/*
 
 Hasznalat:
 local award = FGAward(company, mainnut, multiplier, difficulty);
 award.Award();
 local amountOfAward = award.amount;
 local typeofaward = award.award;
*/

class FGAward {
	company = null;
	available_list = null;
	multiplier = 1; // ez a feladat alapjan levo szorzo, pl szenet konnyebb elszallitani, ezert kevesebb jutalmat kell adni...
	difficulty = -1; // 0-9 ertek, ha kisebb mint 0, akkor nincs, minel nagyobb, annal nehezebb es jobb jutalom
	mainnut = null;
	
	award = null;
	amount = 0;
	
	constructor (acompany, amainnut, amultiplier, adifficulty) {
		if (acompany == null)
			return null;
		
		this.company = acompany; // ennek a company-nak adjuk a dijat, mert tudnunk kell, hogy mi van neki letiltva
		this.multiplier = amultiplier;
		this.difficulty = adifficulty;
		this.mainnut = amainnut;
		
		this.available_list = [];
		this.CreateAvailableList();
	}
	
	static AT_SHOULD_RAILDEPOS = 0;
	static AT_SHOULD_ROADDEPOS = 1;
	static AT_SHOULD_WATERDEPOS = 2;
	
	// stations
	static AT_SHOULD_RAILSTATIONS = 3;
	static AT_SHOULD_TRUCKSTOPS = 4;
	static AT_SHOULD_BUSSTOPS = 5;
	static AT_SHOULD_WATERDOCKS = 6;
	static AT_SHOULD_AIRPORTS = 7;
	
	// vehicles
	static AT_SHOULD_RAILVEHICLES = 8;
	static AT_SHOULD_TRUCKVEHICLES = 9;
	static AT_SHOULD_BUSVEHICLES = 10;
	static AT_SHOULD_WATERVEHICLES = 11;
	static AT_SHOULD_AIRVEHICLES = 12;
	
	static function AddAwardListToCompany(company, awardlist);
	static function AddAwardToCompany(company, award_type, award_amount);
	static function GetAwardStringFromAwardType(award_type, obj_case);
}

function FGAward::CreateAvailableList() {
	if (this.company == null || this.mainnut.transportGoalSettings == null)
		return;
	
	// depos nos
	if (this.company.shouldRailDepos >= 0 && this.mainnut.initialPossibilities.awardRailDepos)
		this.available_list.append(this.AT_SHOULD_RAILDEPOS);
	
	if (this.company.shouldRoadDepos >= 0 && this.mainnut.initialPossibilities.awardRoadDepos)
		this.available_list.append(this.AT_SHOULD_ROADDEPOS);
	if (this.company.shouldWaterDepos >= 0 && this.mainnut.initialPossibilities.awardWaterDepos)
		this.available_list.append(this.AT_SHOULD_WATERDEPOS);
	
	// stations
	if (this.company.shouldRailStations >= 0 && this.mainnut.initialPossibilities.awardRailStations)
		this.available_list.append(this.AT_SHOULD_RAILSTATIONS);
	if (this.company.shouldTruckStops >= 0 && this.mainnut.initialPossibilities.awardTruckStops)
		this.available_list.append(this.AT_SHOULD_TRUCKSTOPS);
	if (this.company.shouldBusStops >= 0 && this.mainnut.initialPossibilities.awardBusStops)
		this.available_list.append(this.AT_SHOULD_BUSSTOPS);
	if (this.company.shouldWaterDocks >= 0 && this.mainnut.initialPossibilities.awardWaterDocks)
		this.available_list.append(this.AT_SHOULD_WATERDOCKS);
	if (this.company.shouldAirPorts >= 0 && this.mainnut.initialPossibilities.awardAirPorts)
		this.available_list.append(this.AT_SHOULD_AIRPORTS);
	
	// vehicles
	if (this.company.shouldRailVehicles >= 0 && this.mainnut.initialPossibilities.awardRailVehicles)
		this.available_list.append(this.AT_SHOULD_RAILVEHICLES);
	if (this.company.shouldTruckVehicles >= 0 && this.mainnut.initialPossibilities.awardTruckVehicles)
		this.available_list.append(this.AT_SHOULD_TRUCKVEHICLES);
	if (this.company.shouldBusVehicles >= 0 && this.mainnut.initialPossibilities.awardBusVehicles)
		this.available_list.append(this.AT_SHOULD_BUSVEHICLES);
	if (this.company.shouldWaterVehicles >= 0 && this.mainnut.initialPossibilities.awardWaterVehicles)
		this.available_list.append(this.AT_SHOULD_WATERVEHICLES);
	if (this.company.shouldAirVehicles >= 0 && this.mainnut.initialPossibilities.awardAirVehicles)
		this.available_list.append(this.AT_SHOULD_AIRVEHICLES);
}

// Itt a lengye, hogy ha difficulty segitsegevel allitottuk be a jatek celjat, akkor annak a difficultyhez melto jutalmat is ad,
//	de ha nem difficultyvel volt beallitva a jatek, akkor random ajandekot ad vissza,
//	tehat szerintem elsosorban ezt kell hasznalni
function FGAward::Award() {
	if (this.difficulty >= 0)
		return this.RandomAwardWithDifficulty(this.difficulty);
	else
		return this.RandomAward();
}

function FGAward::RandomAward() {
	return this.RandomAwardWithDifficulty(GSBase.RandRange(10));
}

// Ezt kell hasznalni, ha sajat magunk akarjuk megadni a difficulty-t
function FGAward::RandomAwardWithDifficulty(diff) {
	if (this.available_list.len() == 0)
		return null;
	
	this.award = this.available_list[GSBase.RandRange(this.available_list.len())];
	local aamount = this.GetAmountOfAwardWithDifficulty(this.award, diff);

	if (aamount > 0) {
		local sa = aamount * this.multiplier;
		this.amount = sa.tointeger();
	}
	else
		this.amount = aamount;
	
	if (this.amount == 0) // azert nem kisebb, mint egy, mert lehet -1 is es az jo!
		this.amount = 1;
	
	return this.amount;
}

function FGAward::GetAmountOfAwardWithDifficulty(a_award, diff) {
	if (this.mainnut.generalGoalSettings.enableChanceToAnyNumberOfAward) {
		local value = GSBase.RandRange(20);
		if (value == 0)
			return -1; // na, ekkor barmennyit vehet az ember...
	}
	
	if (a_award == null) {
		GSLog.Error("[FGAward::GetAmountOfAwardWithDifficulty] a_award az null");
		return 1;
	}
	
	if (diff < 0) {
		GSLog.Error("[FGAward::GetAmountOfAwardWithDifficulty] diff az kisebb mint 0");
		return 1;
	}
	
	local min = 1;
	local max = 1;
	
	if (diff >= 0 && diff < 3) {
		if (a_award == this.AT_SHOULD_RAILDEPOS) {
			min = 1;
			max = 2;
		}
		else if (a_award == this.AT_SHOULD_ROADDEPOS) {
			min = 1;
			max = 2;
		}
		else if (a_award == this.AT_SHOULD_WATERDEPOS) {
			min = 1;
			max = 2;
		}
		else if (a_award == this.AT_SHOULD_RAILSTATIONS) {
			min = 1;
			max = 2;
		}
		else if (a_award == this.AT_SHOULD_TRUCKSTOPS) {
			min = 1;
			max = 2;
		}
		else if (a_award == this.AT_SHOULD_BUSSTOPS) {
			min = 1;
			max = 2;
		}
		else if (a_award == this.AT_SHOULD_WATERDOCKS) {
			min = 1;
			max = 2;
		}
		else if (a_award == this.AT_SHOULD_AIRPORTS) {
			min = 1;
			max = 2;
		}
		else if (a_award == this.AT_SHOULD_RAILVEHICLES) {
			min = 2;
			max = 4;
		}
		else if (a_award == this.AT_SHOULD_TRUCKVEHICLES) {
			min = 2;
			max = 4;
		}
		else if (a_award == this.AT_SHOULD_BUSVEHICLES) {
			min = 2;
			max = 4;
		}
		else if (a_award == this.AT_SHOULD_WATERVEHICLES) {
			min = 2;
			max = 4;
		}
		else if (a_award == this.AT_SHOULD_AIRVEHICLES) {
			min = 2;
			max = 4;
		}
	}
	else if (diff >= 3 && diff < 6) {
		if (a_award == this.AT_SHOULD_RAILDEPOS) {
			min = 2;
			max = 4;
		}
		else if (a_award == this.AT_SHOULD_ROADDEPOS) {
			min = 2;
			max = 4;
		}
		else if (a_award == this.AT_SHOULD_WATERDEPOS) {
			min = 2;
			max = 4;
		}
		else if (a_award == this.AT_SHOULD_RAILSTATIONS) {
			min = 2;
			max = 4;
		}
		else if (a_award == this.AT_SHOULD_TRUCKSTOPS) {
			min = 2;
			max = 4;
		}
		else if (a_award == this.AT_SHOULD_BUSSTOPS) {
			min = 2;
			max = 4;
		}
		else if (a_award == this.AT_SHOULD_WATERDOCKS) {
			min = 2;
			max = 4;
		}
		else if (a_award == this.AT_SHOULD_AIRPORTS) {
			min = 2;
			max = 4;
		}
		else if (a_award == this.AT_SHOULD_RAILVEHICLES) {
			min = 3;
			max = 6;
		}
		else if (a_award == this.AT_SHOULD_TRUCKVEHICLES) {
			min = 3;
			max = 6;
		}
		else if (a_award == this.AT_SHOULD_BUSVEHICLES) {
			min = 4;
			max = 8;
		}
		else if (a_award == this.AT_SHOULD_WATERVEHICLES) {
			min = 4;
			max = 6;
		}
		else if (a_award == this.AT_SHOULD_AIRVEHICLES) {
			min = 3;
			max = 5;
		}
	}
	else if (diff >= 6 && diff < 9) {
		if (a_award == this.AT_SHOULD_RAILDEPOS) {
			min = 3;
			max = 6;
		}
		else if (a_award == this.AT_SHOULD_ROADDEPOS) {
			min = 3;
			max = 6;
		}
		else if (a_award == this.AT_SHOULD_WATERDEPOS) {
			min = 3;
			max = 6;
		}
		else if (a_award == this.AT_SHOULD_RAILSTATIONS) {
			min = 3;
			max = 6;
		}
		else if (a_award == this.AT_SHOULD_TRUCKSTOPS) {
			min = 3;
			max = 6;
		}
		else if (a_award == this.AT_SHOULD_BUSSTOPS) {
			min = 3;
			max = 6;
		}
		else if (a_award == this.AT_SHOULD_WATERDOCKS) {
			min = 3;
			max = 6;
		}
		else if (a_award == this.AT_SHOULD_AIRPORTS) {
			min = 3;
			max = 6;
		}
		else if (a_award == this.AT_SHOULD_RAILVEHICLES) {
			min = 4;
			max = 8;
		}
		else if (a_award == this.AT_SHOULD_TRUCKVEHICLES) {
			min = 4;
			max = 8;
		}
		else if (a_award == this.AT_SHOULD_BUSVEHICLES) {
			min = 6;
			max = 10;
		}
		else if (a_award == this.AT_SHOULD_WATERVEHICLES) {
			min = 4;
			max = 8;
		}
		else if (a_award == this.AT_SHOULD_AIRVEHICLES) {
			min = 5;
			max = 7;
		}
	}
	else {
		if (a_award == this.AT_SHOULD_RAILDEPOS) {
			min = 4;
			max = 8;
		}
		else if (a_award == this.AT_SHOULD_ROADDEPOS) {
			min = 4;
			max = 8;
		}
		else if (a_award == this.AT_SHOULD_WATERDEPOS) {
			min = 4;
			max = 8;
		}
		else if (a_award == this.AT_SHOULD_RAILSTATIONS) {
			min = 4;
			max = 8;
		}
		else if (a_award == this.AT_SHOULD_TRUCKSTOPS) {
			min = 4;
			max = 8;
		}
		else if (a_award == this.AT_SHOULD_BUSSTOPS) {
			min = 4;
			max = 8;
		}
		else if (a_award == this.AT_SHOULD_WATERDOCKS) {
			min = 4;
			max = 8;
		}
		else if (a_award == this.AT_SHOULD_AIRPORTS) {
			min = 4;
			max = 8;
		}
		else if (a_award == this.AT_SHOULD_RAILVEHICLES) {
			min = 6;
			max = 10;
		}
		else if (a_award == this.AT_SHOULD_TRUCKVEHICLES) {
			min = 6;
			max = 10;
		}
		else if (a_award == this.AT_SHOULD_BUSVEHICLES) {
			min = 8;
			max = 12;
		}
		else if (a_award == this.AT_SHOULD_WATERVEHICLES) {
			min = 6;
			max = 10;
		}
		else if (a_award == this.AT_SHOULD_AIRVEHICLES) {
			min = 6;
			max = 8;
		}
	}
	
	return (GSBase.RandRange(max - min + 1) + min);
}

/*
 ha kapott jutalmat a vallalat, akkor true ertek jon vissza
 */
function FGAward::AddAwardListToCompany(company, awardlist, goal) {
	local vanAward = false;
	local et = GSText(GSText.STR_EMPTY);
	local newawardlist = [];
	
	// eloszor vegigfuttatunk egy kerest, megnezzuk, hogy nincs-e egyforma jutalom, mert ha van, akkor ne irjuk mar ki tobbszor ugyanazt, mert felreertelmezheto
	if (awardlist != null && awardlist.len() > 0) {
		foreach (awardtable in awardlist) {
			local atype = awardtable.award_type;
			local award_amount = awardtable.award_amount;
			
			local van = false
			foreach (newtable in newawardlist) {
				local batype = newtable.award_type;
				local baward_amount = newtable.award_amount;
				
				if (atype == batype) {
					if (award_amount == -1)
						baward_amount = -1;
					else
						baward_amount += award_amount; // += kell, mert tobbszor megkapta azt a jutalmat
					
					newtable.award_amount = baward_amount;
					
					van = true;
				}
			}
			
			// ha meg nincs ilyen tipus, akkor hozzaadjuk
			if (!van)
				newawardlist.append(awardtable);
		}
	}
	
	if (this.mainnut.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0)) {
		// storybook-ba be kell kerulnie a szovegnek
		// story book eseteben rendesen kiiratjuk a feladatot honapokkal egyutt, mert kifer
		local awardTexts = [];
		
		local completedText = null; // kiirjuk, hogy sikeresen teljesitett egy feladatot
		
		if (goal.goal_main) {
			completedText = GSText(GSText.STR_COMPLETED_GOAL_MAIN);
		}
		else if (goal.goal_weak) {
			completedText = GSText(GSText.STR_COMPLETED_GOAL_WEAK);
		}
		else {
			completedText = GSText(GSText.STR_COMPLETED_GOAL_AWARD);
		}
		
		if (completedText != null)
			awardTexts.append({type = GSStoryPage.SPET_TEXT, reference = 0, text = completedText});
		
		local feladattext = null;
		
		switch (goal.goal_type) {
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				feladattext = goal.StrTransportedMeCargo(true);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				feladattext = goal.StrGoalCompletedTextMe();
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}
		
		if (feladattext != null && feladattext != "")
			awardTexts.append({type = GSStoryPage.SPET_TEXT, reference = 0, text = feladattext});
		
		if (newawardlist.len() > 0 && company.award_enabled) {
			foreach (awardtable in newawardlist) {
				local atype = awardtable.award_type;
				local award_amount = awardtable.award_amount;
				
				if (this.AddAwardToCompany(company, atype, award_amount))
					vanAward = true;
				
				local typetext = this.GetAwardStringFromAwardType(atype, true);
				
				local atext = null;
				
				if (award_amount == -1) {
					if (atype == this.AT_SHOULD_RAILDEPOS || atype == this.AT_SHOULD_ROADDEPOS ||
						atype == this.AT_SHOULD_WATERDEPOS || atype == this.AT_SHOULD_RAILSTATIONS ||
						atype == this.AT_SHOULD_TRUCKSTOPS || atype == this.AT_SHOULD_BUSSTOPS ||
						atype == this.AT_SHOULD_WATERDOCKS || atype == this.AT_SHOULD_AIRPORTS) {
						
						atext = GSText(GSText.STR_BUILD_MORE, typetext, et, et, et);
					}
					else if (atype == this.AT_SHOULD_RAILVEHICLES || atype == this.AT_SHOULD_TRUCKVEHICLES ||
							 atype == this.AT_SHOULD_BUSVEHICLES || atype == this.AT_SHOULD_WATERVEHICLES ||
							 atype == this.AT_SHOULD_AIRVEHICLES) {
						
							atext = GSText(GSText.STR_PURCHASE_MORE, typetext, et, et, et);
					}
				}
				else {
					if (award_amount < 1)
						award_amount = 1;
					
					local sh_amount = company.GetShouldMoreNum(atype);
					local max_amount = company.GetMaxMoreNum(atype);
					
					if (atype == this.AT_SHOULD_RAILDEPOS || atype == this.AT_SHOULD_ROADDEPOS ||
						atype == this.AT_SHOULD_WATERDEPOS || atype == this.AT_SHOULD_RAILSTATIONS ||
						atype == this.AT_SHOULD_TRUCKSTOPS || atype == this.AT_SHOULD_BUSSTOPS ||
						atype == this.AT_SHOULD_WATERDOCKS || atype == this.AT_SHOULD_AIRPORTS) {
						
						atext = GSText(GSText.STR_BUILD, award_amount, typetext, sh_amount, max_amount);
					}
					else if (atype == this.AT_SHOULD_RAILVEHICLES || atype == this.AT_SHOULD_TRUCKVEHICLES ||
							 atype == this.AT_SHOULD_BUSVEHICLES || atype == this.AT_SHOULD_WATERVEHICLES ||
							 atype == this.AT_SHOULD_AIRVEHICLES) {
						
						atext =  GSText(GSText.STR_PURCHASE, award_amount, typetext, sh_amount, max_amount);
					}
					
					if (atext != null)
						awardTexts.append({type = GSStoryPage.SPET_TEXT, reference = 0, text = atext});
				}
			}
		}
		
		local title = null;
		if (goal.goal_main)
			title = GSText(GSText.STR_ME_COMPLETED_MAIN_GOAL_PAGE_TITLE);
		else if (goal.goal_weak)
			title = GSText(GSText.STR_ME_COMPLETED_WEAK_GOAL_PAGE_TITLE);
		else
			title = GSText(GSText.STR_ME_COMPLETED_AWARD_GOAL_PAGE_TITLE);
		
		if (title != null && awardTexts.len() > 0)
			company.AddNewStoryPage(title, awardTexts, true);
	}
	else {
		local firstAwardText = null;
		local secondAwardText = null;
		local thirdAwardText = null;
		local counter = 0;
		
		local feladattext = null;
		
		switch (goal.goal_type) {
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				feladattext = goal.StrTransportedMeCargo(false);  // itt nem kellenek a honapok, mivel nem fer ki
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				feladattext = goal.StrGoalCompletedTextMe();
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}
		
		if (feladattext != null) {
			local awardtext = null;
			
			if (newawardlist.len() > 0 && company.award_enabled) {
				foreach (awardtable in newawardlist) {
					counter++;
					local atype = awardtable.award_type;
					local award_amount = awardtable.award_amount;
					
					if (this.AddAwardToCompany(company, atype, award_amount))
						vanAward = true;
					
					local typetext = this.GetAwardStringFromAwardType(atype, true);
					
					
					if (award_amount == -1) {
						if (atype == this.AT_SHOULD_RAILDEPOS || atype == this.AT_SHOULD_ROADDEPOS ||
							atype == this.AT_SHOULD_WATERDEPOS || atype == this.AT_SHOULD_RAILSTATIONS ||
							atype == this.AT_SHOULD_TRUCKSTOPS || atype == this.AT_SHOULD_BUSSTOPS ||
							atype == this.AT_SHOULD_WATERDOCKS || atype == this.AT_SHOULD_AIRPORTS) {
							
							switch (counter) {
								case 1:
									firstAwardText = GSText(GSText.STR_BUILD_MORE, typetext, et, et, et);
									break;
								case 2:
									secondAwardText = GSText(GSText.STR_BUILD_MORE, typetext, et, et, et);
									break;
								case 3:
									thirdAwardText = GSText(GSText.STR_BUILD_MORE, typetext, et, et, et);
									break;
							}
						}
						else if (atype == this.AT_SHOULD_RAILVEHICLES || atype == this.AT_SHOULD_TRUCKVEHICLES ||
								 atype == this.AT_SHOULD_BUSVEHICLES || atype == this.AT_SHOULD_WATERVEHICLES ||
								 atype == this.AT_SHOULD_AIRVEHICLES) {
							
							switch (counter) {
								case 1:
									firstAwardText = GSText(GSText.STR_PURCHASE_MORE, typetext, et, et, et);
									break;
								case 2:
									secondAwardText = GSText(GSText.STR_PURCHASE_MORE, typetext, et, et, et);
									break;
								case 3:
									thirdAwardText = GSText(GSText.STR_PURCHASE_MORE, typetext, et, et, et);
									break;
							}
						}
					}
					else {
						if (award_amount < 1)
							award_amount = 1;
						
						local sh_amount = company.GetShouldMoreNum(atype);
						local max_amount = company.GetMaxMoreNum(atype);
						
						if (atype == this.AT_SHOULD_RAILDEPOS || atype == this.AT_SHOULD_ROADDEPOS ||
							atype == this.AT_SHOULD_WATERDEPOS || atype == this.AT_SHOULD_RAILSTATIONS ||
							atype == this.AT_SHOULD_TRUCKSTOPS || atype == this.AT_SHOULD_BUSSTOPS ||
							atype == this.AT_SHOULD_WATERDOCKS || atype == this.AT_SHOULD_AIRPORTS) {
							
							switch (counter) {
								case 1:
									firstAwardText = GSText(GSText.STR_BUILD, award_amount, typetext, sh_amount, max_amount);
									break;
								case 2:
									secondAwardText = GSText(GSText.STR_BUILD, award_amount, typetext, sh_amount, max_amount);
									break;
								case 3:
									thirdAwardText = GSText(GSText.STR_BUILD, award_amount, typetext, sh_amount, max_amount);
									break;
							}
						}
						else if (atype == this.AT_SHOULD_RAILVEHICLES || atype == this.AT_SHOULD_TRUCKVEHICLES ||
								 atype == this.AT_SHOULD_BUSVEHICLES || atype == this.AT_SHOULD_WATERVEHICLES ||
								 atype == this.AT_SHOULD_AIRVEHICLES) {
							
							switch (counter) {
								case 1:
									firstAwardText = GSText(GSText.STR_PURCHASE, award_amount, typetext, sh_amount, max_amount);
									break;
								case 2:
									secondAwardText = GSText(GSText.STR_PURCHASE, award_amount, typetext, sh_amount, max_amount);
									break;
								case 3:
									thirdAwardText = GSText(GSText.STR_PURCHASE, award_amount, typetext, sh_amount, max_amount);
									break;
							}
						}
					}
				}
			}
			
			if (firstAwardText != null && secondAwardText != null && thirdAwardText != null)
				awardtext = GSText(GSText.STR_COMPLETED_GOAL_AWARD_TEXT, firstAwardText, secondAwardText, thirdAwardText);
			else if (firstAwardText != null && secondAwardText != null)
				awardtext = GSText(GSText.STR_COMPLETED_GOAL_AWARD_TEXT, firstAwardText, secondAwardText, et, et, et, et, et);
			else if (firstAwardText != null)
				awardtext = GSText(GSText.STR_COMPLETED_GOAL_AWARD_TEXT, firstAwardText, et, et, et, et, et, et, et, et, et, et);
			else
				awardtext = et;
			
			local text = null;
			
			if (goal.goal_main) {
				// nincs eleg valtozo, hogy kiirassuk a honapokat is, ezert inaktivak ezeke
	//			if (goal.month > 0)
	//				text = GSText(GSText.STR_COMPLETED_GOAL_WITH_AWARD_MAIN, feladattext, awardtext);
	//			else
					text = GSText(GSText.STR_COMPLETED_GOAL_WITH_AWARD_MAIN, feladattext, awardtext);
			}
			else if (goal.goal_weak) {
	//			if (goal.month > 0)
	//				text = GSText(GSText.STR_COMPLETED_GOAL_WITH_AWARD_WEAK, feladattext, awardtext);
	//			else
					text = GSText(GSText.STR_COMPLETED_GOAL_WITH_AWARD_WEAK, feladattext, awardtext);
			}
			else {
	//			if (goal.month > 0)
	//				text = GSText(GSText.STR_COMPLETED_GOAL_WITH_AWARD_AWARD, feladattext, awardtext);
	//			else
					text = GSText(GSText.STR_COMPLETED_GOAL_WITH_AWARD_AWARD, feladattext, awardtext);
			}
			
			if (text != null) {
					company.SendState(text); // regebbi rendszereken uzenetet kuldunk
			}
		}
	}
	
	if (company.award_enabled)
		company.award_enabled = company.AvailableAward();
		
	return vanAward;
}

function FGAward::AddAwardToCompany(company, award_type, award_amount) {
	if (this.mainnut.logmode)
		GSLog.Info("[FGAward::AddAwardToCompany] " + GSCompany.GetName(company.company_id) + " jutalmat kapott: " + award_type + ", mennyiseg: " + award_amount);
		
	if (award_amount == -1) {
		if (this.mainnut.logmode)
			GSLog.Info("[FGAward::AddAwardToCompany] " + GSCompany.GetName(company.company_id) + " orokre megkapta a jutalmat");
		
		switch (award_type) {
			case this.AT_SHOULD_RAILDEPOS:
				company.shouldRailDepos = -1;
				return true;
				break;
			case this.AT_SHOULD_ROADDEPOS:
				company.shouldRoadDepos = -1;
				return true;
				break;
			case this.AT_SHOULD_WATERDEPOS:
				company.shouldWaterDepos = -1;
				return true;
				break;
				
				// stations
			case this.AT_SHOULD_RAILSTATIONS:
				company.shouldRailStations = -1;
				return true;
				break;
			case this.AT_SHOULD_TRUCKSTOPS:
				company.shouldTruckStops = -1;
				return true;
				break;
			case this.AT_SHOULD_BUSSTOPS:
				company.shouldBusStops = -1;
				return true;
				break;
			case this.AT_SHOULD_WATERDOCKS:
				company.shouldWaterDocks = -1;
				return true;
				break;
			case this.AT_SHOULD_AIRPORTS:
				company.shouldAirPorts = -1;
				return true;
				break;
				
				// vehicles
			case this.AT_SHOULD_RAILVEHICLES:
				company.shouldRailVehicles = -1;
				return true;
				break;
			case this.AT_SHOULD_TRUCKVEHICLES:
				company.shouldTruckVehicles = -1;
				return true;
				break;
			case this.AT_SHOULD_BUSVEHICLES:
				company.shouldBusVehicles = -1;
				return true;
				break;
			case this.AT_SHOULD_WATERVEHICLES:
				company.shouldWaterVehicles = -1;
				return true;
				break;
			case this.AT_SHOULD_AIRVEHICLES:
				company.shouldAirVehicles = -1;
				return true;
				break;
		}
	}
	else {
		if (award_amount < 1)
			award_amount = 1;
		
		switch (award_type) {
			case this.AT_SHOULD_RAILDEPOS:
				company.shouldRailDepos += award_amount;
				return true;
				break;
			case this.AT_SHOULD_ROADDEPOS:
				company.shouldRoadDepos += award_amount;
				return true;
				break;
			case this.AT_SHOULD_WATERDEPOS:
				company.shouldWaterDepos += award_amount;
				return true;
				break;
				
				// stations
			case this.AT_SHOULD_RAILSTATIONS:
				company.shouldRailStations += award_amount;
				return true;
				break;
			case this.AT_SHOULD_TRUCKSTOPS:
				company.shouldTruckStops += award_amount;
				return true;
				break;
			case this.AT_SHOULD_BUSSTOPS:
				company.shouldBusStops += award_amount;
				return true;
				break;
			case this.AT_SHOULD_WATERDOCKS:
				company.shouldWaterDocks += award_amount;
				return true;
				break;
			case this.AT_SHOULD_AIRPORTS:
				company.shouldAirPorts += award_amount;
				return true;
				break;
				
				// vehicles
			case this.AT_SHOULD_RAILVEHICLES:
				company.shouldRailVehicles += award_amount;
				return true;
				break;
			case this.AT_SHOULD_TRUCKVEHICLES:
				company.shouldTruckVehicles += award_amount;
				return true;
				break;
			case this.AT_SHOULD_BUSVEHICLES:
				company.shouldBusVehicles += award_amount;
				return true;
				break;
			case this.AT_SHOULD_WATERVEHICLES:
				company.shouldWaterVehicles += award_amount;
				return true;
				break;
			case this.AT_SHOULD_AIRVEHICLES:
				company.shouldAirVehicles += award_amount;
				return true;
				break;
		}
	}
	
	return false;
}

function FGAward::GetAwardStringFromAwardType(award_type, obj_case) {
	switch (award_type) {
		case this.AT_SHOULD_RAILDEPOS:
			if (obj_case)
				return GSText(GSText.STR_BUILDING_RAILDEPOS_OBJ_CASE);
			else
				return GSText(GSText.STR_BUILDING_RAILDEPOS);
			break;
		case this.AT_SHOULD_ROADDEPOS:
			if (obj_case)
				return GSText(GSText.STR_BUILDING_ROADDEPOS_OBJ_CASE);
			else
				return GSText(GSText.STR_BUILDING_ROADDEPOS);
			break;
		case this.AT_SHOULD_WATERDEPOS:
			if (obj_case)
				return GSText(GSText.STR_BUILDING_WATERDEPOS_OBJ_CASE);
			else
				return GSText(GSText.STR_BUILDING_WATERDEPOS);
			break;
			
			// stations
		case this.AT_SHOULD_RAILSTATIONS:
			if (obj_case)
				return GSText(GSText.STR_BUILDING_RAILSTATIONS_OBJ_CASE);
			else
				return GSText(GSText.STR_BUILDING_RAILSTATIONS);
			break;
		case this.AT_SHOULD_TRUCKSTOPS:
			if (obj_case)
				return GSText(GSText.STR_BUILDING_TRUCKSTOPS_OBJ_CASE);
			else
				return GSText(GSText.STR_BUILDING_TRUCKSTOPS);
			break;
		case this.AT_SHOULD_BUSSTOPS:
			if (obj_case)
				return GSText(GSText.STR_BUILDING_BUSSTOPS_OBJ_CASE);
			else
				return GSText(GSText.STR_BUILDING_BUSSTOPS);
			break;
		case this.AT_SHOULD_WATERDOCKS:
			if (obj_case)
				return GSText(GSText.STR_BUILDING_WATERDOCKS_OBJ_CASE);
			else
				return GSText(GSText.STR_BUILDING_WATERDOCKS);
			break;
		case this.AT_SHOULD_AIRPORTS:
			if (obj_case)
				return GSText(GSText.STR_BUILDING_AIRPORTS_OBJ_CASE);
			else
				return GSText(GSText.STR_BUILDING_AIRPORTS);
			break;
			
			// vehicles
		case this.AT_SHOULD_RAILVEHICLES:
			if (obj_case)
				return GSText(GSText.STR_VEHICLE_TRAIN_OBJ_CASE);
			else
				return GSText(GSText.STR_VEHICLE_TRAIN);
			break;
		case this.AT_SHOULD_TRUCKVEHICLES:
			if (obj_case)
				return GSText(GSText.STR_VEHICLE_TRUCK_OBJ_CASE);
			else
				return GSText(GSText.STR_VEHICLE_TRUCK);
			break;
		case this.AT_SHOULD_BUSVEHICLES:
			if (obj_case)
				return GSText(GSText.STR_VEHICLE_BUS_OBJ_CASE);
			else
				return GSText(GSText.STR_VEHICLE_BUS);
			break;
		case this.AT_SHOULD_WATERVEHICLES:
			if (obj_case)
				return GSText(GSText.STR_VEHICLE_SHIP_OBJ_CASE);
			else
				return GSText(GSText.STR_VEHICLE_SHIP);
			break;
		case this.AT_SHOULD_AIRVEHICLES:
			if (obj_case)
				return GSText(GSText.STR_VEHICLE_AIRPLANE_OBJ_CASE);
			else
				return GSText(GSText.STR_VEHICLE_AIRPLANE);
			break;
	}
	
	return null;
}
