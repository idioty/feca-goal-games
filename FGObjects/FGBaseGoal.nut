class FGBaseGoal {
	// GoalTypes
	
	static GoalTypes = {
		GT_INVALID = 0,
		GT_TRANSPORT = 1,
		GT_TRANSPORTONETOONE = 2,
		GT_TOWNGROWTH = 3,
		GT_TRANSPORT_GLOBAL = 4,
		GT_TOWNGROWTH_GLOBAL = 5,
	}
	
	// Ezt majd valtoztatni kell a feladat tipusok hozzaadasaval
	GOAL_TYPES_MIN = 1;
	GOAL_TYPES_MAX = 5;
	
	mainnut = null;
	
	goal_settings = {};
	
	goal_id = null; // ebben taroljuk majd a GSGoal altal kapott id-t
	goal_main = false; // main goalrol van szo?
	goal_award = false; // ez csak jutalomert megy, es nem szamit a gyozelemhez
	goal_weak = false; // ha ez true, akkor nem jelenik meg a celok ablakban, viszont ez semmikepp sem lehet main goal!!! :)
	goal_type = 0; // goal tipusonkent kell majd mast megadnom
	goal_difficulty = 0; // 0 a legkisebb, 0-9 kozti ertek, ezzel szamitjuk ki majd a dijat, amit ezert a feladatert lehet kapni
	goal_company_id = GSCompany.COMPANY_INVALID; // ebben taroljuk, hogy melyik vallalate volt ez a feladat
	
	goal_best = 0; // ebben taroljuk azt, hogy a goal-t hany szazaleknal tart, TODO: ez alapjan lehetne kupat is csinalni
	goal_last_best = 0; // ebben meg taroljuk a goal frissites miatt az utolso
	goal_last_actual = 0; // ebben meg troljuk a jelenlegei teljesitmenyt
	goal_last_best_percent = 0;
	
	goal_begin_month = -1; // ebben fogom tarolni, hogy melyik honapban keletkezett a feladat
//	goal_show_mode = 0; // azaz hogyan jelenjen meg a feladat. SM_NONE = 0, azaz nem latszik (titkos feladat); SM_GLOBAL = 1, azaz globalis feladat; SM_COMPANY = 2, azaz vallalat sajat feladata
	
	// ez a ketto a GSGoal tipus es destination beallitasait veheti fel, hogy tudjuk a feladat megjelenitesekor, hogy mi tortenjen kattintasra
	goal_gt_type = GSGoal.GT_NONE;
	goal_gt_destintation = 0;
	
	

	// class functions, nem kell letrehozni hozza objektumot
	static function BaseSettinsgFromGoalType(goal_type, mainnut);
	static function IsValidGoalType(goal_type);
	static function IsAvailableGoalOnLevel(mainnut, level, goal_type);
}

function FGBaseGoal::BaseSettinsgFromGoalType(goal_type, mainnut) {
	switch (goal_type) {
		case FGBaseGoal.GoalTypes.GT_TRANSPORT:
			return mainnut.baseTransportGoalSettings;
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
			return mainnut.baseTownGrowthGoalSettings;
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
			break;
		// TODO [feladat tipus] tobbi goal-t is hozza kell adni
	}
	
	return null;
}

function FGBaseGoal::IsValidGoalType(goal_type) {
	switch (goal_type) {
		case FGBaseGoal.GoalTypes.GT_TRANSPORT:
		// TODO: egyelore, mig nem csinalom meg ez(eke)t a feladat tipus(oka)t, addig ervenytelen(ek) lesz(nek)
//		case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
//		case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
//		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
			return true;
			break;
		// TODO [feladat tipus] tobbi goal-t is hozza kell adni
	}
	
	return false;
}

function FGBaseGoal::IsAvailableGoalOnLevel(mainnut, level, goal_type) {
	switch (goal_type) {
		case FGBaseGoal.GoalTypes.GT_TRANSPORT:
			local list = mainnut.cargo.AvailableListInGameWithSettings(mainnut.transportGoalSettings, level, false);
			return list.len() > 0;
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
			if (mainnut.townGrowthGoalSettings.level_min > 0 && mainnut.townGrowthGoalSettings.level_min <= level && (mainnut.townGrowthGoalSettings.level_min > mainnut.townGrowthGoalSettings.level_max || mainnut.townGrowthGoalSettings.level_max >= level))
				return true;
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
			break;
			// TODO [feladat tipus] tobbi goal-t is hozza kell adni
	}
	
	return false;
}

/*
// inherited functions, ehhez mar objektumot kell letrehozni, es annal kell ezt meghivni
function valami (valami) {
	
}
*/
