class FGCargo {
	cargotypes = [];
	logmode = false;
	
	// Default cargos
	cargotype_valuables = null;					// VALU
	cargotype_steel = null;						// STEL
	cargotype_ironore = null;					// IORE
	cargotype_wood = null;						// WOOD
	cargotype_grain = null;						// GRAI
	cargotype_goods = null;						// GOOD
	cargotype_livestock = null;					// LVST
	cargotype_oil = null;						// OIL_
	cargotype_mail = null;						// MAIL
	cargotype_coal = null;						// COAL
	cargotype_passengers = null;				// PASS
	
	cargotype_paper = null;						// PAPR	Paper	 0020 Piece goods	ECS
	cargotype_wheat = null;						// WHEA	Wheat	 0010 Bulk			 Arctic; see also GRAI, MAIZ, CERE
	cargotype_food = null;						// FOOD	Food	 0084 Express, refrigerated	ECS	FIRS
	cargotype_gold = null;						// GOLD	Gold	 0008 Armoured	ECS		 Arctic; see also VALU, DIAM
	cargotype_rubber = null;					// RUBR	Rubber	0040 Liquid	ECS
	cargotype_fruit = null;						// FRUT  Fruit	 0090 Bulk, refrigerated	ECS
	cargotype_maize = null;						// MAIZ	Maize	 0010 Bulk			 Tropic; see also GRAI, WHEA, CERE
	cargotype_copperore = null;					// CORE	Copper Ore	 0010 Bulk	ECS
	cargotype_water = null;						// WATR	Water	 0040 Liquid	ECS
	cargotype_diamonds = null;					// DIAM	Diamonds	 0008 Armoured	ECS		 Tropic; see also VALU, GOLD
	cargotype_sugar = null;						// SUGR	Sugar	 0010 Bulk			 Toyland
	cargotype_toys = null;						// TOYS	Toys	 0020 Piece goods			 Toyland
	cargotype_batteries = null;					// BATT	Batteries	 0020 Piece goods			 Toyland
	cargotype_sweets = null;					// SWET	Sweets (Candy)	0004 Express			 Toyland
	cargotype_toffee = null;					// TOFF	Toffee	0010 Bulk			 Toyland
	cargotype_cola = null;						// COLA	Cola	0040 Liquid			 Toyland
	cargotype_cottoncandy = null;				// CTCD	Cotton Candy (Candyfloss)	0010 Bulk			 Toyland
	cargotype_bubbles = null;					// BUBL	Bubbles	0020 Piece goods			 Toyland
	cargotype_plastic = null;					// PLST	Plastic	0040 Liquid			 Toyland; see also PLAS
	cargotype_fizzy = null;						// FZDR	Fizzy Drinks	0020 Piece goods			 Toyland
	
	//	New Cargos	 these cargos are only present when NewGRF industry sets are used
	cargotype_bauxite = null;					// AORE	Bauxite (Aluminium ore)	0010 Bulk	ECS	FIRS
	cargotype_alcohol = null;					// BEER	Alcohol	0064 Express, piece goods, liquids		FIRS
	cargotype_buildingmaterials = null;			// BDMT	Building Materials	0220 Piece goods, covered/sheltered	ECS	FIRS	 In FIRS: 0030 Bulk, piece goods
	cargotype_bricks = null;					// BRCK	Bricks	 0020 Piece goods	ECS
	cargotype_ceramics = null;					// CERA	Ceramics	 0020 Piece goods	ECS
	cargotype_cereals = null;					// CERE	Cereals	 0210 Bulk, covered/sheltered	ECS		[1]
	cargotype_clay = null;						// CLAY	Clay	0210 Bulk covered/sheltered		FIRS	 In FIRS: 0010 Bulk
	cargotype_cement = null;					// CMNT	Cement	 0210 Bulk covered/sheltered	ECS
	cargotype_copper = null;					// COPR	Copper	0020 Piece goods
	cargotype_dyes = null;						// DYES	Dyes	 0060 Piece goods, liquids	ECS
	cargotype_engineeringsupplies = null;		// ENSP	Engineering Supplies	0024 Express, piece goods		FIRS
	cargotype_fertiliser = null;				// FERT	Fertiliser	 0030 Bulk, piece goods	ECS		[1]
	cargotype_fibrecrops = null;				// FICR	Fibre crops	 0030 Bulk, piece goods	ECS	FIRS
	cargotype_fish = null;						// FISH	Fish	 0084 Express, refrigerated	ECS	FIRS
	cargotype_farmsupplies = null;				// FMSP	Farm Supplies	0024 Express, piece goods		FIRS
	cargotype_frvgfruit = null;					// FRVG	Fruit (and optionally Vegetables)	00A4 Express, piece goods, refrigerated		FIRS
	cargotype_glass = null;						// GLAS	Glass	 0420 Piece goods, oversized	ECS
	cargotype_gravelballast = null;				// GRVL	Gravel / Ballast	0010 Bulk		FIRS
	cargotype_limestone = null;					// LIME	Lime stone	 0010 Bulk	ECS
	cargotype_milk = null;						// MILK	Milk	00C4 Express, liquid, refrigerated		FIRS
	cargotype_manufacturingsupplies = null;		// MNSP	Manufacturing Supplies	0024 Piece Goods, express		FIRS
	cargotype_metal = null;						// STEL ez az acelt irja felul!!!
	cargotype_oilseed = null;					// OLSD	Oil seed	 0210 Bulk, covered/sheltered	ECS		[1]
	cargotype_petrolfueloil = null;				// PETR	Petrol / Fuel Oil	 0040 Liquid	ECS	FIRS
	cargotype_plasplastic = null;				// PLAS	Plastic	 0060 Piece goods, liquid	ECS
	cargotype_potash = null;					// POTA	Potash	 0210 Bulk, covered/sheltered	ECS		[1]
	cargotype_recyclables = null;				// RCYC	Recyclables	0220 Piece Goods, covered		FIRS
	cargotype_refinedproducts = null;			// RFPR	Refined products	 0040 Liquid	ECS	FIRS
	cargotype_sand = null;						// SAND	Sand	 0010 Bulk	ECS	FIRS
	cargotype_scraptmetal = null;				// SCMT	Scrap Metal	1010 Bulk, non-pourable		FIRS
	cargotype_sugarbeet = null;					// SGBT	Sugar beet	0010 Bulk		FIRS	 not in tropical
	cargotype_sugarcane = null;					// SGCN	Sugarcane	1010 Bulk, non-pourable		FIRS	 only tropical
	cargotype_sulphur = null;					// SULP	Sulphur	 0210 Bulk, covered/sheltered	ECS		[1]
	cargotype_tourists = null;					// TOUR	Tourists	 0005 Passengers, express	ECS
	cargotype_vehicles = null;					// VEHI	Vehicles	 0420 Piece goods, oversized	ECS
	cargotype_wdprwood = null;					// WDPR	Wood Products	 0030 Bulk, piece goods	ECS	FIRS
	cargotype_wool = null;						// WOOL	Wool	 0220 Piece goods, covered/sheltered	ECS	FIRS	[1]
	
	
	//	Special Cargos	 these cargos are for use outside industry sets and do not represent transporting anything
	cargotype_default = null;					// DFLT	'Default'	 ---- None			 Used by andythenorth to identify default cargo graphics in newgrfs that use code/graphics generation.
	
	cargotype_locomotiveregearing = null;		// GEAR	Locomotive regearing	8000 Special			 Using this cargo may make your vehicle set incompatible with some industry sets
	
	//	Deprecated Cargos	 these cargos are not used by any current industry set, and are listed here for backwards compatibility
	cargotype_fuel = null;						// FUEL	Fuel	0040 Liquid			Use PETR for refined-oil fuel
	cargotype_rawsugar = null;					// RSGR	Raw Sugar	0010 Bulk			 Deprecated in FIRS. See SGBT and SGCN
	cargotype_scrpscrapmetal = null;			// SCRP	Scrap Metal	0010 Piece Goods			 Deprecated in FIRS, use SCMT instead
	cargotype_tropicwood = null;				// TWOD	Tropic Wood	0020 Piece goods			 formerly intended as a default cargo
	cargotype_waste = null;						// WSTE	Waste	0010 Bulk			 Deprecated in FIRS.
	
	isFIRSGame = false;							// FIRS eseteben nem acel lesz a nev, hanem fem
	
	constructor (a_cargo_id, aLogMode) {
		this.logmode = aLogMode;
		this.CheckCargoNames();
		this.CreateCargosList();
		
//		this.cargotypes = [];
		
//		this.cargo_id = a_cargo_id;
//		this.current_cargo_mounted = 0;
//		this.max_cargo_transported = 0;
	}
}

function FGCargo::CargoMultiplier(cargo_id) {
	if (!GSCargo.IsValidCargo(cargo_id))
		return 0;
	
	// x = 1: atlagos, x < 1: nehezebb, x > 1: konnyebb
	
	// TODO: [FGCargo::CargoMultiplier] a szorzast meg kell csinalni a tobbi cargo tipusnal is
	switch (cargo_id) {
		case this.cargotype_valuables:
			return 0.8;
			break;
		case this.cargotype_steel:
			if (this.isFIRSGame) // fem elszallitasa nem acele...
				return 1.0;
			else
				return 0.8;
			break;
		case this.cargotype_ironore:
			return 1.0;
			break;
		case this.cargotype_wood:
			return 1.0;
			break;
		case this.cargotype_grain:
			return 0.9;
			break;
		case this.cargotype_goods:
			return 0.8;
			break;
		case this.cargotype_livestock:
			return 1.0;
			break;
		case this.cargotype_oil:
			if (this.isFIRSGame)
				return 1.2;
			else
				return 1.0;
			break;
		case this.cargotype_mail:
			return 0.7;
			break;
		case this.cargotype_coal:
			if (this.isFIRSGame)
				return 1.0;
			else
				return 1.2;
			break;
		case this.cargotype_passengers:
			if (this.isFIRSGame) // FIRS-ben sokkal konnyebb az utas szallitas.
				return 1.3;
			else
				return 1.1;		// utas szallitas is lehet nagyon konnyu
			break;
	}
	
	// default
	return 1;
}

/*
function FGCargo::DisplayMultiplier(cargo_id) {
	switch (cargo_id) {
		case this.cargotype_oil: // folyekonyak
		case this.cargotype_rubber:
		case this.cargotype_water:
		case this.cargotype_cola:
		case this.cargotype_plastic:
		case this.cargotype_milk:
		case this.cargotype_petrolfueloil:
		case this.cargotype_plasplastic:
		case this.cargotype_refinedproducts:
		case this.cargotype_fuel:
		case this.cargotype_alcohol:
			return 1000;
			break;
	}
	
	return 1;
}
*/

function FGCargo::IsAvailableInGame(cargo_id) {
	foreach (a_cargo_id in this.cargotypes)
		if (a_cargo_id == cargo_id)
			return true;
	
	return false;
}

// level az, hogy hanyadik kornel jarunk
function FGCargo::IsAvailableInSettings(cargo_id, settings, level) {
	switch (cargo_id) {
		case this.cargotype_valuables:
			if (settings.cargotype_valuables_min > 0 && settings.cargotype_valuables_min <= level && (settings.cargotype_valuables_min > settings.cargotype_valuables_max || settings.cargotype_valuables_max >= level))
				return true;
			break;
		case this.cargotype_steel: // FIRS es sima eseten is tonna, szoval itt nem kell valtoztatni...
			if (settings.cargotype_steel_min > 0 && settings.cargotype_steel_min <= level && (settings.cargotype_steel_min > settings.cargotype_steel_max || settings.cargotype_steel_max >= level))
				return true;
			break;
		case this.cargotype_ironore:
			if (settings.cargotype_ironore_min > 0 && settings.cargotype_ironore_min <= level && (settings.cargotype_ironore_min > settings.cargotype_ironore_max || settings.cargotype_ironore_max >= level))
				return true;
			break;
		case this.cargotype_wood:
			if (settings.cargotype_wood_min > 0 && settings.cargotype_wood_min <= level && (settings.cargotype_wood_min > settings.cargotype_wood_max || settings.cargotype_wood_max >= level))
				return true;
			break;
		case this.cargotype_grain:
			if (settings.cargotype_grain_min > 0 && settings.cargotype_grain_min <= level && (settings.cargotype_grain_min > settings.cargotype_grain_max || settings.cargotype_grain_max >= level))
				return true;
			break;
		case this.cargotype_goods:
			if (settings.cargotype_goods_min > 0 && settings.cargotype_goods_min <= level && (settings.cargotype_goods_min > settings.cargotype_goods_max || settings.cargotype_goods_max >= level))
				return true;
			break;
		case this.cargotype_livestock:
			if (settings.cargotype_livestock_min > 0 && settings.cargotype_livestock_min <= level && (settings.cargotype_livestock_min > settings.cargotype_livestock_max || settings.cargotype_livestock_max >= level))
				return true;
			break;
		case this.cargotype_oil:
			if (settings.cargotype_oil_min > 0 && settings.cargotype_oil_min <= level && (settings.cargotype_oil_min > settings.cargotype_oil_max || settings.cargotype_oil_max >= level))
				return true;
			break;
		case this.cargotype_mail:
			if (settings.cargotype_mail_min > 0 && settings.cargotype_mail_min <= level && (settings.cargotype_mail_min > settings.cargotype_mail_max || settings.cargotype_mail_max >= level))
				return true;
			break;
		case this.cargotype_coal:
			if (settings.cargotype_coal_min > 0 && settings.cargotype_coal_min <= level && (settings.cargotype_coal_min > settings.cargotype_coal_max || settings.cargotype_coal_max >= level))
				return true;
			break;
		case this.cargotype_passengers:
			if (settings.cargotype_passengers_min > 0 && settings.cargotype_passengers_min <= level && (settings.cargotype_passengers_min > settings.cargotype_passengers_max || settings.cargotype_passengers_max >= level))
				return true;
			break;
			
			
			
			
			
			
		case this.cargotype_paper:
			if (settings.cargotype_paper_min > 0 && settings.cargotype_paper_min <= level && (settings.cargotype_paper_min > settings.cargotype_paper_max || settings.cargotype_paper_max >= level))
				return true;
			break; // PAPR	Paper	 0020 Piece goods	ECS
		case this.cargotype_wheat:
			if (settings.cargotype_wheat_min > 0 && settings.cargotype_wheat_min <= level && (settings.cargotype_wheat_min > settings.cargotype_wheat_max || settings.cargotype_wheat_max >= level))
				return true;
			break; // WHEA	Wheat	 0010 Bulk			 Arctic; see also GRAI, MAIZ, CERE
		case this.cargotype_food:
			if (settings.cargotype_food_min > 0 && settings.cargotype_food_min <= level && (settings.cargotype_food_min > settings.cargotype_food_max || settings.cargotype_food_max >= level))
				return true;
			break; // FOOD	Food	 0084 Express, refrigerated	ECS	FIRS
		case this.cargotype_gold:
			if (settings.cargotype_gold_min > 0 && settings.cargotype_gold_min <= level && (settings.cargotype_gold_min > settings.cargotype_gold_max || settings.cargotype_gold_max >= level))
				return true;
			break; // GOLD	Gold	 0008 Armoured	ECS		 Arctic; see also VALU, DIAM
		case this.cargotype_rubber:
			if (settings.cargotype_rubber_min > 0 && settings.cargotype_rubber_min <= level && (settings.cargotype_rubber_min > settings.cargotype_rubber_max || settings.cargotype_rubber_max >= level))
				return true;
			break; // RUBR	Rubber	0040 Liquid	ECS
		case this.cargotype_fruit:
			if (settings.cargotype_fruit_min > 0 && settings.cargotype_fruit_min <= level && (settings.cargotype_fruit_min > settings.cargotype_fruit_max || settings.cargotype_fruit_max >= level))
				return true;
			break; // FRUT  Fruit	 0090 Bulk, refrigerated	ECS
		case this.cargotype_maize:
			if (settings.cargotype_maize_min > 0 && settings.cargotype_maize_min <= level && (settings.cargotype_maize_min > settings.cargotype_maize_max || settings.cargotype_maize_max >= level))
				return true;
			break; // MAIZ	Maize	 0010 Bulk			 Tropic; see also GRAI, WHEA, CERE
		case this.cargotype_copperore:
			if (settings.cargotype_copperore_min > 0 && settings.cargotype_copperore_min <= level && (settings.cargotype_copperore_min > settings.cargotype_copperore_max || settings.cargotype_copperore_max >= level))
				return true;
			break; // CORE	Copper Ore	 0010 Bulk	ECS
		case this.cargotype_water:
			if (settings.cargotype_water_min > 0 && settings.cargotype_water_min <= level && (settings.cargotype_water_min > settings.cargotype_water_max || settings.cargotype_water_max >= level))
				return true;
			break; // WATR	Water	 0040 Liquid	ECS
		case this.cargotype_diamonds:
			if (settings.cargotype_diamonds_min > 0 && settings.cargotype_diamonds_min <= level && (settings.cargotype_diamonds_min > settings.cargotype_diamonds_max || settings.cargotype_diamonds_max >= level))
				return true;
			break; // DIAM	Diamonds	 0008 Armoured	ECS		 Tropic; see also VALU, GOLD
		case this.cargotype_sugar:
			if (settings.cargotype_sugar_min > 0 && settings.cargotype_sugar_min <= level && (settings.cargotype_sugar_min > settings.cargotype_sugar_max || settings.cargotype_sugar_max >= level))
				return true;
			break; // SUGR	Sugar	 0010 Bulk			 Toyland
		case this.cargotype_toys:
			if (settings.cargotype_toys_min > 0 && settings.cargotype_toys_min <= level && (settings.cargotype_toys_min >=settings.cargotype_toys_max || settings.cargotype_toys_max >= level))
				return true;
			break; // TOYS	Toys	 0020 Piece goods			 Toyland
		case this.cargotype_batteries:
			if (settings.cargotype_batteries_min > 0 && settings.cargotype_batteries_min <= level && (settings.cargotype_batteries_min > settings.cargotype_batteries_max || settings.cargotype_batteries_max >= level))
				return true;
			break; // BATT	Batteries	 0020 Piece goods			 Toyland
		case this.cargotype_sweets:
			if (settings.cargotype_sweets_min > 0 && settings.cargotype_sweets_min <= level && (settings.cargotype_sweets_min > settings.cargotype_sweets_max || settings.cargotype_sweets_max >= level))
				return true;
			break; // SWET	Sweets (Candy)	0004 Express			 Toyland
		case this.cargotype_toffee:
			if (settings.cargotype_toffee_min > 0 && settings.cargotype_toffee_min <= level && (settings.cargotype_toffee_min > settings.cargotype_toffee_max || settings.cargotype_toffee_max >= level))
				return true;
			break; // TOFF	Toffee	0010 Bulk			 Toyland
		case this.cargotype_cola:
			if (settings.cargotype_cola_min > 0 && settings.cargotype_cola_min <= level && (settings.cargotype_cola_min > settings.cargotype_cola_max || settings.cargotype_cola_max >= level))
				return true;
			break; 			// COLA	Cola	0040 Liquid			 Toyland
		case this.cargotype_cottoncandy:
			if (settings.cargotype_cottoncandy_min > 0 && settings.cargotype_cottoncandy_min <= level && (settings.cargotype_cottoncandy_min > settings.cargotype_cottoncandy_max || settings.cargotype_cottoncandy_max >= level))
				return true;
			break; 		// CTCD	Cotton Candy (Candyfloss)	0010 Bulk			 Toyland
		case this.cargotype_bubbles:
			if (settings.cargotype_bubbles_min > 0 && settings.cargotype_bubbles_min <= level && (settings.cargotype_bubbles_min > settings.cargotype_bubbles_max || settings.cargotype_bubbles_max >= level))
				return true;
			break; 			// BUBL	Bubbles	0020 Piece goods			 Toyland
		case this.cargotype_plastic:
			if (settings.cargotype_plastic_min > 0 && settings.cargotype_plastic_min <= level && (settings.cargotype_plastic_min > settings.cargotype_plastic_max || settings.cargotype_plastic_max >= level))
				return true;
			break; 				// PLST	Plastic	0040 Liquid			 Toyland; see also PLAS
		case this.cargotype_fizzy:
			if (settings.cargotype_fizzy_min > 0 && settings.cargotype_fizzy_min <= level && (settings.cargotype_fizzy_min > settings.cargotype_fizzy_max || settings.cargotype_fizzy_max >= level))
				return true;
			break;  // FZDR	Fizzy Drinks	0020 Piece goods			 Toyland
			
			
			//	New Cargos	 these cargos are only present when NewGRF industry sets are used
		case this.cargotype_bauxite:
			if (settings.cargotype_bauxite_min > 0 && settings.cargotype_bauxite_min <= level && (settings.cargotype_bauxite_min > settings.cargotype_bauxite_max || settings.cargotype_bauxite_max >= level))
				return true;
			break; // AORE	Bauxite (Aluminium ore)	0010 Bulk	ECS	FIRS
		case this.cargotype_alcohol:
			if (settings.cargotype_alcohol_min > 0 && settings.cargotype_alcohol_min <= level && (settings.cargotype_alcohol_min > settings.cargotype_alcohol_max || settings.cargotype_alcohol_max >= level))
				return true;
			break; // BEER	Alcohol	0064 Express, piece goods, liquids		FIRS
		case this.cargotype_buildingmaterials:
			if (settings.cargotype_buildingmaterials_min > 0 && settings.cargotype_buildingmaterials_min <= level && (settings.cargotype_buildingmaterials_min > settings.cargotype_buildingmaterials_max || settings.cargotype_buildingmaterials_max >= level))
				return true;
			break; // BDMT	Building Materials	0220 Piece goods, covered/sheltered	ECS	FIRS	 In FIRS: 0030 Bulk, piece goods
		case this.cargotype_bricks:
			if (settings.cargotype_bricks_min > 0 && settings.cargotype_bricks_min <= level && (settings.cargotype_bricks_min > settings.cargotype_bricks_max || settings.cargotype_bricks_max >= level))
				return true;
			break; 				// BRCK	Bricks	 0020 Piece goods	ECS
		case this.cargotype_ceramics:
			if (settings.cargotype_ceramics_min > 0 && settings.cargotype_ceramics_min <= level && (settings.cargotype_ceramics_min > settings.cargotype_ceramics_max || settings.cargotype_ceramics_max >= level))
				return true;
			break; 				// CERA	Ceramics	 0020 Piece goods	ECS
		case this.cargotype_cereals:
			if (settings.cargotype_cereals_min > 0 && settings.cargotype_cereals_min <= level && (settings.cargotype_cereals_min > settings.cargotype_cereals_max || settings.cargotype_cereals_max >= level))
				return true;
			break; 				// CERE	Cereals	 0210 Bulk, covered/sheltered	ECS		[1]
		case this.cargotype_clay:
			if (settings.cargotype_clay_min > 0 && settings.cargotype_clay_min <= level && (settings.cargotype_clay_min > settings.cargotype_clay_max || settings.cargotype_clay_max >= level))
				return true;
			break; 						// CLAY	Clay	0210 Bulk covered/sheltered		FIRS	 In FIRS: 0010 Bulk
		case this.cargotype_cement:
			if (settings.cargotype_cement_min > 0 && settings.cargotype_cement_min <= level && (settings.cargotype_cement_min > settings.cargotype_cement_max || settings.cargotype_cement_max >= level))
				return true;
			break; 				// CMNT	Cement	 0210 Bulk covered/sheltered	ECS
		case this.cargotype_copper:
			if (settings.cargotype_copper_min > 0 && settings.cargotype_copper_min <= level && (settings.cargotype_copper_min > settings.cargotype_copper_max || settings.cargotype_copper_max >= level))
				return true;
			break; 				// COPR	Copper	0020 Piece goods
		case this.cargotype_dyes:
			if (settings.cargotype_dyes_min > 0 && settings.cargotype_dyes_min <= level && (settings.cargotype_dyes_min > settings.cargotype_dyes_max || settings.cargotype_dyes_max >= level))
				return true;
			break; 						// DYES	Dyes	 0060 Piece goods, liquids	ECS
		case this.cargotype_engineeringsupplies:
			if (settings.cargotype_engineeringsupplies_min > 0 && settings.cargotype_engineeringsupplies_min <= level && (settings.cargotype_engineeringsupplies_min > settings.cargotype_engineeringsupplies_max || settings.cargotype_engineeringsupplies_max >= level))
				return true;
			break; 		// ENSP	Engineering Supplies	0024 Express, piece goods		FIRS
		case this.cargotype_fertiliser:
			if (settings.cargotype_fertiliser_min > 0 && settings.cargotype_fertiliser_min <= level && (settings.cargotype_fertiliser_min > settings.cargotype_fertiliser_max || settings.cargotype_fertiliser_max >= level))
				return true;
			break; 				// FERT	Fertiliser	 0030 Bulk, piece goods	ECS		[1]
		case this.cargotype_fibrecrops:
			if (settings.cargotype_fibrecrops_min > 0 && settings.cargotype_fibrecrops_min <= level && (settings.cargotype_fibrecrops_min > settings.cargotype_fibrecrops_max || settings.cargotype_fibrecrops_max >= level))
				return true;
			break; 				// FICR	Fibre crops	 0030 Bulk, piece goods	ECS	FIRS
		case this.cargotype_fish:
			if (settings.cargotype_fish_min > 0 && settings.cargotype_fish_min <= level && (settings.cargotype_fish_min > settings.cargotype_fish_max || settings.cargotype_fish_max >= level))
				return true;
			break; 						// FISH	Fish	 0084 Express, refrigerated	ECS	FIRS
		case this.cargotype_farmsupplies:
			if (settings.cargotype_farmsupplies_min > 0 && settings.cargotype_farmsupplies_min <= level && (settings.cargotype_farmsupplies_min > settings.cargotype_farmsupplies_max || settings.cargotype_farmsupplies_max >= level))
				return true;
			break; 				// FMSP	Farm Supplies	0024 Express, piece goods		FIRS
		case this.cargotype_frvgfruit:
			if (settings.cargotype_frvgfruit_min > 0 && settings.cargotype_frvgfruit_min <= level && (settings.cargotype_frvgfruit_min > settings.cargotype_frvgfruit_max || settings.cargotype_frvgfruit_max >= level))
				return true;
			break; 				// FRVG	Fruit (and optionally Vegetables)	00A4 Express, piece goods, refrigerated		FIRS
		case this.cargotype_glass:
			if (settings.cargotype_glass_min > 0 && settings.cargotype_glass_min <= level && (settings.cargotype_glass_min > settings.cargotype_glass_max || settings.cargotype_glass_max >= level))
				return true;
			break; 						// GLAS	Glass	 0420 Piece goods, oversized	ECS
		case this.cargotype_gravelballast:
			if (settings.cargotype_gravelballast_min > 0 && settings.cargotype_gravelballast_min <= level && (settings.cargotype_gravelballast_min > settings.cargotype_gravelballast_max || settings.cargotype_gravelballast_max >= level))
				return true;
			break; 				// GRVL	Gravel / Ballast	0010 Bulk		FIRS
		case this.cargotype_limestone:
			if (settings.cargotype_limestone_min > 0 && settings.cargotype_limestone_min <= level && (settings.cargotype_limestone_min > settings.cargotype_limestone_max || settings.cargotype_limestone_max >= level))
				return true;
			break; 					// LIME	Lime stone	 0010 Bulk	ECS
		case this.cargotype_milk:
			if (settings.cargotype_milk_min > 0 && settings.cargotype_milk_min <= level && (settings.cargotype_milk_min >=settings.cargotype_milk_max || settings.cargotype_milk_max >= level))
				return true;
			break; 						// MILK	Milk	00C4 Express, liquid, refrigerated		FIRS
		case this.cargotype_manufacturingsupplies:
			if (settings.cargotype_manufacturingsupplies_min > 0 && settings.cargotype_manufacturingsupplies_min <= level && (settings.cargotype_manufacturingsupplies_min > settings.cargotype_manufacturingsupplies_max || settings.cargotype_manufacturingsupplies_max >= level))
				return true;
			break; 		// MNSP	Manufacturing Supplies	0024 Piece Goods, express		FIRS
		case this.cargotype_oilseed:
			if (settings.cargotype_oilseed_min > 0 && settings.cargotype_oilseed_min <= level && (settings.cargotype_oilseed_min > settings.cargotype_oilseed_max || settings.cargotype_oilseed_max >= level))
				return true;
			break; 					// OLSD	Oil seed	 0210 Bulk, covered/sheltered	ECS		[1]
		case this.cargotype_petrolfueloil:
			if (settings.cargotype_petrolfueloil_min > 0 && settings.cargotype_petrolfueloil_min <= level && (settings.cargotype_petrolfueloil_min > settings.cargotype_petrolfueloil_max || settings.cargotype_petrolfueloil_max >= level))
				return true;
			break; 				// PETR	Petrol / Fuel Oil	 0040 Liquid	ECS	FIRS
		case this.cargotype_plasplastic:
			if (settings.cargotype_plasplastic_min > 0 && settings.cargotype_plasplastic_min <= level && (settings.cargotype_plasplastic_min > settings.cargotype_plasplastic_max || settings.cargotype_plasplastic_max >= level))
				return true;
			break; 				// PLAS	Plastic	 0060 Piece goods, liquid	ECS
		case this.cargotype_potash:
			if (settings.cargotype_potash_min > 0 && settings.cargotype_potash_min <= level && (settings.cargotype_potash_min > settings.cargotype_potash_max || settings.cargotype_potash_max >= level))
				return true;
			break; 			// POTA	Potash	 0210 Bulk, covered/sheltered	ECS		[1]
		case this.cargotype_recyclables:
			if (settings.cargotype_recyclables_min > 0 && settings.cargotype_recyclables_min <= level && (settings.cargotype_recyclables_min > settings.cargotype_recyclables_max || settings.cargotype_recyclables_max >= level))
				return true;
			break; 			// RCYC	Recyclables	0220 Piece Goods, covered		FIRS
		case this.cargotype_refinedproducts:
			if (settings.cargotype_refinedproducts_min > 0 && settings.cargotype_refinedproducts_min <= level && (settings.cargotype_refinedproducts_min > settings.cargotype_refinedproducts_max || settings.cargotype_refinedproducts_max >= level))
				return true;
			break; 			// RFPR	Refined products	 0040 Liquid	ECS	FIRS
		case this.cargotype_sand:
			if (settings.cargotype_sand_min > 0 && settings.cargotype_sand_min <= level && (settings.cargotype_sand_min > settings.cargotype_sand_max || settings.cargotype_sand_max >= level))
				return true;
			break; 						// SAND	Sand	 0010 Bulk	ECS	FIRS
		case this.cargotype_scraptmetal:
			if (settings.cargotype_scraptmetal_min > 0 && settings.cargotype_scraptmetal_min <= level && (settings.cargotype_scraptmetal_min > settings.cargotype_scraptmetal_max || settings.cargotype_scraptmetal_max >= level))
				return true;
			break; 	// SCMT	Scrap Metal	1010 Bulk, non-pourable		FIRS
		case this.cargotype_sugarbeet:
			if (settings.cargotype_sugarbeet_min > 0 && settings.cargotype_sugarbeet_min <= level && (settings.cargotype_sugarbeet_min > settings. cargotype_sugarbeet_max|| settings.cargotype_sugarbeet_max >= level))
				return true;
			break; 					// SGBT	Sugar beet	0010 Bulk		FIRS	 not in tropical
		case this.cargotype_sugarcane:
			if (settings.cargotype_sugarcane_min > 0 && settings.cargotype_sugarcane_min <= level && (settings.cargotype_sugarcane_min > settings.cargotype_sugarcane_max || settings.cargotype_sugarcane_max >= level))
				return true;
			break; 				// SGCN	Sugarcane	1010 Bulk, non-pourable		FIRS	 only tropical
		case this.cargotype_sulphur:
			if (settings.cargotype_sulphur_min > 0 && settings.cargotype_sulphur_min <= level && (settings.cargotype_sulphur_min > settings.cargotype_sulphur_max || settings.cargotype_sulphur_max >= level))
				return true;
			break; 					// SULP	Sulphur	 0210 Bulk, covered/sheltered	ECS		[1]
		case this.cargotype_tourists:
			if (settings.cargotype_tourists_min > 0 && settings.cargotype_tourists_min <= level && (settings.cargotype_tourists_min > settings.cargotype_tourists_max || settings.cargotype_tourists_max >= level))
				return true;
			break; 					// TOUR	Tourists	 0005 Passengers, express	ECS
		case this.cargotype_vehicles:
			if (settings.cargotype_vehicles_min > 0 && settings.cargotype_vehicles_min <= level && (settings.cargotype_vehicles_min > settings.cargotype_vehicles_max || settings.cargotype_vehicles_max >= level))
				return true;
			break; 					// VEHI	Vehicles	 0420 Piece goods, oversized	ECS
		case this.cargotype_wdprwood:
			if (settings.cargotype_wdprwood_min > 0 && settings.cargotype_wdprwood_min <= level && (settings.cargotype_wdprwood_min > settings.cargotype_wdprwood_max || settings.cargotype_wdprwood_max >= level))
				return true;
			break; 				// WDPR	Wood Products	 0030 Bulk, piece goods	ECS	FIRS
		case this.cargotype_wool:
			if (settings.cargotype_wool_min > 0 && settings.cargotype_wool_min <= level && (settings.cargotype_wool_min > settings.cargotype_wool_max || settings.cargotype_wool_max >= level))
				return true;
			break; 						// WOOL	Wool	 0220 Piece goods, covered/sheltered	ECS	FIRS	[1]
		case this.cargotype_default:
			if (settings.cargotype_default_min > 0 && settings.cargotype_default_min <= level && (settings.cargotype_default_min > settings.cargotype_default_max || settings.cargotype_default_max >= level))
				return true;
			break; // DFLT	'Default'	 ---- None			 Used by andythenorth to identify default cargo graphics in newgrfs that use code/graphics generation.
		case this.cargotype_locomotiveregearing:
			if (settings.cargotype_locomotiveregearing_min > 0 && settings.cargotype_locomotiveregearing_min <= level && (settings.cargotype_locomotiveregearing_min > settings.cargotype_locomotiveregearing_max || settings.cargotype_locomotiveregearing_max >= level))
				return true;
			break; 	// GEAR	Locomotive regearing	8000 Special			 Using this cargo may make your vehicle set incompatible with some industry sets
		case this.cargotype_fuel:
			if (settings.cargotype_fuel_min > 0 && settings.cargotype_fuel_min <= level && (settings.cargotype_fuel_min > settings.cargotype_fuel_max || settings.cargotype_fuel_max >= level))
				return true;
			break; 				// FUEL	Fuel	0040 Liquid			Use PETR for refined-oil fuel
		case this.cargotype_rawsugar:
			if (settings.cargotype_rawsugar_min > 0 && settings.cargotype_rawsugar_min <= level && (settings.cargotype_rawsugar_min > settings.cargotype_rawsugar_max || settings.cargotype_rawsugar_max >= level))
				return true;
			break; 				// RSGR	Raw Sugar	0010 Bulk			 Deprecated in FIRS. See SGBT and SGCN
		case this.cargotype_scrpscrapmetal:
			if (settings.cargotype_scrpscrapmetal_min > 0 && settings.cargotype_scrpscrapmetal_min <= level && (settings.cargotype_scrpscrapmetal_min > settings.cargotype_scrpscrapmetal_max || settings.cargotype_scrpscrapmetal_max >= level))
				return true;
			break; 		// SCRP	Scrap Metal	0010 Piece Goods			 Deprecated in FIRS, use SCMT instead
		case this.cargotype_tropicwood:
			if (settings.cargotype_tropicwood_min > 0 && settings.cargotype_tropicwood_min <= level && (settings.cargotype_tropicwood_min > settings.cargotype_tropicwood_max || settings.cargotype_tropicwood_max >= level))
				return true;
			break; 			// TWOD	Tropic Wood	0020 Piece goods			 formerly intended as a default cargo
		case this.cargotype_waste:
			if (settings.cargotype_waste_min > 0 && settings.cargotype_waste_min <= level && (settings.cargotype_waste_min > settings.cargotype_waste_max || settings.cargotype_waste_max >= level))
				return true;
			break; 					// WSTE	Waste	0010 Bulk			 Deprecated in FIRS.
	}
	
	return false;
}

function FGCargo::AvailableListInGameWithSettings(settings, level, returnAllIfEmpty) {
	/*
	 * ha esetleg 0 lenne a level, akkor az azt jelenti, hogy barmilyen feladat lehet a terkep szerint
	 */
	if (level == 0) {
		// hohho, a masolatat kell elkuldeni, nehogy ebbol a listabol toroljenek
		local masikcargotypes = [];
		masikcargotypes.extend(this.cargotypes);
		return masikcargotypes;
	}
	
	local list = [];
	
	foreach (cargo_id in this.cargotypes) {
		// leellenorizzuk, hogy ilyen feladat egyaltalan adhato. ha nem, akkor nem adjuk hozza
		local van = true;
		if (cargo_id == this.cargotype_passengers || cargo_id == this.cargotype_mail) {
			// ekkor nem az industrie list kell...
			local tl = GSTownList();
			if (tl.Count() < 2)
				van = false;
		}
		else if (cargo_id == this.cargotype_goods) {
			local minimumGoodsTownCount = 4;
			local currentGoodsTownCount = 0;
			
			foreach (town, _ in GSTownList()) {
				if (currentGoodsTownCount < minimumGoodsTownCount) {
					local town_tile = GSTown.GetLocation(town);
					local acceptance = GSTile.GetCargoAcceptance(town_tile, cargo_id, 1, 1, 5);
					if (acceptance >= 8) // ez a nagyobb egyenlo 8 a leirasbol valo
						currentGoodsTownCount++;
					
					if (currentGoodsTownCount < minimumGoodsTownCount) {
						van = false;
					}
				}
			}
		}
		else {
			local ca = GSIndustryList_CargoAccepting(cargo_id);
			local cp = GSIndustryList_CargoProducing(cargo_id);
			
			// legalabb 4 bank legyen azert a terkepen
			// TODO: lehet ezt a bank szamot a terkep nagysaga alapjan kellene allitani, meglatjuk...
			if (cargo_id == this.cargotype_valuables) { // ha banknak kell szallitani, akkor ketto bank kell hozza
				if (ca.Count() < 4 || cp.Count() < 4)
					van = false;
			}
			else if (ca.Count() < 1 || cp.Count() < 1)
				van = false;
		}

//		GSLog.Info("cargo (" + GSCargo.GetCargoLabel(cargo_id) + ") is " + ((this.IsAvailableInSettings(cargo_id, settings, level)) ? "available" : "not available") + " on level: " + level);

		if (van && this.IsAvailableInSettings(cargo_id, settings, level))
			list.append(cargo_id);
	}
	
	
	if (returnAllIfEmpty && list.len() == 0) {
		GSLog.Warning("[FGCargo::AvailableListInGameWithSettings] no available transport goal with settings.");
		// hohho, a masolatat kell elkuldeni, nehogy ebbol a listabol toroljenek
		local masikcargotypes = [];
		masikcargotypes.extend(this.cargotypes);
		return masikcargotypes;
	}
	
	return list;
}

function FGCargo::CreateCargosList () {
	if (this.cargotype_valuables != null && GSCargo.IsValidCargo(this.cargotype_valuables))
		this.cargotypes.append(this.cargotype_valuables);
	if (this.cargotype_steel != null && GSCargo.IsValidCargo(this.cargotype_steel)) { // a FIRS-ben at van nevezve a STEL acelrol femre
		if (this.cargotype_alcohol == null) // tehat nem FIRS van betoltve
			this.cargotypes.append(this.cargotype_steel);
		else
			this.cargotypes.append(this.cargotype_metal);
		
		this.isFIRSGame = this.cargotype_alcohol != null;
	}
	if (this.cargotype_ironore != null && GSCargo.IsValidCargo(this.cargotype_ironore))
		this.cargotypes.append(this.cargotype_ironore);
	if (this.cargotype_wood != null && GSCargo.IsValidCargo(this.cargotype_wood))
		this.cargotypes.append(this.cargotype_wood);
	if (this.cargotype_grain != null && GSCargo.IsValidCargo(this.cargotype_grain))
		this.cargotypes.append(this.cargotype_grain);
	if (this.cargotype_goods != null && GSCargo.IsValidCargo(this.cargotype_goods))
		this.cargotypes.append(this.cargotype_goods);
	if (this.cargotype_livestock != null && GSCargo.IsValidCargo(this.cargotype_livestock))
		this.cargotypes.append(this.cargotype_livestock);
	if (this.cargotype_oil != null && GSCargo.IsValidCargo(this.cargotype_oil))
		this.cargotypes.append(this.cargotype_oil);
	if (this.cargotype_mail != null && GSCargo.IsValidCargo(this.cargotype_mail))
		this.cargotypes.append(this.cargotype_mail);
	if (this.cargotype_coal != null && GSCargo.IsValidCargo(this.cargotype_coal))
		this.cargotypes.append(this.cargotype_coal);
	if (this.cargotype_passengers != null && GSCargo.IsValidCargo(this.cargotype_passengers))
		this.cargotypes.append(this.cargotype_passengers);
	
	
	
	if (this.cargotype_paper != null && GSCargo.IsValidCargo(this.cargotype_paper))
		this.cargotypes.append(this.cargotype_paper);
	if (this.cargotype_wheat != null && GSCargo.IsValidCargo(this.cargotype_wheat))
		this.cargotypes.append(this.cargotype_wheat);
	if (this.cargotype_food != null && GSCargo.IsValidCargo(this.cargotype_food))
		this.cargotypes.append(this.cargotype_food);
	if (this.cargotype_gold != null && GSCargo.IsValidCargo(this.cargotype_gold))
		this.cargotypes.append(this.cargotype_gold);
	if (this.cargotype_rubber != null && GSCargo.IsValidCargo(this.cargotype_rubber))
		this.cargotypes.append(this.cargotype_rubber);
	if (this.cargotype_fruit != null && GSCargo.IsValidCargo(this.cargotype_fruit))
		this.cargotypes.append(this.cargotype_fruit);
	if (this.cargotype_maize != null && GSCargo.IsValidCargo(this.cargotype_maize))
		this.cargotypes.append(this.cargotype_maize);
	if (this.cargotype_copperore != null && GSCargo.IsValidCargo(this.cargotype_copperore))
		this.cargotypes.append(this.cargotype_copperore);
	if (this.cargotype_water != null && GSCargo.IsValidCargo(this.cargotype_water))
		this.cargotypes.append(this.cargotype_water);
	if (this.cargotype_diamonds != null && GSCargo.IsValidCargo(this.cargotype_diamonds))
		this.cargotypes.append(this.cargotype_diamonds);
	if (this.cargotype_sugar != null && GSCargo.IsValidCargo(this.cargotype_sugar))
		this.cargotypes.append(this.cargotype_sugar);
	if (this.cargotype_toys != null && GSCargo.IsValidCargo(this.cargotype_toys))
		this.cargotypes.append(this.cargotype_toys);
	if (this.cargotype_batteries != null && GSCargo.IsValidCargo(this.cargotype_batteries))
		this.cargotypes.append(this.cargotype_batteries);
	if (this.cargotype_sweets != null && GSCargo.IsValidCargo(this.cargotype_sweets))
		this.cargotypes.append(this.cargotype_sweets);
	if (this.cargotype_toffee != null && GSCargo.IsValidCargo(this.cargotype_toffee))
		this.cargotypes.append(this.cargotype_toffee);
	if (this.cargotype_cola != null && GSCargo.IsValidCargo(this.cargotype_cola))
		this.cargotypes.append(this.cargotype_cola);
	if (this.cargotype_cottoncandy != null && GSCargo.IsValidCargo(this.cargotype_cottoncandy))
		this.cargotypes.append(this.cargotype_cottoncandy);
	if (this.cargotype_bubbles != null && GSCargo.IsValidCargo(this.cargotype_bubbles))
		this.cargotypes.append(this.cargotype_bubbles);
	if (this.cargotype_plastic != null && GSCargo.IsValidCargo(this.cargotype_plastic))
		this.cargotypes.append(this.cargotype_plastic);
	if (this.cargotype_fizzy != null && GSCargo.IsValidCargo(this.cargotype_fizzy))
		this.cargotypes.append(this.cargotype_fizzy);
	
	//	New Cargos	 these cargos are only present when NewGRF industry sets are used
	if (this.cargotype_bauxite != null && GSCargo.IsValidCargo(this.cargotype_bauxite))
		this.cargotypes.append(this.cargotype_bauxite);
	if (this.cargotype_alcohol != null && GSCargo.IsValidCargo(this.cargotype_alcohol))
		this.cargotypes.append(this.cargotype_alcohol);
	if (this.cargotype_buildingmaterials != null && GSCargo.IsValidCargo(this.cargotype_buildingmaterials))
		this.cargotypes.append(this.cargotype_buildingmaterials);
	if (this.cargotype_bricks != null && GSCargo.IsValidCargo(this.cargotype_bricks))
		this.cargotypes.append(this.cargotype_bricks);
	if (this.cargotype_ceramics != null && GSCargo.IsValidCargo(this.cargotype_ceramics))
		this.cargotypes.append(this.cargotype_ceramics);
	if (this.cargotype_cereals != null && GSCargo.IsValidCargo(this.cargotype_cereals))
		this.cargotypes.append(this.cargotype_cereals);
	if (this.cargotype_clay != null && GSCargo.IsValidCargo(this.cargotype_clay))
		this.cargotypes.append(this.cargotype_clay);
	if (this.cargotype_cement != null && GSCargo.IsValidCargo(this.cargotype_cement))
		this.cargotypes.append(this.cargotype_cement);
	if (this.cargotype_copper != null && GSCargo.IsValidCargo(this.cargotype_copper))
		this.cargotypes.append(this.cargotype_copper);
	if (this.cargotype_dyes != null && GSCargo.IsValidCargo(this.cargotype_dyes))
		this.cargotypes.append(this.cargotype_dyes);
	if (this.cargotype_engineeringsupplies != null && GSCargo.IsValidCargo(this.cargotype_engineeringsupplies))
		this.cargotypes.append(this.cargotype_engineeringsupplies);
	if (this.cargotype_fertiliser != null && GSCargo.IsValidCargo(this.cargotype_fertiliser))
		this.cargotypes.append(this.cargotype_fertiliser);
	if (this.cargotype_fibrecrops != null && GSCargo.IsValidCargo(this.cargotype_fibrecrops))
		this.cargotypes.append(this.cargotype_fibrecrops);
	if (this.cargotype_fish != null && GSCargo.IsValidCargo(this.cargotype_fish))
		this.cargotypes.append(this.cargotype_fish);
	if (this.cargotype_farmsupplies != null && GSCargo.IsValidCargo(this.cargotype_farmsupplies))
		this.cargotypes.append(this.cargotype_farmsupplies);
	if (this.cargotype_frvgfruit != null && GSCargo.IsValidCargo(this.cargotype_frvgfruit))
		this.cargotypes.append(this.cargotype_frvgfruit);
	if (this.cargotype_glass != null && GSCargo.IsValidCargo(this.cargotype_glass))
		this.cargotypes.append(this.cargotype_glass);
	if (this.cargotype_gravelballast != null && GSCargo.IsValidCargo(this.cargotype_gravelballast))
		this.cargotypes.append(this.cargotype_gravelballast);
	if (this.cargotype_limestone != null && GSCargo.IsValidCargo(this.cargotype_limestone))
		this.cargotypes.append(this.cargotype_limestone);
	if (this.cargotype_milk != null && GSCargo.IsValidCargo(this.cargotype_milk))
		this.cargotypes.append(this.cargotype_milk);
	if (this.cargotype_manufacturingsupplies != null && GSCargo.IsValidCargo(this.cargotype_manufacturingsupplies))
		this.cargotypes.append(this.cargotype_manufacturingsupplies);
	if (this.cargotype_oilseed != null && GSCargo.IsValidCargo(this.cargotype_oilseed))
		this.cargotypes.append(this.cargotype_oilseed);
	if (this.cargotype_petrolfueloil != null && GSCargo.IsValidCargo(this.cargotype_petrolfueloil))
		this.cargotypes.append(this.cargotype_petrolfueloil);
	if (this.cargotype_plasplastic != null && GSCargo.IsValidCargo(this.cargotype_plasplastic))
		this.cargotypes.append(this.cargotype_plasplastic);
	if (this.cargotype_potash != null && GSCargo.IsValidCargo(this.cargotype_potash))
		this.cargotypes.append(this.cargotype_potash);
	if (this.cargotype_recyclables != null && GSCargo.IsValidCargo(this.cargotype_recyclables))
		this.cargotypes.append(this.cargotype_recyclables);
	if (this.cargotype_refinedproducts != null && GSCargo.IsValidCargo(this.cargotype_refinedproducts))
		this.cargotypes.append(this.cargotype_refinedproducts);
	if (this.cargotype_sand != null && GSCargo.IsValidCargo(this.cargotype_sand))
		this.cargotypes.append(this.cargotype_sand);
	if (this.cargotype_scraptmetal != null && GSCargo.IsValidCargo(this.cargotype_scraptmetal))
		this.cargotypes.append(this.cargotype_scraptmetal);
	if (this.cargotype_sugarbeet != null && GSCargo.IsValidCargo(this.cargotype_sugarbeet))
		this.cargotypes.append(this.cargotype_sugarbeet);
	if (this.cargotype_sugarcane != null && GSCargo.IsValidCargo(this.cargotype_sugarcane))
		this.cargotypes.append(this.cargotype_sugarcane);
	if (this.cargotype_sulphur != null && GSCargo.IsValidCargo(this.cargotype_sulphur))
		this.cargotypes.append(this.cargotype_sulphur);
	if (this.cargotype_tourists != null && GSCargo.IsValidCargo(this.cargotype_tourists))
		this.cargotypes.append(this.cargotype_tourists);
	if (this.cargotype_vehicles != null && GSCargo.IsValidCargo(this.cargotype_vehicles))
		this.cargotypes.append(this.cargotype_vehicles);
	if (this.cargotype_wdprwood != null && GSCargo.IsValidCargo(this.cargotype_wdprwood))
		this.cargotypes.append(this.cargotype_wdprwood);
	if (this.cargotype_wool != null && GSCargo.IsValidCargo(this.cargotype_wool))
		this.cargotypes.append(this.cargotype_wool);
	
	
	//	Special Cargos	 these cargos are for use outside industry sets and do not represent transporting anything
	if (this.cargotype_default != null && GSCargo.IsValidCargo(this.cargotype_default))
		this.cargotypes.append(this.cargotype_default);
	
	if (this.cargotype_locomotiveregearing != null && GSCargo.IsValidCargo(this.cargotype_locomotiveregearing))
		this.cargotypes.append(this.cargotype_locomotiveregearing);
	
	//	Deprecated Cargos	 these cargos are not used by any current industry set, and are listed here for backwards compatibility
	if (this.cargotype_fuel != null && GSCargo.IsValidCargo(this.cargotype_fuel))
		this.cargotypes.append(this.cargotype_fuel);
	if (this.cargotype_rawsugar != null && GSCargo.IsValidCargo(this.cargotype_rawsugar))
		this.cargotypes.append(this.cargotype_rawsugar);
	if (this.cargotype_scrpscrapmetal != null && GSCargo.IsValidCargo(this.cargotype_scrpscrapmetal))
		this.cargotypes.append(this.cargotype_scrpscrapmetal);
	if (this.cargotype_tropicwood != null && GSCargo.IsValidCargo(this.cargotype_tropicwood))
		this.cargotypes.append(this.cargotype_tropicwood);
	if (this.cargotype_waste != null && GSCargo.IsValidCargo(this.cargotype_waste))
		this.cargotypes.append();
}

function FGCargo::CheckCargoNames () {
	local cargos = GSCargoList();
	local i = 0;
	foreach (cargo_id, _ in cargos) {
		local label = GSCargo.GetCargoLabel(cargo_id);
		if (this.logmode)
			GSLog.Info("[FGCargo::CheckCargoNames] Cargo id: " + (cargo_id < 10 ? ("0" + cargo_id) : cargo_id) + ", label: " + label);
		
		i++;
		
		if (label == "VALU")
			this.cargotype_valuables = cargo_id;
		else if (label == "STEL") {
			this.cargotype_steel = cargo_id;
			this.cargotype_metal = cargo_id; // ugyanugy STEL mind a ketto, csak a FIRS-ben metal a neve
		}
		else if (label == "IORE")
			this.cargotype_ironore = cargo_id;
		else if (label == "WOOD")
			this.cargotype_wood = cargo_id;
		else if (label == "GRAI")
			this.cargotype_grain = cargo_id;
		else if (label == "GOOD")
			this.cargotype_goods = cargo_id;
		else if (label == "LVST")
			this.cargotype_livestock = cargo_id;
		else if (label == "OIL_")
			this.cargotype_oil = cargo_id;
		else if (label == "MAIL")
			this.cargotype_mail = cargo_id;
		else if (label == "COAL")
			this.cargotype_coal = cargo_id;
		else if (label == "PASS")
			this.cargotype_passengers = cargo_id;
	
		else if (label == "PAPR")
			this.cargotype_paper = cargo_id;
		else if (label == "WHEA")
			this.cargotype_wheat = cargo_id;
		else if (label == "FOOD")
			this.cargotype_food = cargo_id;
		else if (label == "GOLD")
			this.cargotype_gold = cargo_id;
		else if (label == "RUBR")
			this.cargotype_rubber = cargo_id;
		else if (label == "FRUT")
			this.cargotype_fruit = cargo_id;
		else if (label == "MAIZ")
			this.cargotype_maize = cargo_id;
		else if (label == "CORE")
			this.cargotype_copperore = cargo_id;
		else if (label == "WATR")
			this.cargotype_water = cargo_id;
		else if (label == "DIAM")
			this.cargotype_diamonds = cargo_id;
		else if (label == "SUGR")
			this.cargotype_sugar = cargo_id;
		else if (label == "TOYS")
			this.cargotype_toys = cargo_id;
		else if (label == "BATT")
			this.cargotype_batteries = cargo_id;
		else if (label == "SWET")
			this.cargotype_sweets = cargo_id;
		else if (label == "TOFF")
			this.cargotype_toffee = cargo_id;
		else if (label == "COLA")
			this.cargotype_cola = cargo_id;
		else if (label == "CTCD")
			this.cargotype_cottoncandy = cargo_id;
		else if (label == "BUBL")
			this.cargotype_bubbles = cargo_id;
		else if (label == "PLST")
			this.cargotype_plastic = cargo_id;
		else if (label == "FZDR")
			this.cargotype_fizzy = cargo_id;
		
		//	New Cargos	 these cargos are only present when NewGRF industry sets are used
		else if (label == "AORE")
			this.cargotype_bauxite = cargo_id;
		else if (label == "BEER")
			this.cargotype_alcohol = cargo_id;
		else if (label == "BDMT")
			this.cargotype_buildingmaterials = cargo_id;
		else if (label == "BRCK")
			this.cargotype_bricks = cargo_id;
		else if (label == "CERA")
			this.cargotype_ceramics = cargo_id;
		else if (label == "CERE")
			this.cargotype_cereals = cargo_id;
		else if (label == "CLAY")
			this.cargotype_clay = cargo_id;
		else if (label == "CMNT")
			this.cargotype_cement = cargo_id;
		else if (label == "COPR")
			this.cargotype_copper = cargo_id;
		else if (label == "DYES")
			this.cargotype_dyes = cargo_id;
//		else if (label == "ENSP")
//			this.cargotype_engineeringsupplies = cargo_id;
		else if (label == "FERT")
			this.cargotype_fertiliser = cargo_id;
		else if (label == "FICR")
			this.cargotype_fibrecrops = cargo_id;
		else if (label == "FISH")
			this.cargotype_fish = cargo_id;
//		else if (label == "FMSP")
//			this.cargotype_farmsupplies = cargo_id;
		else if (label == "FRVG")
			this.cargotype_frvgfruit = cargo_id;
		else if (label == "GLAS")
			this.cargotype_glass = cargo_id;
		else if (label == "GRVL")
			this.cargotype_gravelballast = cargo_id;
		else if (label == "LIME")
			this.cargotype_limestone = cargo_id;
		else if (label == "MILK")
			this.cargotype_milk = cargo_id;
//		else if (label == "MNSP")
//			this.cargotype_manufacturingsupplies = cargo_id;
		else if (label == "OLSD")
			this.cargotype_oilseed = cargo_id;
		else if (label == "PETR")
			this.cargotype_petrolfueloil = cargo_id;
		else if (label == "PLAS")
			this.cargotype_plasplastic = cargo_id;
		else if (label == "POTA")
			this.cargotype_potash = cargo_id;
		else if (label == "RCYC")
			this.cargotype_recyclables = cargo_id;
		else if (label == "RFPR")
			this.cargotype_refinedproducts = cargo_id;
		else if (label == "SAND")
			this.cargotype_sand = cargo_id;
		else if (label == "SCMT")
			this.cargotype_scraptmetal = cargo_id;
		else if (label == "SGBT")
			this.cargotype_sugarbeet = cargo_id;
		else if (label == "SGCN")
			this.cargotype_sugarcane = cargo_id;
		else if (label == "SULP")
			this.cargotype_sulphur = cargo_id;
		else if (label == "TOUR")
			this.cargotype_tourists = cargo_id;
		else if (label == "VEHI")
			this.cargotype_vehicles = cargo_id;
		else if (label == "WDPR")
			this.cargotype_wdprwood = cargo_id;
		else if (label == "WOOL")
			this.cargotype_wool = cargo_id;
		
		
		//	Special Cargos	 these cargos are for use outside industry sets and do not represent transporting anything
		else if (label == "DFLT")
			this.cargotype_default = cargo_id;
		
		else if (label == "GEAR")
			this.cargotype_locomotiveregearing = cargo_id;
		
		//	Deprecated Cargos	 these cargos are not used by any current industry set, and are listed here for backwards compatibility
		else if (label == "FUEL")
			this.cargotype_fuel = cargo_id;
		else if (label == "RSGR")
			this.cargotype_rawsugar = cargo_id;
		else if (label == "SCRP")
			this.cargotype_scrpscrapmetal = cargo_id;
		else if (label == "TWOD")
			this.cargotype_tropicwood = cargo_id;
		else if (label == "WSTE")
			this.cargotype_waste = cargo_id;
	}

	if (this.logmode) {
		GSLog.Info("[FGCargo::CheckCargoNames] Available in map " + i + " cargo type");
		GSLog.Info("");
	}
}
