//MESSAGE_Competition_ALL_COMPANY <- 0;
//MESSAGE_


class FGCompany {
	// ide akkor jon, ha letrehozunk egy objektumot ebbol az osztalybol, itt kell inicializalni
	
	// eloszor jon az inicializalando valtozokat
//	create_transport_goal = null;
	company_id = null;
	vehicles = null;
	
	stations = null;
	towns = null;
	cargo = null; // main cargo objectje
	mainnut = null; // az a main delegate objektuma

	plus_cargos = null; // ebben fogom tarolni azokat a cargokat, amiket egy elvegzett feladat utan megmaradt plusz mennyiseg, hogy egy masik feladathoz hozza tudjam adni
	
	main_goals = null; // ez lehet barmilyen goal, a lenyeg, ami ebben van, akkor a gyozelemhez csak ezeket kell teljesiteni
	award_goals = null; // itt majd azokat taroljuk, amik nem kellenek a gyozelemhez, itt csak jutalmakat osztunk erte
	weak_award_goals = null; // itt majd azokat taroljuk, amik nem kellenek a gyozelemhez, itt csak jutalmakat osztunk erte, es nem lathato
	
	main_goals_completed = 0; // ebben taroljuk, hogy hany main goalt vegzett el
	main_goals_completed_first_plusz_scores = 0;
	main_goals_completed_months = 0; // ebben taroljuk majd, hogy hany honap alatt teljesitette a feladatot, es mennyi pontot kapott utana, pl 60-5, tehat 5 honap alatt teljesitette, igy 55 pontot kap
	
	main_goals_scores = 0;
	award_goals_scores = 0;
	weak_award_goals_scores = 0;
	
	award_enabled = true;
	
	// messages
	last_error_build_message = -1;
	
	
	// story book
	possibleWindowUpdateEnabled = true;			// ne frissitsuk feleslegesen az oldalt, ha nem muszaj
	
	possibleStoryPageID = null;					// ez a possible story oldal azonosito
	possibleElementList = null;					// ebben tarolom az elemeket
	possibleListNeedUpdate = false;				// ha ez true-ra vna alalitva, akkor frissitodik csak a szoveg (masodpercenkent frissulne amugy), egyebkent nem fog
	possibleStoryPageEnabled = true;			// ezzel meg vegleg kikapcsolhatjuk az oldal frissiteset
	
	companysGoalStatesStoryPageID = null;		// vallalat mellek kuldeteseinek kiiratasa tortenik itt
	companysGoalElementList = null;				// ebben tarolom az oldal elemeit
	
	gameStateSoryPageID = null;					// ebben taroljuk majd a jatek allasat
	gameStateElementList = null;				// ebben taroljuk majd a jatek allasanak elemeit


	fggc_id = 0; // Feca Goal Games Controller Company egyedi azonositoja
	fggc_joined = false; // sikeresen csatlakozott a jatekos a jatekhoz, es ez azert kell, hogy feleslegesen ne kuldozgessunk uzenetet, ha megsincs regisztralva a vallalat
	fggc_question = 0; // GSGoal.Question azonosito, hogy tudjam melyiket kell bezarni azonositas utan; ezt egyeleore nem mentem, mert betoltes utan ugy sem lesz mar erdekes...
	fggc_lastupdated_scores = 0 // az utolso pontok, amiket osszegyujtott es felfrissitesttuk mar
	fggc_versenyID = 0; // azert van ez itt, mert meg lehet igy csinalni, hogy tobb kulombozo kategoriaban jelentkeznek egyazon palyan
	
	
	// should = -1: barmennyi lehet
	
	// depos
	shouldRailDepos = -1;
	railDepos = null;
	shouldRoadDepos = -1;
	roadDepos = null;
	shouldWaterDepos = -1;
	waterDepos = null;
	
	// stations
	shouldRailStations = -1;
	railStations = null;
	shouldTruckStops = -1;
	truckStops = null;
	shouldBusStops = -1;
	busStops = null;
	shouldWaterDocks = -1;
	waterDocks = null;
	shouldAirPorts = -1;
	airPorts = null;
	
	// vehicles
	shouldRailVehicles = -1;
	railVehicles = null;
	shouldTruckVehicles = -1;
	truckVehicles = null;
	shouldBusVehicles = -1;
	busVehicles = null;
	shouldWaterVehicles = -1;
	waterVehicles = null;
	shouldAirVehicles = -1;
	airVehicles = null;
	
	// aztan a contructor
	constructor (a_company_id, a_cargo) {
		this.fggc_id = GSBase.RandRange(9000) + 1000
		GSLog.Info("companyid: " + a_company_id + ", fggc_id: " + fggc_id);
		this.company_id = a_company_id;
		this.vehicles = [];
		this.main_goals = [];
		this.award_goals = [];
		this.weak_award_goals = [];
		this.main_goals_scores = 0;
		this.award_goals_scores = 0;
		this.weak_award_goals_scores = 0;
		this.plus_cargos = [];

		this.main_goals_completed = 0; // ebben taroljuk, hogy hany main goalt vegzett el
		this.main_goals_completed_first_plusz_scores = 0;
		this.main_goals_completed_months = 0;
		
		this.stations = [];
		this.towns = [];
		this.cargo = a_cargo;
		
		// depos
		this.shouldRailDepos = -1;
		this.railDepos = [];
		this.shouldRoadDepos = -1;
		this.roadDepos = [];
		this.shouldWaterDepos = -1;
		this.waterDepos = [];
		
		// station
		this.shouldRailStations = -1;
		this.railStations = [];
		this.shouldTruckStops = -1;
		this.truckStops = [];
		this.shouldBusStops = -1;
		this.busStops = [];
		this.shouldWaterDocks = -1;
		this.waterDocks = [];
		this.shouldAirPorts = -1;
		this.airPorts = [];
		
		// vehicles
		this.shouldRailVehicles = -1;
		this.railVehicles = [];
		this.shouldTruckVehicles = -1;
		this.truckVehicles = [];
		this.shouldBusVehicles = -1;
		this.busVehicles = [];
		this.shouldWaterVehicles = -1;
		this.waterVehicles = [];
		this.shouldAirVehicles = -1;
		this.airVehicles = [];
		
		
		this.possibleElementList = [];
		this.companysGoalElementList = [];
		this.gameStateElementList = [];
	}
	
	// aztan az egyeb funkciok
}

function FGCompany::SaveToTable() {
	local goals = [];
	foreach (goal in this.main_goals) {
		goals.append(goal.SaveToTable());
	}
	foreach (goal in this.award_goals) {
		goals.append(goal.SaveToTable());
	}
	foreach (goal in this.weak_award_goals) {
		goals.append(goal.SaveToTable());
	}
	
	// igazaboal minden el van mentve, es igy automatikusan letre lehet hozni a golt, nem kell kulon elmentegetni
	/*
	local amain_goals = [];
	foreach (goal in this.main_goals) {
		amain_goals.append(goal.SaveToTable());
	}
	
	local aaward_goals = [];
	foreach (goal in this.award_goals) {
		aaward_goals.append(goal.SaveToTable());
	}
	
	local aweak_award_goals = [];
	foreach (goal in this.weak_award_goals) {
		aweak_award_goals.append(goal.SaveToTable());
	}
	*/
	
	return {
		company_id = this.company_id,

		// ADMIN PORT mentesek
		fggc_id = this.fggc_id,
		fggc_joined = this.fggc_joined,
		fggc_lastupdated_scores = this.fggc_lastupdated_scores,
		fggc_versenyID = this.fggc_versenyID,

		vehicles = this.vehicles,
		stations = this.stations,
		towns = this.towns,
		
		goals = goals,
//		main_goals = amain_goals,
//		award_goals = aaward_goals,
//		weak_award_goals = aweak_award_goals,

		plus_cargos = this.plus_cargos,

		main_goals_completed = this.main_goals_completed,
		main_goals_completed_first_plusz_scores = this.main_goals_completed_first_plusz_scores,
		main_goals_completed_months = this.main_goals_completed_months,
		
		main_goals_scores = this.main_goals_scores,
		award_goals_scores = this.award_goals_scores,
		weak_award_goals_scores = this.weak_award_goals_scores,	
		
		
		award_enabled = this.award_enabled,
		last_error_build_message = this.last_error_build_message,

		// depos
		shouldRailDepos = this.shouldRailDepos,
		railDepos = this.railDepos,
		shouldRoadDepos = this.shouldRoadDepos,
		roadDepos = this.roadDepos,
		shouldWaterDepos = this.shouldWaterDepos,
		waterDepos = this.waterDepos,
		
		// stations
		shouldRailStations = this.shouldRailStations,
		railStations = this.railStations,
		shouldTruckStops = this.shouldTruckStops,
		truckStops = this.truckStops,
		shouldBusStops = this.shouldBusStops,
		busStops = this.busStops,
		shouldWaterDocks = this.shouldWaterDocks,
		waterDocks = this.waterDocks,
		shouldAirPorts = this.shouldAirPorts,
		airPorts = this.airPorts,
		
		// vehicles
		shouldRailVehicles = this.shouldRailVehicles,
		railVehicles =  this.railVehicles,
		shouldTruckVehicles = this.shouldTruckVehicles,
		truckVehicles = this.truckVehicles,
		shouldBusVehicles = this.shouldBusVehicles,
		busVehicles = this.busVehicles,
		shouldWaterVehicles =  this.shouldWaterVehicles,
		waterVehicles =  this.waterVehicles,
		shouldAirVehicles = this.shouldAirVehicles,
		airVehicles = this.airVehicles,
		
		possibleStoryPageID = this.possibleStoryPageID,							// ez a possible story oldal azonosito
		possibleElementList = this.possibleElementList,							// ebben tarolom az elemeket
		possibleListNeedUpdate = this.possibleListNeedUpdate,						// ha ez true-ra vna alalitva, akkor frissitodik csak a szoveg (masodpercenkent frissulne amugy), egyebkent nem fog
		possibleStoryPageEnabled = this.possibleStoryPageEnabled,					// ezzel meg vegleg kikapcsolhatjuk az oldal frissiteset
		
		companysGoalStatesStoryPageID = this.companysGoalStatesStoryPageID,		// vallalat mellek kuldeteseinek kiiratasa tortenik itt
		companysGoalElementList = this.companysGoalElementList,					// ebben tarolom az oldal elemeit
		
		gameStateSoryPageID = this.gameStateSoryPageID,							// ebben taroljuk majd a jatek allasat
		gameStateElementList = this.gameStateElementList,							// ebben taroljuk majd a jatek allasanak elemeit
	}
}

function FGCompany::LoadFromTable(table, version, goalsettings) {
	this.company_id = table.company_id;
	
	this.vehicles = table.vehicles;
	this.stations = table.stations;
	this.towns = table.towns;

	// ADMIN PORT mentesek
	this.fggc_id = table.fggc_id;
	this.fggc_joined = table.fggc_joined;
	this.fggc_lastupdated_scores = table.fggc_lastupdated_scores;
	this.fggc_versenyID = table.fggc_versenyID;

	foreach (goalTable in table.goals) {
		// goaloknak megfelelo loadot kell csinalni, mert pl a tobbinek nem biztos, hogy kell a cargo is
		switch (goalTable.goal_type) {
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				local goal = FGTransportGoal(null, this.mainnut);
				goal.LoadFromTable(goalTable, version, this.cargo, goalsettings);
				this.AddGoal(goal);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				local goal = FGTownGrowthGoal(null, this.mainnut, goalTable.goal_town_type, goalTable.town_id);
				goal.LoadFromTable(goalTable, version, goalsettings);
				this.AddGoal(goal);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
			default:
				GSLog.Error("[FGCompany::LoadFromTable] Invalid goal type!");
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}
	}
	
//	this.main_goals = ;
//	this.award_goals = table.award_goals;
//	this.weak_award_goals = table.weak_award_goals;

	this.plus_cargos = table.plus_cargos;

	this.main_goals_completed = table.main_goals_completed;
	this.main_goals_completed_first_plusz_scores = table.main_goals_completed_first_plusz_scores;
	this.main_goals_completed_months = table.main_goals_completed_months;
	
	this.main_goals_scores = table.main_goals_scores;
	this.award_goals_scores = table.award_goals_scores;
	this.weak_award_goals_scores = table.weak_award_goals_scores;
	
	this.award_enabled = table.award_enabled;
	this.last_error_build_message = table.last_error_build_message;
	
	// depos
	this.shouldRailDepos = table.shouldRailDepos;
	this.railDepos = table.railDepos;
	this.shouldRoadDepos = table.shouldRoadDepos;
	this.roadDepos = table.roadDepos;
	this.shouldWaterDepos = table.shouldWaterDepos;
	this.waterDepos = table.waterDepos;
	
	// stations
	this.shouldRailStations = table.shouldRailStations;
	this.railStations = table.railStations;
	this.shouldTruckStops = table.shouldTruckStops;
	this.truckStops = table.truckStops;
	this.shouldBusStops = table.shouldBusStops;
	this.busStops = table.busStops;
	this.shouldWaterDocks = table.shouldWaterDocks;
	this.waterDocks = table.waterDocks;
	this.shouldAirPorts = table.shouldAirPorts;
	this.airPorts = table.airPorts;
	
	// vehicles
	this.shouldRailVehicles = table.shouldRailVehicles;
	this.railVehicles =  table.railVehicles;
	this.shouldTruckVehicles = table.shouldTruckVehicles;
	this.truckVehicles = table.truckVehicles;
	this.shouldBusVehicles = table.shouldBusVehicles;
	this.busVehicles = table.busVehicles;
	this.shouldWaterVehicles =  table.shouldWaterVehicles;
	this.waterVehicles =  table.waterVehicles;
	this.shouldAirVehicles = table.shouldAirVehicles;
	this.airVehicles = table.airVehicles;
	
	// story book	
	this.possibleStoryPageID = table.possibleStoryPageID;							// ez a possible story oldal azonosito
	this.possibleElementList = table.possibleElementList;							// ebben tarolom az elemeket
	this.possibleListNeedUpdate = table.possibleListNeedUpdate;						// ha ez true-ra vna alalitva, akkor frissitodik csak a szoveg (masodpercenkent frissulne amugy); egyebkent nem fog
	this.possibleStoryPageEnabled = table.possibleStoryPageEnabled;					// ezzel meg vegleg kikapcsolhatjuk az oldal frissiteset
	
	this.companysGoalStatesStoryPageID = table.companysGoalStatesStoryPageID;		// vallalat mellek kuldeteseinek kiiratasa tortenik itt
	this.companysGoalElementList = table.companysGoalElementList;					// ebben tarolom az oldal elemeit
	
	this.gameStateSoryPageID = table.gameStateSoryPageID;							// ebben taroljuk majd a jatek allasat
	this.gameStateElementList = table.gameStateElementList;							// ebben taroljuk majd a jatek allasanak elemeit
}

// - # FGCompany init
function FGCompany::setdelegate(delegte) {
	this.mainnut = delegte;
}

function FGCompany::SetCargo(a_cargo) {
	this.cargo = a_cargo;
}

function FGCompany::GetCompanyMode() {
	for (local company_id_t = GSCompany.COMPANY_FIRST; company_id_t <= GSCompany.COMPANY_LAST; company_id_t++) {
		local a_company_id = GSCompany.ResolveCompanyID(company_id_t);
		if (a_company_id == GSCompany.COMPANY_INVALID) continue;
		if (a_company_id == this.company_id) {
			return company_id_t;
			break;
		}
	}
	
	GSLog.Warning("Invalid (bankrupt???) company?");
	return null;
}

function FGCompany::SetPossible(possibleTable) {
	this.shouldRailDepos = possibleTable.shouldRailDepos;
	this.shouldRoadDepos = possibleTable.shouldRoadDepos;
	this.shouldWaterDepos = possibleTable.shouldWaterDepos;
	this.shouldRailStations = possibleTable.shouldRailStations;
	this.shouldTruckStops = possibleTable.shouldTruckStops;
	this.shouldBusStops = possibleTable.shouldBusStops;
	this.shouldWaterDocks = possibleTable.shouldWaterDocks;
	this.shouldAirPorts = possibleTable.shouldAirPorts;
	this.shouldRailVehicles = possibleTable.shouldRailVehicles;
	this.shouldTruckVehicles = possibleTable.shouldTruckVehicles;
	this.shouldBusVehicles = possibleTable.shouldBusVehicles;
	this.shouldWaterVehicles = possibleTable.shouldWaterVehicles;
	this.shouldAirVehicles = possibleTable.shouldAirVehicles;
}

function FGCompany::SendCode() {
	this.fggc_question = this.mainnut.message_counter++;
	GSGoal.Question(this.fggc_question, this.company_id, GSText(GSText.STR_ADMINPORT_COMPANY_WEBKEY, this.fggc_id), GSGoal.QT_INFORMATION, GSGoal.BUTTON_CLOSE);
}


/***********************************************************************************************************************************************
 
 FGVehicle support

***********************************************************************************************************************************************/
// - # FGVehicle support
/*
function FGCompany::CheckVehicle(vehicle_id) {
	if (!GSVehicle.IsValidVehicle(vehicle_id))
		return;

	local counter = 0;
	local van = false;
	foreach (vehicle in this.vehicles) {
		// transport goalokat is ellenorizni kell
		local kellvalamittransportalni = false;
		foreach (transportgoal in this.main_goals) {
			if (GSVehicle.GetCapacity(vehicle_id, transportgoal.cargo_id) > 0) {
				kellvalamittransportalni = true;
			}
		}

		if (kellvalamittransportalni) {
			if (vehicle.vehicle_id == vehicle_id) {
				van = true;
				break;
			}
		}
		else {
			// ha nem kell semmit sem szallitani, de megis hozzavan adva, akkor torolni kellene
			if (vehicle.vehicle_id == vehicle_id) {
				// itt kellene torolni
				this.vehicles.remove(counter);
				van = true;
				break;
			}
			
			van = true;
		}
		
		counter++;
	}
	
	if (!van) {
		local vehicle = FGVehicle(vehicle_id);
		this.vehicles.append(vehicle);
		vehicle.CheckCargos();
	}
}

function FGCompany::VehicleForID(vehicle_id) {
	if (!GSVehicle.IsValidVehicle(vehicle_id))
		return null;
		
	foreach (vehicle in this.vehicles) {
		if (vehicle.vehicle_id == vehicle_id) {
			return vehicle;
		}
	}
	
	return null;
}

// ez mar nem vehicleid alapjan megy, hanem az FGVehicle alapjan
function FGCompany::VehicleState(vehicle) {
	if (!GSVehicle.IsValidVehicle(vehicle.vehicle_id)) {
		// el kell tavolitani a vehicle/t a megfigyelesbol
		local counter = 0;
		foreach (avehicle in this.vehicles) {
			if (avehicle.vehicle_id == vehicle.vehicle_id) {
				this.vehicles.remove(counter);
				break;
			}

			counter++;
		}
		
		return null; // nincs ilyen vehicle mar
	}
	
	if (GSVehicle.GetState(vehicle.vehicle_id) == GSVehicle.VS_AT_STATION) {
		return 1; // allomason rakodik
	}
	else {
		return 0; // uton
	}
}
*/

/***********************************************************************************************************************************************
 
 FGStation support
 
 ***********************************************************************************************************************************************/
// - # FGStation support
/*
function FGCompany::UpdateStations() {
	// eloszor ellenorizzuk az osszes allomast, hogy nem szunt-e meg kozben valamelyik
	for (local i = (this.stations.len() - 1); i >= 0; i--) {
		if (!GSStation.IsValidStation(this.stations[i].station_id))
			this.stations.remove(i);
	}
	
	local allStations = GSStationList();
	foreach (station_id, _ in allStations) {
		this.AddStation(station_id); // hozzaadjuk, es sem adja hozza, ha mar van ilyen...
	}
}

function FGCompany::AddStation(station_id) {
	if (!GSStation.IsValidStation(station_id))
		return;
	
	local van = false;
	foreach (station in this.stations) {
		if (station.station_id == station_id) {
			van = true;
			break;
		}
	}
	
	if (!van) {
		this.stations.append(FGStation(station_id));
	}
}

function FGCompany::RemoveStation(station_id) {
	local counter = 0;
	foreach (station in this.stations) {
		if (station.station_id == station_id) {
			this.stations.remove(counter);
			return;
		}
		counter++;
	}
}

function FGCompany::GetStations() {
	return this.stations;
}
*/

/***********************************************************************************************************************************************
 
 FGTown support
 
 ***********************************************************************************************************************************************/
// - # FGTown support
/*
function FGCompany::UpdateTowns () {
	// eloszor ellenorizzuk az osszes allomast, hogy nem szunt-e meg kozben valamelyik
	for (local i = (this.towns.len() - 1); i >= 0; i--) {
		if (!GSTown.IsValidTown(this.towns[i]))
			this.towns.remove(i);
	}
	
	local townlist = GSTownList();
	
	foreach (goal in this.main_goals) {
		local goal_id = goal.goal_id;
		local cargo_id = goal.cargo_id;
		
		local allIndustries = GSIndustryList_CargoAccepting(cargo_id);
		foreach (industry_id, _ in allIndustries) {
			local ind_loc = GSIndustry.GetLocation(industry_id);
			local town_id = GSTile.GetTownAuthority(ind_loc);
			// ha nincs ervenyes varos, amie lenne a terulet, akkor a legkozelebbi varose...
			if (!GSTown.IsValidTown(town_id))
				town_id = GSTile.GetClosestTown(ind_loc);
			this.AddTown(town_id);
		}
	}
}

function FGCompany::AddTown (town_id) {
	if (!GSTown.IsValidTown(town_id))
		return;
	
	local van = false;
	foreach (a_town_id in this.towns) {
		if (a_town_id == town_id) {
			van = true;
			break;
		}
	}
	
	if (!van) {
//		GSLog.Info("[FGCompany::AddTown] Add Town: " + GSTown.GetName(town_id));
		this.towns.append(town_id);
	}
}

function FGCompany::RemoveTown (town_id) {
	local counter = 0;
	foreach (a_town_id in this.towns) {
		if (a_town_id == town_id) {
			this.stations.remove(counter);
			return;
		}
		counter++;
	}
}

function FGCompany::GetTowns () {
	return this.towns;
}
*/



/***********************************************************************************************************************************************
 
 FGMainGoal support
 
 ***********************************************************************************************************************************************/

// - # FGBaseGoal support
function FGCompany::GetTableOfGoal(goal) {
	if (goal == null)
		return null;
	
	foreach (a_goal in this.main_goals)
		if (a_goal == goal)
			return this.main_goals;
	
	foreach (a_goal in this.award_goals)
		if (a_goal == goal)
			return this.award_goals;
	
	foreach (a_goal in this.weak_award_goals)
		if (a_goal == goal)
			return this.weak_award_goals;
	
	return null;
}

function FGCompany::ResetCompany() {
	this.RemoveAllGoals();
	this.SetPossible(this.mainnut.initialPossibilities);
	
	this.vehicles.clear();
	this.stations.clear();
	this.towns.clear();
	
	this.main_goals.clear(); // ez lehet barmilyen goal, a lenyeg, ami ebben van, akkor a gyozelemhez csak ezeket kell teljesiteni
	this.award_goals.clear(); // itt majd azokat taroljuk, amik nem kellenek a gyozelemhez, itt csak jutalmakat osztunk erte
	this.weak_award_goals.clear(); // itt majd azokat taroljuk, amik nem kellenek a gyozelemhez, itt csak jutalmakat osztunk erte, es nem lathato
	
	this.main_goals_completed = 0; // ebben taroljuk, hogy hany main goalt vegzett el
	this.main_goals_completed_first_plusz_scores = 0;
	
	this.main_goals_scores = 0;
	this.award_goals_scores = 0;
	this.weak_award_goals_scores = 0;
	
	this.award_enabled = true;
	
	// messages
	this.last_error_build_message = -1;
	
	// should = -1: barmennyi lehet
	
	// depos
	this.railDepos.clear();
	this.roadDepos.clear();
	this.waterDepos.clear();
	
	// stations
	this.railStations.clear();
	this.truckStops.clear();
	this.busStops.clear();
	this.waterDocks.clear();
	this.airPorts.clear();
	
	// vehicles
	this.railVehicles.clear();
	this.truckVehicles.clear();
	this.busVehicles.clear();
	this.waterVehicles.clear();
	this.airVehicles.clear();
}

function FGCompany::AddGoal(goal) {
	if (goal.goal_main)
		this.AddGoalToMain(goal);
	else if (goal.goal_weak)
		this.AddGoalToWeakAward(goal);
	else
		this.AddGoalToAward(goal);
}

function FGCompany::RemoveGoal(goal) {
	if (this.mainnut.logmode)
		GSLog.Info("[FGCompany::RemoveGoal] remove goal id: " + goal.goal_id);
	if (goal.goal_main)
		this.RemoveMainGoal(goal.goal_id);
	else if (goal.goal_weak)
		this.RemoveWeakAwardGoal(goal.goal_id);
	else
		this.RemoveAwardGoal(goal.goal_id);
}

function FGCompany::RemoveAllGoals() {
	local i;
	if (this.main_goals.len() > 0) {
		for (i = this.main_goals.len() - 1; i >= 0; i--) {
			local goal_id = this.main_goals[i].goal_id;
			if (goal_id != null) {
				if (GSGoal.IsValidGoal(goal_id))
					GSGoal.Remove(goal_id);
			}
			this.main_goals.remove(i);
		}
	}
	
	if (this.award_goals.len() > 0) {
		for (i = this.award_goals.len() - 1; i >= 0; i--)
			this.award_goals.remove(i);
	}
	
	if (this.weak_award_goals.len() > 0) {
		for (i = this.weak_award_goals.len() - 1; i >= 0; i--)
			this.weak_award_goals.remove(i);
	}
}

function FGCompany::UpdateGoals() {
	for (local i = 0; i < this.main_goals.len(); i++) {
		local goal = this.main_goals[i];
		local goal_id = goal.goal_id;
		
		if (goal_id == null)
			continue; // ha meg nem jelent meg, akkor nem ellenorizzuk
		
		switch (goal.goal_type) {
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				// ezt a feladatot itt nem kell ellenorizni
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				if (!goal.isCompleted) {
					goal.UpdateGoal();
					
					if (goal.isCompleted) {
						this.main_goals.remove(i);
						i--;
						this.mainnut.CompanyCompletedGoal(this, goal);
						if (GSGoal.IsValidGoal(goal_id))
							GSGoal.Remove(goal_id);
					}
				}
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
		}
		// TODO [feladat tipus] tobbi goal-t is hozza kell adni
	}
}

function FGCompany::UpdateAllScoresAndTexts() {
	// ide a main goal es az award goal szovegek frissitesei jonnek
	foreach (goal in this.main_goals) {
		this.UpdateScoresAndTextsToGoal(goal);
	}
	
	foreach (goal in this.award_goals) {
		this.UpdateScoresAndTextsToGoal(goal);
	}
	
	foreach (goal in this.weak_award_goals) {
		this.UpdateScoresAndTextsToGoal(goal);
	}
}

function FGCompany::UpdateScoresAndTextsToGoal(goal) {
	// ha nem latszik a goal (mert sorba adjuk a feladatokat), akkor ez az ertek null
	if (goal.goal_id == null)
		return;
	
	local actualGoalPercent = goal.GetActualGoalPercent();
	
	// ha nemvaltozott egyik ertek sem, akkor nincs mit frissiteni...
	if (goal.goal_best == goal.goal_last_best && actualGoalPercent == goal.goal_last_actual)
		return;
	
//	if (this.mainnut.logmode)
//		GSLog.Info("[FGCompany::UpdateScoresAndTextsToGoal] update goal id: " + goal.goal_id + ", to company: " + GSCompany.GetName(this.company_id));

	if (goal.goal_main) {
		// eltavolitjuk a regi goal kiirast
		if (GSGoal.IsValidGoal(goal.goal_id)) {
			if (this.mainnut.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0)) {
				GSGoal.SetProgress(goal.goal_id, GSText(GSText.STR_GOAL_TRANSPORTED_PERCENT, actualGoalPercent, goal.GetMaxGoalPercent()));
			}
			else {
				local szoveg = "";
				local gt = GSGoal.GT_NONE;
				local dest = 0;
				
				switch (goal.goal_type) {
					case FGBaseGoal.GoalTypes.GT_TRANSPORT:
						szoveg = goal.StrTransportCargoGoal(false);
						dest = 0;
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
						gt = GSGoal.GT_TOWN;
						szoveg = goal.StrGoalText();
						dest = goal.town_id;
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
						break;
						// TODO [feladat tipus] tobbi goal-t is hozza kell adni
				}
				
				if (szoveg == "" || szoveg == null) {
					GSLog.Error("[FGCompany::UpdateScoresAndTextsToGoal] Can not create goal text!");
					return;
				}
				
				// 1.4.0 verzio elotti verziokban nem volt SetText funkcio, ott torolni kellett eloszor a goal-t, es ujra hozza kellett adni, hogy megvaltozzon a szoveg
				// TODO: talan kellene egy beallitas, hogy ne toroljuk a listabol a kesz feladatokat.
				GSGoal.Remove(goal.goal_id);
			
				// hozzarendeljuk az uj ID-t a reighez
				goal.goal_id = GSGoal.New(this.company_id, szoveg, gt, dest);
				if (goal.goal_id == null || !GSGoal.IsValidGoal(goal.goal_id)) {
					GSLog.Error("[FGCompany::UpdateScoresAndTextsToGoal] Can not create goal, invalid goal_id: " + goal.goal_id);
				}
				else {
					// egyszeruen kell a goal_id, azert van ketszer a lekerdezes
					switch (goal.goal_type) {
						case FGBaseGoal.GoalTypes.GT_TRANSPORT:
							// azonnal feliratkozunk erre a feladatra
							// goal.month - 1 azert kell, mert ha month == 0, akkor nem erdekel minket a dolog, igy az egeszet lehet monitorozni,
							// de ha month == 1, akkor a 0 kell nekunk, mert az az aktualis honap
							this.mainnut.cargoMonitor.SubscribeToMonitor(this.company_id, goal.cargo_id, goal.goal_id, FGCargoMonitor.REFRESH_IMMEDIATELY, FGCargoMonitor.MONITOR_ALL_DELIVERY, 0, goal.month - 1);
							break;
						case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
							break;
						case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
							szoveg = goal.StrGoalText();
							break;
						case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
							break;
						case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
							break;
							// TODO [feladat tipus] tobbi goal-t is hozza kell adni
					}
				}
			}
		}
	}
	
	
	local goalmaxpercent = goal.GetMaxGoalPercent();
	if (goal.goal_last_best_percent != goalmaxpercent) {
		// a lenyeg, hogy megprobaljuk hozzaadni az uj pont kulonbseget
		local kulonbseg = goalmaxpercent - goal.goal_last_best_percent;
		
		if (goal.goal_main)
			this.main_goals_scores += kulonbseg;
		else if (goal.goal_weak)
			this.weak_award_goals_scores += kulonbseg;
		else
			this.award_goals_scores += kulonbseg;

		if (this.fggc_joined) { // elkuldjuk a frissitest, ha be van jelentkezve
			local ascores = this.GetScores();
			if (!GSAdmin.Send({updatescore = ascores, companyID = this.company_id, versenyID = this.fggc_versenyID}) && this.mainnut.logmode)
				GSLog.Warning("Can not send company (" + this.company_id + ") scores to AdminPort!");
		}
		
		goal.goal_last_best_percent = goalmaxpercent;
	}
	
	// es beallitjuk az utolso mentett allapoto
	if (goal.goal_best != goal.goal_last_best)
		goal.goal_last_best = goal.goal_best;
	
	if (actualGoalPercent != goal.goal_last_actual)
		goal.goal_last_actual = actualGoalPercent;
	
	
	// ezt a vegen kell meghivni, hogy szepen latszodjanak az uj pontok, szazalekok
	// frissiteni kell a story page-t is, hogy hany szazaleknal jarunk
	if (this.mainnut.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0)) {
		if (!goal.goal_weak) // ha award goalrol van szo, akkor frissitjuk a feladatok story oldalt
			this.UpdateAwardGoalsStoryPage(goal);
		
		if (this.mainnut.generalGoalSettings.enableStoryCupUpdates > 0)
			this.mainnut.UpdateGameStateStoryBoards();
	}
}

function FGCompany::GetScores() {
	local mscores = this.main_goals_scores * this.mainnut.generalGoalSettings.completion_main_scores / 100;
	local mainscores = mscores.tointeger();
	local ascores = this.award_goals_scores * this.mainnut.generalGoalSettings.completion_award_scores / 100; // csak 10% pontot kapunk erte
	local awardscores = ascores.tointeger();
	local wscores = this.weak_award_goals_scores * this.mainnut.generalGoalSettings.completion_weak_scores / 100; // csak 10% pontot kapunk erte
	local weakawardscores = wscores.tointeger();
	
//	if (this.mainnut.logmode)
//		GSLog.Info("[" + GSCompany.GetName(this.company_id) + "] main: " + mainscores + ", award: " + awardscores + ", weak: " + weakawardscores + ", mainlevel: " + this.main_goals_completed_first_plusz_scores);
	
	// hozzaadjuk a vegen a szintenkent kapott plusz pontot
	return mainscores + awardscores + weakawardscores + this.main_goals_completed_first_plusz_scores + this.main_goals_completed_months;
}


/***************************************************
 
 StoryBook methodes
 
 ***************************************************/

// - # StoryBook methoeds
function FGCompany::UpdateAwardGoalsStoryPage(update_goal) {
	// ha eppen betoltes van, akkor nem frissitunk
	if (this.mainnut.on_loading)
		return;
	
	// ha goal == null, akkor az osszes goal-t frissitjuk
	if (this.companysGoalStatesStoryPageID == null || this.companysGoalStatesStoryPageID == GSStoryPage.STORY_PAGE_INVALID) {
		this.companysGoalStatesStoryPageID = GSStoryPage.New(this.company_id, GSText(GSText.STR_AWARD_GOALS_PAGE_TITLE));
		if (this.companysGoalStatesStoryPageID != GSStoryPage.STORY_PAGE_INVALID) {
			// hozzaadok egy ures sort:
			GSStoryPage.NewElement(this.companysGoalStatesStoryPageID, GSStoryPage.SPET_TEXT, 0, GSText(GSText.STR_EMPTY));
		}
	}
	
	if (this.companysGoalStatesStoryPageID == GSStoryPage.STORY_PAGE_INVALID) {
		GSLog.Error("[FGCompany::UpdateAwardGoalsStoryPage] Nem sikerült a companysGoalStates story oldal létrehozása!");
	}
	else {
		for (local i = 0; i < this.award_goals.len(); i ++) {
			local goal = this.award_goals[i];
			
			if (update_goal != null && update_goal != goal) // csak akkor frissitjuk, ha szukseg van ra
				continue;
			
			local text = null;
			switch (goal.goal_type) {
				case FGBaseGoal.GoalTypes.GT_TRANSPORT:
					text = goal.StrTransportCargoGoal(true);
					if (this.mainnut.logmode)
						GSLog.Info("[FGCompany::UpdateAwardGoalsStoryPage] Update to " + GSCompany.GetName(this.company_id) + " cargo: " + GSCargo.GetCargoLabel(goal.cargo_id));
					break;
				case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
					break;
				case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
					text = goal.StrGoalText();
					break;
				case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
					break;
				case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
					break;
					// TODO [feladat tipus] tobbi goal-t is hozza kell adni
			}
			
			if (text != null) {
				if (this.companysGoalElementList.len() > i) {
					if (!GSStoryPage.UpdateElement(this.companysGoalElementList[i], 0, text))
						GSLog.Error("[FGCompany::UpdateAwardGoalsStoryPage] Nem sikerült a story elem frissitese!");
				}
				else {
					local speID = GSStoryPage.NewElement(this.companysGoalStatesStoryPageID, GSStoryPage.SPET_TEXT, 0, text);
					if (speID == GSStoryPage.STORY_PAGE_ELEMENT_INVALID)
						GSLog.Error("[FGCompany::UpdateAwardGoalsStoryPage] Nem sikerült a story elem létrehozása!");
					else
						this.companysGoalElementList.append(speID);
				}
			}
		}
		
		// ha esetleg tobb bejegyzes lenne, akkor ki kell nullazni a feliratot
		if (this.award_goals.len() < this.companysGoalElementList.len()) {
			for (local i = this.award_goals.len(); i < this.companysGoalElementList.len(); i++) {
				if (!GSStoryPage.UpdateElement(this.companysGoalElementList[i], 0, GSText(GSText.STR_EMPTY)))
					GSLog.Error("[FGCompany::UpdateAwardGoalsStoryPage] Nem sikerült a story elem frissitese!");
			}
		}
	}
}

/**
 itt fogjuk frissiteni a jatek allasat. egyszeruen az elemeket iratjuk ki, mert nem kellene kiszamolni egyesevel a jatek allasat
 */
function FGCompany::UpdateGameStateStoryPage(elements) {
	if (elements == null || elements.len() == 0)
		return;
	
	if (this.gameStateSoryPageID == null || this.gameStateSoryPageID == GSStoryPage.STORY_PAGE_INVALID)
		this.gameStateSoryPageID = GSStoryPage.New(this.company_id, GSText(GSText.STR_GAME_STATE_TITLE));
	
	if (this.gameStateSoryPageID == GSStoryPage.STORY_PAGE_INVALID) {
		GSLog.Error("[FGCompany::UpdateGameStateStoryPage] Nem sikerült a game state story oldal létrehozása!");
	}
	else {
		for (local i = 0; i < elements.len(); i ++) {
			if (this.gameStateElementList.len() > i) {
				if (!GSStoryPage.UpdateElement(this.gameStateElementList[i], 0, elements[i]))
					GSLog.Error("[FGCompany::UpdateGameStateStoryPage] Nem sikerült a story elem frissitese!");
			}
			else {
				local speID = GSStoryPage.NewElement(this.gameStateSoryPageID, GSStoryPage.SPET_TEXT, 0, elements[i]);
				if (speID == GSStoryPage.STORY_PAGE_ELEMENT_INVALID)
					GSLog.Error("[FGCompany::UpdateGameStateStoryPage] Nem sikerült a story elem létrehozása!");
				else
					this.gameStateElementList.append(speID);
			}
		}
		
		// ha esetleg tobb bejegyzes lenne, akkor ki kell nullazni a feliratot
		if (elements.len() < this.gameStateElementList.len()) {
			for (local i = elements.len(); i < this.gameStateElementList.len(); i++) {
				if (!GSStoryPage.UpdateElement(this.gameStateElementList[i], 0, GSText(GSText.STR_EMPTY)))
					GSLog.Error("[FGCompany::UpdateGameStateStoryPage] Nem sikerült a story elem frissitese!");
			}
		}
	}
}

/**
 ezzel a funkcióval hozzáadhatunk egy új story lapot, elég egyszerűen.
 textlist enum: {
	type:		GSStoryPage.StoryPageElementType
	reference:	integer
	text:		optional text for location and text
 }
 */
function FGCompany::AddNewStoryPage(title, textslist, show) {
	local pageID = GSStoryPage.New(this.company_id, title);
	
	if (pageID == GSStoryPage.STORY_PAGE_INVALID) {
		GSLog.Error("[FGCompany::AddNewStoryPageWithText] Nem sikerült a game over story oldal létrehozása!");
		return;
	}
	
	for (local i = 0; i < textslist.len(); i++)
		GSStoryPage.NewElement(pageID, textslist[i].type, textslist[i].reference, textslist[i].text);
//	GSStoryPage.NewElement(pageID, GSStoryPage.SPET_TEXT, 0, text);
	
	if (show)
		GSStoryPage.Show(pageID);
}

/***************************************************
 
 Goal Methodes
 
 ***************************************************/

// - # all goal methodes

function FGCompany::SubscribeToNextGoal(cargo_id) {

}

function FGCompany::SubscribeToCargoMonitorIfNeed(goal) {
	switch (goal.goal_type) {
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
			// TODO [feladat tipus] tobbi goal-t is hozza kell adni

			// unsupported goal types
			return;
	}

	if (goal.goal_id == null || goal.goal_id == "")
		return; // aminek nincs id-ja, azt monitorozni sem kell

	local hozzaadhato = false;

	if (this.mainnut.transportGoalSettings.transportGoalSettingsUpdateAll ||
		!this.IsAvailableCargoTypeInMain(goal.cargo_id)) {

		if (goal.goal_main) {
			hozzaadhato = true;
		}
		else {
			if (!this.IsAvailableCargoTypeInAward(goal.cargo_id)) {
				if (!goal.goal_weak) {
					hozzaadhato = true;
				}
				else {
					if (!this.IsAvailableCargoTypeInWeakAward(goal.cargo_id))
						hozzaadhato = true;
				}
			}
		}
	}

	if (hozzaadhato)
		// azonnal feliratkozunk erre a feladatra
		// goal.month - 1 azert kell, mert ha month == 0, akkor nem erdekel minket a dolog, igy az egeszet lehet monitorozni,
		// de ha month == 1, akkor a 0 kell nekunk, mert az az aktualis honap
		this.mainnut.cargoMonitor.SubscribeToMonitor(company_id, goal.cargo_id, goal.goal_id, FGCargoMonitor.REFRESH_IMMEDIATELY, FGCargoMonitor.MONITOR_ALL_DELIVERY, 0, goal.month - 1);

	// ha hozzaadhato, akkor mar hozza is adtuk
	return hozzaadhato;
}

// - # main goals
function FGCompany::AddGoalToMain(goal) {
	// preconditions
	switch (goal.goal_type) {
		case FGBaseGoal.GoalTypes.GT_TRANSPORT:
			if (goal.cargo_id == null) {
				GSLog.Error("[FGCompany::AddGoalToMain] " + GSCompany.GetName(this.company_id) + " goal.cargo_id != null");
				return;
			}
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
			if (goal.town_id == null || !GSTown.IsValidTown(goal.town_id)) {
				GSLog.Error("[FGCompany::AddGoalToMain] " + GSCompany.GetName(this.company_id) + " goal.town_id != null");
				return;
			}
			if (goal.goal_town_type == FGTownGrowthGoal.TS_INVALID) {
				GSLog.Error("[FGCompany::AddGoalToMain] " + GSCompany.GetName(this.company_id) + " goal.goal_town_type != FGTownGrowthGoal.TS_INVALID");
				return;
			}
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
			break;
			// TODO [feladat tipus] tobbi goal-t is hozza kell adni
	}
	
	foreach (agoal in this.main_goals) {
		if (goal.goal_id != null && agoal.goal_id == goal.goal_id)
			return; // letezot nem adunk megegyszer hozza, viszon null erteket igen, mert azt kesobb engedelyezzuk
	}
	
	goal.goal_weak = false; // a biztonsag kedveert ezt beallitjuk, mert ez nem lehet
	goal.goal_main = true; // main goal, a kiirasnal van szerepe
	goal.goal_company_id = this.company_id;


	if (this.mainnut.logmode) {
		local additioanlText = GSCompany.GetName(this.company_id) +  " main goal: ";
		switch (goal.goal_type) {
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				additioanlText += goal.amount + " " + GSCargo.GetCargoLabel(goal.cargo_id);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				additioanlText += GSTown.GetName(goal.town_id);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}

		GSLog.Info("[FGCompany::AddGoalToMain] " + additioanlText + " (goal_id: " + goal.goal_id + ")");
	}

	// hozzaadas elott feliratkoztatjuk a montorozasra ha kell...
	this.SubscribeToCargoMonitorIfNeed(goal);
	
	this.main_goals.append(goal);
	
	// hozzaadjuk a main-hez is, hogy bankrupt utan at tudjam nezni a letezoket
	this.mainnut.allMainGoals.append(goal);
}
/*
function FGCompany::ShowMainGoalAtIndex(index) {
	if (this.main_goals.len() > index)
		this.ShowGoal(this.main_goals[index]);
}

function FGCompany::ShowFirstItemsMainGoal(item_count) {
	for (local i = 0; i < item_count; i++)
		this.ShowMainGoalAtIndex(i);
}
*/
function FGCompany::RemoveMainGoal(goal_id) {
	for (local i = this.main_goals.len() - 1; i >= 0; i--) {
		if (this.main_goals[i].goal_id == goal_id) {
			this.main_goals.remove(i);
			// itt eltavolitjuk a listarol a main goal-t
			if (GSGoal.IsValidGoal(goal_id)) {
				GSGoal.Remove(goal_id);
				if (this.mainnut.logmode)
					GSLog.Info("[FGCompany::RemoveMainGoal] Succesfull remove MAIN goal id: " + goal_id);
			}
			else if (this.mainnut.logmode)
					GSLog.Info("[FGCompany::RemoveMainGoal] Unsuccesfull remove MAIN goal id: " + goal_id);
			
			break;
		}
	}
}

function FGCompany::CountOfMainGoals() {
	return this.main_goals.len();
}

function FGCompany::GetMainGoals() {
	local goals = [];
	
	foreach (goal in this.main_goals)
		goals.append(goal);
	
	return goals;
}

function FGCompany::IsAvailableCargoTypeInMain(cargo_id) {
	if (cargo_id == null)
		return false;

	foreach (goal in this.main_goals) {
		switch (goal.goal_type) {
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				if (goal.cargo_id == cargo_id) {
					return true;
				}
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}
	}

	return false;
}

// award goals
// - # award goals
function FGCompany::AddGoalToAward(goal) {
	if (goal.goal_id == null) {
		GSLog.Error("[FGCompany::AddGoalToAward] " + GSCompany.GetName(this.company_id) + " goal.goal_id != null");
		return; // nem adjuk hozza az id nelkuli celokat
	}
	
	switch (goal.goal_type) {
		case FGBaseGoal.GoalTypes.GT_TRANSPORT:
			if (goal.cargo_id == null) {
				GSLog.Error("[FGCompany::AddGoalToAward] " + GSCompany.GetName(this.company_id) + " goal.cargo_id != null");
				return;
			}
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
			break;
		// TODO [feladat tipus] tobbi goal-t is hozza kell adni
	}
	
	foreach (agoal in this.award_goals)
		if (agoal.goal_id == goal.goal_id)
			return; // letezot nem adunk megegyszer hozza

	goal.goal_company_id = this.company_id;
	
	if (this.mainnut.logmode) {
		local text = null;
		switch (goal.goal_type) {
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				text = goal.amount + " " + GSCargo.GetCargoLabel(goal.cargo_id);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				text = GSTown.GetName(goal.town_id);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}
		if (text != null)
			GSLog.Info("[FGCompany::AddGoalToAward] " + GSCompany.GetName(this.company_id) + " award goal: " + text + ", a goal az " + (goal.goal_id == null ? "null" : "nem null") + " (goal_id: " + goal.goal_id + ")");
	}

	// hozzaadas elott feliratkoztatjuk a montorozasra ha kell...
	this.SubscribeToCargoMonitorIfNeed(goal);
	
	this.award_goals.append(goal);
	
	if (this.mainnut.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0))
		this.UpdateAwardGoalsStoryPage(null); // azert null, hogy mindenkeppen frissuljon az egesz oldal, hiba keletkezett, ha csak ezt az uj goal-t updateltem
}

function FGCompany::RemoveAwardGoal(goal_id) {
	for (local i = this.award_goals.len() - 1; i >= 0; i--) {
		if (this.award_goals[i].goal_id == goal_id) {
			this.award_goals.remove(i);
			
			// frissiteni kell a story page-t is, hogy hany szazaleknal jarunk
			if (this.mainnut.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0))
				this.UpdateAwardGoalsStoryPage(null); // az osszes goalt frissiteni kell
			
			if (this.mainnut.logmode)
				GSLog.Info("[FGCompany::RemoveAwardGoal] Succesfull remove AWARD goal id: " + goal_id);
			
			return;
		}
	}
}

function FGCompany::CountOfAwardGoals() {
	return this.award_goals.len();
}

function FGCompany::GetAwardGoals() {
	local goals = [];
	
	foreach (goal in this.award_goals)
	goals.append(goal);
	
	return goals;
}

function FGCompany::IsAvailableCargoTypeInAward(cargo_id) {
	if (cargo_id == null)
		return false;

	foreach (goal in this.award_goals) {
		switch (goal.goal_type) {
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				if (goal.cargo_id == cargo_id) {
					return true;
				}
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}
	}

	return false;
}

// weak goals / szinte ugyanaz, mint az award goals, mert hat ugyebar ott tarojuk oket is! :)
function FGCompany::AddGoalToWeakAward(goal) {
	switch (goal.goal_type) {
		case FGBaseGoal.GoalTypes.GT_TRANSPORT:
			if (goal.cargo_id == null) {
				GSLog.Error("[FGCompany::AddGoalToWeakAward] " + GSCompany.GetName(this.company_id) + " goal.cargo_id != null");
				return;
			}
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
			break;
		// TODO [feladat tipus] tobbi goal-t is hozza kell adni
	}
	
	if (goal.goal_id == null) {
		GSLog.Error("[FGCompany::AddGoalToWeakAward] " + GSCompany.GetName(this.company_id) + " goal.goal_id != null");
		return; // nem adjuk hozza az id nelkuli celokat
	}
	
	foreach (agoal in this.weak_award_goals) {
		if (agoal.goal_id == goal.goal_id) {
			return; // letezot nem adunk megegyszer hozza
		}
	}
	
	goal.goal_weak = true;
	goal.goal_company_id = this.company_id;
	
	if (this.mainnut.logmode) {
		local text = null;
		switch (goal.goal_type) {
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				text = goal.amount + " " + GSCargo.GetCargoLabel(goal.cargo_id);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				text = GSTown.GetName(goal.town_id);
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}
		if (text != null)
			GSLog.Info("[FGCompany::AddGoalToWeakAward] " + GSCompany.GetName(this.company_id) + " weak goal: " + text + ", a goal az " + (goal.goal_id == null ? "null" : "nem null") + " (goal_id: " + goal.goal_id + ")");
	}

	// hozzaadas elott feliratkoztatjuk a montorozasra ha kell...
	this.SubscribeToCargoMonitorIfNeed(goal);
		
	this.weak_award_goals.append(goal);
}

function FGCompany::RemoveWeakAwardGoal(goal_id) {
	for (local i = this.weak_award_goals.len() - 1; i >= 0; i--) {
		if (this.weak_award_goals[i].goal_id == goal_id) {
			this.weak_award_goals.remove(i);
			if (this.mainnut.logmode)
				GSLog.Info("[FGCompany::RemoveAwardGoal] Succesfull remove WEAK goal id: " + goal_id);
			return;
		}
	}
}

function FGCompany::CountOfWeakGoals() {
	local goals = [];
	
	foreach (goal in this.weak_award_goals)
		goals.append(goal);
	
	return goals.len();
}

function FGCompany::GetWeakGoals() {
	local goals = [];
	
	foreach (goal in this.weak_award_goals)
		goals.append(goal);
	
	return goals;
}

function FGCompany::IsAvailableCargoTypeInWeakAward(cargo_id) {
	if (cargo_id == null)
		return false;

	foreach (goal in this.weak_award_goals) {
		switch (goal.goal_type) {
			case FGBaseGoal.GoalTypes.GT_TRANSPORT:
				if (goal.cargo_id == cargo_id) {
					return true;
				}
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
				break;
			case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
				break;
			case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
				break;
				// TODO [feladat tipus] tobbi goal-t is hozza kell adni
		}
	}

	return false;
}


/***************************************************
 
 FGTransportGoal functions
 
 ***************************************************/
// - # FGTransportGoal functions
// uj, alatta a regi
function FGCompany::TransportedCargo(goal_id, amount) {
	// megkeressuk a goalt
	local goal = null;
	foreach (agoal in this.main_goals) {
		if (agoal.goal_id == goal_id) {
			goal = agoal;
			break;
		}
	}
	
	if (goal == null) {
		foreach (agoal in this.award_goals) {
			if (agoal.goal_id == goal_id) {
				goal = agoal;
				break;
			}
		}
	}
	
	if (goal == null) {
		foreach (agoal in this.weak_award_goals) {
			if (agoal.goal_id == goal_id) {
				goal = agoal;
				break;
			}
		}
	}
	
	if (goal == null) {
		GSLog.Error("[FGCompany::TransportedCargo] Can not found valid goal");
		return;
	}
	
	local updateAll = true;
	
	switch (goal.goal_type) {
		case FGBaseGoal.GoalTypes.GT_TRANSPORT:
			updateAll = this.mainnut.transportGoalSettings.transportGoalSettingsUpdateAll;
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
			GSLog.Error("[FGCompany::TransportedCargo] Unsupported goal_id: " + goal_id);
			return;
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
			GSLog.Error("[FGCompany::TransportedCargo] Unsupported goal_id: " + goal_id);
			return;
			break;
			// TODO [feladat tipus] tobbi goal-t is hozza kell adni
	}
	
	local nemnyert = false;
	local megvan = false;
	
	if (!updateAll) {
//		if (this.mainnut.logmode)
//			GSLog.Info("[FGCompany::TransportedCargo] Not update all goal");

		foreach (agoal in this.main_goals) {
			switch (agoal.goal_type) {
				case FGBaseGoal.GoalTypes.GT_TRANSPORT:
					if (agoal.cargo_id == goal.cargo_id && agoal.goal_id != null) {
						megvan = true;
						if (agoal.goal_id != goal.goal_id)
							nemnyert = true;
					}
					break;
				case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
					break;
				case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
					break;
				case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
					break;
				case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
					break;
					// TODO [feladat tipus] tobbi goal-t is hozza kell adni
			}

			if (megvan)
				break;
		}
		
		if (!megvan) {
			foreach (agoal in this.award_goals) {
				switch (agoal.goal_type) {
					case FGBaseGoal.GoalTypes.GT_TRANSPORT:
						if (agoal.cargo_id == goal.cargo_id && agoal.goal_id != null) {
							megvan = true;
							if (agoal.goal_id != goal.goal_id)
								nemnyert = true;
						}
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
						break;
						// TODO [feladat tipus] tobbi goal-t is hozza kell adni
				}

				if (megvan)
					break;
			}
		}
		
		if (!megvan) {
			foreach (agoal in this.weak_award_goals) {
				switch (agoal.goal_type) {
					case FGBaseGoal.GoalTypes.GT_TRANSPORT:
						if (agoal.cargo_id == goal.cargo_id) {
							if (agoal.goal_id != goal.goal_id)
								nemnyert = true;
						}
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
						break;
						// TODO [feladat tipus] tobbi goal-t is hozza kell adni
				}

				if (megvan)
					break;
			}
		}
	}
	
	if (nemnyert) { // azaz valamelyik masik feladat fontosabb, nagyobb prioritast elvez
//		if (this.mainnut.logmode)
//			GSLog.Info("[FGCompany::TransportedCargo] Available priored goal.");
		return;
	}

	GSLog.Error("transported: " + goal_id + ", amount :" + amount);
	
	local current_month = this.mainnut.GetMonthsCountFromStarted();
	if (!GSCargo.IsValidCargo(goal.cargo_id) || (amount < 1) || (current_month < 0))
		return;
	
	
	switch (goal.goal_type) {
		case FGBaseGoal.GoalTypes.GT_TRANSPORT:
			goal.AddTransportedCargo(amount);
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
			break;
		case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
			break;
		case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
			break;
			// TODO [feladat tipus] tobbi goal-t is hozza kell adni
	}

	if (this.IsTransportGoalCompleted(goal.goal_id)) {
		this.UpdateScoresAndTextsToGoal(goal); // ez azert kell, hogy megkapjuk a helyes pontokat, mert enelkul nem kapnanak meg az utolso pontokat)
		this.mainnut.CompanyCompletedGoal(this, goal);
	}
}

// # - goal tobblet kezeles
// ez a funkcio kizarolag akkor ervenyes, ha nem egyszerre frissitjuk a feladatokat.
// tehat ezt nem kell ellenorizni
// elraktarozzuk a nap kesobbi reszere, hogy maradt meg elszamolni valo cargo az egyik teljesitett feladat utan
// azert kell a CheckPlusTransportedCargos(), es nem jo rogton leellenorizni, mert ekkor meg nem lett eltavolitva a feladat, igy ugyanazt talalna meg prioritas szerint
function FGCompany::AddPlusTransportedCargo(cargo_id, amount) {
	local pluscargo = null;
	foreach (pc in this.plus_cargos) {
		if (pc.cargo_id == cargo_id) {
			pluscargo = pc;
			break;
		}
	}

	if (pluscargo != null) {
		pluscargo.amount += amount;
	}
	else {
		this.plus_cargos.append({cargo_id = cargo_id, amount = amount});
	}
}

/* 
 a CheckPlusTransportedCargos() funkcio miatt nem kellenek ezek, mert addig nezi a listat, mig van erteke
 ha esetleg adodik uj cucc, akkor a fentebbi AddPlusTransportedCargo novelni fogja a listat,
 ezert automatikusan novekszik, ugy nem kellenek ezek a funkciok

// ezt kell legutoljara meghivni, hogy a nap vegen torolje a listat,
// nehogy kesobb hozzaadjuk egy ujabb feladathoz, pedig akkor mar nem is kell neki...
function FGCompany::RemoveAllPlusCargos() {
	this.plus_cargos.clear();
}

function FGCompany::IsAvailablePlusCargos() {
	return this.plus_cargos.len() > 0;
}
*/

// ez a funkcio kizarolag akkor ervenyes, ha nem egyszerre frissitjuk a feladatokat.
// tehat ezt nem kell ellenorizni
function FGCompany::CheckPlusTransportedCargos() {
	while (this.plus_cargos.len() > 0) {
		// mar most elmentjuk az ertekeket es toroljuk a listabol, ezaltal is gyorsitjuk a kodot
		local cargo_id = this.plus_cargos[0].cargo_id;
		local amount = this.plus_cargos[0].amount;
		this.plus_cargos.remove(0);

		// megkeressuk a goal_id-t, amihez tartozik, mert ahhoz a goal_id-hez fogjuk hozzaadni az amountot
		// es akkor megegszer bekerulhet a tobblet.
		// ha nincs goal_id, akkor toroljuk a cargot, hogy eltunjon a listarol es tovabbmenjen a jatek
		local goal = null;

		foreach (agoal in this.main_goals) {
			switch (agoal.goal_type) {
				case FGBaseGoal.GoalTypes.GT_TRANSPORT:
					if (agoal.cargo_id == cargo_id && agoal.goal_id != null) {
						goal = agoal;
					}
					break;
				case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
					break;
				case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
					break;
				case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
					break;
				case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
					break;
					// TODO [feladat tipus] tobbi goal-t is hozza kell adni
			}

			if (goal != null)
				break;
		}

		if (goal == null) {
			foreach (agoal in this.award_goals) {
				switch (agoal.goal_type) {
					case FGBaseGoal.GoalTypes.GT_TRANSPORT:
						if (agoal.cargo_id == cargo_id && agoal.goal_id != null) {
							goal = agoal;
						}
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
						break;
						// TODO [feladat tipus] tobbi goal-t is hozza kell adni
				}

				if (goal != null)
					break;
			}
		}

		if (goal == null) {
			foreach (agoal in this.weak_award_goals) {
				switch (agoal.goal_type) {
					case FGBaseGoal.GoalTypes.GT_TRANSPORT:
						if (agoal.cargo_id == cargo_id && agoal.goal_id != null) {
							goal = agoal;
						}
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORTONETOONE:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH:
						break;
					case FGBaseGoal.GoalTypes.GT_TRANSPORT_GLOBAL:
						break;
					case FGBaseGoal.GoalTypes.GT_TOWNGROWTH_GLOBAL:
						break;
						// TODO [feladat tipus] tobbi goal-t is hozza kell adni
				}

				if (goal != null)
					break;
			}
		}

		if (goal != null) {
			GSLog.Warning("Talaltam goalt!!! jeeeee: " + goal.goal_id + ", amount: " + amount);
			this.TransportedCargo(goal.goal_id, amount);
		}
		else {
			GSLog.Warning("nem talaltam goal-t");
		}
	}
}

// # - Get Amount of goals
function FGCompany::GetAllAmountOfTransportGoal(goal) {
	if (!GSCargo.IsValidCargo(goal.cargo_id))
		return 0;
		
	return goal.GetAllTransportedCargo();
}

function FGCompany::GetTransportGoals() {
	local goals = [];
	
	foreach (goal in this.main_goals)
		if (goal.goal_type == FGBaseGoal.GoalTypes.GT_TRANSPORT)
			goals.append(goal);
	
	foreach (goal in this.award_goals)
		if (goal.goal_type == FGBaseGoal.GoalTypes.GT_TRANSPORT)
			goals.append(goal);
	
	foreach (goal in this.weak_award_goals)
		if (goal.goal_type == FGBaseGoal.GoalTypes.GT_TRANSPORT)
			goals.append(goal);
	
	return goals;
}
// TODO [feladat tipus] tobbi goal-t is hozza kell adni

function FGCompany::IsTransportGoalCompleted(goal_id) {
	local result = false;
	local van = false;
	
	local transportgoals = this.GetTransportGoals();
	foreach (goal in transportgoals) {
		if (goal.goal_id == goal_id) {
			if (goal.IsGoalCompleted())
				result = true;
			van = true;
			break;
		}
	}
	
	return result;
}

function FGCompany::TransportedCargoFromIndustries(cargo_id) {
	local transported = 0;
	local industries_cargos_accepted = GSIndustryList_CargoAccepting(cargo_id);
	foreach (industry_id, _ in industries_cargos_accepted) {
		if (!GSIndustry.IsValidIndustry(industry_id))
			continue;
		
		if (GSIndustry.GetAmountOfStationsAround(industry_id) < 1)
			continue;
		
		local most_transported = GSCargoMonitor.GetIndustryDeliveryAmount(this.company_id, cargo_id, industry_id, true);
		transported += most_transported;
	}
	
	return transported;
}


/***********************************************************************************************************************************************
 
 send
 
 ***********************************************************************************************************************************************/

function FGCompany::SendErrorMessage(text) {
	if (this.last_error_build_message >= 0)
		GSGoal.CloseQuestion(this.last_error_build_message);
	this.last_error_build_message = this.mainnut.message_counter++;
	if (GSCompany.ResolveCompanyID(company_id) != GSCompany.COMPANY_INVALID) // lehet, hogy kozben brankrupted lett a company
		GSGoal.Question(this.last_error_build_message, this.company_id, text, GSGoal.QT_ERROR, GSGoal.BUTTON_CLOSE);
//	GSGoal.Question(this.mainnut.message_counter++, this.company_id, text, GSGoal.QT_ERROR, GSGoal.BUTTON_CLOSE);
	if (GSError.GetLastErrorString() != "ERR_NONE")
		GSLog.Error(GSError.GetLastErrorString());
}

function FGCompany::SendState(text) {
	GSGoal.Question(this.mainnut.message_counter++, this.company_id, text, GSGoal.QT_INFORMATION, GSGoal.BUTTON_CLOSE);
	if (GSError.GetLastErrorString() != "ERR_NONE")
		GSLog.Error(GSError.GetLastErrorString());
}

function FGCompany::SendPossibleState(welcome) {
	if ((!welcome && !this.possibleListNeedUpdate) || (!welcome && !this.possibleStoryPageEnabled))
		return; // ha nem kell udvozlo uzenet es nem kell frissiteni, vagy nem kell udvozlo uzenet es nincs mit kiiratni, akkor rogton ne fusson le ez a resz
	
	this.possibleListNeedUpdate = false; // beallitjuk, hogy ne fusson le megy egyszer, csak ha kell...
	local et = GSText(GSText.STR_EMPTY);
	
	/*
	 FIGYELEM:
	 Ne torold az ures et-ket innen a possible uzenetektol, mert a regi 1.3.x verzio uzenet megjelenitesenel 2-2 hely van lefoglalva minden uzenetnek, es rosszul jelenhet meg...
	 */
	
	local possibleList = [];
	if (this.possibleStoryPageEnabled) {
		if (this.shouldRailDepos >= 0) {
			local mit = GSText(GSText.STR_BUILDING_RAILDEPOS_OBJ_CASE);
			if (this.shouldRailDepos == 0 && !this.mainnut.initialPossibilities.awardRailDepos) {
				possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_BUILD_IN_GAME, mit, et));
			}
			else {
				local sh_amount = this.GetShouldMoreNum(FGAward.AT_SHOULD_RAILDEPOS);
				if (sh_amount >= 0) {
					if (sh_amount == 0) {
						if (this.mainnut.initialPossibilities.awardRailDepos)
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_MORE_BUILD, mit, et));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NOT_MORE_BUILD, mit, et));
					}
					else {
						if (this.mainnut.initialPossibilities.awardRailDepos)
							possibleList.append(GSText(GSText.STR_POSSIBLE_NUM_BUILD, sh_amount, mit));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NUM_BUILD, sh_amount, mit));
					}
				}
			}
		}
		
		if (this.shouldRoadDepos >= 0) {
			local mit = GSText(GSText.STR_BUILDING_ROADDEPOS_OBJ_CASE);
			if (this.shouldRoadDepos == 0 && !this.mainnut.initialPossibilities.awardRoadDepos) {
				possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_BUILD_IN_GAME, mit, et));
			}
			else {
				local sh_amount = this.GetShouldMoreNum(FGAward.AT_SHOULD_ROADDEPOS);
				if (sh_amount >= 0) {
					if (sh_amount == 0) {
						if (this.mainnut.initialPossibilities.awardRoadDepos)
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_MORE_BUILD, mit, et));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NOT_MORE_BUILD, mit, et));
					}
					else {
						if (this.mainnut.initialPossibilities.awardRoadDepos)
							possibleList.append(GSText(GSText.STR_POSSIBLE_NUM_BUILD, sh_amount, mit));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NUM_BUILD, sh_amount, mit));
					}
				}
			}
		}
		
		if (this.shouldWaterDepos >= 0) {
			local mit = GSText(GSText.STR_BUILDING_WATERDEPOS_OBJ_CASE);
			if (this.shouldRoadDepos == 0 && !this.mainnut.initialPossibilities.awardWaterDepos) {
				possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_BUILD_IN_GAME, mit, et));
			}
			else {
				local sh_amount = this.GetShouldMoreNum(FGAward.AT_SHOULD_WATERDEPOS);
				if (sh_amount >= 0) {
					if (sh_amount == 0) {
						if (this.mainnut.initialPossibilities.awardWaterDepos)
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_MORE_BUILD, mit, et));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NOT_MORE_BUILD, mit, et));
					}
					else {
						if (this.mainnut.initialPossibilities.awardWaterDepos)
							possibleList.append(GSText(GSText.STR_POSSIBLE_NUM_BUILD, sh_amount, mit));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NUM_BUILD, sh_amount, mit));
					}
				}
			}
		}
		
		if (this.shouldRailStations >= 0) {
			local mit = GSText(GSText.STR_BUILDING_RAILSTATIONS_OBJ_CASE);
				if (this.shouldRailStations == 0 && !this.mainnut.initialPossibilities.awardRailStations) {
				possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_BUILD_IN_GAME, mit, et));
			}
			else {
				local sh_amount = this.GetShouldMoreNum(FGAward.AT_SHOULD_RAILSTATIONS);
				if (sh_amount >= 0) {
					if (sh_amount == 0) {
						if (this.mainnut.initialPossibilities.awardRailStations)
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_MORE_BUILD, mit, et));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NOT_MORE_BUILD, mit, et));
					}
					else {
						if (this.mainnut.initialPossibilities.awardRailStations)
							possibleList.append(GSText(GSText.STR_POSSIBLE_NUM_BUILD, sh_amount, mit));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NUM_BUILD, sh_amount, mit));
					}
				}
			}
		}
		
		if (this.shouldTruckStops >= 0) {
			local mit = GSText(GSText.STR_BUILDING_TRUCKSTOPS_OBJ_CASE);
			if (this.shouldTruckStops == 0 && !this.mainnut.initialPossibilities.awardTruckStops) {
				possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_BUILD_IN_GAME, mit, et));
			}
			else {
				local sh_amount = this.GetShouldMoreNum(FGAward.AT_SHOULD_TRUCKSTOPS);
				if (sh_amount >= 0) {
					if (sh_amount == 0) {
						if (this.mainnut.initialPossibilities.awardTruckStops)
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_MORE_BUILD, mit, et));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NOT_MORE_BUILD, mit, et));
					}
					else {
						if (this.mainnut.initialPossibilities.awardTruckStops)
							possibleList.append(GSText(GSText.STR_POSSIBLE_NUM_BUILD, sh_amount, mit));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NUM_BUILD, sh_amount, mit));
					}
				}
			}
		}
		
		if (this.shouldBusStops >= 0) {
			local mit = GSText(GSText.STR_BUILDING_BUSSTOPS_OBJ_CASE);
			if (this.shouldBusStops == 0 && !this.mainnut.initialPossibilities.awardBusStops) {
				possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_BUILD_IN_GAME, mit, et));
			}
			else {
				local sh_amount = this.GetShouldMoreNum(FGAward.AT_SHOULD_BUSSTOPS);
				if (sh_amount >= 0) {
					if (sh_amount == 0) {
						if (this.mainnut.initialPossibilities.awardBusStops)
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_MORE_BUILD, mit, et));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NOT_MORE_BUILD, mit, et));
					}
					else {
						if (this.mainnut.initialPossibilities.awardBusStops)
							possibleList.append(GSText(GSText.STR_POSSIBLE_NUM_BUILD, sh_amount, mit));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NUM_BUILD, sh_amount, mit));
					}
				}
			}
		}
		
		if (this.shouldWaterDocks >= 0) {
			local mit = GSText(GSText.STR_BUILDING_WATERDOCKS_OBJ_CASE);
			if (this.shouldWaterDocks == 0 && !this.mainnut.initialPossibilities.awardWaterDocks) {
				possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_BUILD_IN_GAME, mit, et));
			}
			else {
				local sh_amount = this.GetShouldMoreNum(FGAward.AT_SHOULD_WATERDOCKS);
				if (sh_amount >= 0) {
					if (sh_amount == 0) {
						if (this.mainnut.initialPossibilities.awardWaterDocks)
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_MORE_BUILD, mit, et));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NOT_MORE_BUILD, mit, et));
					}
					else {
						if (this.mainnut.initialPossibilities.awardWaterDocks)
							possibleList.append(GSText(GSText.STR_POSSIBLE_NUM_BUILD, sh_amount, mit));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NUM_BUILD, sh_amount, mit));
					}
				}
			}
		}
		
		if (this.shouldAirPorts >= 0) {
			local mit = GSText(GSText.STR_BUILDING_AIRPORTS_OBJ_CASE);
			if (this.shouldAirPorts == 0 && !this.mainnut.initialPossibilities.awardAirPorts) {
				possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_BUILD_IN_GAME, mit, et));
			}
			else {
				local sh_amount = this.GetShouldMoreNum(FGAward.AT_SHOULD_AIRPORTS);
				if (sh_amount >= 0) {
					if (sh_amount == 0) {
						if (this.mainnut.initialPossibilities.awardAirPorts)
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_MORE_BUILD, mit, et));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NOT_MORE_BUILD, mit, et));
					}
					else {
						if (this.mainnut.initialPossibilities.awardAirPorts)
							possibleList.append(GSText(GSText.STR_POSSIBLE_NUM_BUILD, sh_amount, mit));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NUM_BUILD, sh_amount, mit));
					}
				}
			}
		}
		
		if (this.shouldRailVehicles >= 0) {
			local mit = GSText(GSText.STR_VEHICLE_TRAIN_OBJ_CASE);
			if (this.shouldRailVehicles == 0 && !this.mainnut.initialPossibilities.awardRailVehicles) {
				possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_PURCHASE_IN_GAME, mit, et));
			}
			else {
				local sh_amount = this.GetShouldMoreNum(FGAward.AT_SHOULD_RAILVEHICLES);
				if (sh_amount >= 0) {
					if (sh_amount == 0) {
						if (this.mainnut.initialPossibilities.awardRailVehicles)
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_MORE_PURCHASE, mit, et));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NOT_MORE_PURCHASE, mit, et));
					}
					else {
						if (this.mainnut.initialPossibilities.awardRailVehicles)
							possibleList.append(GSText(GSText.STR_POSSIBLE_NUM_PURCHASE, sh_amount, mit));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NUM_PURCHASE, sh_amount, mit));
					}
				}
			}
		}
		
		if (this.shouldTruckVehicles >= 0) {
			local mit = GSText(GSText.STR_VEHICLE_TRUCK_OBJ_CASE);
			if (this.shouldTruckVehicles == 0 && !this.mainnut.initialPossibilities.awardTruckVehicles) {
				possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_PURCHASE_IN_GAME, mit, et));
			}
			else {
				local sh_amount = this.GetShouldMoreNum(FGAward.AT_SHOULD_TRUCKVEHICLES);
				if (sh_amount >= 0) {
					if (sh_amount == 0) {
						if (this.mainnut.initialPossibilities.awardTruckVehicles)
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_MORE_PURCHASE, mit, et));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NOT_MORE_PURCHASE, mit, et));
					}
					else {
						if (this.mainnut.initialPossibilities.awardTruckVehicles)
							possibleList.append(GSText(GSText.STR_POSSIBLE_NUM_PURCHASE, sh_amount, mit));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NUM_PURCHASE, sh_amount, mit));
					}
				}
			}
		}
		
		if (this.shouldBusVehicles >= 0) {
			local mit = GSText(GSText.STR_VEHICLE_BUS_OBJ_CASE);
			if (this.shouldBusVehicles == 0 && !this.mainnut.initialPossibilities.awardBusVehicles) {
				possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_PURCHASE_IN_GAME, mit, et));
			}
			else {
				local sh_amount = this.GetShouldMoreNum(FGAward.AT_SHOULD_BUSVEHICLES);
				if (sh_amount >= 0) {
					if (sh_amount == 0) {
						if (this.mainnut.initialPossibilities.awardBusVehicles)
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_MORE_PURCHASE, mit, et));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NOT_MORE_PURCHASE, mit, et));
					}
					else {
						if (this.mainnut.initialPossibilities.awardBusVehicles)
							possibleList.append(GSText(GSText.STR_POSSIBLE_NUM_PURCHASE, sh_amount, mit));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NUM_PURCHASE, sh_amount, mit));
					}
				}
			}
		}
		
		if (this.shouldWaterVehicles >= 0) {
			local mit = GSText(GSText.STR_VEHICLE_SHIP_OBJ_CASE);
			if (this.shouldWaterVehicles == 0 && !this.mainnut.initialPossibilities.awardWaterVehicles) {
				possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_PURCHASE_IN_GAME, mit, et));
			}
			else {
				local sh_amount = this.GetShouldMoreNum(FGAward.AT_SHOULD_WATERVEHICLES);
				if (sh_amount >= 0) {
					if (sh_amount == 0) {
						if (this.mainnut.initialPossibilities.awardWaterVehicles)
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_MORE_PURCHASE, mit, et));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NOT_MORE_PURCHASE, mit, et));
					}
					else {
						if (this.mainnut.initialPossibilities.awardWaterVehicles)
							possibleList.append(GSText(GSText.STR_POSSIBLE_NUM_PURCHASE, sh_amount, mit));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NUM_PURCHASE, sh_amount, mit));
					}
				}
			}
		}
		
		if (this.shouldAirVehicles >= 0) {
			local mit = GSText(GSText.STR_VEHICLE_AIRPLANE_OBJ_CASE);
			if (this.shouldAirVehicles == 0 && !this.mainnut.initialPossibilities.awardAirVehicles) {
				possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_PURCHASE_IN_GAME, mit, et));
			}
			else {
				local sh_amount = this.GetShouldMoreNum(FGAward.AT_SHOULD_AIRVEHICLES);
				if (sh_amount >= 0) {
					if (sh_amount == 0) {
						if (this.mainnut.initialPossibilities.awardAirVehicles)
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_MORE_PURCHASE, mit, et));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NOT_MORE_PURCHASE, mit, et));
					}
					else {
						if (this.mainnut.initialPossibilities.awardAirVehicles)
							possibleList.append(GSText(GSText.STR_POSSIBLE_NUM_PURCHASE, sh_amount, mit));
						else
							possibleList.append(GSText(GSText.STR_POSSIBLE_ERROR_NUM_PURCHASE, sh_amount, mit));
					}
				}
			}
		}
	}
	
	if (this.possibleStoryPageEnabled && possibleList.len() == 0)
		this.possibleStoryPageEnabled = false; // ha nincs semmi korlatozas, akkor a kesobbiekben sem lesz
	
	if (this.mainnut.versionController.IsCurrentVersionGreatherThanOrEqualTo(1, 4, 0)) {		
		if (!this.possibleStoryPageEnabled)
			return; // nincs megkotes, igy nem kell frissiteni sem
		
		if (this.possibleStoryPageID == null)
			this.possibleStoryPageID = GSStoryPage.New(this.company_id, GSText(GSText.STR_KORLATOZASOK_PAGE_TITLE));
		
		if (this.possibleStoryPageID == GSStoryPage.STORY_PAGE_INVALID) {
			GSLog.Error("[FGCompany::SendPossibleState] Nem sikerült a possible story oldal létrehozása!");
		}
		else {
			for (local i = 0; i < possibleList.len(); i ++) {
				if (this.possibleElementList.len() > i) {
					if (!GSStoryPage.UpdateElement(this.possibleElementList[i], 0, possibleList[i]))
						GSLog.Error("[FGCompany::SendPossibleState] Nem sikerült a story elem frissitese!");
				}
				else {
					local speID = GSStoryPage.NewElement(this.possibleStoryPageID, GSStoryPage.SPET_TEXT, 0, possibleList[i]);
					if (speID == GSStoryPage.STORY_PAGE_ELEMENT_INVALID)
						GSLog.Error("[FGCompany::SendPossibleState] Nem sikerült a story elem létrehozása!");
					else
						this.possibleElementList.append(speID);
				}
			}
			
			// ha esetleg tobb bejegyzes lenne, akkor ki kell nullazni a feliratot
			if (possibleList.len() < this.possibleElementList.len()) {
				for (local i = possibleList.len(); i < this.possibleElementList.len(); i++) {
					if (!GSStoryPage.UpdateElement(this.possibleElementList[i], 0, GSText(GSText.STR_EMPTY)))
						GSLog.Error("[FGCompany::SendPossibleState] Nem sikerült a story elem frissitese!");
				}
			}
		}
	}
	else {
		// 1.4.0 elotti rendszereken nincs meg storyboard, ezert csak hatosaval tudtam megoldani a kiiratast.
		// azert csinaljuk ezt, hogy forditva kuldkuk ki a listat
		local flista = []
		for (local i = 0; i < possibleList.len(); i += 6) {
			local text1 = GSText(GSText.STR_POSSIBLE_EMPTY, et);
			local text2 = GSText(GSText.STR_POSSIBLE_EMPTY, et);
			local text3 = GSText(GSText.STR_POSSIBLE_EMPTY, et);
			local text4 = GSText(GSText.STR_POSSIBLE_EMPTY, et);
			local text5 = GSText(GSText.STR_POSSIBLE_EMPTY, et);
			local text6 = GSText(GSText.STR_POSSIBLE_EMPTY, et);
			
			local van = false;
			if (i < possibleList.len()) {
				text1 = possibleList[i];
				van = true;
			}
			
			if (i + 1 < possibleList.len())
				text2 = possibleList[i + 1];
			
			if (i + 2 < possibleList.len())
				text3 = possibleList[i + 2];
			
			if (i + 3 < possibleList.len())
				text4 = possibleList[i + 3];
			
			if (i + 4 < possibleList.len())
				text5 = possibleList[i + 4];
			
			if (i + 5 < possibleList.len())
				text6 = possibleList[i + 5];
			
			local koszonto = et;
			
			if (i == 0 && welcome)
				koszonto = this.mainnut.GetWelcomeMessage();
			
			
			if (van)
				flista.append(GSText(GSText.STR_POSSIBLE, koszonto, text1, text2, text3, text4, text5, text6))
		}
		
		if (flista.len() > 0) {
			for (local i = flista.len() - 1; i >= 0; i--)
				this.SendState(flista[i]);
		}
		else if (welcome)
			this.SendState(this.mainnut.GetWelcomeMessage()); // legyen mar udvozlo uzenet akkor is, ha nincs semmi megkotes...
	}
}


// - # should support

/***********************************************************************************************************************************************
 
 should support
 
 ***********************************************************************************************************************************************/

// should_type: az az FGAward.AT_x
function FGCompany::GetShouldMoreNum(should_type) {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	
	local companymode = GSCompanyMode(cstr);
	
	switch (should_type) {
		case FGAward.AT_SHOULD_RAILDEPOS:
			if (this.shouldRailDepos < 1) {
				return this.shouldRailDepos;
			}
			else {
				local deposList = GSDepotList(GSTile.TRANSPORT_RAIL);
				if (deposList.Count() > this.shouldRailDepos)
					return 0;
				else
					return (this.shouldRailDepos - deposList.Count());
			}
			break;
		case FGAward.AT_SHOULD_ROADDEPOS:
			if (this.shouldRoadDepos < 1) {
				return this.shouldRoadDepos;
			}
			else {
				local deposList = GSDepotList(GSTile.TRANSPORT_ROAD);
				if (deposList.Count() > this.shouldRoadDepos)
					return 0;
				else
					return (this.shouldRoadDepos - deposList.Count());
			}
			break;
		case FGAward.AT_SHOULD_WATERDEPOS:
			if (this.shouldWaterDepos < 1) {
				return this.shouldWaterDepos;
			}
			else {
				local deposList = GSDepotList(GSTile.TRANSPORT_WATER);
				if (deposList.Count() > this.shouldWaterDepos)
					return 0;
				else
					return (this.shouldWaterDepos - deposList.Count());
			}
			break;
			
			// stations
		case FGAward.AT_SHOULD_RAILSTATIONS:
			if (this.shouldRailStations < 1) {
				return this.shouldRailStations;
			}
			else {
				local stationList = GSStationList(GSStation.STATION_TRAIN);
				if (stationList.Count() > this.shouldRailStations)
					return 0;
				else
					return (this.shouldRailStations - stationList.Count());
			}
			break;
		case FGAward.AT_SHOULD_TRUCKSTOPS:
			if (this.shouldTruckStops < 1) {
				return this.shouldTruckStops;
			}
			else {
				local stationList = GSStationList(GSStation.STATION_TRUCK_STOP);
				if (stationList.Count() > this.shouldTruckStops)
					return 0;
				else
					return (this.shouldTruckStops - stationList.Count());
			}
			break;
		case FGAward.AT_SHOULD_BUSSTOPS:
			if (this.shouldBusStops < 1) {
				return this.shouldBusStops;
			}
			else {
				local stationList = GSStationList(GSStation.STATION_BUS_STOP);
				if (stationList.Count() > this.shouldBusStops)
					return 0;
				else
					return (this.shouldBusStops - stationList.Count());
			}
			break;
		case FGAward.AT_SHOULD_WATERDOCKS:
			if (this.shouldWaterDocks < 1) {
				return this.shouldWaterDocks;
			}
			else {
				local stationList = GSStationList(GSStation.STATION_DOCK);
				if (stationList.Count() > this.shouldWaterDocks)
					return 0;
				else
					return (this.shouldWaterDocks - stationList.Count());
			}
			break;
		case FGAward.AT_SHOULD_AIRPORTS:
			if (this.shouldAirPorts < 1) {
				return this.shouldAirPorts;
			}
			else {
				local stationList = GSStationList(GSStation.STATION_AIRPORT);
				if (stationList.Count() > this.shouldAirPorts)
					return 0;
				else
					return (this.shouldAirPorts - stationList.Count());
			}
				break;
			
			// vehicles
		case FGAward.AT_SHOULD_RAILVEHICLES:
			if (this.shouldRailVehicles < 1) {
				return this.shouldRailVehicles;
			}
			else {
				local aList = GSVehicleList();
				
				if (aList.Count() == 0)
					return this.shouldRailVehicles;
				
				local vehiclesList = GSList();
				
				foreach (vehicle, _ in aList)
					if (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_RAIL)
						vehiclesList.AddItem(vehicle, vehiclesList.Count());
				
				if (vehiclesList.Count() > this.shouldRailVehicles)
					return 0;
				else
					return (this.shouldRailVehicles - vehiclesList.Count());
			}
			break;
		case FGAward.AT_SHOULD_TRUCKVEHICLES:
			if (this.shouldTruckVehicles < 1) {
				return this.shouldTruckVehicles;
			}
			else {
				local aList = GSVehicleList();
				
				if (aList.Count() == 0)
					return this.shouldTruckVehicles;
				
				local vehiclesList = GSList();
				
				foreach (vehicle, _ in aList)
				if ((GSVehicle.GetRoadType(vehicle) == GSRoad.ROADTYPE_ROAD) && (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_ROAD) && (GSVehicle.GetCapacity(vehicle, this.cargo.cargotype_passengers) == 0))
					vehiclesList.AddItem(vehicle, vehiclesList.Count());
				
				if (vehiclesList.Count() > this.shouldTruckVehicles)
					return 0;
				else
					return (this.shouldTruckVehicles - vehiclesList.Count());
			}
			break;
		case FGAward.AT_SHOULD_BUSVEHICLES:
			if (this.shouldBusVehicles < 1) {
				return this.shouldBusVehicles;
			}
			else {
				local aList = GSVehicleList();
				
				if (aList.Count() == 0)
					return this.shouldBusVehicles;
				
				local vehiclesList = GSList();
				
				foreach (vehicle, _ in aList)
				if ((GSVehicle.GetRoadType(vehicle) == GSRoad.ROADTYPE_ROAD) && (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_ROAD) && (GSVehicle.GetCapacity(vehicle, this.cargo.cargotype_passengers) > 0))
					vehiclesList.AddItem(vehicle, vehiclesList.Count());
				
				if (vehiclesList.Count() > this.shouldBusVehicles)
					return 0;
				else
					return (this.shouldBusVehicles - vehiclesList.Count());
			}
			break;
		case FGAward.AT_SHOULD_WATERVEHICLES:
			if (this.shouldWaterVehicles < 1) {
				return this.shouldWaterVehicles;
			}
			else {
				local aList = GSVehicleList();
				
				if (aList.Count() == 0)
					return this.shouldWaterVehicles;
				
				local vehiclesList = GSList();
				
				foreach (vehicle, _ in aList)
				if (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_WATER)
					vehiclesList.AddItem(vehicle, vehiclesList.Count());
				
				if (vehiclesList.Count() > this.shouldWaterVehicles)
					return 0;
				else
					return (this.shouldWaterVehicles - vehiclesList.Count());
			}
			break;
		case FGAward.AT_SHOULD_AIRVEHICLES:
			if (this.shouldAirVehicles < 1) {
				return this.shouldAirVehicles;
			}
			else {
				local aList = GSVehicleList();
				
				if (aList.Count() == 0)
					return this.shouldAirVehicles;
				
				local vehiclesList = GSList();
				
				foreach (vehicle, _ in aList)
				if (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_AIR)
					vehiclesList.AddItem(vehicle, vehiclesList.Count());
				
				GSLog.Error("this.shouldAirVehicles: " + vehiclesList.Count());
				if (vehiclesList.Count() > this.shouldAirVehicles)
					return 0;
				else
					return (this.shouldAirVehicles - vehiclesList.Count());
			}
			break;
	}
	
	return 0;
}

// should_type: az az FGAward.AT_x
function FGCompany::GetMaxMoreNum(should_type) {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	switch (should_type) {
		case FGAward.AT_SHOULD_RAILDEPOS:
			return this.shouldRailDepos;
			break;
		case FGAward.AT_SHOULD_ROADDEPOS:
			return this.shouldRoadDepos;
			break;
		case FGAward.AT_SHOULD_WATERDEPOS:
			return this.shouldWaterDepos;
			break;
			
			// stations
		case FGAward.AT_SHOULD_RAILSTATIONS:
			return this.shouldRailStations;
			break;
		case FGAward.AT_SHOULD_TRUCKSTOPS:
			return this.shouldTruckStops;
			break;
		case FGAward.AT_SHOULD_BUSSTOPS:
			return this.shouldBusStops;
			break;
		case FGAward.AT_SHOULD_WATERDOCKS:
			return this.shouldWaterDocks;
			break;
		case FGAward.AT_SHOULD_AIRPORTS:
			return this.shouldAirPorts;
			break;
			
			// vehicles
		case FGAward.AT_SHOULD_RAILVEHICLES:
			return this.shouldRailVehicles;
			break;
		case FGAward.AT_SHOULD_TRUCKVEHICLES:
			return this.shouldTruckVehicles;
			break;
		case FGAward.AT_SHOULD_BUSVEHICLES:
			return this.shouldBusVehicles;
			break;
		case FGAward.AT_SHOULD_WATERVEHICLES:
			return this.shouldWaterVehicles;
			break;
		case FGAward.AT_SHOULD_AIRVEHICLES:
			return this.shouldAirVehicles;
			break;
	}
	
	return 0;
}

/*
 Itt megvizsgaljuk, hogy van-e meg award, ha legalabb valamelyik nem null, akkor van meg award
 */
function FGCompany::AvailableAward() {
	// ha eleve nincsenek jutalmak, akkor minek strapaljuk magunkat?
	if (!this.mainnut.award_enabled_by_possible)
		return false;
	
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return false;
	local companymode = GSCompanyMode(cstr);
	
	
	// ha -1, akkor barmennyit vehet
	// rail depos
	if (this.shouldRailDepos >= 0 && this.mainnut.initialPossibilities.awardRailDepos)
		return true;
	
	// road depos
	if (this.shouldRoadDepos >= 0 && this.mainnut.initialPossibilities.awardRoadDepos)
		return true;
	
	// water depos
	if (this.shouldWaterDepos >= 0 && this.mainnut.initialPossibilities.awardWaterDepos)
		return true;
	
	// railstation
	if (this.shouldRailStations >= 0 && this.mainnut.initialPossibilities.awardRailStations)
		return true;
	
	// truckstops
	if (this.shouldTruckStops >= 0 && this.mainnut.initialPossibilities.awardTruckStops)
		return true;
	
	// busstops
	if (this.shouldBusStops >= 0 && this.mainnut.initialPossibilities.awardBusStops)
		return true;
	
	// docks
	if (this.shouldWaterDocks >= 0 && this.mainnut.initialPossibilities.awardWaterDocks)
		return true;
	
	// airports
	if (this.shouldAirPorts >= 0 && this.mainnut.initialPossibilities.awardAirPorts)
		return true;
	
	// railvehicles
	if (this.shouldRailVehicles >= 0 && this.mainnut.initialPossibilities.awardRailVehicles)
		return true;
	
	// truckvehicles
	if (this.shouldTruckVehicles >= 0 && this.mainnut.initialPossibilities.awardTruckVehicles)
		return true;
	
	// busvehicles
	if (this.shouldBusVehicles >= 0 && this.mainnut.initialPossibilities.awardBusVehicles)
		return true;
	
	// watervehicles
	if (this.shouldWaterVehicles >= 0 && this.mainnut.initialPossibilities.awardWaterVehicles)
		return true;
	
	// airports
	if (this.shouldAirVehicles >= 0 && this.mainnut.initialPossibilities.awardAirVehicles)
		return true;
	
	if (this.mainnut.logmode)
		GSLog.Warning("[FGCompany::AvailableAward] " + GSCompany.GetName(this.company_id) + " nem kaphat tobb jutalmat!");
	
	return false;
}

function FGCompany::CheckAllShould() {
	// depos
	local result = this.CheckRailDeposShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckRoadDeposShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckWaterDeposShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	
	// stations, stops, docks, airports
	local result = this.CheckRailStationsShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckTruckStopsShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckBusStopsShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckWaterDocksShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckAirPortsShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	
	// vehicles
	local result = this.CheckRailVehiclesShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckTruckVehiclesShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckBusVehiclesShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckWaterVehiclesShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckAirVehiclesShould();
	if (result != null)
		this.SendErrorMessage(result);
	
}

// Rail depos
function FGCompany::CheckRailDeposShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldRailDepos < 0) // barmennyi depo lehet
		return null;
	
	local deposList = GSDepotList(GSTile.TRANSPORT_RAIL);
	
	if (deposList.Count() == 0)
		return null;
	
	local result = null;
	if (this.shouldRailDepos == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_MORE_RAILDEPOS);
		// ekkor eltavolitjuk az osszes depot
		foreach (depo, _ in deposList) {
			GSRail.SetCurrentRailType(GSRail.GetRailType(depo));
			
			if (GSRail.GetCurrentRailType() == GSRail.RAILTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckRailDeposShould] Érvénytelen vasút típus!");
			
			if (!GSTile.DemolishTile(depo))
				GSLog.Error("[FGCompany::CheckRailDeposShould] Vasút depó törlése sikertelen!");
		}
 
	}
	else {
		// eloszor hozzaadjuk a depokat, ha meg lehet
		if (this.shouldRailDepos > this.railDepos.len()) {
			if (deposList.Count() > this.railDepos.len()) {
				// ekkor hozzarakjuk a nem hozzaadott depokat
				foreach (adepo, _ in deposList) {
					if (this.shouldRailDepos > this.railDepos.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (depo in this.railDepos) {
							if (!van) {
								if (adepo == depo) {
									van = true;
								}
							}
						}
						if (!van) {
							this.railDepos.append(adepo);
							this.possibleListNeedUpdate = true; // a possible story page listaban meg kell jeleniteni a valtozast
							if (this.mainnut.logmode)
								GSLog.Warning("[FGCompany::CheckRailDeposShould] hozzaadjuk a depot");
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb deponk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldRailDepos < this.railDepos.len()) {
			for (local i = this.railDepos.len() - 1; ((i > (this.shouldRailDepos - 1)) && i >= 0); i--) {
				this.railDepos.remove(i);
				if (this.mainnut.logmode)
					GSLog.Warning("[FGCompany::CheckRailDeposShould] eltavolitunk depot a listabol");
			}
		}
		
		// tehat most elvileg pont anny van a railDeposban, amennyi megengedett
		// tehat most eltavolitjuk az osszes depot, ami nincs benne a listaban
		if (deposList.Count() > this.railDepos.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_RAILDEPOS, this.shouldRailDepos);
			
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (adepo, _ in deposList) {
			local van = false;
			foreach (depo in this.railDepos) {
				if (adepo == depo) {
					van = true;
				}
			}
			if (!van)
				removeList.append(adepo);
		}
		
		foreach (depo in removeList) {
			GSRail.SetCurrentRailType(GSRail.GetRailType(depo));
			
			if (GSRail.GetCurrentRailType() == GSRail.RAILTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckRailDeposShould] Érvénytelen vasút típus!");
			
			if (!GSTile.DemolishTile(depo))
				GSLog.Error("[FGCompany::CheckRailDeposShould] Vasút depó törlése sikertelen!");
			
			if (this.mainnut.logmode)
				GSLog.Warning("[FGCompany::CheckRailDeposShould] felrobbantjuk a depot");
		}

		if (removeList.len() > 0) {
			if (this.shouldRailDepos > GSDepotList(GSTile.TRANSPORT_RAIL).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_MORE_RAILDEPOS);
		}	}
	
	return result;
}

// Road depos
function FGCompany::CheckRoadDeposShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldRoadDepos < 0) // barmennyi depo lehet
		return null;
	
	local deposList = GSDepotList(GSTile.TRANSPORT_ROAD);
	
	if (deposList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldRoadDepos == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_MORE_ROADDEPOS);
		// ekkor eltavolitjuk az osszes depot
		foreach (depo, _ in deposList) {
			local rtype = GSRoad.ROADTYPE_ROAD;
			if (!GSRoad.HasRoadType(depo, rtype)) {
				rtype = GSRoad.ROADTYPE_TRAM;
				if (!GSRoad.HasRoadType(depo, rtype))
					continue;
			}
			
			GSRoad.SetCurrentRoadType(rtype);
			
			if (GSRoad.GetCurrentRoadType() == GSRoad.ROADTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckRoadDeposShould] Érvénytelen út típus!");
			
			if (!GSRoad.RemoveRoadDepot(depo)) {
				GSLog.Warning("[FGCompany::CheckRoadDeposShould] Road depó eltávolítása sikertelen!");
				if (!GSTile.DemolishTile(depo))
					GSLog.Error("[FGCompany::CheckRoadDeposShould] Road depó törlése sikertelen!");
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a depokat, ha meg lehet
		if (this.shouldRoadDepos > this.roadDepos.len()) {
			if (deposList.Count() > this.roadDepos.len()) {
				// ekkor hozzarakjuk a nem hozzaadott depokat
				foreach (adepo, _ in deposList) {
					if (this.shouldRoadDepos > this.roadDepos.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (depo in this.roadDepos) {
							if (!van) {
								if (adepo == depo) {
									van = true;
								}
							}
						}
						if (!van) {
							this.roadDepos.append(adepo);
							this.possibleListNeedUpdate = true; // a possible story page listaban meg kell jeleniteni a valtozast
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb deponk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldRoadDepos < this.roadDepos.len()) {
			for (local i = this.roadDepos.len() - 1; ((i > (this.shouldRoadDepos - 1)) && i >= 0); i--)
				this.roadDepos.remove(i);
		}
		
		// tehat most elvileg pont anny van a roadDeposban, amennyi megengedett
		// tehat most eltavolitjuk az osszes depot, ami nincs benne a listaban
		if (deposList.Count() > this.roadDepos.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_ROADDEPOS, this.shouldRoadDepos);
		
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (adepo, _ in deposList) {
			local van = false;
			foreach (depo in this.roadDepos) {
				if (adepo == depo) {
					van = true;
				}
			}
			if (!van)
				removeList.append(adepo);
		}
		
		foreach (depo in removeList) {
			local rtype = GSRoad.ROADTYPE_ROAD;
			if (!GSRoad.HasRoadType(depo, rtype)) {
				rtype = GSRoad.ROADTYPE_TRAM;
				if (!GSRoad.HasRoadType(depo, rtype))
					continue;
			}
				
			GSRoad.SetCurrentRoadType(rtype);
			
			if (GSRoad.GetCurrentRoadType() == GSRoad.ROADTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckRoadDeposShould] Érvénytelen út típus!");
			
			if (!GSRoad.RemoveRoadDepot(depo)) {
				GSLog.Warning("[FGCompany::CheckRoadDeposShould] Road depó eltávolítása sikertelen!");
				if (!GSTile.DemolishTile(depo))
					GSLog.Error("[FGCompany::CheckRoadDeposShould] Road depó törlése sikertelen!");
			}
		}

		if (removeList.len() > 0) {
			if (this.shouldRoadDepos > GSDepotList(GSTile.TRANSPORT_ROAD).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_ROADDEPOS);
		}
	}
	
	return result;
}

// Water depos
function FGCompany::CheckWaterDeposShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldWaterDepos < 0) // barmennyi depo lehet
		return null;
	
	local deposList = GSDepotList(GSTile.TRANSPORT_WATER);
	if (deposList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldWaterDepos == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_MORE_WATERDEPOS);
		// ekkor eltavolitjuk az osszes depot
		foreach (depo, _ in deposList) {
			if (GSMarine.IsWaterDepotTile(depo)) {
				if (!GSMarine.RemoveWaterDepot(depo)) {
					GSLog.Warning("[FGCompany::CheckWaterDeposShould] Nem sikerült eltávolítni a hajó depót!");
					if (!GSTile.DemolishTile(depo))
						GSLog.Error("[FGCompany::CheckWaterDeposShould] Hajó depó törlése sikertelen!");
				}
			}
			else
				GSLog.Error("[FGCompany::CheckWaterDeposShould] Nem hajó depó mező");
		}
		
	}
	else {
		// eloszor hozzaadjuk a depokat, ha meg lehet
		if (this.shouldWaterDepos > this.waterDepos.len()) {
			if (deposList.Count() > this.waterDepos.len()) {
				// ekkor hozzarakjuk a nem hozzaadott depokat
				foreach (adepo, _ in deposList) {
					if (this.shouldWaterDepos > this.waterDepos.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (depo in this.waterDepos) {
							if (!van) {
								if (adepo == depo) {
									van = true;
								}
							}
						}
						if (!van) {
							this.waterDepos.append(adepo);
							this.possibleListNeedUpdate = true; // a possible story page listaban meg kell jeleniteni a valtozast
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb deponk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldWaterDepos < this.waterDepos.len()) {
			for (local i = this.waterDepos.len() - 1; ((i > (this.shouldWaterDepos - 1)) && i >= 0); i--)
				this.waterDepos.remove(i);
		}
		
		
		// tehat most elvileg pont anny van a roadDeposban, amennyi megengedett
		// tehat most eltavolitjuk az osszes depot, ami nincs benne a listaban
		if (deposList.Count() > this.waterDepos.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_WATERDEPOS, this.shouldWaterDepos);
		
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (adepo, _ in deposList) {
			local van = false;
			foreach (depo in this.waterDepos) {
				if (adepo == depo) {
					van = true;
				}
			}
			if (!van)
				removeList.append(adepo);
		}
		
		foreach (depo in removeList) {
			if (GSMarine.IsWaterDepotTile(depo)) {
				if (!GSMarine.RemoveWaterDepot(depo)) {
					GSLog.Warning("[FGCompany::CheckWaterDeposShould] Nem sikerült eltávolítni a hajó depót!");
					if (!GSTile.DemolishTile(depo))
						GSLog.Error("[FGCompany::CheckWaterDeposShould] Hajó depó törlése sikertelen!");
				}
			}
			else
				GSLog.Error("[FGCompany::CheckWaterDeposShould] Nem hajó depó mező");
		}

		if (removeList.len() > 0) {
			if (this.shouldWaterDepos > GSDepotList(GSTile.TRANSPORT_WATER).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_WATERDEPOS);
		}
	}
	
	return result;
}

// stations, stops, docks, airports
// railStation
function FGCompany::CheckRailStationsShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldRailStations < 0) // barmennyi depo lehet
		return null;
	
	local stationList = GSStationList(GSStation.STATION_TRAIN);
	
	if (stationList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldRailStations == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_MORE_RAILSTATIONS);
		// ekkor eltavolitjuk az osszes stationt
		foreach (station, _ in stationList) {
			local loc = GSBaseStation.GetLocation(station);
			GSRail.SetCurrentRailType(GSRail.GetRailType(loc));
			
			if (GSRail.GetCurrentRailType() == GSRail.RAILTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckRailStationsShould] Érvénytelen vasút típus!");
			
			local stationTileList = GSTileList_StationType(station, GSStation.STATION_TRAIN);
			foreach (tile, _ in stationTileList) {
				if (!GSRail.RemoveRailStationTileRectangle (tile, tile, false)) {
					GSLog.Warning("[FGCompany::CheckRailStationsShould] Vasútállomás mező eltávolítása sikertelen!");
					if (!GSTile.DemolishTile(station))
						GSLog.Error("[FGCompany::CheckRailStationsShould] Vasútállomás mező törlése sikertelen!");
				}
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldRailStations > this.railStations.len()) {
			if (stationList.Count() > this.railStations.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (astation, _ in stationList) {
					if (this.shouldRailStations > this.railStations.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (station in this.railStations) {
							if (!van) {
								if (astation == station) {
									van = true;
								}
							}
						}
						if (!van) {
							this.railStations.append(astation);
							this.possibleListNeedUpdate = true; // a possible story page listaban meg kell jeleniteni a valtozast
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldRailStations < this.railStations.len()) {
			for (local i = this.railStations.len() - 1; ((i > (this.shouldRailStations - 1)) && i >= 0); i--)
				this.railStations.remove(i);
		}
		
		// tehat most elvileg pont anny van a railstationsban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (stationList.Count() > this.railStations.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_RAILSTATIONS, this.shouldRailStations);
		
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (astation, _ in stationList) {
			local van = false;
			foreach (station in this.railStations) {
				if (astation == station) {
					van = true;
				}
			}
			if (!van)
				removeList.append(astation);
		}
		
		foreach (station in removeList) {
			local loc = GSBaseStation.GetLocation(station);
			GSRail.SetCurrentRailType(GSRail.GetRailType(loc));
			
			if (GSRail.GetCurrentRailType() == GSRail.RAILTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckRailStationsShould] Érvénytelen vasút típus!");
			
			local stationTileList = GSTileList_StationType(station, GSStation.STATION_TRAIN);
			foreach (tile, _ in stationTileList) {
				if (!GSRail.RemoveRailStationTileRectangle (tile, tile, false)) {
					GSLog.Warning("[FGCompany::CheckRailStationsShould] Vasútállomás mező eltávolítása sikertelen!");
					if (!GSTile.DemolishTile(station))
						GSLog.Error("[FGCompany::CheckRailStationsShould] Vasútállomás mező törlése sikertelen!");
				}
			}
		}

		if (removeList.len() > 0) {
			if (this.shouldRailStations > GSStationList(GSStation.STATION_TRAIN).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_RAILSTATIONS);
		}
	}
	
	return result;
}

// truckStops
function FGCompany::CheckTruckStopsShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldTruckStops < 0) // barmennyi depo lehet
		return null;
	
	local stationList = GSStationList(GSStation.STATION_TRUCK_STOP);
	
	if (stationList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldTruckStops == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_MORE_TRUCKSTOPS);
		// ekkor eltavolitjuk az osszes stationt
		foreach (station, _ in stationList) {
			local loc = GSBaseStation.GetLocation(station);
			local rtype = GSRoad.ROADTYPE_ROAD;
			if (!GSRoad.HasRoadType(loc, rtype)) {
				rtype = GSRoad.ROADTYPE_TRAM;
				if (!GSRoad.HasRoadType(loc, rtype))
					continue;
			}
			
			GSRoad.SetCurrentRoadType(rtype);
			
			if (GSRoad.GetCurrentRoadType() == GSRoad.ROADTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckTruckStopsShould] Érvénytelen út típus!");
			
			local stationTileList = GSTileList_StationType(station, GSStation.STATION_TRUCK_STOP);
			foreach (tile, _ in stationTileList) {
				if (!GSRoad.RemoveRoadStation (tile)) {
					GSLog.Warning("[FGCompany::CheckTruckStopsShould] Teherautót állomás mező eltávolítása sikertelen!");
					if (!GSTile.DemolishTile(tile))
						GSLog.Error("[FGCompany::CheckTruckStopsShould] Teherautót állomás mező törlése sikertelen!");
				}
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldTruckStops > this.truckStops.len()) {
			if (stationList.Count() > this.truckStops.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (astation, _ in stationList) {
					if (this.shouldTruckStops > this.truckStops.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (station in this.truckStops) {
							if (!van) {
								if (astation == station) {
									van = true;
								}
							}
						}
						if (!van) {
							this.truckStops.append(astation);
							this.possibleListNeedUpdate = true; // a possible story page listaban meg kell jeleniteni a valtozast
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldTruckStops < this.truckStops.len()) {
			for (local i = this.truckStops.len() - 1; ((i > (this.shouldTruckStops - 1)) && i >= 0); i--)
				this.truckStops.remove(i);
		}
		
		// tehat most elvileg pont anny van a truckStopsban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (stationList.Count() > this.truckStops.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_TRUCKSTOPS, this.shouldTruckStops);
	
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (astation, _ in stationList) {
			local van = false;
			foreach (station in this.truckStops) {
				if (astation == station) {
					van = true;
				}
			}
			if (!van)
				removeList.append(astation);
		}
		
		foreach (station in removeList) {
			local loc = GSBaseStation.GetLocation(station);
			local rtype = GSRoad.ROADTYPE_ROAD;
			if (!GSRoad.HasRoadType(loc, rtype)) {
				rtype = GSRoad.ROADTYPE_TRAM;
				if (!GSRoad.HasRoadType(loc, rtype))
					continue;
			}
			
			GSRoad.SetCurrentRoadType(rtype);
			
			if (GSRoad.GetCurrentRoadType() == GSRoad.ROADTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckTruckStopsShould] Érvénytelen út típus!");
			
			local stationTileList = GSTileList_StationType(station, GSStation.STATION_TRUCK_STOP);
			foreach (tile, _ in stationTileList) {
				if (!GSRoad.RemoveRoadStation (tile)) {
					GSLog.Warning("[FGCompany::CheckTruckStopsShould] Teherautót állomás mező eltávolítása sikertelen!");
					if (!GSTile.DemolishTile(tile))
						GSLog.Error("[FGCompany::CheckTruckStopsShould] Teherautót állomás mező törlése sikertelen!");
				}
			}
		}

		if (removeList.len() > 0) {
			if (this.shouldTruckStops > GSStationList(GSStation.STATION_TRUCK_STOP).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_TRUCKSTOPS);
		}
	}
	
	return result;
}

// busStops
function FGCompany::CheckBusStopsShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldBusStops < 0) // barmennyi depo lehet
		return null;

	local stationList = GSStationList(GSStation.STATION_BUS_STOP);
	if (stationList.Count() == 0)
		return null;
	
	local result = null;

	if (this.shouldBusStops == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_MORE_BUSSTOPS);
		// ekkor eltavolitjuk az osszes stationt
		foreach (station, _ in stationList) {
			local loc = GSBaseStation.GetLocation(station);
			local rtype = GSRoad.ROADTYPE_ROAD;
			if (!GSRoad.HasRoadType(loc, rtype)) {
				rtype = GSRoad.ROADTYPE_TRAM;
				if (!GSRoad.HasRoadType(loc, rtype))
					continue;
			}
			
			GSRoad.SetCurrentRoadType(rtype);
			
			if (GSRoad.GetCurrentRoadType() == GSRoad.ROADTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckBusStopsShould] Érvénytelen út típus!");
			
			local stationTileList = GSTileList_StationType(station, GSStation.STATION_BUS_STOP);
			foreach (tile, _ in stationTileList) {
				if (!GSRoad.RemoveRoadStation (tile)) {
					GSLog.Warning("[FGCompany::CheckBusStopsShould] Busz állomás mező eltávolítása sikertelen!");
					if (!GSTile.DemolishTile(tile))
						GSLog.Error("[FGCompany::CheckBusStopsShould] Busz állomás mező törlése sikertelen!");
				}
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldBusStops > this.busStops.len()) {
			if (stationList.Count() > this.busStops.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (astation, _ in stationList) {
					if (this.shouldBusStops > this.busStops.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (station in this.busStops) {
							if (!van) {
								if (astation == station) {
									van = true;
								}
							}
						}
						if (!van) {
							this.busStops.append(astation);
							this.possibleListNeedUpdate = true; // a possible story page listaban meg kell jeleniteni a valtozast
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldBusStops < this.busStops.len()) {
			for (local i = this.busStops.len() - 1; ((i > (this.shouldBusStops - 1)) && i >= 0); i--)
				this.busStops.remove(i);
		}
		
		// tehat most elvileg pont anny van a BusStopsban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (stationList.Count() > this.busStops.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_BUSSTOPS, this.shouldBusStops);
	
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (astation, _ in stationList) {
			local van = false;
			foreach (station in this.busStops) {
				if (astation == station) {
					van = true;
				}
			}
			if (!van)
				removeList.append(astation);
		}
		
		foreach (station in removeList) {
			local loc = GSBaseStation.GetLocation(station);
			local rtype = GSRoad.ROADTYPE_ROAD;
			if (!GSRoad.HasRoadType(loc, rtype)) {
				rtype = GSRoad.ROADTYPE_TRAM;
				if (!GSRoad.HasRoadType(loc, rtype))
					continue;
			}
			
			GSRoad.SetCurrentRoadType(rtype);
			
			if (GSRoad.GetCurrentRoadType() == GSRoad.ROADTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckBusStopsShould] Érvénytelen út típus!");
			
			local stationTileList = GSTileList_StationType(station, GSStation.STATION_BUS_STOP);
			foreach (tile, _ in stationTileList) {
				if (!GSRoad.RemoveRoadStation (tile)) {
					GSLog.Warning("[FGCompany::CheckBusStopsShould] Busz állomás mező eltávolítása sikertelen!");
					if (!GSTile.DemolishTile(tile))
						GSLog.Error("[FGCompany::CheckBusStopsShould] Busz állomás mező törlése sikertelen!");
				}
			}
		}

		if (removeList.len() > 0) {
			if (this.shouldBusStops > GSStationList(GSStation.STATION_BUS_STOP).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_BUSSTOPS);
		}
	}
	
	return result;
}

// waterDocks
function FGCompany::CheckWaterDocksShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldWaterDocks < 0) // barmennyi depo lehet
		return null;
	
	local stationList = GSStationList(GSStation.STATION_DOCK);
	if (stationList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldWaterDocks == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_MORE_WATERDOCKS);
		// ekkor eltavolitjuk az osszes stationt
		foreach (station, _ in stationList) {
			local loc = GSBaseStation.GetLocation(station);
			
			if (!GSMarine.RemoveDock(loc)) {
				GSLog.Warning("[FGCompany::CheckWaterDocksShould] Hajó dock mező eltávolítása sikertelen!");
				if (!GSTile.DemolishTile(loc))
					GSLog.Error("[FGCompany::CheckWaterDocksShould] Hajó dock mező törlése sikertelen!");
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldWaterDocks > this.waterDocks.len()) {
			if (stationList.Count() > this.waterDocks.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (astation, _ in stationList) {
					if (this.shouldWaterDocks > this.waterDocks.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (station in this.waterDocks) {
							if (!van) {
								if (astation == station) {
									van = true;
								}
							}
						}
						if (!van) {
							this.waterDocks.append(astation);
							this.possibleListNeedUpdate = true; // a possible story page listaban meg kell jeleniteni a valtozast
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldWaterDocks < this.waterDocks.len()) {
			for (local i = this.waterDocks.len() - 1; ((i > (this.shouldWaterDocks - 1)) && i >= 0); i--)
				this.waterDocks.remove(i);
		}
		
		// tehat most elvileg pont anny van a WaterDocksban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (stationList.Count() > this.waterDocks.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_WATERDOCKS, this.shouldWaterDocks);
	
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (astation, _ in stationList) {
			local van = false;
			foreach (station in this.waterDocks) {
				if (astation == station) {
					van = true;
				}
			}
			if (!van)
				removeList.append(astation);
		}
		
		foreach (station in removeList) {
			local loc = GSBaseStation.GetLocation(station);
			
			if (!GSMarine.RemoveDock(loc)) {
				GSLog.Warning("[FGCompany::CheckWaterDocksShould] Hajó dock mező eltávolítása sikertelen!");
				if (!GSTile.DemolishTile(loc))
					GSLog.Error("[FGCompany::CheckWaterDocksShould] Hajó dock mező törlése sikertelen!");
			}
		}

		if (removeList.len() > 0) {
			if (this.shouldWaterDocks > GSStationList(GSStation.STATION_DOCK).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_WATERDOCKS);
		}
	}
	
	return result;
}

// airPorts
function FGCompany::CheckAirPortsShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldAirPorts < 0) // barmennyi depo lehet
		return null;
	
	local stationList = GSStationList(GSStation.STATION_AIRPORT);
	if (stationList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldAirPorts == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_MORE_AIRPORTS);
		// ekkor eltavolitjuk az osszes stationt
		foreach (station, _ in stationList) {
			local loc = GSBaseStation.GetLocation(station);
			
			if (!GSAirport.RemoveAirport(loc)) {
				GSLog.Warning("[FGCompany::CheckAirPortsShould] Repülőtér mező eltávolítása sikertelen!");
				if (!GSTile.DemolishTile(loc))
					GSLog.Error("[FGCompany::CheckAirPortsShould] Repülőtér mező törlése sikertelen!");
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldAirPorts > this.airPorts.len()) {
			if (stationList.Count() > this.airPorts.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (astation, _ in stationList) {
					if (this.shouldAirPorts > this.airPorts.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (station in this.airPorts) {
							if (!van) {
								if (astation == station) {
									van = true;
								}
							}
						}
						if (!van) {
							this.airPorts.append(astation);
							this.possibleListNeedUpdate = true; // a possible story page listaban meg kell jeleniteni a valtozast
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldAirPorts < this.airPorts.len()) {
			for (local i = this.airPorts.len() - 1; ((i > (this.shouldAirPorts - 1)) && i >= 0); i--)
				this.airPorts.remove(i);
		}
		
		// tehat most elvileg pont anny van a airPortsban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (stationList.Count() > this.airPorts.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_AIRPORTS, this.shouldAirPorts);
	
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (astation, _ in stationList) {
			local van = false;
			foreach (station in this.airPorts) {
				if (astation == station) {
					van = true;
				}
			}
			if (!van)
				removeList.append(astation);
		}
		
		foreach (station in removeList) {
			local loc = GSBaseStation.GetLocation(station);
			
			if (!GSAirport.RemoveAirport(loc)) {
				GSLog.Warning("[FGCompany::CheckAirPortsShould] Repülőtér mező eltávolítása sikertelen!");
				if (!GSTile.DemolishTile(loc))
					GSLog.Error("[FGCompany::CheckAirPortsShould] Repülőtér mező törlése sikertelen!");
			}
		}

		if (removeList.len() > 0) {
			if (this.shouldAirPorts > GSStationList(GSStation.STATION_AIRPORT).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_AIRPORTS);
		}
	}
	
	return result;
}

// railVehicles
function FGCompany::CheckRailVehiclesShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldRailVehicles < 0) // barmennyi depo lehet
		return null;
	
	local aList = GSVehicleList();
	
	if (aList.Count() == 0)
		return null;
	
	local vehiclesList = GSList();
	
	foreach (vehicle, _ in aList)
		if (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_RAIL)
			vehiclesList.AddItem(vehicle, vehiclesList.Count());
	
	if (vehiclesList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldRailVehicles == 0) {
		result = GSText(GSText.STR_PURCHASE_ERROR_MORE_RAILVEHICLES);
		// ekkor eltavolitjuk az osszes stationt
		foreach (vehicle, _ in vehiclesList) {
			if (GSVehicle.IsStoppedInDepot(vehicle)) {
				if (!GSVehicle.SellVehicle(vehicle))
					GSLog.Error("[FGCompany::CheckRailVehiclesShould] Nem sikerült eladni a vonatot!");
			}
			else {
				if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
					if (!GSVehicle.SendVehicleToDepot(vehicle))
						GSLog.Error("[FGCompany::CheckRailVehiclesShould] Nem sikerült a vonatot a depóba küldeni!");
			}
		}
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldRailVehicles > this.railVehicles.len()) {
			if (vehiclesList.Count() > this.railVehicles.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (avehicle, _ in vehiclesList) {
					if (this.shouldRailVehicles > this.railVehicles.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (vehicle in this.railVehicles) {
							if (!van) {
								if (avehicle == vehicle) {
									van = true;
								}
							}
						}
						if (!van) {
							this.railVehicles.append(avehicle);
							this.possibleListNeedUpdate = true; // a possible story page listaban meg kell jeleniteni a valtozast
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldRailVehicles < this.railVehicles.len()) {
			for (local i = this.railVehicles.len() - 1; ((i > (this.shouldRailVehicles - 1)) && i >= 0); i--)
				this.railVehicles.remove(i);
		}
		
		// tehat most elvileg pont anny van a railVehiclesban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (vehiclesList.Count() > this.railVehicles.len()) {
			result = GSText(GSText.STR_PURCHASE_ERROR_NUM_RAILVEHICLES, this.shouldRailVehicles);
			local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
			foreach (avehicle, _ in vehiclesList) {
				local van = false;
				foreach (vehicle in this.railVehicles) {
					if (avehicle == vehicle) {
						van = true;
					}
				}
				if (!van)
					removeList.append(avehicle);
			}
			
			foreach (vehicle in removeList) {
				if (GSVehicle.IsStoppedInDepot(vehicle)) {
					if (!GSVehicle.SellVehicle(vehicle))
						GSLog.Error("[FGCompany::CheckRailVehiclesShould] Nem sikerült eladni a vonatot!");
				}
				else {
					if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
						if (!GSVehicle.SendVehicleToDepot(vehicle))
							GSLog.Error("[FGCompany::CheckRailVehiclesShould] Nem sikerült a vonatot a depóba küldeni!");
				}
			}
		}
	}
	
	return result;
}

// truckVehicles
function FGCompany::CheckTruckVehiclesShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldTruckVehicles < 0) // barmennyi depo lehet
		return null;
	
	local aList = GSVehicleList();
	
	if (aList.Count() == 0)
		return null;
	
	local vehiclesList = GSList();
	
	foreach (vehicle, _ in aList)
	if ((GSVehicle.GetRoadType(vehicle) == GSRoad.ROADTYPE_ROAD) && (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_ROAD) && (GSVehicle.GetCapacity(vehicle, this.cargo.cargotype_passengers) == 0))
		vehiclesList.AddItem(vehicle, vehiclesList.Count());
	
	if (vehiclesList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldTruckVehicles == 0) {
		result = GSText(GSText.STR_PURCHASE_ERROR_MORE_TRUCKVEHICLES);
		// ekkor eltavolitjuk az osszes stationt
		foreach (vehicle, _ in vehiclesList) {
			if (GSVehicle.IsStoppedInDepot(vehicle)) {
				if (!GSVehicle.SellVehicle(vehicle))
					GSLog.Error("[FGCompany::CheckTruckVehiclesShould] Nem sikerült eladni a teherautót!");
			}
			else {
				if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
					if (!GSVehicle.SendVehicleToDepot(vehicle))
						GSLog.Error("[FGCompany::CheckTruckVehiclesShould] Nem sikerült a teherautót a depóba küldeni!");
			}
		}
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldTruckVehicles > this.truckVehicles.len()) {
			if (vehiclesList.Count() > this.truckVehicles.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (avehicle, _ in vehiclesList) {
					if (this.shouldTruckVehicles > this.truckVehicles.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (vehicle in this.truckVehicles) {
							if (!van) {
								if (avehicle == vehicle) {
									van = true;
								}
							}
						}
						if (!van) {
							this.truckVehicles.append(avehicle);
							this.possibleListNeedUpdate = true; // a possible story page listaban meg kell jeleniteni a valtozast
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldTruckVehicles < this.truckVehicles.len()) {
			for (local i = this.truckVehicles.len() - 1; ((i > (this.shouldTruckVehicles - 1)) && i >= 0); i--)
				this.truckVehicles.remove(i);
		}
		
		// tehat most elvileg pont anny van a railVehiclesban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (vehiclesList.Count() > this.truckVehicles.len()) {
			result = GSText(GSText.STR_PURCHASE_ERROR_NUM_TRUCKVEHICLES, this.shouldTruckVehicles);
			local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
			foreach (avehicle, _ in vehiclesList) {
				local van = false;
				foreach (vehicle in this.truckVehicles) {
					if (avehicle == vehicle) {
						van = true;
					}
				}
				if (!van)
					removeList.append(avehicle);
			}
			
			foreach (vehicle in removeList) {
				if (GSVehicle.IsStoppedInDepot(vehicle)) {
					if (!GSVehicle.SellVehicle(vehicle))
						GSLog.Error("[FGCompany::CheckTruckVehiclesShould] Nem sikerült eladni a teherautót!");
				}
				else {
					if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
						if (!GSVehicle.SendVehicleToDepot(vehicle))
							GSLog.Error("[FGCompany::CheckTruckVehiclesShould] Nem sikerült a teherautót a depóba küldeni!");
				}
			}
		}
	}
	
	return result;
}

// busVehicles
function FGCompany::CheckBusVehiclesShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldBusVehicles < 0) // barmennyi depo lehet
		return null;
	
	local aList = GSVehicleList();
	
	if (aList.Count() == 0)
		return null;
	
	local vehiclesList = GSList();
	
	foreach (vehicle, _ in aList)
	if ((GSVehicle.GetRoadType(vehicle) == GSRoad.ROADTYPE_ROAD) && (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_ROAD) && (GSVehicle.GetCapacity(vehicle, this.cargo.cargotype_passengers) > 0))
		vehiclesList.AddItem(vehicle, vehiclesList.Count());
	
	if (vehiclesList.Count() == 0)
		return null;
	
	
	local result = null;
	
	if (this.shouldBusVehicles == 0) {
		result = GSText(GSText.STR_PURCHASE_ERROR_MORE_BUSVEHICLES);
		// ekkor eltavolitjuk az osszes stationt
		foreach (vehicle, _ in vehiclesList) {
			if (GSVehicle.IsStoppedInDepot(vehicle)) {
				if (!GSVehicle.SellVehicle(vehicle))
					GSLog.Error("[FGCompany::CheckBusVehiclesShould] Nem sikerült eladni a buszt!");
			}
			else {
				if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
					if (!GSVehicle.SendVehicleToDepot(vehicle))
						GSLog.Error("[FGCompany::CheckBusVehiclesShould] Nem sikerült a buszt a depóba küldeni!");
			}
		}
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldBusVehicles > this.busVehicles.len()) {
			if (vehiclesList.Count() > this.busVehicles.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (avehicle, _ in vehiclesList) {
					if (this.shouldBusVehicles > this.busVehicles.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (vehicle in this.busVehicles) {
							if (!van) {
								if (avehicle == vehicle) {
									van = true;
								}
							}
						}
						if (!van) {
							this.busVehicles.append(avehicle);
							this.possibleListNeedUpdate = true; // a possible story page listaban meg kell jeleniteni a valtozast
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldBusVehicles < this.busVehicles.len()) {
			for (local i = this.busVehicles.len() - 1; ((i > (this.shouldBusVehicles - 1)) && i >= 0); i--)
				this.busVehicles.remove(i);
		}
		
		// tehat most elvileg pont anny van a railVehiclesban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (vehiclesList.Count() > this.busVehicles.len()) {
			result = GSText(GSText.STR_PURCHASE_ERROR_NUM_BUSVEHICLES, this.shouldBusVehicles);
			local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
			foreach (avehicle, _ in vehiclesList) {
				local van = false;
				foreach (vehicle in this.busVehicles) {
					if (avehicle == vehicle) {
						van = true;
					}
				}
				if (!van)
					removeList.append(avehicle);
			}
			
			foreach (vehicle in removeList) {
				if (GSVehicle.IsStoppedInDepot(vehicle)){
					if (!GSVehicle.SellVehicle(vehicle))
						GSLog.Error("[FGCompany::CheckBusVehiclesShould] Nem sikerült eladni a buszt!");
				}
				else {
					if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
						if (!GSVehicle.SendVehicleToDepot(vehicle))
							GSLog.Error("[FGCompany::CheckBusVehiclesShould] Nem sikerült a buszt a depóba küldeni!");
				}
			}
		}
	}
	
	return result;
}

// watervehicles
function FGCompany::CheckWaterVehiclesShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldWaterVehicles < 0) // barmennyi depo lehet
		return null;
	
	local aList = GSVehicleList();
	
	if (aList.Count() == 0)
		return null;
	
	local vehiclesList = GSList();
	
	foreach (vehicle, _ in aList)
	if (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_WATER)
		vehiclesList.AddItem(vehicle, vehiclesList.Count());
	
	if (vehiclesList.Count() == 0)
		return null;
	
	local result = null;
	if (this.shouldWaterVehicles == 0) {
		result = GSText(GSText.STR_PURCHASE_ERROR_MORE_WATERVEHICLES);
		// ekkor eltavolitjuk az osszes stationt
		foreach (vehicle, _ in vehiclesList) {
			if (GSVehicle.IsStoppedInDepot(vehicle)) {
				if (!GSVehicle.SellVehicle(vehicle))
					GSLog.Error("[FGCompany::CheckWaterVehiclesShould] Nem sikerült eladni a vízi járművet!");
			}
			else {
				if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
					if (!GSVehicle.SendVehicleToDepot(vehicle))
						GSLog.Error("[FGCompany::CheckWaterVehiclesShould] Nem sikerült a vízi járművet a depóba küldeni!");
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldWaterVehicles > this.waterVehicles.len()) {
			if (vehiclesList.Count() > this.waterVehicles.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (avehicle, _ in vehiclesList) {
					if (this.shouldWaterVehicles > this.waterVehicles.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (vehicle in this.waterVehicles) {
							if (!van) {
								if (avehicle == vehicle) {
									van = true;
								}
							}
						}
						if (!van) {
							this.waterVehicles.append(avehicle);
							this.possibleListNeedUpdate = true; // a possible story page listaban meg kell jeleniteni a valtozast
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldWaterVehicles < this.waterVehicles.len()) {
			for (local i = this.waterVehicles.len() - 1; ((i > (this.shouldWaterVehicles - 1)) && i >= 0); i--)
				this.waterVehicles.remove(i);
		}
		
		// tehat most elvileg pont anny van a railVehiclesban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (vehiclesList.Count() > this.waterVehicles.len()) {
			result = GSText(GSText.STR_PURCHASE_ERROR_NUM_WATERVEHICLES, this.shouldWaterVehicles);
			local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
			foreach (avehicle, _ in vehiclesList) {
				local van = false;
				foreach (vehicle in this.waterVehicles) {
					if (avehicle == vehicle) {
						van = true;
					}
				}
				if (!van)
					removeList.append(avehicle);
			}
			
			foreach (vehicle in removeList) {
				if (GSVehicle.IsStoppedInDepot(vehicle)) {
					if (!GSVehicle.SellVehicle(vehicle))
						GSLog.Error("[FGCompany::CheckWaterVehiclesShould] Nem sikerült eladni a vízi járművet!");
				}
				else {
					if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
						if (!GSVehicle.SendVehicleToDepot(vehicle))
							GSLog.Error("[FGCompany::CheckWaterVehiclesShould] Nem sikerült a vízi járművet a depóba küldeni!");
				}
			}
		}
	}
	
	return result;
}

// airVehicles
function FGCompany::CheckAirVehiclesShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldAirVehicles < 0) // barmennyi depo lehet
		return null;
	
	local aList = GSVehicleList();
	
	if (aList.Count() == 0)
		return null;
	
	local vehiclesList = GSList();
	
	foreach (vehicle, _ in aList)
	if (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_AIR)
		vehiclesList.AddItem(vehicle, vehiclesList.Count());
	
	if (vehiclesList.Count() == 0)
		return null;
	
	local result = null;
	if (this.shouldAirVehicles == 0) {
		result = GSText(GSText.STR_PURCHASE_ERROR_MORE_AIRVEHICLES);
		// ekkor eltavolitjuk az osszes stationt
		foreach (vehicle, _ in vehiclesList) {
			if (GSVehicle.IsStoppedInDepot(vehicle)) {
				if (!GSVehicle.SellVehicle(vehicle))
					GSLog.Error("[FGCompany::CheckAirVehiclesShould] Nem sikerült eladni a repülőt!");
			}
			else {
				if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
					if (!GSVehicle.SendVehicleToDepot(vehicle))
						GSLog.Error("[FGCompany::CheckAirVehiclesShould] Nem sikerült a repülőt a hangárba küldeni!");
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldAirVehicles > this.airVehicles.len()) {
			if (vehiclesList.Count() > this.airVehicles.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (avehicle, _ in vehiclesList) {
					if (this.shouldAirVehicles > this.airVehicles.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (vehicle in this.airVehicles) {
							if (!van) {
								if (avehicle == vehicle) {
									van = true;
								}
							}
						}
						if (!van) {
							this.airVehicles.append(avehicle);
							this.possibleListNeedUpdate = true; // a possible story page listaban meg kell jeleniteni a valtozast
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldAirVehicles < this.airVehicles.len()) {
			for (local i = this.airVehicles.len() - 1; ((i > (this.shouldAirVehicles - 1)) && i >= 0); i--)
				this.airVehicles.remove(i);
		}
		
		// tehat most elvileg pont anny van a railVehiclesban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (vehiclesList.Count() > this.airVehicles.len()) {
			result = GSText(GSText.STR_PURCHASE_ERROR_NUM_AIRVEHICLES, this.shouldAirVehicles);
			local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
			foreach (avehicle, _ in vehiclesList) {
				local van = false;
				foreach (vehicle in this.airVehicles) {
					if (avehicle == vehicle) {
						van = true;
					}
				}
				if (!van)
					removeList.append(avehicle);
			}
			
			foreach (vehicle in removeList) {
				if (GSVehicle.IsStoppedInDepot(vehicle)) {
					if (!GSVehicle.SellVehicle(vehicle))
						GSLog.Error("[FGCompany::CheckAirVehiclesShould] Nem sikerült eladni a repülőgépet!");
				}
				else {
					if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
						if (!GSVehicle.SendVehicleToDepot(vehicle))
							GSLog.Error("[FGCompany::CheckAirVehiclesShould] Nem sikerült a repólőgépet a hangárba küldeni!");
				}
			}
		}
	}
	
	return result;
}
