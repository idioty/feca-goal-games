class FGBaseGoalSettings {
	minMainGoal = 0;
	maxMainGoal = 0;
	minAwardGoal = 0;
	maxAwardGoal = 0;
	minWeakAwardGoal = 0;
	maxWeakAwardGoal = 0;
	
	awardsForSuccesGoalMainMin = 0; // mennyi jutalmat kaphat egy feladatert minimum
	awardsForSuccesGoalMainMax = 0; // mennyi jutalmat kaphat egy feladatert maximum
	awardsForSuccesGoalAwardMin = 0; // mennyi jutalmat kaphat egy feladatert minimum
	awardsForSuccesGoalAwardMax = 0; // mennyi jutalmat kaphat egy feladatert maximum
	awardsForSuccesGoalWeakAwardMin = 0; // mennyi jutalmat kaphat egy feladatert minimum
	awardsForSuccesGoalWeakAwardMax = 0; // mennyi jutalmat kaphat egy feladatert maximum
	
	enableMultiplierForAfterMainGoals = false; // ha ez igaz, akkor a kesobb megjeleno main goalok egyre nehezebbek lesznek... false-ra allitottam, hogy alapbol ki legyen kapcsolva a feladat tipusoknal
	afterMainGoalsMultiplier = 0; // a kesobb megjeleno feladatok mind egyre nehezebbek lesznek, ha az e feletti igaz

	// Feladat nehezseg. Ketfele beallitasi lehetoseg van:
	//	egy a min es max difficultyval,
	//	masik pedig siman a difficultyval.
	//	Pl a towngrowth-nal oriasi kulonbsegek vannak, ezrt kiszuras lenne a masikkal nagyon
	minDifficulty = 0; // ez a legkonnyebb
	maxDifficulty = 0; // ez a legkonnyebb
	// TODO: talan ezt engedelyezni kell, meg nem tudom, fentebb a leiras, hogy mit akartam
//	difficulty = 0;		// van olyan

	monthWithDifficulty = true;
	minMonthDifficulty = -1; // igy ki van kapcsolva
	maxMonthDifficulty = -1; // igy ki van kapcsolva
	minMonthNumber = 0;
	maxMonthNumber = 0;
	
	constructor (table, version) {
		if (table != null)
			this.LoadFromTable(table, version);
	}
}

function FGBaseGoalSettings::SaveToTable() {
	return {
		minMainGoal = this.minMainGoal,
		maxMainGoal = this.maxMainGoal,
		minAwardGoal = this.minAwardGoal,
		maxAwardGoal = this.maxAwardGoal,
		minWeakAwardGoal = this.minWeakAwardGoal,
		maxWeakAwardGoal = this.maxWeakAwardGoal,
		
		awardsForSuccesGoalMainMin = this.awardsForSuccesGoalMainMin,
		awardsForSuccesGoalMainMax = this.awardsForSuccesGoalMainMax,
		awardsForSuccesGoalAwardMin = this.awardsForSuccesGoalAwardMin,
		awardsForSuccesGoalAwardMax = this.awardsForSuccesGoalAwardMax,
		awardsForSuccesGoalWeakAwardMin = this.awardsForSuccesGoalWeakAwardMin,
		awardsForSuccesGoalWeakAwardMax = this.awardsForSuccesGoalWeakAwardMax,
		
		enableMultiplierForAfterMainGoals = this.enableMultiplierForAfterMainGoals,
		afterMainGoalsMultiplier = this.afterMainGoalsMultiplier,
		
		minDifficulty = this.minDifficulty,
		maxDifficulty = this.maxDifficulty,
		monthWithDifficulty = this.monthWithDifficulty,
		minMonthDifficulty = this.minMonthDifficulty,
		maxMonthDifficulty = this.maxMonthDifficulty,
		minMonthNumber = this.minMonthNumber,
		maxMonthNumber = this.maxMonthNumber,
	}
}

function FGBaseGoalSettings::LoadFromTable(table, version) {
	this.minMainGoal = table.minMainGoal;
	this.maxMainGoal = table.maxMainGoal;
	this.minAwardGoal = table.minAwardGoal;
	this.maxAwardGoal = table.maxAwardGoal;
	this.minWeakAwardGoal = table.minWeakAwardGoal;
	this.maxWeakAwardGoal = table.maxWeakAwardGoal;
	
	this.awardsForSuccesGoalMainMin = table.awardsForSuccesGoalMainMin;
	this.awardsForSuccesGoalMainMax = table.awardsForSuccesGoalMainMax;
	this.awardsForSuccesGoalAwardMin = table.awardsForSuccesGoalAwardMin;
	this.awardsForSuccesGoalAwardMax = table.awardsForSuccesGoalAwardMax;
	this.awardsForSuccesGoalWeakAwardMin = table.awardsForSuccesGoalWeakAwardMin;
	this.awardsForSuccesGoalWeakAwardMax = table.awardsForSuccesGoalWeakAwardMax;
	
	this.enableMultiplierForAfterMainGoals = table.enableMultiplierForAfterMainGoals;
	this.afterMainGoalsMultiplier = table.afterMainGoalsMultiplier;
	
	this.minDifficulty = table.minDifficulty;
	this.maxDifficulty = table.maxDifficulty;
	this.monthWithDifficulty = table.monthWithDifficulty;
	this.minMonthDifficulty = table.minMonthDifficulty;
	this.maxMonthDifficulty = table.maxMonthDifficulty;
	this.minMonthNumber = table.minMonthNumber;
	this.maxMonthNumber = table.maxMonthNumber;
}
