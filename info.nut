
class FecaGame extends GSInfo {
	function GetAuthor()			{ return "idioty"; }
	function GetName()			{ return "Feca Goal Games"; }
	function GetDescription() 	{ return "Goals with many things! :)"; }
	function GetVersion()		{ return 8; }
	function GetDate()			{ return "2014-01-20"; }
	function CreateInstance()		{ return "FecaGame"; }
	function GetShortName()		{ return "FEGG"; }
	function GetAPIVersion()		{ return "1.3"; }
	function GetUrl()			{ return ""; }
	function GetSettings() {
		
		/*
		CONFIG_NONE,          ez az alap
		CONFIG_RANDOM,        veletlenszeruen valaszt a min es maxbol
		CONFIG_BOOLEAN,       az ertek bool tipusu: 0 false, 1 true
		CONFIG_INGAME,        jatek alatt is megvaltoztathato ez a parameter
		CONFIG_DEVELOPER      csak akkor jelenik meg, ha a development tools aktiv
		*/
		
		// valami meg valami nos vazze
		AddSetting({name = "startAfterMinute", description									= "Játék hány perc múlva induljon", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 43200, flags = CONFIG_INGAME});
		AddSetting({name = "startGameImmediately", description								= "Játék indítása azonnal!", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, flags = CONFIG_BOOLEAN | CONFIG_INGAME});
		AddSetting({name = "timeZone", description										= "Időzóna", min_value = 0, max_value = 24, easy_value = 12, medium_value = 12, hard_value = 12, custom_value = 12, flags = CONFIG_INGAME});
		
		// ures sor
		AddSetting({name = "info0000", description = "", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		
		AddSetting({name = "info0001", description										= "Általános feladatok beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "gameTime", description										= "        Hány hónapig tartson a játék", easy_value = 0, medium_value = 120, hard_value = 60, custom_value = 0, min_value = 0, max_value = 12000, flags = 0});
		AddSetting({name = "allAtOnceTimeShowedMainGoals", description						= "        Mennyi fő feladat jelenjen meg egyszerre", easy_value = 0, medium_value = 3, hard_value = 1, custom_value = 0, min_value = 0, max_value = 36, flags = 0});
		AddSetting({name = "allAtOnceTimeShowedAwardGoals", description						= "        Mennyi mellék küldetés jelenjen meg egyszerre", easy_value = 0, medium_value = 3, hard_value = 1, custom_value = 0, min_value = 0, max_value = 36, flags = 0});
		AddSetting({name = "enableChanceToAnyNumberOfAward", description						= "        Bármennyit vehet jutalom", easy_value = 1, medium_value = 0, hard_value = 0, custom_value = 0, flags = CONFIG_BOOLEAN});
		AddSetting({name = "enableDoubleAmountOfAwardWhenWeak", description					= "        Kétszeres jutalom az elrejtett jutalmakért", easy_value = 1, medium_value = 0, hard_value = 0, custom_value = 0, flags = CONFIG_BOOLEAN});
		AddSetting({name = "neverRunOutMain", description									= "        Fő küldetések sosem fogynak el", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = CONFIG_BOOLEAN});
		AddSetting({name = "neverRunOutAwards", description									= "        Mellék küldetések sosem fogynak el", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "neverRunOutWeakAwards", description								= "        Titkos feladatok sosem fogynak el", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, flags = CONFIG_BOOLEAN});
		
		// ures sor
		AddSetting({name = "info0002", description = "", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		
		AddSetting({name = "info0003", description										= "Pont beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "mainGoalScores", description									= "        Fő feladatért járó pont", easy_value = 100, medium_value = 100, hard_value = 100, custom_value = 100, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "awardGoalScores", description									= "        Mellék küldetésért járó pont", easy_value = 10, medium_value = 10, hard_value = 10, custom_value = 10, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "weakAwardGoalScores", description								= "        Titkos feladatért járó pont", easy_value = 5, medium_value = 5, hard_value = 5, custom_value = 5, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "monthsScores", description										= "        Hány hónap alatt kell teljesíteni a feladatot ('ez az érték - feladat elvégzéséhez szükséges hónapok' száma után jár a pont; csak fő feladatnál érvényes)", easy_value = 60, medium_value = 60, hard_value = 0, custom_value = 100, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "levelsScores", description										= "        Szint tejlesítésenként járó pont (tehát aki leggyorsabban végez az első vagy a második... feladataával; csak fő feladatnál érvényes)", easy_value = 30, medium_value = 30, hard_value = 0, custom_value = 100, min_value = 0, max_value = 1000, flags = 0});
		
		// ures sor
		AddSetting({name = "info0004", description = "", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		
		AddSetting({name = "info0005", description										= "Üzenet és napló beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "newsForAll", description										= "        Minden vállalat kapjön üzenetet, ha valaki befejezett egy feladatot", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "enableStoryCupUpdates", description								= "        Naplóban a verseny állásának megjelenése", easy_value = 2, medium_value = 1, hard_value = 0, custom_value = 1, min_value = 0, max_value = 2, flags = 0});
		AddSetting({name = "enableCupUpdatesPoints", description								= "        Verseny állásában látszódjon-e a megszerzett pontok", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "cupUpdates", description										= "        Hány havonta irjuk ki a verseny állásást", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 12, flags = 0});
		AddSetting({name = "comapnyAwardGoalsUpdateViaQuestion", description					= "        Hány havonta irjuk ki a vállalatok saját információit", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 12, flags = 0});
		AddSetting({name = "detailedStoryBook", description									= "        Események naplóbejegyzés engedélyezése (alapítás, csőd, felvásárlás, feladat elvégezve)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, flags = CONFIG_BOOLEAN});
		
		
		// ures sor
		AddSetting({name = "info0006", description = "", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		
		// possibilities
		AddSetting({name = "info0007", description										= "Játék indulásakor engedélyezett/letiltott dolgok és jutalom beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "info0008", description										= "        Fűtőház beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "shouldRailDeposMore", description								= "                Bármennyi fűtőház engedélyezett", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldRailDeposNum", description								= "                Engedélyezett fűtőházak száma", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "awardRailDepos", description									= "                Fűtőház lehet jutalom", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "info0009", description										= "        Garázs beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "shouldRoadDeposMore", description								= "                Bármennyi garázs engedélyezett", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldRoadDeposNum", description								= "                Engedélyezett garázsok száma", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "awardRoadDepos", description									= "                Garázs lehet jutalom", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "info0010", description										= "        Dokk beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "shouldWaterDeposMore", description								= "                Bármennyi dokk engedélyezett", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldWaterDeposNum", description								= "                Engedélyezett dokkok száma", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "awardWaterDepos", description									= "                Dokk lehet jutalom", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "info0011", description										= "        Vasútállomás beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "shouldRailStationsMore", description								= "                Bármennyi vasútállomás engedélyezett", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldRailStationsNum", description								= "                Engedélyezett vasútállomások száma", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "awardRailStations", description									= "                Vasútállomás lehet jutalom", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "info0012", description										= "        Teherautó-rakodóhely beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "shouldTruckStopsMore", description								= "                Bármennyi teherautó-rakodóhely engedélyezett", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldTruckStopsNum", description								= "                Engedélyezett teherautó-rakodóhelyek száma", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "awardTruckStops", description									= "                Teherautó-rakodóhely lehet jutalom", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "info0013", description										= "        Buszmegálló beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "shouldBusStopsMore", description								= "                Bármennyi buszmegálló engedélyezett", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldBusStopsNum", description									= "                Engedélyezett buszmegállók száma", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "awardBusStops", description									= "                Buszmegálló lehet jutalom", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "info0014", description										= "        Kikötő beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "shouldWaterDocksMore", description								= "                Bármennyi kikötő engedélyezett", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldWaterDocksNum", description								= "                Engedélyezett kikötők száma", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "awardWaterDocks", description									= "                Kikötő lehet jutalom", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "info0015", description										= "        Repülőtér beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "shouldAirPortsMore", description								= "                Bármennyi repülőtér engedélyezett", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldAirPortsNum", description									= "                Engedélyezett repülőterek száma", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "awardAirPorts", description									= "                Repülőtér lehet jutalom", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "info0016", description										= "        Mozdony beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "shouldRailVehiclesMore", description								= "                Bármennyi mozdony engedélyezett", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldRailVehiclesNum", description								= "                Engedélyezett mozdonyok száma", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "awardRailVehicles", description									= "                Mozdony lehet jutalom", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "info0017", description										= "        Teherautó beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "shouldTruckVehiclesMore", description							= "                Bármennyi teherautó engedélyezett", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldTruckVehiclesNum", description								= "                Engedélyezett teherautók száma", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "awardTruckVehicles", description								= "                Teherautó lehet jutalom", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "info0018", description										= "        Busz beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "shouldBusVehiclesMore", description								= "                Bármennyi busz engedélyezett", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldBusVehiclesNum", description								= "                Engedélyezett buszok száma", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "awardBusVehicles", description									= "                Busz lehet jutalom", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "info0019", description										= "        Vízi jármű beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "shouldWaterVehiclesMore", description							= "                Bármennyi vízi jármű engedélyezett", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldWaterVehiclesNum", description								= "                Engedélyezett vízi járművek száma", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "awardWaterVehicles", description								= "                Vízi jármű lehet jutalom", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "info0020", description										= "        Repülőgép beállítások", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "shouldAirVehiclesMore", description								= "                Bármennyi repülőgép engedélyezett", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldAirVehiclesNum", description								= "                Engedélyezett repülőgépek száma", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "awardAirVehicles", description									= "                Repülőgép lehet jutalom", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});

		AddLabels("info0000", {_0 = ""});
		AddLabels("info0001", {_0 = ""});
		AddLabels("info0002", {_0 = ""});
		AddLabels("info0003", {_0 = ""});
		AddLabels("info0004", {_0 = ""});
		AddLabels("info0005", {_0 = ""});
		AddLabels("info0006", {_0 = ""});
		AddLabels("info0007", {_0 = ""});
		AddLabels("info0008", {_0 = ""});
		AddLabels("info0009", {_0 = ""});
		AddLabels("info0010", {_0 = ""});
		AddLabels("info0011", {_0 = ""});
		AddLabels("info0012", {_0 = ""});
		AddLabels("info0013", {_0 = ""});
		AddLabels("info0014", {_0 = ""});
		AddLabels("info0015", {_0 = ""});
		AddLabels("info0016", {_0 = ""});
		AddLabels("info0017", {_0 = ""});
		AddLabels("info0018", {_0 = ""});
		AddLabels("info0019", {_0 = ""});
		AddLabels("info0020", {_0 = ""});
		
	
		AddLabels("timeZone", {_0 = "-12", _1 = "-11", _2 = "-10", _3 = "-9", _4 = "-8", _5 = "-7", _6 = "-6", _7 = "-5", _8 = "-4", _9 = "-3", _10 = "-2", _11 = "-1", _12 = "0", _13 = "+1", _14 = "+2", _15 = "+3", _16 = "+4", _17 = "+5", _18 = "+6", _19 = "+7", _20 = "+8", _21 = "+9", _22 = "+10", _23 = "+11", _24 = "+12"});
		AddLabels("allAtOnceTimeShowedMainGoals", {_0 = "Összes feldat látható"});
		AddLabels("enableStoryCupUpdates", {_0 = "Ne jelenjen meg", _1 = "Pontok nélkül jelenejen meg", _2 = "Pontokkal együtt jelenjen meg"});
		AddLabels("allAtOnceTimeShowedAwardGoals", {_0 = "Összes feldat látható"});
		AddLabels("monthsScores", {_0 = "Kikapcsolva, nem jár érte pont"});
		AddLabels("levelsScores", {_0 = "Kikapcsolva, nem jár érte pont"});
	
		// ures sor
		AddSetting({name = "info0100", description = "", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "info0101", description										= "Városfejlesztési feladatok beállításai", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "townGrowthGoalSettingsMinMainGoal", description					= "        Feladatok száma minimum", easy_value = 1, medium_value = 5, hard_value = 10, custom_value = 3, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "townGrowthGoalSettingsMaxMainGoal", description					= "        Feladatok száma maximum", easy_value = 1, medium_value = 5, hard_value = 10, custom_value = 3, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "townGrowthGoalSettingsMinLevel", description						= "        Hanyadik feladatként jelenjen meg minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "townGrowthGoalSettingsMaxLevel", description						= "        Hanyadik feladatként jelenjen meg maximum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "townGrowthGoalSettingsMinDifficulty", description					= "        Feladat nehézség minimum", easy_value = 0, medium_value = 2, hard_value = 4, custom_value = 0, min_value = 0, max_value = 5, flags = 0});
		AddSetting({name = "townGrowthGoalSettingsMaxDifficulty", description					= "        Feladat nehézség maximum", easy_value = 0, medium_value = 2, hard_value = 4, custom_value = 0, min_value = 0, max_value = 5, flags = 0});
		AddSetting({name = "townGrowthGoalSettingsCustomLevel", description					= "        Egyéni feladat nehézségi szint (mekkora népességet kell elérni a győzelemhez)", easy_value = 1000, medium_value = 5000, hard_value = 12000, custom_value = 2000, min_value = 1000, max_value = 64000, flags = 0});
		AddSetting({name = "townGrowthGoalSettingsAwardsForSuccesGoalMainMin", description		= "        Jutalmak száma minimum", easy_value = 3, medium_value = 2, hard_value = 1, custom_value = 0, min_value = 0, max_value = 3, flags = 0});
		AddSetting({name = "townGrowthGoalSettingsAwardsForSuccesGoalMainMax", description		= "        Jutalmak száma maximum", easy_value = 3, medium_value = 2, hard_value = 1, custom_value = 0, min_value = 0, max_value = 3, flags = 0});
		AddSetting({name = "townGrowthGoalSettingsDifficultyGoal", description					= "        Városok növekedéséhez feladatok is kelljenek (például utast vagy levelet kelljen szállítani időközönként, hogy növekedjen a város.)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = CONFIG_BOOLEAN});
		AddSetting({name = "townGrowthGoalSettingsPopulation", description						= "        Városok népessége kezdetben", easy_value = 0, medium_value = 2, hard_value = 4, custom_value = 1, min_value = 0, max_value = 5, flags = 0});
		AddSetting({name = "townGrowthGoalSettingsCustomPopulationMin", description				= "        Egyéni népesség minimum", easy_value = 0, medium_value = 801, hard_value = 2001, custom_value = 201, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "townGrowthGoalSettingsCustomPopulationMax", description				= "        Egyéni népesség maximum", easy_value = 200, medium_value = 1500, hard_value = 2500, custom_value = 800, min_value = 0, max_value = 64000, flags = 0});
//		AddSetting({name = "townGrowthGoalSettingsPopulation_max", description					= "        Városok népessége kezdetben maximum", easy_value = 0, medium_value = 2, hard_value = 4, custom_value = 1, min_value = 0, max_value = 4, flags = 0});
		
		AddLabels("info0100", {_0 = ""});
		AddLabels("info0101", {_0 = ""});
		AddLabels("townGrowthGoalSettingsMinDifficulty", {_0 = "Nagyon könnyű", _1 = "Könnyű", _2 = "Közepes", _3 = "Nehéz", _4 = "Nagyon nehéz", _5 = "Egyéni"});
		AddLabels("townGrowthGoalSettingsMaxDifficulty", {_0 = "Nagyon könnyű", _1 = "Könnyű", _2 = "Közepes", _3 = "Nehéz", _4 = "Nagyon nehéz", _5 = "Egyéni"});
		AddLabels("townGrowthGoalSettingsPopulation", {_0 = "0-200 fő", _1 = "201-800 fő", _2 = "801-1500 fő", _3 = "1501-2000 fő", _4 = "2001-2500 fő", _5 = "egyéni"});
//		AddLabels("townGrowthGoalSettingsPopulation_max", {_0 = "200", _1 = "800", _2 = "1500", _3 = "2000", _4 = "2500"});
		// TODO: a feliratokat meg kell csinalni a Town Growt beallitasainak
		
		// ures sor
		AddSetting({name = "info1200", description = "", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		
		AddSetting({name = "info1201", description										= "Rakomány szállítási feladatok beállításai", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "transportGoalSettingsUpdateAll", description						= "        Összes feladat frissítése egyszerre (ha ki van kapcsolva, akkor elszállításkor csak az első feladatot frissíti)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = CONFIG_BOOLEAN});
		AddSetting({name = "transportGoalSettingsMinDifficulty", description					= "        Rakomány nehézség minimum", easy_value = 0, medium_value = 2, hard_value = 4, custom_value = 1, min_value = 0, max_value = 4, flags = 0});
		AddSetting({name = "transportGoalSettingsMaxDifficulty", description					= "        Rakomány nehézség maximum", easy_value = 0, medium_value = 2, hard_value = 4, custom_value = 1, min_value = 0, max_value = 4, flags = 0});
		AddSetting({name = "transportGoalSettingsMonthWithDifficulty", description				= "        Hónap beállítása neházségi fokozattal (ha ki van kapcsolva, akkor az alatta lévő számok lépnek érvénybe)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = CONFIG_BOOLEAN});
		AddSetting({name = "transportGoalSettingsMinMonthDifficulty", description				= "                Hónap nehézségi fokozat minimum", easy_value = 1, medium_value = 3, hard_value = 5, custom_value = 0, min_value = 0, max_value = 5, flags = 0});
		AddSetting({name = "transportGoalSettingsMaxMonthDifficulty", description				= "                Hónap nehézségi fokozat maximum", easy_value = 1, medium_value = 3, hard_value = 5, custom_value = 0, min_value = 0, max_value = 5, flags = 0});
		AddSetting({name = "transportGoalSettingsMinMonthWithNumber", description				= "                Hány hónap alatt kelljen megoldani a feladatokat minimum", easy_value = 12, medium_value = 5, hard_value = 1, custom_value = 0, min_value = 0, max_value = 36, flags = 0});
		AddSetting({name = "transportGoalSettingsMaxMonthWithNumber", description				= "                Hány hónap alatt kelljen megoldani a feladatokat maximum", easy_value = 12, medium_value = 5, hard_value = 1, custom_value = 0, min_value = 0, max_value = 36, flags = 0});
		AddSetting({name = "info1202", description										= "        Győzelemhez szükséges fő feladatok és beállításai", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "transportGoalSettingsMinMainGoal", description						= "                Feladatok száma minimum", easy_value = 1, medium_value = 5, hard_value = 10, custom_value = 3, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "transportGoalSettingsMaxMainGoal", description						= "                Feladatok száma maximum", easy_value = 1, medium_value = 5, hard_value = 10, custom_value = 3, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "transportGoalSettingsAwardsForSuccesGoalMainMin", description		= "                Jutalmak száma minimum", easy_value = 3, medium_value = 2, hard_value = 1, custom_value = 0, min_value = 0, max_value = 3, flags = 0});
		AddSetting({name = "transportGoalSettingsAwardsForSuccesGoalMainMax", description		= "                Jutalmak száma maximum", easy_value = 3, medium_value = 2, hard_value = 1, custom_value = 0, min_value = 0, max_value = 3, flags = 0});
		AddSetting({name = "transportGoalSettingsEnableMultiplierForAfterMainGoals", description	= "                Később megjelenő feladatok egyre nehezebbek legyenek", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "transportGoalSettingsAfterMainGoalsMultiplier", description			= "                Később megjelenő feladatok nehézségi szorzója", easy_value = 0, medium_value = 2, hard_value = 4, custom_value = 0, min_value = 0, max_value = 4, flags = 0});
		AddSetting({name = "info1203", description										= "        Jutalom feladatok és beállításai", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "transportGoalSettingsMinAwardGoal", description					= "                Feladatok száma minimum", easy_value = 1, medium_value = 5, hard_value = 10, custom_value = 3, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "transportGoalSettingsMaxAwardGoal", description					= "                Feladatok száma maximum", easy_value = 1, medium_value = 5, hard_value = 10, custom_value = 3, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "transportGoalSettingsAwardsForSuccesGoalAwardMin", description		= "                Jutalmak száma minimum", easy_value = 3, medium_value = 2, hard_value = 1, custom_value = 0, min_value = 0, max_value = 3, flags = 0});
		AddSetting({name = "transportGoalSettingsAwardsForSuccesGoalAwardMax", description		= "                Jutalmak száma maximum", easy_value = 3, medium_value = 2, hard_value = 1, custom_value = 0, min_value = 0, max_value = 3, flags = 0});
		AddSetting({name = "info1204", description										= "        Elrejtett jutalom feladatok és beállításai", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0});
		AddSetting({name = "transportGoalSettingsMinWeakAwardGoal", description					= "                Feladatok száma minimum", easy_value = 1, medium_value = 5, hard_value = 10, custom_value = 3, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "transportGoalSettingsMaxWeakAwardGoal", description					= "                Feladatok száma maximum", easy_value = 1, medium_value = 5, hard_value = 10, custom_value = 3, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "transportGoalSettingsAwardsForSuccesGoalWeakAwardMin", description	= "                Jutalmak száma minimum", easy_value = 3, medium_value = 2, hard_value = 1, custom_value = 0, min_value = 0, max_value = 3, flags = 0});
		AddSetting({name = "transportGoalSettingsAwardsForSuccesGoalWeakAwardMax", description	= "                Jutalmak száma maximum", easy_value = 3, medium_value = 2, hard_value = 1, custom_value = 0, min_value = 0, max_value = 3, flags = 0});
		
		
		
		AddLabels("transportGoalSettingsMinDifficulty", {_0 = "Nagyon könnyű", _1 = "Könnyű", _2 = "Közepes", _3 = "Nehéz", _4 = "Nagyon nehéz"});
		AddLabels("transportGoalSettingsMaxDifficulty", {_0 = "Nagyon könnyű", _1 = "Könnyű", _2 = "Közepes", _3 = "Nehéz", _4 = "Nagyon nehéz"});
		AddLabels("transportGoalSettingsMinMonthDifficulty", {_0 = "Nincs", _1 = "Nagyon könnyű", _2 = "Könnyű", _3 = "Közepes", _4 = "Nehéz", _5 = "Nagyon nehéz"});
		AddLabels("transportGoalSettingsMaxMonthDifficulty", {_0 = "Nincs", _1 = "Nagyon könnyű", _2 = "Könnyű", _3 = "Közepes", _4 = "Nehéz", _5 = "Nagyon nehéz"});
		AddLabels("transportGoalSettingsAfterMainGoalsMultiplier", {_0 = "Alap (0.1)", _1 = "Könnyű (0.2)", _2 = "Közepes (0.5)", _3 = "Nehéz (0.8)", _4 = "Nagyon nehéz (1.0)"});
		
		
		AddLabels("info1200", {_0 = ""});
		AddLabels("info1201", {_0 = ""});
		AddLabels("info1202", {_0 = ""});
		AddLabels("info1203", {_0 = ""});
		AddLabels("info1204", {_0 = ""});
		
		
		// ures sor
		AddSetting({name = "info1210", description = "", min_value = 0, max_value = 0, easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0})
		
		// initial  cargo types
		AddSetting({name = "enableAllCargoTypes", description									= "        Összes rakomány típus engedélyezése (ha ki van kapcsolva, akkor érvényes az alatta lévők)", easy_value = 0, medium_value = 0, hard_value = 1, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "onlyMainGoalLevels", description									= "              Csak a fő feladatok legyenek szintenként létrehozva", easy_value = 0, medium_value = 0, hard_value = 1, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "cargotype_valuables_min", description								= "              Értéktárgyak minimum", easy_value = 0, medium_value = 2, hard_value = 1, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_valuables_max", description								= "              Értéktárgyak maximum", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_steel_min", description									= "              Acél minimum", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_steel_max", description									= "              Acél maximum", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_ironore_min", description									= "              Vasérc minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_ironore_max", description									= "              Vasérc maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_wood_min", description									= "              Fa minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_wood_max", description									= "              Fa maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_grain_min", description									= "              Búza minimum", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_grain_max", description									= "              Búza maximum", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_goods_min", description									= "              Áru minimum", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_goods_max", description									= "              Áru maximum", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_livestock_min", description								= "              Állat minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_livestock_max", description								= "              Állat maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_oil_min", description										= "              Olaj minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_oil_max", description										= "              Olaj maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_mail_min", description									= "              Levél minimum", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_mail_max", description									= "              Levél maximum", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_coal_min", description									= "              Szén minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_coal_max", description									= "              Szén maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_passengers_min", description								= "              Utas minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});
		AddSetting({name = "cargotype_passengers_max", description								= "              Utas maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});
		
		
		
		AddSetting({name = "cargotype_paper_min", description									= "              Papír minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// PAPR	Paper	 0020 Piece goods	ECS
		AddSetting({name = "cargotype_paper_max", description									= "              Papír maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// PAPR	Paper	 0020 Piece goods	ECS
		AddSetting({name = "cargotype_wheat_min", description									= "              Búza minimum (wheat)", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});							// WHEA	Wheat	 0010 Bulk			 Arctic; see also GRAI, MAIZ, CERE
		AddSetting({name = "cargotype_wheat_max", description									= "              Búza maximum (wheat)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// WHEA	Wheat	 0010 Bulk			 Arctic; see also GRAI, MAIZ, CERE
		AddSetting({name = "cargotype_food_min", description									= "              Étel minimum (ECS és FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});					// FOOD	Food	 0084 Express, refrigerated	ECS	FIRS
		AddSetting({name = "cargotype_food_max", description									= "              Étel maximum (ECS és FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});					// FOOD	Food	 0084 Express, refrigerated	ECS	FIRS
		AddSetting({name = "cargotype_gold_min", description									= "              Arany minimum (ECS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// GOLD	Gold	 0008 Armoured	ECS		 Arctic; see also VALU, DIAM
		AddSetting({name = "cargotype_gold_max", description									= "              Arany  maximum(ECS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// GOLD	Gold	 0008 Armoured	ECS		 Arctic; see also VALU, DIAM
		AddSetting({name = "cargotype_rubber_min", description									= "              Gumi minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// RUBR	Rubber	0040 Liquid	ECS
		AddSetting({name = "cargotype_rubber_max", description									= "              Gumi maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// RUBR	Rubber	0040 Liquid	ECS
		AddSetting({name = "cargotype_fruit_min", description									= "              Gyümölcs minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});						// FRUT  Fruit	 0090 Bulk, refrigerated	ECS
		AddSetting({name = "cargotype_fruit_max", description									= "              Gyümölcs maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});						// FRUT  Fruit	 0090 Bulk, refrigerated	ECS
		AddSetting({name = "cargotype_maize_min", description									= "              Kukorica minimum", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});							// MAIZ	Maize	 0010 Bulk			 Tropic; see also GRAI, WHEA, CERE
		AddSetting({name = "cargotype_maize_max", description									= "              Kukorica maximum", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// MAIZ	Maize	 0010 Bulk			 Tropic; see also GRAI, WHEA, CERE
		AddSetting({name = "cargotype_copperore_min", description								= "              Rézérc minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// CORE	Copper Ore	 0010 Bulk	ECS
		AddSetting({name = "cargotype_copperore_max", description								= "              Rézérc maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// CORE	Copper Ore	 0010 Bulk	ECS
		AddSetting({name = "cargotype_water_min", description									= "              Víz minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// WATR	Water	 0040 Liquid	ECS
		AddSetting({name = "cargotype_water_max", description									= "              Víz maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// WATR	Water	 0040 Liquid	ECS
		AddSetting({name = "cargotype_diamonds_min", description									= "              Gyémánt minimum (ECS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});						// DIAM	Diamonds	 0008 Armoured	ECS		 Tropic; see also VALU, GOLD
		AddSetting({name = "cargotype_diamonds_max", description									= "              Gyémánt maximum (ECS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});						// DIAM	Diamonds	 0008 Armoured	ECS		 Tropic; see also VALU, GOLD
		AddSetting({name = "cargotype_sugar_min", description									= "              Cukor minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});								// SUGR	Sugar	 0010 Bulk			 Toyland
		AddSetting({name = "cargotype_sugar_max", description									= "              Cukor maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});								// SUGR	Sugar	 0010 Bulk			 Toyland
		AddSetting({name = "cargotype_toys_min", description									= "              Játék minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});								// TOYS	Toys	 0020 Piece goods			 Toyland
		AddSetting({name = "cargotype_toys_max", description									= "              Játék maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});								// TOYS	Toys	 0020 Piece goods			 Toyland
		AddSetting({name = "cargotype_batteries_min", description								= "              Elemek minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});								// BATT	Batteries	 0020 Piece goods			 Toyland
		AddSetting({name = "cargotype_batteries_max", description								= "              Elemek maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});								// BATT	Batteries	 0020 Piece goods			 Toyland
		AddSetting({name = "cargotype_sweets_min", description									= "              Cukorka minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});								// SWET	Sweets (Candy)	0004 Express			 Toyland
		AddSetting({name = "cargotype_sweets_max", description									= "              Cukorka maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});								// SWET	Sweets (Candy)	0004 Express			 Toyland
		AddSetting({name = "cargotype_toffee_min", description									= "              Tejkaramella minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// TOFF	Toffee	0010 Bulk			 Toyland
		AddSetting({name = "cargotype_toffee_max", description									= "              Tejkaramella maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// TOFF	Toffee	0010 Bulk			 Toyland
		AddSetting({name = "cargotype_cola_min", description									= "              Kóla minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});								// COLA	Cola	0040 Liquid			 Toyland
		AddSetting({name = "cargotype_cola_max", description									= "              Kóla maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});								// COLA	Cola	0040 Liquid			 Toyland
		AddSetting({name = "cargotype_cottoncandy_min", description								= "              Vattacukor minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// CTCD	Cotton Candy (Candyfloss)	0010 Bulk			 Toyland
		AddSetting({name = "cargotype_cottoncandy_max", description								= "              Vattacukor maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// CTCD	Cotton Candy (Candyfloss)	0010 Bulk			 Toyland
		AddSetting({name = "cargotype_bubbles_min", description									= "              Buborék minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});								// BUBL	Bubbles	0020 Piece goods			 Toyland
		AddSetting({name = "cargotype_bubbles_max", description									= "              Buborék maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});								// BUBL	Bubbles	0020 Piece goods			 Toyland
		AddSetting({name = "cargotype_plastic_min", description									= "              Műanyag minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});								// PLST	Plastic	0040 Liquid			 Toyland; see also PLAS
		AddSetting({name = "cargotype_plastic_max", description									= "              Műanyag maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});								// PLST	Plastic	0040 Liquid			 Toyland; see also PLAS
		AddSetting({name = "cargotype_fizzy_min", description									= "              Szénsavas ital minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});						// FZDR	Fizzy Drinks	0020 Piece goods			 Toyland
		AddSetting({name = "cargotype_fizzy_max", description									= "              Szénsavas ital maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});						// FZDR	Fizzy Drinks	0020 Piece goods			 Toyland
		
		
		
		//	New Cargos	 these cargos are only present when NewGRF industry sets are used
		AddSetting({name = "cargotype_bauxite_min", description									= "              Bauxit minimum (ECS és FIRS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});					// AORE	Bauxite (Aluminium ore)	0010 Bulk	ECS	FIRS
		AddSetting({name = "cargotype_bauxite_max", description									= "              Bauxit maximum (ECS és FIRS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});					// AORE	Bauxite (Aluminium ore)	0010 Bulk	ECS	FIRS
		AddSetting({name = "cargotype_alcohol_min", description									= "              Szeszesital minimum (FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});					// BEER	Alcohol	0064 Express, piece goods, liquids		FIRS
		AddSetting({name = "cargotype_alcohol_max", description									= "              Szeszesital maximum (FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});					// BEER	Alcohol	0064 Express, piece goods, liquids		FIRS
		AddSetting({name = "cargotype_buildingmaterials_min", description							= "              Építőanyag minimum (ECS és FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});				// BDMT	Building Materials	0220 Piece goods, covered/sheltered	ECS	FIRS	 In FIRS: 0030 Bulk, piece goods
		AddSetting({name = "cargotype_buildingmaterials_max", description							= "              Építőanyag maximum (ECS és FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});				// BDMT	Building Materials	0220 Piece goods, covered/sheltered	ECS	FIRS	 In FIRS: 0030 Bulk, piece goods
		AddSetting({name = "cargotype_bricks_min", description									= "              Tégla minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// BRCK	Bricks	 0020 Piece goods	ECS
		AddSetting({name = "cargotype_bricks_max", description									= "              Tégla maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// BRCK	Bricks	 0020 Piece goods	ECS
		AddSetting({name = "cargotype_ceramics_min", description									= "              Kerámia minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});						// CERA	Ceramics	 0020 Piece goods	ECS
		AddSetting({name = "cargotype_ceramics_max", description									= "              Kerámia maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});						// CERA	Ceramics	 0020 Piece goods	ECS
		AddSetting({name = "cargotype_cereals_min", description									= "              Búza minimum (Cereals) (ECS)", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});					// CERE	Cereals	 0210 Bulk, covered/sheltered	ECS		[1]
		AddSetting({name = "cargotype_cereals_max", description									= "              Búza maximum (Cereals) (ECS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});					// CERE	Cereals	 0210 Bulk, covered/sheltered	ECS		[1]
		AddSetting({name = "cargotype_clay_min", description									= "              Agyag minimum (FIRS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// CLAY	Clay	0210 Bulk covered/sheltered		FIRS	 In FIRS: 0010 Bulk
		AddSetting({name = "cargotype_clay_max", description									= "              Agyag maximum (FIRS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// CLAY	Clay	0210 Bulk covered/sheltered		FIRS	 In FIRS: 0010 Bulk
		AddSetting({name = "cargotype_cement_min", description									= "              Cement minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// CMNT	Cement	 0210 Bulk covered/sheltered	ECS
		AddSetting({name = "cargotype_cement_max", description									= "              Cement maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// CMNT	Cement	 0210 Bulk covered/sheltered	ECS
		AddSetting({name = "cargotype_copper_min", description									= "              Vörös réz minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// COPR	Copper	0020 Piece goods
		AddSetting({name = "cargotype_copper_max", description									= "              Vörös réz maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// COPR	Copper	0020 Piece goods
		AddSetting({name = "cargotype_dyes_min", description									= "              Festék minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// DYES	Dyes	 0060 Piece goods, liquids	ECS
		AddSetting({name = "cargotype_dyes_max", description									= "              Festék maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// DYES	Dyes	 0060 Piece goods, liquids	ECS
		AddSetting({name = "cargotype_engineeringsupplies_min", description						= "              Ipari segédeszköz minimum (FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});				// ENSP	Engineering Supplies	0024 Express, piece goods		FIRS
		AddSetting({name = "cargotype_engineeringsupplies_max", description						= "              Ipari segédeszköz maximum (FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});				// ENSP	Engineering Supplies	0024 Express, piece goods		FIRS
		AddSetting({name = "cargotype_fertiliser_min", description								= "              Műtrágya minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});						// FERT	Fertiliser	 0030 Bulk, piece goods	ECS		[1]
		AddSetting({name = "cargotype_fertiliser_max", description								= "              Műtrágya maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});						// FERT	Fertiliser	 0030 Bulk, piece goods	ECS		[1]
		AddSetting({name = "cargotype_fibrecrops_min", description								= "              Növényi rost minimum (ECS és FIRS)", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});				// FICR	Fibre crops	 0030 Bulk, piece goods	ECS	FIRS
		AddSetting({name = "cargotype_fibrecrops_max", description								= "              Növényi rost maximum (ECS és FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});				// FICR	Fibre crops	 0030 Bulk, piece goods	ECS	FIRS
		AddSetting({name = "cargotype_fish_min", description									= "              Hal minimum (ECS és FIRS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});						// FISH	Fish	 0084 Express, refrigerated	ECS	FIRS
		AddSetting({name = "cargotype_fish_max", description									= "              Hal maximum (ECS és FIRS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});						// FISH	Fish	 0084 Express, refrigerated	ECS	FIRS
		AddSetting({name = "cargotype_farmsupplies_min", description								= "              Mezőgazdasági felszerelés minimum (FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});			// FMSP	Farm Supplies	0024 Express, piece goods		FIRS
		AddSetting({name = "cargotype_farmsupplies_max", description								= "              Mezőgazdasági felszerelés maximum (FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});			// FMSP	Farm Supplies	0024 Express, piece goods		FIRS
		AddSetting({name = "cargotype_frvgfruit_min", description								= "              Gyümölcs minimum (FIRS)", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});						// FRVG	Fruit (and optionally Vegetables)	00A4 Express, piece goods, refrigerated		FIRS
		AddSetting({name = "cargotype_frvgfruit_max", description								= "              Gyümölcs maximum (FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});						// FRVG	Fruit (and optionally Vegetables)	00A4 Express, piece goods, refrigerated		FIRS
		AddSetting({name = "cargotype_glass_min", description									= "              Üveg minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// GLAS	Glass	 0420 Piece goods, oversized	ECS
		AddSetting({name = "cargotype_glass_max", description									= "              Üveg maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// GLAS	Glass	 0420 Piece goods, oversized	ECS
		AddSetting({name = "cargotype_gravelballast_min", description								= "              Kő minimum (FIRS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// GRVL	Gravel / Ballast	0010 Bulk		FIRS
		AddSetting({name = "cargotype_gravelballast_max", description								= "              Kő maximum (FIRS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// GRVL	Gravel / Ballast	0010 Bulk		FIRS
		AddSetting({name = "cargotype_limestone_min", description								= "              Mészkő minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// LIME	Lime stone	 0010 Bulk	ECS
		AddSetting({name = "cargotype_limestone_max", description								= "              Mészkő maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// LIME	Lime stone	 0010 Bulk	ECS
		AddSetting({name = "cargotype_milk_min", description									= "              Tej minimum (FIRS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// MILK	Milk	00C4 Express, liquid, refrigerated		FIRS
		AddSetting({name = "cargotype_milk_max", description									= "              Tej maximum (FIRS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// MILK	Milk	00C4 Express, liquid, refrigerated		FIRS
		AddSetting({name = "cargotype_manufacturingsupplies_min", description						= "              Gyártási segédeszköz minimum (FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});				// MNSP	Manufacturing Supplies	0024 Piece Goods, express		FIRS
		AddSetting({name = "cargotype_manufacturingsupplies_max", description						= "              Gyártási segédeszköz maximum (FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});				// MNSP	Manufacturing Supplies	0024 Piece Goods, express		FIRS
		AddSetting({name = "cargotype_metal_min", description									= "              Fém minimum (FIRS, acél helyett)", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});				// STEL ez az acelt irja felul!!!
		AddSetting({name = "cargotype_metal_max", description									= "              Fém maximum (FIRS, acél helyett)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});				// STEL ez az acelt irja felul!!!
		AddSetting({name = "cargotype_oilseed_min", description									= "              Olajos magvak minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});						// OLSD	Oil seed	 0210 Bulk, covered/sheltered	ECS		[1]
		AddSetting({name = "cargotype_oilseed_max", description									= "              Olajos magvak maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});						// OLSD	Oil seed	 0210 Bulk, covered/sheltered	ECS		[1]
		AddSetting({name = "cargotype_petrolfueloil_min", description								= "              Üzemagyag minimum (ECS és FIRS)", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});				// PETR	Petrol / Fuel Oil	 0040 Liquid	ECS	FIRS
		AddSetting({name = "cargotype_petrolfueloil_max", description								= "              Üzemagyag maximum (ECS és FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});				// PETR	Petrol / Fuel Oil	 0040 Liquid	ECS	FIRS
		AddSetting({name = "cargotype_plasplastic_min", description								= "              Műanyag minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});						// PLAS	Plastic	 0060 Piece goods, liquid	ECS
		AddSetting({name = "cargotype_plasplastic_max", description								= "              Műanyag maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});						// PLAS	Plastic	 0060 Piece goods, liquid	ECS
		AddSetting({name = "cargotype_potash_min", description									= "              Műtrágya minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});						// POTA	Potash	 0210 Bulk, covered/sheltered	ECS		[1]
		AddSetting({name = "cargotype_potash_max", description									= "              Műtrágya maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});						// POTA	Potash	 0210 Bulk, covered/sheltered	ECS		[1]
		AddSetting({name = "cargotype_recyclables_min", description								= "              Újrahasznosítható áru minimum (FIRS)", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});			// RCYC	Recyclables	0220 Piece Goods, covered		FIRS
		AddSetting({name = "cargotype_recyclables_max", description								= "              Újrahasznosítható áru maximum (FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});			// RCYC	Recyclables	0220 Piece Goods, covered		FIRS
		AddSetting({name = "cargotype_refinedproducts_min", description							= "              Vegyszer minimum (ECS és FIRS)", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});					// RFPR	Refined products	 0040 Liquid	ECS	FIRS
		AddSetting({name = "cargotype_refinedproducts_max", description							= "              Vegyszer maximum (ECS és FIRS)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});					// RFPR	Refined products	 0040 Liquid	ECS	FIRS
		AddSetting({name = "cargotype_sand_min", description									= "              Homok minimum (ECS és FIRS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});					// SAND	Sand	 0010 Bulk	ECS	FIRS
		AddSetting({name = "cargotype_sand_max", description									= "              Homok maximum (ECS és FIRS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});					// SAND	Sand	 0010 Bulk	ECS	FIRS
		AddSetting({name = "cargotype_scraptmetal_min", description								= "              Fémhulladék minimum (FIRS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});					// SCMT	Scrap Metal	1010 Bulk, non-pourable		FIRS
		AddSetting({name = "cargotype_scraptmetal_max", description								= "              Fémhulladék maximum (FIRS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});					// SCMT	Scrap Metal	1010 Bulk, non-pourable		FIRS
		AddSetting({name = "cargotype_sugarbeet_min", description								= "              Cukorrépa minimum (FIRS, kivéve trópusi tájon)", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});	// SGBT	Sugar beet	0010 Bulk		FIRS	 not in tropical
		AddSetting({name = "cargotype_sugarbeet_max", description								= "              Cukorrépa maximum (FIRS, kivéve trópusi tájon)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});	// SGBT	Sugar beet	0010 Bulk		FIRS	 not in tropical
		AddSetting({name = "cargotype_sugarcane_min", description								= "              Cukornád minimum (FIRS, csak trópusi tájon)", easy_value = 2, medium_value = 2, hard_value = 1, custom_value = 2, min_value = 0, max_value = 64000, flags = 0});		// SGCN	Sugarcane	1010 Bulk, non-pourable		FIRS	 only tropical
		AddSetting({name = "cargotype_sugarcane_max", description								= "              Cukornád maximum (FIRS, csak trópusi tájon)", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});		// SGCN	Sugarcane	1010 Bulk, non-pourable		FIRS	 only tropical
		AddSetting({name = "cargotype_sulphur_min", description									= "              Kén minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// SULP	Sulphur	 0210 Bulk, covered/sheltered	ECS		[1]
		AddSetting({name = "cargotype_sulphur_max", description									= "              Kén maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// SULP	Sulphur	 0210 Bulk, covered/sheltered	ECS		[1]
		AddSetting({name = "cargotype_tourists_min", description									= "              Túrista minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});						// TOUR	Tourists	 0005 Passengers, express	ECS
		AddSetting({name = "cargotype_tourists_max", description									= "              Túrista maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});						// TOUR	Tourists	 0005 Passengers, express	ECS
		AddSetting({name = "cargotype_vehicles_min", description									= "              Jármű minimum (ECS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});							// VEHI	Vehicles	 0420 Piece goods, oversized	ECS
		AddSetting({name = "cargotype_vehicles_max", description									= "              Jármű maximum (ECS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});							// VEHI	Vehicles	 0420 Piece goods, oversized	ECS
		AddSetting({name = "cargotype_wdprwood_min", description									= "              Fűrészáru minimum (ECS és FIRS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});				// WDPR	Wood Products	 0030 Bulk, piece goods	ECS	FIRS
		AddSetting({name = "cargotype_wdprwood_max", description									= "              Fűrészáru maximum (ECS és FIRS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});				// WDPR	Wood Products	 0030 Bulk, piece goods	ECS	FIRS
		AddSetting({name = "cargotype_wool_min", description									= "              Gyapjú minimum (ECS és FIRS)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});					// WOOL	Wool	 0220 Piece goods, covered/sheltered	ECS	FIRS	[1]
		AddSetting({name = "cargotype_wool_max", description									= "              Gyapjú  maximum(ECS és FIRS)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});					// WOOL	Wool	 0220 Piece goods, covered/sheltered	ECS	FIRS	[1]

		
		//	Special Cargos	 these cargos are for use outside industry sets and do not represent tra
//		AddSetting({name = "cargotype_default_min", description									= "            Alap minimum", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});											// DFLT	'Default'	 ---- None			 Used by andythenorth to identify default cargo graphics in newgrfs that use code/graphics generation.
//		AddSetting({name = "cargotype_default_max", description									= "            Alap maximum",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});											// DFLT	'Default'	 ---- None			 Used by andythenorth to identify default cargo graphics in newgrfs that use code/graphics generation.

		AddSetting({name = "cargotype_locomotiveregearing_max", description						= "              Lokomotiv minimum (Nincs használatban)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});			// GEAR	Locomotive regearing	8000 Special			 Using this cargo may make your vehicle set incompatible with some industry sets
		AddSetting({name = "cargotype_locomotiveregearing_min", description						= "              Lokomotiv maximum (Nincs használatban)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});			// GEAR	Locomotive regearing	8000 Special			 Using this cargo may make your vehicle set incompatible with some industry sets
		
		//	Deprecated Cargos	 these cargos are not used by any current industry set, and are listed here for backwards compatibility
		AddSetting({name = "cargotype_fuel_min", description									= "              Üzemanyag minimum (Nincs használatban)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});			// FUEL	Fuel	0040 Liquid			Use PETR for refined-oil fuel
		AddSetting({name = "cargotype_fuel_max", description									= "              Üzemanyag maximum (Nincs használatban)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});			// FUEL	Fuel	0040 Liquid			Use PETR for refined-oil fuel
		AddSetting({name = "cargotype_rawsugar_min", description									= "              Cukor minimum (Nincs használatban)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});				// RSGR	Raw Sugar	0010 Bulk			 Deprecated in FIRS. See SGBT and SGCN
		AddSetting({name = "cargotype_rawsugar_max", description									= "              Cukor maximum (Nincs használatban)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});				// RSGR	Raw Sugar	0010 Bulk			 Deprecated in FIRS. See SGBT and SGCN
		AddSetting({name = "cargotype_scrpscrapmetal_min", description							= "              Fémhulladék minimum (Nincs használatban)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});			// SCRP	Scrap Metal	0010 Piece Goods			 Deprecated in FIRS, use SCMT instead
		AddSetting({name = "cargotype_scrpscrapmetal_max", description							= "              Fémhulladék maximum (Nincs használatban)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});			// SCRP	Scrap Metal	0010 Piece Goods			 Deprecated in FIRS, use SCMT instead
		AddSetting({name = "cargotype_tropicwood_min", description								= "              Pálmafa minimum (Nincs használatban)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});			// TWOD	Tropic Wood	0020 Piece goods			 formerly intended as a default cargo
		AddSetting({name = "cargotype_tropicwood_max", description								= "              Pálmafa maximum (Nincs használatban)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});			// TWOD	Tropic Wood	0020 Piece goods			 formerly intended as a default cargo
		AddSetting({name = "cargotype_waste_min", description									= "              Hulladék minimum (Nincs használatban)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, min_value = 0, max_value = 64000, flags = 0});			// WSTE	Waste	0010 Bulk			 Deprecated in FIRS.
		AddSetting({name = "cargotype_waste_max", description									= "              Hulladék maximum (Nincs használatban)",easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, min_value = 0, max_value = 64000, flags = 0});			// WSTE	Waste	0010 Bulk			 Deprecated in FIRS.
		
		
		AddLabels("info1210", {_0 = ""});
		
		AddLabels("cargotype_valuables_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_valuables_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_steel_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_steel_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_ironore_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_ironore_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_wood_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_wood_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_grain_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_grain_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_goods_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_goods_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_livestock_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_livestock_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_oil_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_oil_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_mail_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_mail_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_coal_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_coal_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_passengers_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_passengers_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		
		AddLabels("cargotype_paper_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_paper_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_wheat_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_wheat_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_food_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_food_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_gold_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_gold_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_rubber_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_rubber_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_fruit_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_fruit_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_maize_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_maize_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_copperore_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_copperore_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_water_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_water_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_diamonds_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_diamonds_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_sugar_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_sugar_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_toys_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_toys_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_batteries_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_batteries_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_sweets_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_sweets_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_toffee_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_toffee_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_cola_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_cola_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_cottoncandy_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_cottoncandy_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_bubbles_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_bubbles_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_plastic_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_plastic_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_fizzy_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_fizzy_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		
		AddLabels("cargotype_bauxite_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_bauxite_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_alcohol_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_alcohol_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_buildingmaterials_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_buildingmaterials_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_bricks_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_bricks_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_ceramics_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_ceramics_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_cereals_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_cereals_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_clay_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_clay_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_cement_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_cement_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_copper_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_copper_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_dyes_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_dyes_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_engineeringsupplies_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_engineeringsupplies_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_fertiliser_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_fertiliser_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_fibrecrops_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_fibrecrops_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_fish_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_fish_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_farmsupplies_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_farmsupplies_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_frvgfruit_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_frvgfruit_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_glass_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_glass_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_gravelballast_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_gravelballast_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_limestone_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_limestone_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_milk_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_milk_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_manufacturingsupplies_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_manufacturingsupplies_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_metal_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_metal_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_oilseed_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_oilseed_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_petrolfueloil_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_petrolfueloil_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_plasplastic_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_plasplastic_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_potash_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_potash_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_recyclables_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_recyclables_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_refinedproducts_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_refinedproducts_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_sand_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_sand_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_scraptmetal_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_scraptmetal_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_sugarbeet_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_sugarbeet_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_sugarcane_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_sugarcane_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_sulphur_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_sulphur_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_tourists_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_tourists_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_vehicles_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_vehicles_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_wdprwood_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_wdprwood_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_wool_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_wool_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		
//		AddLabels("cargotype_default_min", {_0 = "Nincs szállítás"});
//		AddLabels("cargotype_default_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		
		AddLabels("cargotype_locomotiveregearing_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_locomotiveregearing_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		
		AddLabels("cargotype_fuel_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_fuel_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_rawsugar_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_rawsugar_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_scrpscrapmetal_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_scrpscrapmetal_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_tropicwood_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_tropicwood_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
		AddLabels("cargotype_waste_min", {_0 = "Nincs szállítás"});
		AddLabels("cargotype_waste_max", {_0 = "Nincs szállítás/Bármikor szállíthat"});
	 
	}
}

RegisterGS(FecaGame());
